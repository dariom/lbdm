R=4;

A=ones(20,40,2);
A([1],:,:)=3;
A([end],:,:)=0;
[zz xx]=meshgrid(1:40,1:20);
[xx zz]=meshgrid(1:40,1:20);
S=sqrt((xx-8.5).**2+(zz-10.5).**2)<4;
A(S==1)=2;
A(:,:,2)=A(:,:,1);

U=uint8(A(:));
name=sprintf('reb2d.tp.bin.txt');
dlmwrite(name,U);

A(A==2)=1;

U=uint8(A(:));
name=sprintf('reb2d.sp.bin.txt');
dlmwrite(name,U);
