for i=0:8:40;
load(sprintf('../../out/dump.REB4.%05d.h5',i));
if (i==0);
sol=double(LBdm.sol);
end;
con=LBdm.con;
tem=LBdm.tem;
ct2(i/8+1)=sum(con(:))./sum(sol(:));
tt2(i/8+1)=sum(tem(:).*sol(:))./sum(sol(:));
end;

M=[ct2(1:end)',tt2(1:end)'];
save('data2.dat','-ascii','-append','M');
