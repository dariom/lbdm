#!/bin/bash -e

echo " "
echo "#lbdm# Test 013 Reaction test for Scalar 2d"
echo " "

git checkout user-defined.c

if [ -f "*.bin.txt" ]; then
rm *.bin.txt
fi

if [ -f "dump.CONV.h5" ]; then
rm dump.CONV.h5
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

if [ -f "react_lbdm_1.dat" ]; then
rm react_lbdm_1.dat
fi

if [ -f "react_lbdm_2.dat" ]; then
rm react_lbdm_2.dat
fi

if [ -f "profile_1.png" ]; then
rm profile_1.png
fi

if [ -f "profile_2.png" ]; then
rm profile_2.png
fi

if [ -f "react_th_1.dat" ]; then
rm react_th_1.dat
fi

if [ -f "react_th_2.dat" ]; then
rm react_th_2.dat
fi

#theoretical solution
octave analytical.m

octave create_bin-txt.m
cp react2d.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..

make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/013_react2d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave con_react2d_h5_1.m

paste react_th_1.dat react_lbdm_1.dat | awk 'NR<=38 {print ($1/$3-1)}' > error.dat 
paste react_th_1.dat react_lbdm_1.dat | awk 'NR<=38 {print ($2/$4-1)}' >> error.dat 

###########

## We ll check FASTER reaction
sed -i -e 's/define A2 .*/define A2 0.1/g' user-defined.c
sed -i -e 's/define A3 .*/define A3 0.1/g' user-defined.c
sed -i -e 's/define MAX_ITER .*/define MAX_ITER 8000/g' user-defined.c

cp user-defined.c ../../user/
cd ../..

make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/013_react2d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave con_react2d_h5_2.m

paste react_th_2.dat react_lbdm_2.dat | awk 'NR<=38 {print ($1/$3-1)}' >> error.dat 
paste react_th_2.dat react_lbdm_2.dat | awk 'NR<=38 {print ($2/$4-1)}' >> error.dat 
awk 'NR<=152 {if (($1*$1)^0.5<=0.06) print "ok"; else print "no" }' error.dat >> errchk.dat

cat error.dat

##########

# clean source
cd ../..
make clean
rm in/react2d.bin.txt

# clean local test
cd test/013_react2d/
git checkout user-defined.c
rm *.txt

########### 1) Check NaN 

for file in react_lbdm_1.dat react_lbdm_2.dat error.dat errchk.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 152 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
