function [liq_sol_ijk]=geoC_3d(n,dfA,dvA)

df=dfA*n;
dv=dvA*n;
hf=df+dv;

nx=hf;
ny=hf;
nz=hf;

ncarb_x=1;
ncarb_y=1;
ncarb_z=1;


disp('¡check INTEGERS!');
ncarb_x, ncarb_y, ncarb_z,
forecast_porosity=1-1/(dv+df)^3*1/6*pi()*df^3
pause(0),

wS=2; wN=2;

sp=dv/n;
ss=nx/(n)-sp;
nz=nz+wN+wS;

A=ones(nz,nx,ny);

%% carbon circle
for ncz=1:2; fct=factor(ncz); if fct~=2;
for ncy=1;
for ncx=1;
ycarb=(sp+ss)*n/2+(ncy-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+(ncz-1)/2*(sp+ss)*n+wS;
xcarb=(sp+ss)*n/2+(ncx-1)*(sp+ss)*n+1;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ycarb+y-0.5)=0;
end; end; end; end; else;
for ncy=1:ncarb_y-1;
for ncx=1:ncarb_x-1;
ycarb=(sp+ss)*n+(ncy-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+(ncz-1)/2*(sp+ss)*n+wS;
xcarb=(sp+ss)*n+(ncx-1)*(sp+ss)*n+1;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ycarb+y-0.5)=0;
end; end; end; end;
for ncy=1:ncarb_y-1;
ycarb=(sp+ss)*n+(ncy-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+(ncz-1)/2*(sp+ss)*n+wS;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,1:ryr,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,1:ryr,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,ycarb+y-0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,ycarb+y-0.5)=0;
end; end; end;
for ncx=1:ncarb_x-1;
xcarb=(sp+ss)*n+(ncx-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+(ncz-1)/2*(sp+ss)*n+wS;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,1+y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,1+y-0.5)=0;
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ny-y+0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ny-y+0.5)=0;
% angle
A(nz-zcarb-z+1,1:ryr,1+y-0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,1+y-0.5)=0;
A(nz-zcarb-z+1,1:ryr,ny-y+0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,ny-y+0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,1+y-0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,1+y-0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,ny-y+0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,ny-y+0.5)=0;
end; end; end;
end; end;


%%%%%%%%%%%%% correction %%%%%%%%%%%%%%%%%%%%

for ncz=[-0.5]%,0.5*ncarb_z]
for ncy=1:ncarb_y-1;
for ncx=1:ncarb_x-1;
ycarb=(sp+ss)*n+(ncy-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+ncz*(sp+ss)*n+wS;
xcarb=(sp+ss)*n+(ncx-1)*(sp+ss)*n+1;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ycarb+y-0.5)=0;
end; end; end; end;
for ncy=1:ncarb_y-1;
ycarb=(sp+ss)*n+(ncy-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+ncz*(sp+ss)*n+wS;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,1:ryr,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,1:ryr,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,ycarb+y-0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,ycarb+y-0.5)=0;
end; end; end;
for ncx=1:ncarb_x-1;
xcarb=(sp+ss)*n+(ncx-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+ncz*(sp+ss)*n+wS;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,1+y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,1+y-0.5)=0;
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ny-y+0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ny-y+0.5)=0;
% angle
A(nz-zcarb-z+1,1:ryr,1+y-0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,1+y-0.5)=0;
A(nz-zcarb-z+1,1:ryr,ny-y+0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,ny-y+0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,1+y-0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,1+y-0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,ny-y+0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,ny-y+0.5)=0;
end; end; end;
end;

A=A(1:nz,:,:);


%% BUBBLE 
A(A==0)=2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% boundaries
A([1:wN,nz-wS+1:nz],[1:nx,1:nx],[1:ny,1:ny])=0;

porosity=sum(sum(sum(A,3),2),1)/(nx*ny*nz);
concentration=1-porosity,

 figure(1), isosurface(A,0); 
 pbaspect([nx,nz,ny])
 xlim([0,nx]);
 ylim([0,ny]);
 zlim([0,nz]);
 %pause(), close(1);

%% indices
[isolid,jsolid,ksolid]=ind2sub(size(A),find(A==0));
[iliquid,jliquid,kliquid]=ind2sub(size(A),find(A==2));
Length=max(length(isolid),length(iliquid));
liq_sol_ijk=zeros(Length,7);
liq_sol_ijk(1:length(isolid),1)=isolid;
liq_sol_ijk(1:length(isolid),2)=jsolid;
liq_sol_ijk(1:length(isolid),3)=ksolid;
liq_sol_ijk(1:length(iliquid),4)=iliquid;
liq_sol_ijk(1:length(iliquid),5)=jliquid;
liq_sol_ijk(1:length(iliquid),6)=kliquid;

liq_sol_ijk(1:4,7)=[nz,nx,ny,porosity]';
