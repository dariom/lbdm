load('dump.ZERO.h5');
sol=double(LBdm.sol);

load('dump.CONV.h5');

dpl=0.000078125;
rs=5;
mu=1/6;

ux=LBdm.ux;
solfs=sol(1:end,:,1:end);
uxfs=ux(1:end,:,1:end);
e=mean(solfs(:));
U=mean(uxfs(:).*solfs(:))./e;
K=U*e*dpl**(-1)*mu;
drag=2*rs**2/9/K*1/(1-e);

M=[(1-e),drag];
save('drag_lbdm.dat','-ascii','M');
