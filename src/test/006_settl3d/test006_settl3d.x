#!/bin/bash -e

echo " "
echo "#lbdm# Test 006 Settling Sphere Inside Pipe 3d"
echo " "

git checkout user-defined.c

if [ -f "plot_dx.png" ]; then
rm plot_dx.png 
fi

if [ -f "plot_uu.png" ]; then
rm plot_uu.png 
fi

if [ -f "ut_stokes.dat" ]; then
rm ut_stokes.dat
fi

if ls settl.*.dat 1> /dev/null 2>&1; then
rm settl.*.dat
fi

if [ -f "*.bin.txt" ]; then
rm *.bin.txt
fi

if [ -f "dump.CONV.h5" ]; then
rm dump.CONV.h5
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

octave create_bin-txt.m
cp settl3d.pipe.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/006_settl3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave tvel_h5.m
octave plot_ustokes.m

paste ut_lb.dat ut_stokes.dat | awk 'NR<=1 {print ($1/$3-1)}' > error.dat 
paste ut_lb.dat ut_stokes.dat | awk 'NR<=1 {print ($2/$3-1)}' >> error.dat 

###########

awk 'NR<=2 {if (($1*$1)^0.5<=0.04) print "ok"; else print "no" }' error.dat > errchk.dat

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/settl3d.pipe.bin.txt

# clean local test
cd test/006_settl3d/
git checkout user-defined.c
rm *.txt

########### 1) Check NaN 

for file in ut_lb.dat ut_stokes.dat error.dat errchk.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 2 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
