A=ones(20,4,2);
A(end-1:end,:,:)=0;
U=uint8(A(:));
dlmwrite('fs2d.bin.txt',U);

[xx zz yy] = meshgrid(1:4,1:20,1:20);
B = (sqrt((zz-0.5).**2+(yy-20.5).**2) <= 18);
%B(1,:,end)=0;
U=uint8(B(:));
dlmwrite('fs3d.bin.txt',U);

[xx zz yy] = meshgrid(1:16,1:16,1:16);
A = (sqrt((xx-8.5).^2+(yy-8.5).^2+(zz-0.5).^2) > 5) & (sqrt((xx-8.5).^2+(yy-8.5).^2+(zz-16.5).^2) > 5);

%A(1,:,1)=0;
%A(end,:,end)=0;
%A(1,:,end)=0;
%A(end,:,1)=0;

U=uint8(A(:));
dlmwrite('fs3d.sp.bin.txt',U);
