
function [rho]=rhocalc(f,Nr,Mc,Ld,Channel3D,g_density,l_density,w_density,phb_density)

% at present, in order to assign different hydrophobicity values to bbs

hw=Nr-find(Channel3D(:,Mc/2,Ld/2),1,'last');

surpl=w_density-g_density;
inhib=0.0;

surpl_phb=phb_density-g_density;
inhib_phb=0.0;

w2D=1-Channel3D;
w2Dphb=w2D.*[zeros(Nr-hw,Mc,Ld);ones(hw,Mc,Ld)];

rho=sum(f,4);

rho=Channel3D.*rho+(w2D-w2Dphb).*w_density+(w2Dphb).*phb_density;



