#include<stdio.h>
#include<stdlib.h>
#include<math.h>
//#include "fftw.h"
//#include "rfftw.h"
#include "hdf5.h"


  #define NX (240) 
  #define NY (240)
  #define NZ (240)

  #define XS (0) 
  #define XE (240)
  #define YS (0)
  #define YE (240)
  #define ZS (0)
  #define ZE (240)
 
  // It seems that h5create does it with zyx order .. 
  #define IDXN(i,j,k)  (k + (i + j * (XE-XS) ) * (ZE-ZS) );

typedef double scalar;
char *fdir=NULL,*fnewname=NULL,*forigname=NULL;


void readtopo (scalar * f0) {
  int i, j, k, count, count0;
  float idx1;
  char fname[100];
  FILE *fread;

  // .......................................

  sprintf (fname, "%s", forigname);
  fprintf (stderr, "(1) read %s \n", fname);

  /* Open an existing file. */
  fread=fopen(fname,"r");
  count=0; count0=0; 

  while ( fscanf(fread,"%g\n",&idx1)!=EOF && count0<(NX*NY*NZ) ) {
  count0++;

    f0[count]=(double)idx1;
    //fprintf(stderr,"(r) %d %d\n",count,(int)f0[count]);
    count++;

  }
}

void writetopo (scalar * f) {
  hid_t file_id, group_id, dataset_id;  /* identifiers */
  hid_t property_id, plist_id, efilespace;
  hsize_t efile_3d[3];
  herr_t status;
  char fname[128];

  sprintf (fname, "%s%s.h5", fdir, fnewname);
  fprintf (stderr, "(2) write %s \n", fname);

  efile_3d[0] = YE-YS;
  efile_3d[1] = XE-XS;
  efile_3d[2] = ZE-ZS;
  efilespace = H5Screate_simple (3, efile_3d, NULL);

  file_id = H5Fcreate (fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  dataset_id = H5Dcreate (file_id, "/scan", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Dwrite (dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, f);
  status = H5Dclose (dataset_id);
  status = H5Fclose (file_id);
  printf("(*) done!\n");

}



void zero (scalar * f0, scalar * f) {
  int i;
  for (i = 0; i < NX * NY * NZ; i++) {
    f0[i] = 0.0;
  }
  for (i = 0; i < (XE-XS) * (YE-YS) * (ZE-ZS) ; i++) {
    f[i] = 0.0;
  }
}


void main (int argc, char **argv) {

  int idx, i, j, k;
  int ii, ji, ki ,count, count0;
  scalar *f0, *f;
  
  f0 = (scalar *) malloc (sizeof (scalar) * NX * NY * NZ);
  f = (scalar *) malloc (sizeof (scalar) * (XE-XS) * (YE-YS) * (ZE-ZS));
 
  if (argc != 4) {
    fprintf (stderr,
             "Error! Usage is %s ORIGINAL-name out-dir XXXX-name \n",
             argv[0]);
    exit (2);
  }

  fdir=argv[2];
  forigname=argv[1];
  fnewname=argv[3];
  printf("*original file: %s\n*dirout: %s\n*name-out: %s\n\n",forigname,fdir,fnewname);



  zero (f0,f);
  readtopo (f0);

  count=0;
  count0=0;

  for (j = 0; j < NY; j++) {
    for (i = 0; i < NX; i++) {
      for (k = 0; k < NZ; k++) {
      
        if ( (i>=(int)XS)&&(i<(int)XE)&&(j>=(int)YS)&&(j<(int)YE)&&(k>=(int)ZS)&&(k<(int)ZE) ) {
        
	ii = i - (int)XS;
	ji = j - (int)YS;
	ki = k - (int)ZS;
	idx=IDXN(ii,ji,ki);
	f[idx]=f0[count0];
        //fprintf(stdout,"(w) %d %d %d | %d %d %d | %d %d %d | %d \n",ii,ji,ki,i,j,k,(int)XE,(int)YE,(int)ZE,idx);
        count++;

        }

      count0++;
      }      
    }
  }


  writetopo (f);

}
