#!/bin/bash -e

echo " "
echo "#lbdm# Test 017 Herschel-Bulkley Non-Newtonian 2d"
echo " "

 git checkout user-defined.c
 git checkout calc_bn.m

if [ -f "*.bin.txt" ]; then
rm *.bin.txt
fi

if [ -f "dump.CONV.h5" ]; then
rm dump.CONV.h5
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

if [ -f "ux_profile_lbdm.dat" ]; then
rm ux_profile_lbdm.dat
fi

if [ -f "input.dat" ]; then
rm input.dat
fi

octave create_bin-txt.m
cp po2d.bin.txt ../../in/

# 1 ###
sed -i -e 's/n=.*/n=1.0/g' calc_bn.m
sed -i -e 's/pi0=.*/pi0=10/g' calc_bn.m
sed -i -e 's/Bn=.*/Bn=0.0/g' calc_bn.m

octave calc_bn.m
sed -e '154,156d' user-defined.c > user-tmp.c
rm user-defined.c
sed '153r input.dat' user-tmp.c > user-defined.c
rm user-tmp.c

cp user-defined.c ../../user/
cd ../..

make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/017_nnew2d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave ux_h5_po2d.m

paste poiseuille_wiki.dat ux_profile_lbdm.dat | awk 'NR>=2&&NR<=17 {print ($2/$4-1)}' > error.dat 


# 2 ###
sed -i -e 's/n=.*/n=0.9/g' calc_bn.m
sed -i -e 's/pi0=.*/pi0=10/g' calc_bn.m
sed -i -e 's/Bn=.*/Bn=1.0/g' calc_bn.m

octave calc_bn.m
sed -e '154,156d' user-defined.c > user-tmp.c
rm user-defined.c
sed '153r input.dat' user-tmp.c > user-defined.c
rm user-tmp.c

cp user-defined.c ../../user/
cd ../..

make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/017_nnew2d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave ux_h5_po2d.m

paste n09bn1_wiki.dat ux_profile_lbdm.dat | awk 'NR>=2&&NR<=17 {print ($2/$4-1)}' >> error.dat


# 3 ###
sed -i -e 's/n=.*/n=1.1/g' calc_bn.m
sed -i -e 's/pi0=.*/pi0=10/g' calc_bn.m
sed -i -e 's/Bn=.*/Bn=1.0/g' calc_bn.m

octave calc_bn.m
sed -e '154,156d' user-defined.c > user-tmp.c
rm user-defined.c
sed '153r input.dat' user-tmp.c > user-defined.c
rm user-tmp.c

cp user-defined.c ../../user/
cd ../..

make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (3) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/017_nnew2d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave ux_h5_po2d.m

paste n11bn1_wiki.dat ux_profile_lbdm.dat | awk 'NR>=2&&NR<=17 {print ($2/$4-1)}' >> error.dat

###########

awk 'NR<=48 {if (($1*$1)^0.5<=0.05) print "ok"; else print "no" }' error.dat > errchk.dat

###########

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/po2d.bin.txt

# clean local test
cd test/017_nnew2d/
git checkout user-defined.c
git checkout calc_bn.m
rm *.txt

########### 1) Check NaN 

for file in ux_profile.dat error.dat errchk.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 48 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
