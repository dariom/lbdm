warning('off');

%%%% threshold
load('dump.CONV.h5');

fluxratio = (LBdm.tem(50)*1.5-LBdm.tem(49)*2+LBdm.tem(48)*0.5)/(-LBdm.tem(51)*1.5+LBdm.tem(52)*2-LBdm.tem(53)*0.5),

save('conj.dat','-ascii','-append','fluxratio');

