mu=0.16667,
H=18;
dpx=2e-5;

n=0.0;
pi0=00;
Bn=0.0;

% zero eq
V = dpx*H**2/mu,
% first eq
k = H/pi0*(H/V)**n*dpx,
% second eq
t = Bn*k*(H/V)**(-n),

par{1}=sprintf('#define NN_KAPPA %0.7f',k);
par{2}=sprintf('#define NN_YIELD %0.9f',t);
par{3}=sprintf('#define NN_EXPON %0.2f',n);


   fid=fopen("input.dat", "w");
   fprintf(fid,'%s\n',par{:})
   fclose(fid);
