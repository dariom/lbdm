
 function [liq_sol_ijk]=geoS_3d(n,rA,ncA,oA,vA,fA,spr,decl)


 pp=1;

nx=80*n;
ny=80*n;
nz=80*n;

lfactor = 1;%2/3/sqrt(3);

if (((vA==1 || vA==4) || (vA==2 && oA==4)) && fA==1);

A=ones(nz,nx,ny);

lmax=30*n;
lmin=25*n;
r=n*rA;

wN=0; wS=0;
wU=0; wD=0;


%% ORIENTATIONS %%
% random     --> oA=1;
% pref x,y,z --> oA=2;
% full x,y,z --> oA=3;
% pref T y,z --> oA=4;
% full T y,z --> oA=5;

if oA==1;
% seed1=43;%505;
% seed2=20;%7;
if decl==80; seed2=202; seed1=218; end;
if decl==81; seed2=234; seed1=205; end;

rand(seed1); 
costheta_ran=rand(ncA,1);
rand(seed2);
phi_ran=rand(ncA,1).*360;

%%% Matlab
%rs=RandStream('mt19937ar','Seed',seed1);
%RandStream.setGlobalStream(rs);
%costheta_ran=rand(ncA,1);
%rs=RandStream('mt19937ar','Seed',seed2);
%RandStream.setGlobalStream(rs);
%phi_ran=rand(ncA,1).*360;

end;

if oA==2;
seed1=43;%M677;%N65;%O168;%S19;%P43;%Q505;
seed2=20;%M19;%N67;%O170;%S62;%P20;%Q7
% if decl==74; seed1=202; seed2=218; end;
% if decl==75; seed1=234; seed2=205; end;
% if decl==76; seed1=217; seed2=236; end;

rand(seed1); 
costheta_ran=rand(ncA,1);
rand(seed2);
phi_ran=rand(ncA,1).*360;

% Matlab
%rs=RandStream('mt19937ar','Seed',seed1);
%RandStream.setGlobalStream(rs);
%costheta_ran=rand(ncA,1).*(1-sqrt(2)/2)+sqrt(2)/2;
%rs=RandStream('mt19937ar','Seed',seed2);
%RandStream.setGlobalStream(rs);
%phi_ran=rand(ncA,1).*360;

end;

%
%if oA==3;
%costheta_ran=ones(ncA,1);
%phi_ran=zeros(ncA,1); %randi([0,360],[ncA,1]);
%end;

if oA==4;
%seed1=43;%505;
%seed2=20;%7;
% if decl==80; seed2=236; seed1=218; end;
% if decl==81; seed2=217; seed1=205; end;
if decl==77; seed1=202; seed2=218; end;
if decl==78; seed1=234; seed2=205; end;
if decl==79; seed1=217; seed2=236; end;
if decl==74; seed1=316; seed2=302; end;
if decl==75; seed1=358; seed2=352; end;

rand(seed1); 
costheta_ran=rand(ncA,1);
rand(seed2);
phi_ran=rand(ncA,1).*360;

% Matlab
%rs=RandStream('mt19937ar','Seed',seed1);
%RandStream.setGlobalStream(rs);
%costheta_ran=rand(ncA,1).*sqrt(2)/2;
%rs=RandStream('mt19937ar','Seed',seed2);
%RandStream.setGlobalStream(rs);
%phi_ran=rand(ncA,1).*360; %randi([0,360],[ncA,1]);

end;

if oA==5;
seed1=0;
seed2=505;

rand(seed1); 
costheta_ran=rand(ncA,1);
rand(seed2);
phi_ran=rand(ncA,1).*360;

% Matlab
%rs=RandStream('mt19937ar','Seed',seed1);
%RandStream.setGlobalStream(rs);
%costheta_ran=zeros(ncA,1);
%rs=RandStream('mt19937ar','Seed',seed2);
%RandStream.setGlobalStream(rs);
%phi_ran=rand(ncA,1).*360; %randi([0,360],[ncA,1]);

end;

nranx=find(costheta_ran>=sqrt(2)/2);
nrany=find((costheta_ran<sqrt(2)/2 & (phi_ran<=45 | ...
( phi_ran>=135 & phi_ran<=225) | phi_ran>=315 )) );
nranz=find(costheta_ran<sqrt(2)/2 & ( (phi_ran>45 & phi_ran<135) | ...
( phi_ran>225 & phi_ran<315)) );

ncarbx=size(nranx,1);
ncarby=size(nrany,1);
ncarbz=size(nranz,1);

x_ran_x=randi([1,nx],[ncarbx,1]);
y_ran_x=randi([1,ny],[ncarbx,1]);
z_ran_x=randi([1,nz],[ncarbx,1]);

x_ran_y=randi([1,nx],[ncarby,1]);
y_ran_y=randi([1,ny],[ncarby,1]);
z_ran_y=randi([1,nz],[ncarby,1]);

x_ran_z=randi([1,nx],[ncarbz,1]);
y_ran_z=randi([1,ny],[ncarbz,1]);
z_ran_z=randi([1,nz],[ncarbz,1]);

costhetax=costheta_ran;
sinthetax=sqrt(1-costhetax.^2);
phix=pi/180.*(phi_ran);

costhetay=sinthetax.*cos(phix);
sinthetay=sqrt(1-costhetay.^2);
phiy=asin(costhetax./sqrt(1-sinthetax.^2.*(cos(phix)).^2));

costhetaz=sinthetax.*sin(phix);
sinthetaz=sqrt(1-costhetaz.^2);
phiz=acos(costhetax./sqrt(1-sinthetax.^2.*(sin(phix)).^2));

phiy(isnan(phiy))=0;
phiz(isnan(phiz))=0;

phix=(real(phix));
phiy=(real(phiy));
phiz=(real(phiz));

carb_iter_x=0;
carb_iter_y=0;
carb_iter_z=0;

% check
% figure(1),
% subplot(1,2,1),hist(costheta_ran,15);
% subplot(1,2,2),hist(phi_ran,15);
% pause(), close(1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% x oriented %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for ran=1:ncarbx,

carb_iter_x=carb_iter_x+1,

costheta=costhetax(nranx(ran));
sintheta=sinthetax(nranx(ran));
phi=phix(nranx(ran));
ryz0=abs(r/costheta);

x0=x_ran_x(ran)+0.5;
y0=y_ran_x(ran)+0.5;
z0=z_ran_x(ran)+0.5;
l=nx/2*sqrt(3)*lfactor ;%round(l_ran_x(ran)/2);

x1=ceil(x0-l*abs(costheta));
x2=floor(x0+l*abs(costheta));

for x=x0+0.5:x2
li=(x-x0)/(costheta);

if (x<1); x=mod(x-1,nx)+1; end; % x=nx+x; end;
if (x>nx); x=mod(x-1,nx)+1; end; % x=x-nx; end;
y=round(li*sintheta*cos(phi)+y0);
z=round(li*sintheta*sin(phi)+z0);

%[i,j]=ellipse(y,z,r,costheta,sintheta,phi,l,ny,nz);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

yc=[-r:0.2:r];
xc=[-sqrt(r^2-yc.^2),sqrt(r^2-yc.^2)];
yc=[yc,yc];

ye=[-r:0.2:r];
xe=[-r./costheta.*sqrt(1-(ye./r).^2),r./costheta.*sqrt(1-(ye./r).^2)];
ye=[ye,ye];

R=[cos(phi),-sin(phi);sin(phi),cos(phi)];

[Y,X]=ind2sub([nz,ny],1:nz*ny);

[XYR]=(R)*([xe;ye])+repmat([ny/2+0.5;nz/2+0.5],1,size(xe,2));
ij=find(inhull([Y;X]',[XYR(2,:);XYR(1,:)]'));
j=ij-floor(ij/nz)*nz-nz/2;
i=ceil(ij/nz)-ny/2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

yi=(i+y);
zj=nz-(j+z)+1;

for ij=1:size(zj);
if zj(ij)<1; zj(ij)=mod(zj(ij)-1,nz)+1; end; % zj(ij)=nz+zj(ij); end;
if zj(ij)>nz; zj(ij)=mod(zj(ij)-1,nz)+1; end; % zj(ij)=zj(ij)-nz; end;
if yi(ij)<1; yi(ij)=mod(yi(ij)-1,ny)+1; end; % yi(ij)=ny+yi(ij); end;
if yi(ij)>ny; yi(ij)=mod(yi(ij)-1,ny)+1; end; % yi(ij)=yi(ij)-ny; end;
A(zj(ij),x,yi(ij))=0;
end;

end;

for x=x1:x0-0.5;
li=(x0-x)/(costheta);

if (x<1); x=mod(x-1,nx)+1; end; % x=nx+x; end;
if (x>nx); x=mod(x-1,nx)+1; end; % x=x-nx; end;
y=round(y0-li*sintheta*cos(phi));
z=round(z0-li*sintheta*sin(phi));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

yc=[-r:0.2:r];
xc=[-sqrt(r^2-yc.^2),sqrt(r^2-yc.^2)];
yc=[yc,yc];

ye=[-r:0.2:r];
xe=[-r./costheta.*sqrt(1-(ye./r).^2),r./costheta.*sqrt(1-(ye./r).^2)];
ye=[ye,ye];

R=[cos(phi),-sin(phi);sin(phi),cos(phi)];

[Y,X]=ind2sub([nz,ny],1:nz*ny);

[XYR]=(R)*([xe;ye])+repmat([ny/2+0.5;nz/2+0.5],1,size(xe,2));
ij=find(inhull([Y;X]',[XYR(2,:);XYR(1,:)]'));
j=ij-floor(ij/nz)*nz-nz/2;
i=ceil(ij/nz)-ny/2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

yi=(i+y);
zj=nz-(j+z)+1;

for ij=1:size(zj);
if zj(ij)<1; zj(ij)=mod(zj(ij)-1,nz)+1; end; % zj(ij)=nz+zj(ij); end;
if zj(ij)>nz; zj(ij)=mod(zj(ij)-1,nz)+1; end; % zj(ij)=zj(ij)-nz; end;
if yi(ij)<1; yi(ij)=mod(yi(ij)-1,ny)+1; end; % yi(ij)=ny+yi(ij); end;
if yi(ij)>ny; yi(ij)=mod(yi(ij)-1,ny)+1; end; % yi(ij)=yi(ij)-ny; end;
A(zj(ij),x,yi(ij))=0;
end;

end; end;

carb_iter_y=carb_iter_x;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% y oriented %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for ran=1:ncarby;

carb_iter_y=carb_iter_y+1,

costheta=costhetay(nrany(ran));
sintheta=sinthetay(nrany(ran));
phi=phiy(nrany(ran));
rzx0=abs(r/costheta);

x0=x_ran_y(ran)+0.5;
y0=y_ran_y(ran)+0.5;
z0=z_ran_y(ran)+0.5;
l=ny/2*sqrt(3)*lfactor;%round(l_ran_x(ran)/2);

y1=ceil(y0-l*abs(costheta));
y2=floor(y0+l*abs(costheta));

for y=y0+0.5:y2;
li=(y-y0)/(costheta);

if (y<1); y=mod(y-1,ny)+1; end; % y=ny+y; end;
if (y>ny); y=mod(y-1,ny)+1; end; % y=y-ny; end;
z=round(li*sintheta*cos(phi)+z0);
x=round(li*sintheta*sin(phi)+x0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

yc=[-r:0.2:r];
xc=[-sqrt(r^2-yc.^2),sqrt(r^2-yc.^2)];
yc=[yc,yc];

ye=[-r:0.2:r];
xe=[-r./costheta.*sqrt(1-(ye./r).^2),r./costheta.*sqrt(1-(ye./r).^2)];
ye=[ye,ye];

R=[cos(phi),-sin(phi);sin(phi),cos(phi)];

[Y,X]=ind2sub([nx,nz],1:nx*nz);

[XYR]=(R)*([xe;ye])+repmat([nz/2+0.5;nx/2+0.5],1,size(xe,2));
ij=find(inhull([Y;X]',[XYR(2,:);XYR(1,:)]'));
j=ij-floor(ij/nx)*nx-nx/2;
i=ceil(ij/nx)-nz/2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

zi=nz-(i+z)+1;
xj=(j+x);

for ij=1:size(xj);
if zi(ij)<1; zi(ij)=mod(zi(ij)-1,nz)+1; end; % zi(ij)=nz+zi(ij); end;
if zi(ij)>nz; zi(ij)=mod(zi(ij)-1,nz)+1; end; % zi(ij)=zi(ij)-nz; end;
if xj(ij)<1; xj(ij)=mod(xj(ij)-1,nx)+1; end; % xj(ij)=nx+xj(ij); end;
if xj(ij)>nx; xj(ij)=mod(xj(ij)-1,nx)+1; end; % xj(ij)=xj(ij)-nx; end;
A(zi(ij),xj(ij),y)=0;
end;

end;

for y=y1:y0-0.5;
li=(y0-y)/(costheta);

if (y<1); y=mod(y-1,ny)+1; end; % y=ny+y; end;
if (y>ny); y=mod(y-1,ny)+1; end; % y=y-ny; end;
z = round(z0-li*sintheta*cos(phi));
x = round(x0-li*sintheta*sin(phi));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

yc=[-r:0.2:r];
xc=[-sqrt(r^2-yc.^2),sqrt(r^2-yc.^2)];
yc=[yc,yc];

ye=[-r:0.2:r];
xe=[-r./costheta.*sqrt(1-(ye./r).^2),r./costheta.*sqrt(1-(ye./r).^2)];
ye=[ye,ye];

R=[cos(phi),-sin(phi);sin(phi),cos(phi)];

[Y,X]=ind2sub([nx,nz],1:nx*nz);

[XYR]=(R)*([xe;ye])+repmat([nz/2+0.5;nx/2+0.5],1,size(xe,2));
ij=find(inhull([Y;X]',[XYR(2,:);XYR(1,:)]'));
j=ij-floor(ij/nx)*nx-nx/2;
i=ceil(ij/nx)-nz/2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

zi=nz-(i+z)+1;
xj=(j+x);

for ij=1:size(xj);
if zi(ij)<1; zi(ij)=mod(zi(ij)-1,nz)+1; end; % zi(ij)=nz+zi(ij); end;
if zi(ij)>nz; zi(ij)=mod(zi(ij)-1,nz)+1; end; % zi(ij)=zi(ij)-nz; end;
if xj(ij)<1; xj(ij)=mod(xj(ij)-1,nx)+1; end; % xj(ij)=nx+xj(ij); end;
if xj(ij)>nx; xj(ij)=mod(xj(ij)-1,nx)+1; end; % xj(ij)=xj(ij)-nx; end;
A(zi(ij),xj(ij),y)=0;
end;

end; end;

carb_iter_z=carb_iter_y;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% z oriented %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for ran=1:ncarbz

carb_iter_z=carb_iter_z+1,

costheta=costhetaz(nranz(ran));
sintheta=sinthetaz(nranz(ran));
phi=phiz(nranz(ran));
rxy0=abs(r/costheta);

x0=x_ran_z(ran)+0.5;
y0=y_ran_z(ran)+0.5;
z0=z_ran_z(ran)+0.5;
l=nz/2*sqrt(3)*lfactor;%round(l_ran_x(ran)/2);

z1=ceil(z0-l*abs(costheta));
z2=floor(z0+l*abs(costheta));

for z=z0+0.5:z2;
li=(z-z0)/(costheta);

if (z<1); z=mod(z-1,nz)+1; end; % z=nz+z; end;
if (z>nz); z=mod(z-1,nz)+1; end; % z=z-nz; end;
x=round(li*sintheta*cos(phi)+x0);
y=round(li*sintheta*sin(phi)+y0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

yc=[-r:0.2:r];
xc=[-sqrt(r^2-yc.^2),sqrt(r^2-yc.^2)];
yc=[yc,yc];

ye=[-r:0.2:r];
xe=[-r./costheta.*sqrt(1-(ye./r).^2),r./costheta.*sqrt(1-(ye./r).^2)];
ye=[ye,ye];

R=[cos(phi),-sin(phi);sin(phi),cos(phi)];

[Y,X]=ind2sub([ny,nx],1:ny*nx);

[XYR]=(R)*([xe;ye])+repmat([nx/2+0.5;ny/2+0.5],1,size(xe,2));
ij=find(inhull([Y;X]',[XYR(2,:);XYR(1,:)]'));
j=ij-floor(ij/ny)*ny-ny/2;
i=ceil(ij/ny)-nx/2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xi=(i+x);
yj=(j+y);

for ij=1:size(yj);
if yj(ij)<1; yj(ij)=ny+yj(ij); end;
if yj(ij)>ny; yj(ij)=yj(ij)-ny; end;
if xi(ij)<1; xi(ij)=mod(xi(ij)-1,nx)+1; end; % xi(ij)=nx+xi(ij); end;
if xi(ij)>nx; xi(ij)=mod(xi(ij)-1,nx)+1; end; % xi(ij)=xi(ij)-nx; end;
A(z,xi(ij),yj(ij))=0;
end;

end;

for z=z1:z0-0.5;
li=(z0-z)/(costheta);

if (z<1); z=mod(z-1,nz)+1; end; % z=nz+z; end;
if (z>nz); z=mod(z-1,nz)+1; end; % z=z-nz; end;
x=round(x0-li*sintheta*cos(phi));
y=round(y0-li*sintheta*sin(phi));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

yc=[-r:0.2:r];
xc=[-sqrt(r^2-yc.^2),sqrt(r^2-yc.^2)];
yc=[yc,yc];

ye=[-r:0.2:r];
xe=[-r./costheta.*sqrt(1-(ye./r).^2),r./costheta.*sqrt(1-(ye./r).^2)];
ye=[ye,ye];

R=[cos(phi),-sin(phi);sin(phi),cos(phi)];

[Y,X]=ind2sub([ny,nx],1:ny*nx);

[XYR]=(R)*([xe;ye])+repmat([nx/2+0.5;ny/2+0.5],1,size(xe,2));
ij=find(inhull([Y;X]',[XYR(2,:);XYR(1,:)]'));
j=ij-floor(ij/ny)*ny-ny/2;
i=ceil(ij/ny)-nx/2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xi=(i+x);
yj=(j+y);

for ij=1:size(yj);
if yj(ij)<1; yj(ij)=mod(yj(ij)-1,ny)+1; end; % yj(ij)=ny+yj(ij); end;
if yj(ij)>ny; yj(ij)=mod(yj(ij)-1,ny)+1; end; %yj(ij)=yj(ij)-ny; end;
if xi(ij)<1; xi(ij)=mod(xi(ij)-1,nx)+1; end; % xi(ij)=nx+xi(ij); end;
if xi(ij)>nx; xi(ij)=mod(xi(ij)-1,nx)+1; end; % xi(ij)=xi(ij)-nx; end;
A(z,xi(ij),yj(ij))=0;
end;

end; end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save([ 'out/' spr '_stoca.mat' ], ...
		'A','costheta_ran','phi_ran', ...
		'x_ran_x','x_ran_y','x_ran_z', ...
		'y_ran_x','y_ran_y','y_ran_z', ...
		'z_ran_x','z_ran_y','z_ran_z' );

    %% boundaries
% A([1:wN,nz-wS+1:nz],[1:nx,1:nx],[1:ny,1:ny])=0;
% A([1:nz],[1:nx],[1:wU,ny-wD+1:ny])=1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end;

if ((oA==1)); % && (vA==2 || vA==3 || vA==6 || vA==7)));

    
if (oA==1 && ncA==50 && (vA==1 || vA==2 || vA==3));
spr= 'P[po1_or1_re1_v1]';
end;    

%if (oA==1 && ncA==50 && (vA==5 || vA==6 || vA==7));
%spr= 'P[po1_or1_re1_v1]';    
%end;  

% if (ncA==280 && oA==1) ;
% spr= 'S[po3_or1_re1_v1]';    
% end;
% 
% if (ncA==280 && oA==2) ;
% spr= 'S[po3_or2_re1_v1]';    
% end;

load([ 'out/' spr '_stoca.mat' ], ...
		'A','costheta_ran','phi_ran', ...
		'x_ran_x','x_ran_y','x_ran_z', ...
		'y_ran_x','y_ran_y','y_ran_z', ...
		'z_ran_x','z_ran_y','z_ran_z' );

printf([ 'loaded ' spr '\n\n' ]),

if (oA==1);    
if (vA==2 || vA==6); A=permute(A,[2 3 1]); end;
if (vA==3 || vA==7); A=permute(A,[3,1,2]); end;
end;

end;

if (fA~=1);

% if (ncA==50 && oA==4);
% spr= 'S[po1_or4_re1_v2]';    
% end;    
% 
% if (ncA==50 && oA==5);
% spr= 'S[po1_or5_re1_v2]';    
% end;    
% 
% if (ncA==180 && oA==4) ;
% spr= 'S[po3_or4_re1_v2]';    
% end;
% 
% if (ncA==180 && oA==5) ;
% spr= 'S[po3_or5_re1_v2]';    
% end;
% 
% if (ncA==50 && oA==1) ;
% spr= 'S[po1_or1_re1_v1]';    
% end;
% 
% if (ncA==180 && oA==1) ;
% spr= 'S[po3_or1_re1_v1]';    
% end;
% 
% if (ncA==115 && oA==1) ;
% spr= 'S[po2_or1_re1_v1]';    
% end;
% 
% if (ncA==115 && oA==4) ;
% spr= 'S[po2_or4_re1_v2]';    
% end;
% 
% if (ncA==115 && oA==5) ;
% spr= 'S[po2_or5_re1_v2]';    
% end;
% 
% if (ncA==115 && oA==2) ;
% spr= 'S[po2_or2_re1_v1]';    
% end;

% load([ 'K-VPM-xlpt/' spr '_stoca.mat' ], ...
% 		'A','costheta_ran','phi_ran', ...
% 		'x_ran_x','x_ran_y','x_ran_z', ...
% 		'y_ran_x','y_ran_y','y_ran_z', ...
% 		'z_ran_x','z_ran_y','z_ran_z' );
end;
    

%% coeff
porosity=sum(sum(sum(A,3),2),1)/(nz*nx*ny),
%pause(),

%figg=figure(1), 
%subplot(1,2,1), 
%isosurface(permute(A,[3,2,1])),
%pbaspect([nx,nz,ny]), view(3),
%camlight;
%xlim([1,nx]);
%ylim([1,nz]);
%zlim([1,ny]);
%subplot(1,2,2), 
%scatter(costheta_ran,phi_ran);
%xlim([0,1]);
%ylim([0,360]);
%
%
% pause(1), close(1),

%for y=1:ny;
%B(:,y,:)=A(:,:,y);
%end

%figure, imshow3D(B)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% indices
[isolid,jsolid,ksolid]=ind2sub(size(A),find(A==0));
[iliquid,jliquid,kliquid]=ind2sub(size(A),find(A==2));
Length=max(length(isolid),1);
liq_sol_ijk=zeros(Length,4);
liq_sol_ijk(1:length(isolid),1)=isolid;
liq_sol_ijk(1:length(isolid),2)=jsolid;
liq_sol_ijk(1:length(isolid),3)=ksolid;

liq_sol_ijk(1:4,4)=[nz,nx,ny,porosity]';
