/* ! ATTENTION do not delete or add strange rows !
Just either comment or uncomment the "Capabilities" 
and replace values of "Macros" with the desired ones */

/* ******************* */
/*  Capabilities here  */
/* ******************* */

#define DPDLX

#define DUMPHDF5
#define INASCII

//#define LBMRT
//#define TWOPHASE
//#define THERMAL
#define SCALAR

//#define FREESLIP_XP
//#define FREESLIP_XM
//#define FREESLIP_YP
//#define FREESLIP_YM
//#define FREESLIP_ZP
//#define FREESLIP_ZM

//#define GRAVITY
//#define NEIGHBOURING

//#define DENSITY_XP
//#define DENSITY_XM
//#define DENSITY_YP
//#define DENSITY_YM
//#define DENSITY_ZP
//#define DENSITY_ZM

//#define VELOCITY_XP
//#define VELOCITY_XM
//#define VELOCITY_YP
//#define VELOCITY_YM
//#define VELOCITY_ZP
//#define VELOCITY_ZM

//#define CONCENTRATION_XM
//#define CONCENTRATION_XP

//#define CONCENTRATION_EQ_XM
#define CONCENTRATION_EQ_XP

#define CONCENTRATION_HALF_XM
//#define CONCENTRATION_HALF_XP

//#define INNER_TIME_FLU_BCS
//#define INNER_TIME_TEM_BCS
//#define INNER_TIME_CON_BCS

//#define ACOUSTIC_PULSE

/* ******************* */
/*  Macros for folks   */
/* ******************* */

// Name identifiers
#define NAME "SCALAR2D"
#define GEOM "scalar2d.bin.txt"

// N-PROC
#define NP 2

// N-SIZE
#define NX 42
#define NY 2
#define NZ 22

// N-PROC PER EDGE
#define NEX 2
#define NEY 1
#define NEZ 1


// INIT DENSITY & dP/dL
#define RHO0 1.0
#define DPDL 0.00059535

// Iteration max,chk,out
#define MAX_ITER 3000
#define CHK_ITER 1
#define OUT_ITER 200

// Q-speeds and t-relax
#define NQ 19
#define TAU 1.0

// RESIDUAL convergence
#define RESIDUAL 0.0
#define CLENGTH 20

// TWOPHASE parameters
#define GG 0.0
#define RHO2 1.0
#define RHOZ 1.0
#define RHOWALL 1.0

// THERMAL parameters
#define TAUT 1.0
#define TINIT 1.0
#define TWALL 1.0
#define TSUB  1.0

// SCALAR parameters
#define TAUC 1.0
#define CINIT 0.0

// INLET/OUTLET parameters
#define DIN  1.0
#define DOUT 1.0
#define VIN  0.0 
#define VOUT 0.0

// In/Out Scalar
#define CIN 1.0
#define CIN2 0.0
#define COUT 1.0
#define COUT2 0.0

// ACOUSTIC parameters
#define DRHO 0.001
#define FWAVE 4.00
