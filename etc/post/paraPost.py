#!/usr/bin/env python
'''
Script for generating a xdmf file with given lbdm h5 data to use in paraView

usage: ./paraPost.py FILENAMES
e.g.: ./paraPost.py dump.*.h5

'''


import numpy as np
import h5py
import sys


def main():
    FILES = sys.argv[1:]
    outFile = "paraPost.xdmf"


    # So far all takes the tags from the first file
    fname = FILES[0]
    f = h5py.File(fname,'r')
    data = f['LBdm']

    var = ""
    for x in data:
        var = x
    
    shape = data[var].shape

    # Starting assemble string to print
    toFile = XDMFHeader()

    for i, fName in enumerate(FILES):
        toFile += XDMFTimestepSetup(i+1,shape)
        for vName in data.keys():
            toFile +=XDMFAddAttribute(fName, vName, shape)
        toFile += XDMFTimestepClose()

    toFile += XDMFFooter()
        
    with open(outFile,'w') as f:
        f.write(toFile)


def XDMFHeader():
    text = '''<?xml version="1.0" ?>
<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>
<Xdmf Version="2.0">
  <Domain>
    <Grid Name="CellTime" GridType="Collection" CollectionType="Temporal">'''
    return text
    
def XDMFFooter():
    text = '''    
    </Grid>
  </Domain>
</Xdmf>'''
    return text

def XDMFTimestepSetup(t, shape):
    # %(time,sx+1,sy+1,sz+1,name,sx,sy,sz
    text = '''
    <Grid Name="Structured Grid" GridType="Uniform">
    <Time Value="%d" />
      <Topology TopologyType="3DCORECTMesh" NumberOfElements="%d %d %d"/>
      <Geometry GeometryType="ORIGIN_DXDYDZ">
        <DataItem Name="Origin" Dimensions="3" NumberType="Float" Precision="4" Format="XML">
          0 0 0
        </DataItem>
        <DataItem Name="Spacing" Dimensions="3" NumberType="Float" Precision="4" Format="XML">
          0.1 0.1 0.1
        </DataItem>
      </Geometry>
      '''%(t,shape[0]+1,shape[1]+1,shape[2]+1)
    return text


def XDMFTimestepClose():
    text = '''
    </Grid>
    '''
    return text

def XDMFAddAttribute(fName, vName,shape):
    text = '''
      <Attribute Name="%s" AttributeType="Scalar" Center="Cell">
        <DataItem Dimensions="%d %d %d" NumberType="Float" Precision="8" Format="HDF">
            ./%s:/LBdm/%s
        </DataItem>
      </Attribute>
    '''%(vName,shape[0], shape[1], shape[2], fName, vName)
    return text


if __name__=="__main__":
    main()



