dt=load('DT.dat');

rg=dt(:,1);
rl=dt(:,2);
tg=dt(:,3);
tl=dt(:,4);

dr=0.01;
G1=9.5;
G2=4.0;


TT1=[tg(1:6)+tl(1:6)]'./2;
TT2=[tg(7:12)+tg(7:12)]'./2;

NC1=100;
NC2=40;

rgth=zeros(NC1,5);
rlth=zeros(NC1,5);

for it=1:size(TT1,2);

G=G1;
DR=0.00005;
rgth(1,it)=0.67*rg(it);
err=1;

  for c=2:NC1;

  rgth(c,it)=rgth(c-1,it)+DR;
  P01(it)=rgth(c,it).*TT1(it)-G/6*exp(-2/rgth(c,it));
  vr=[rgth(c,it):dr:8];

  sig0=1; sigc=0; int0=0;

    A(it)=0;
    for j=2:size(vr,2);

    sig=-(P01(it)-TT1(it).*vr(j)+G/6.*exp(-2./vr(j)));
    sigj(j)=sig;
    if (sig>0); sig1=1; end;
    if (sig<0); sig1=0; end;
    if (sig1~=sig0); sigc=1+sigc; sig0=sig1; end;
    if (sigc>1); rlth(c,it)=vr(j); break; end;

    A(it)=(((P01(it)-TT1(it).*vr(j)+G/6.*exp(-2./vr(j)))./vr(j).**2).*dr)+A(it);
    end;

  err(c)=abs(A(it))/P01(it)/rgth(c,it);

  end;

 %figure(1),
 %hold on, plot(log10(err(2:end)));

 [ERR1(it) jj(it)]=min(err(2:end));
 RL1(it)=rlth(jj(it)+1,it);
 RG1(it)=rgth(jj(it)+1,it);

 %hold on, plot(jj(it),log10(ERR1(it)),'o')

end;

 figure(2), plot(tg(1:6),rg(1:6),'o')
 hold on, plot(TT1,RG1,'-')
 hold on, plot(tl(1:6),rl(1:6),'o')
 hold on, plot(TT1,RL1,'-')
 title('density & Maxwell construction 9.5');
 saveas(gcf,'maxwell1.png','png');

rgth=zeros(NC2,5);
rlth=zeros(NC2,5);

for it=1:size(TT2,2);

G=G2;
DR=0.0004;
rgth(1,it)=0.97*rg(it+6);
err=1;

  for c=2:NC2;

  rgth(c,it)=rgth(c-1,it)+DR;
  P02(it)=rgth(c,it).*TT2(it)-G/6*exp(-2/rgth(c,it));
  vr=[rgth(c,it):dr:8];

  sig0=1; sigc=0; int0=0;

    A(it)=0;
    for j=2:size(vr,2);

    sig=-(P02(it)-TT2(it).*vr(j)+G/6.*exp(-2./vr(j)));
    sigj(j)=sig;
    if (sig>0); sig1=1; end;
    if (sig<0); sig1=0; end;
    if (sig1~=sig0); sigc=1+sigc; sig0=sig1; end;
    if (sigc>1); rlth(c,it)=vr(j); break; end;

    A(it)=(((P02(it)-TT2(it).*vr(j)+G/6.*exp(-2./vr(j)))./vr(j).**2).*dr)+A(it);
    end;

  err(c)=abs(A(it))/P02(it)/rgth(c,it);

  end;

 %figure(3),
 %hold on, plot(log10(err(2:end)));

 [ERR2(it) jj(it)]=min(err(2:end));
 RL2(it)=rlth(jj(it)+1,it);
 RG2(it)=rgth(jj(it)+1,it);

 %hold on, plot(jj(it),log10(ERR2(it)),'o')

end;

 figure(4), plot(tg(7:12),rg(7:12),'o')
 hold on, plot(TT2,RG2,'-')
 hold on, plot(tl(7:12),rl(7:12),'o')
 hold on, plot(TT2,RL2,'-')
 title('density & Maxwell construction 4.0');
 saveas(gcf,'maxwell2.png','png');
 
 M=abs([1-RG1'./rg(1:6) ; 1-RL1'./rl(1:6) ; 1-RG2'./rg(7:12) ; 1-RL2'./rl(7:12) ]);
 save('error.dat','-ascii','M');
