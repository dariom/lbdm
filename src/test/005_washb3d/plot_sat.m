lb=load('sat_lb.dat');

graphics_toolkit ("gnuplot");
setenv ("GNUTERM", "qt");

xx=lb(:,1);
yy=0.5.*xx.**0.5;

figure, plot(xx,lb(:,2),'o')
hold on, plot(xx,yy,'-')
set(gca,'xscale','log','yscale','log')
title 'log - angle 72 degrees'
saveas(gcf,'plot_sat-log.png','png');

close;

figure, plot(xx,lb(:,2),'o')
hold on, plot(xx,yy,'-')
title 'lin - angle 72 degrees'
saveas(gcf,'plot_sat-lin.png','png');

ang=72;
pos=68;

load('dump.CONV.h5');
rho=LBdm.rho;
figure, contourf(rho(:,:,5));
pbaspect([8 1])
hold on, plot([pos,pos-5*cos(pi()/180*ang)],1.5+[0,5*sin(pi()/180*ang)],'-','linewidth',2,'color','white')
hold on, plot([41,81],[1.5,1.5],'-','linewidth',2,'color','white')
saveas(gcf,'plot_wash.png')

