itmax=5000;
itint=200;
itratiopost=1/5;

LX=100;
LY=40;
LZ=40;

g=5*10**-6;
nu=1/6;
rhof=0.12;
rhol=2.40;
R=10;

D=38;
corr=D/(D+2.105*2*R);

ut_stokes = 2/9 * R**2 * (rhol-rhof)*g * (1/(rhof*nu)) * (corr) ,
re_stokes = ut_stokes * 2*R / nu,

for it=0:itint:itmax;
name=sprintf('../../out/dump.SETTL3DPIPE.%05d.h5',it);
load(name);
ux=LBdm.ux;
rho=LBdm.rho;
VX(it/itint+1,:)=ux(LZ/2,:,LY/2);
RH(it/itint+1,:)=rho(LZ/2,:,LY/2);
UXlt(it/itint+1)=sum(ux(:).*(rho(:)>1))./sum(rho(:)>1);
UXgt(it/itint+1)=sum(ux(:).*(rho(:)<1))./sum(rho(:)<1);
end;

%for it=1:itmax/itint+1;
%x=1;
%DR(it,x)=2*RH(it,x)-5*RH(it,x+1)+4*RH(it,x+2)-RH(it,x+3);
%for x=2:LX-1;
%DR(it,x)=RH(it,x-1)-2*RH(it,x)+RH(it,x+1);
%end;
%x=LX;
%DR(it,x)=-2*RH(it,x)+5*RH(it,x-1)-4*RH(it,x-2)+RH(it,x-3);
%end;

fcmax=0;
fcmin=0;
THR=abs(RH-1);

for it=2:itmax/itint+1;

it;

[val i1]=min(THR(it,:));
THRR=[1,THR(it,:),1];

[val i2]=min(THRR([1:i1,i1+2:end]));
if (i2<=i1);
i2=i2-1;
end;

[i1,i2];

imax(it)=max(i1,i2);
imin(it)=min(i1,i2);

if (imax(it)-imin(it)>3*R)
flag0(it)=1;
else;
flag0(it)=0;
end;

if (flag0(it)~=flag0(it-1));
flag=flag+1;
end;

if (flag>0);
if (mod(flag,2)~=0);
fcmax=(flag+1)/2;
itmp=imax(it);
imax(it)=imin(it);
imin(it)=itmp;
else;
fcmin=(flag/2);
end;end;

imax(it)=imax(it)+fcmax*LX;
imin(it)=imin(it)+fcmin*LX;

idthr(it)=(imax(it)+imin(it))/2;

end;

XY=[(0:itint:itmax)',idthr'];
XY0=XY;

nit=round(itmax/itint*itratiopost);

[m1 q1] = polyfit(XY(end-nit:end,1),XY(end-nit:end,2)-XY(2,2),1);
m2=sum(UXlt(end-nit:end))/(nit+1);

ut_lb1=m1(1)-UXgt(end),
ut_lb2=(m2(1))-UXgt(end),
%err1=abs(1-ut_lb1/ut_stokes),
%err2=abs(1-ut_lb2/ut_stokes),

M=[m1(1),m1(2),m2(1),nit];
N=XY;
O=[UXlt',UXgt'];
V=[ut_lb1,ut_lb2];

save('settl.xy.dat','-ascii','N');
save('settl.uu.dat','-ascii','O');
save('settl.mm.dat','-ascii','M');
save('ut_lb.dat','-ascii','V');
save('ut_stokes.dat','-ascii','ut_stokes');
