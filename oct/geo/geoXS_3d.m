% %%% triperiodico, ancora mette i centroidi delle fibre in posizioni discrete (int)

clear all

n=2;

L=240;
Lsmall=240;

rA=1.5;
ncA=100;
oA=1;

buff=0;%n*Lsmall;
water=L/2;

dir='../in' 
spr='artfgdl_11';

% %%%%%%%%%%%%%%%%%%%%
pp=1;

nx=L;
ny=Lsmall;
nz=Lsmall;

lfibe=sqrt(3)*Lsmall;
delta=0.00;

A=ones(nz,nx,ny);
r=n*rA;

wN=0; wS=0;
wU=0; wD=0;

% % seed
seed1=43;
seed2=20;

seed3=111;
seed4=327;
seed5=835;
seed6=18;
seed7=86;
seed8=14;
seed9=28;
seed10=61;
seed11=41;

% % ORIENTATIONS %%
% random     --> oA=1;
% pref x,y,z --> oA=2;
% full x,y,z --> oA=3;
% pref T y,z --> oA=4;
% full T y,z --> oA=5;
% custom     --> oA=6;

if oA==1;

rand('seed',seed1); 
costheta_ran=rand(ncA,1);
figure, plot(costheta_ran,'o');
rand('seed',seed2);
phi_ran=rand(ncA,1).*360;
figure, plot(phi_ran,'o');

end;

% %%%%%%%%%%%%%%%%%%%%%%

if oA==2;

rand('seed',seed1); 
costheta_ran=rand(ncA,1).*(1-sqrt(2)/2)+sqrt(2)/2;
rand('seed',seed2);
phi_ran=rand(ncA,1).*360;

end;

% %%%%%%%%%%%%%%%%%%%%%%

%
%if oA==3;
%costheta_ran=ones(ncA,1);
%phi_ran=zeros(ncA,1); %randi([0,360],[ncA,1]);
%end;

% %%%%%%%%%%%%%%%%%%%%%%

if oA==4;
seed1=43;%505;
seed2=20;%7;

rand('seed',seed1); 
costheta_ran=rand(ncA,1).*sqrt(2)/2;
rand('seed',seed2);
phi_ran=rand(ncA,1).*360;

end;

% %%%%%%%%%%%%%%%%%%%%%

if oA==5;
seed1=0;
seed2=505;

rand('seed',seed1);
costheta_ran=rand(ncA,1).*0;
%figure, plot(costheta_ran,'o');
rand('seed',seed2);
phi_ran=rand(ncA,1).*360;
%figure, plot(phi_ran,'o');

end;

% %%%%%%%%%%%%%%%%%%%%

if oA==6;

costheta_ran = 0.85;%zeros(ncA,1); 
phi_ran = 25;%zeros(ncA,1);

x_ran_x = ...  
	  [20 ...
	  ];
y_ran_x = ...
	  [20 ...
	  ]; 
z_ran_x = ...
	  [20 ...
	  ]; 

end;

% %%%%%%%%%%%%%%%%%%%%

nranx=find(costheta_ran>=sqrt(2)/2);
nrany=find((costheta_ran<sqrt(2)/2 & (phi_ran<=45 | ...
( phi_ran>=135 & phi_ran<=225) | phi_ran>=315 )) );
nranz=find(costheta_ran<sqrt(2)/2 & ( (phi_ran>45 & phi_ran<135) | ...
( phi_ran>225 & phi_ran<315)) );

ncarbx=size(nranx,1);
ncarby=size(nrany,1);
ncarbz=size(nranz,1);

if oA~=6;
rand('seed',seed3);
x_ran_x=randi([0,nx],[ncarbx,1]);
%figure, plot(x_ran_x,'o');
rand('seed',seed4);
y_ran_x=randi([0,ny],[ncarbx,1]);
%figure, plot(y_ran_x,'o');
rand('seed',seed5);
z_ran_x=randi([0,nz],[ncarbx,1]);
%figure, plot(z_ran_x,'o');

rand('seed',seed6);
x_ran_y=randi([0,nx],[ncarby,1]);
%figure, plot(x_ran_y,'o');
rand('seed',seed7);
y_ran_y=randi([0,ny],[ncarby,1]);
%figure, plot(y_ran_y,'o');
rand('seed',seed8);
z_ran_y=randi([0,nz],[ncarby,1]);
%figure, plot(z_ran_y,'o');

rand('seed',seed9);
x_ran_z=randi([0,nx],[ncarbz,1]);
%figure, plot(x_ran_z,'o');
rand('seed',seed10);
y_ran_z=randi([0,ny],[ncarbz,1]);
%figure, plot(y_ran_z,'o');
rand('seed',seed11);
z_ran_z=randi([0,nz],[ncarbz,1]);
%figure, plot(z_ran_z,'o');
end;

%pause();
%close all;

costhetax=costheta_ran;
sinthetax=sqrt(1-costhetax.^2);
phix=pi/180.*(phi_ran);

costhetay=sinthetax.*cos(phix);
sinthetay=sqrt(1-costhetay.^2);
phiy=asin(costhetax./sqrt(1-sinthetax.^2.*(cos(phix)).^2));

costhetaz=sinthetax.*sin(phix);
sinthetaz=sqrt(1-costhetaz.^2);
phiz=acos(costhetax./sqrt(1-sinthetax.^2.*(sin(phix)).^2));

phiy(isnan(phiy))=0;
phiz(isnan(phiz))=0;

phix=(real(phix));
phiy=(real(phiy));
phiz=(real(phiz));

carb_iter_x=0;
carb_iter_y=0;
carb_iter_z=0;

% check
 figure(1),
 subplot(1,2,1),hist(costheta_ran,15);
 subplot(1,2,2),hist(phi_ran,15);
 pause(), close(1);


% %%%% PREPROCESSOR

yr=[-r:0.2:r];
xc=[-sqrt(r^2-yr.^2),sqrt(r^2-yr.^2)];
yc=[yr,yr];
ye=[yr,yr];

[Y,X]=ind2sub([nz,ny],1:nz*ny);



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % x oriented %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for ran=1:ncarbx,

carb_iter_x=carb_iter_x+1,

costheta=costhetax(nranx(ran));
sintheta=sinthetax(nranx(ran));
phi=phix(nranx(ran));
ryz0=abs(r/costheta);

x0=x_ran_x(ran)+0.5;
y0=y_ran_x(ran)+0.5;
z0=z_ran_x(ran)+0.5;
l= lfibe/2;%round(l_ran_x(ran)/2);

x1=ceil(x0-l*abs(costheta));
x2=floor(x0+l*abs(costheta));

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xe=[-r./costheta.*sqrt(1-(yr./r).^2),r./costheta.*sqrt(1-(yr./r).^2)];
R=[cos(phi),-sin(phi);sin(phi),cos(phi)];

[XYR]=(R)*([xe;ye])+repmat([ny/2+0.5;nz/2+0.5],1,size(xe,2));
ij=find(inhull([Y;X]',[XYR(2,:);XYR(1,:)]'));
j=ij-floor(ij/nz)*nz-nz/2;
i=ceil(ij/nz)-ny/2;

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for x=x0+0.5:x2
li=(x-x0)/(costheta);

if (x<1); x=mod(x-1,nx)+1; end; % x=nx+x; end;
if (x>nx); x=mod(x-1,nx)+1; end; % x=x-nx; end;
y=floor(li*sintheta*cos(phi)+y0);
z=floor(li*sintheta*sin(phi)+z0);

yi=(i+y);
zj=nz-(j+z)+1;

for ij=1:size(zj);
if zj(ij)<1; zj(ij)=mod(zj(ij)-1,nz)+1; end; % zj(ij)=nz+zj(ij); end;
if zj(ij)>nz; zj(ij)=mod(zj(ij)-1,nz)+1; end; % zj(ij)=zj(ij)-nz; end;
if yi(ij)<1; yi(ij)=mod(yi(ij)-1,ny)+1; end; % yi(ij)=ny+yi(ij); end;
if yi(ij)>ny; yi(ij)=mod(yi(ij)-1,ny)+1; end; % yi(ij)=yi(ij)-ny; end;
A(zj(ij),x,yi(ij))=0;
end;

end;

for x=x1:x0-0.5;
li=(x0-x)/(costheta);

if (x<1); x=mod(x-1,nx)+1; end; % x=nx+x; end;
if (x>nx); x=mod(x-1,nx)+1; end; % x=x-nx; end;
y=floor(y0-li*sintheta*cos(phi));
z=floor(z0-li*sintheta*sin(phi));


yi=(i+y);
zj=nz-(j+z)+1;

for ij=1:size(zj);
if zj(ij)<1; zj(ij)=mod(zj(ij)-1,nz)+1; end; % zj(ij)=nz+zj(ij); end;
if zj(ij)>nz; zj(ij)=mod(zj(ij)-1,nz)+1; end; % zj(ij)=zj(ij)-nz; end;
if yi(ij)<1; yi(ij)=mod(yi(ij)-1,ny)+1; end; % yi(ij)=ny+yi(ij); end;
if yi(ij)>ny; yi(ij)=mod(yi(ij)-1,ny)+1; end; % yi(ij)=yi(ij)-ny; end;
A(zj(ij),x,yi(ij))=0;
end;

end; end;

carb_iter_y=carb_iter_x;



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % y oriented %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for ran=1:ncarby;

carb_iter_y=carb_iter_y+1,

costheta=costhetay(nrany(ran));
sintheta=sinthetay(nrany(ran));
phi=phiy(nrany(ran));
rzx0=abs(r/costheta);

x0=x_ran_y(ran)+0.5;
y0=y_ran_y(ran)+0.5;
z0=z_ran_y(ran)+0.5;
l= lfibe/2;%round(l_ran_x(ran)/2);

y1=ceil(y0-l*abs(costheta));
y2=floor(y0+l*abs(costheta));

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xe=[-r./costheta.*sqrt(1-(yr./r).^2),r./costheta.*sqrt(1-(yr./r).^2)];
R=[cos(phi),-sin(phi);sin(phi),cos(phi)];

[Y,X]=ind2sub([nx,nz],1:nx*nz);

[XYR]=(R)*([xe;ye])+repmat([nz/2+0.5;nx/2+0.5],1,size(xe,2));
ij=find(inhull([Y;X]',[XYR(2,:);XYR(1,:)]'));
j=ij-floor(ij/nx)*nx-nx/2;
i=ceil(ij/nx)-nz/2;

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for y=y0+0.5:y2;
li=(y-y0)/(costheta);

if (y<1); y=mod(y-1,ny)+1; end; % y=ny+y; end;
if (y>ny); y=mod(y-1,ny)+1; end; % y=y-ny; end;
z=floor(li*sintheta*cos(phi)+z0-delta);
x=floor(li*sintheta*sin(phi)+x0-delta);

zi=nz-(i+z)+1;
xj=(j+x);

for ij=1:size(xj);
if zi(ij)<1; zi(ij)=mod(zi(ij)-1,nz)+1; end; % zi(ij)=nz+zi(ij); end;
if zi(ij)>nz; zi(ij)=mod(zi(ij)-1,nz)+1; end; % zi(ij)=zi(ij)-nz; end;
if xj(ij)<1; xj(ij)=mod(xj(ij)-1,nx)+1; end; % xj(ij)=nx+xj(ij); end;
if xj(ij)>nx; xj(ij)=mod(xj(ij)-1,nx)+1; end; % xj(ij)=xj(ij)-nx; end;
A(zi(ij),xj(ij),y)=0;
end;

end;

for y=y1:y0-0.5;
li=(y0-y)/(costheta);

if (y<1); y=mod(y-1,ny)+1; end; % y=ny+y; end;
if (y>ny); y=mod(y-1,ny)+1; end; % y=y-ny; end;
z = floor(z0-li*sintheta*cos(phi)-delta);
x = floor(x0-li*sintheta*sin(phi)-delta);

zi=nz-(i+z)+1;
xj=(j+x);

for ij=1:size(xj);
if zi(ij)<1; zi(ij)=mod(zi(ij)-1,nz)+1; end; % zi(ij)=nz+zi(ij); end;
if zi(ij)>nz; zi(ij)=mod(zi(ij)-1,nz)+1; end; % zi(ij)=zi(ij)-nz; end;
if xj(ij)<1; xj(ij)=mod(xj(ij)-1,nx)+1; end; % xj(ij)=nx+xj(ij); end;
if xj(ij)>nx; xj(ij)=mod(xj(ij)-1,nx)+1; end; % xj(ij)=xj(ij)-nx; end;
A(zi(ij),xj(ij),y)=0;
end;

end; end;

carb_iter_z=carb_iter_y;


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %% z oriented %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for ran=1:ncarbz

carb_iter_z=carb_iter_z+1,

costheta=costhetaz(nranz(ran));
sintheta=sinthetaz(nranz(ran));
phi=phiz(nranz(ran));
rxy0=abs(r/costheta);

x0=x_ran_z(ran)+0.5;
y0=y_ran_z(ran)+0.5;
z0=z_ran_z(ran)+0.5;
l= lfibe/2;%round(l_ran_x(ran)/2);

z1=ceil(z0-l*abs(costheta));
z2=floor(z0+l*abs(costheta));

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xe=[-r./costheta.*sqrt(1-(yr./r).^2),r./costheta.*sqrt(1-(yr./r).^2)];
R=[cos(phi),-sin(phi);sin(phi),cos(phi)];

[Y,X]=ind2sub([ny,nx],1:ny*nx);

[XYR]=(R)*([xe;ye])+repmat([nx/2+0.5;ny/2+0.5],1,size(xe,2));
ij=find(inhull([Y;X]',[XYR(2,:);XYR(1,:)]'));
j=ij-floor(ij/ny)*ny-ny/2;
i=ceil(ij/ny)-nx/2;

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for z=z0+0.5:z2;
li=(z-z0)/(costheta);

if (z<1); z=mod(z-1,nz)+1; end; % z=nz+z; end;
if (z>nz); z=mod(z-1,nz)+1; end; % z=z-nz; end;
x=floor(li*sintheta*cos(phi)+x0);
y=floor(li*sintheta*sin(phi)+y0);

xi=(i+x);
yj=(j+y);

for ij=1:size(yj);
if yj(ij)<1; yj(ij)=ny+yj(ij); end;
if yj(ij)>ny; yj(ij)=yj(ij)-ny; end;
if xi(ij)<1; xi(ij)=mod(xi(ij)-1,nx)+1; end; % xi(ij)=nx+xi(ij); end;
if xi(ij)>nx; xi(ij)=mod(xi(ij)-1,nx)+1; end; % xi(ij)=xi(ij)-nx; end;
A(z,xi(ij),yj(ij))=0;
end;

end;

for z=z1:z0-0.5;
li=(z0-z)/(costheta);

if (z<1); z=mod(z-1,nz)+1; end; % z=nz+z; end;
if (z>nz); z=mod(z-1,nz)+1; end; % z=z-nz; end;
x=floor(x0-li*sintheta*cos(phi));
y=floor(y0-li*sintheta*sin(phi));

xi=(i+x);
yj=(j+y);

for ij=1:size(yj);
if yj(ij)<1; yj(ij)=mod(yj(ij)-1,ny)+1; end; % yj(ij)=ny+yj(ij); end;
if yj(ij)>ny; yj(ij)=mod(yj(ij)-1,ny)+1; end; %yj(ij)=yj(ij)-ny; end;
if xi(ij)<1; xi(ij)=mod(xi(ij)-1,nx)+1; end; % xi(ij)=nx+xi(ij); end;
if xi(ij)>nx; xi(ij)=mod(xi(ij)-1,nx)+1; end; % xi(ij)=xi(ij)-nx; end;
A(z,xi(ij),yj(ij))=0;
end;

end; end;


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save([ dir '/' spr '_stoca.mat' ], ...
		'A','costheta_ran','phi_ran', ...
		'x_ran_x','x_ran_y','x_ran_z', ...
		'y_ran_x','y_ran_y','y_ran_z', ...
		'z_ran_x','z_ran_y','z_ran_z' );

    %% boundaries
% A([1:wN,nz-wS+1:nz],[1:nx,1:nx],[1:ny,1:ny])=0;
% A([1:nz],[1:nx],[1:wU,ny-wD+1:ny])=1;

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




% % coeff
porosity=sum(sum(sum(A,3),2),1)/(nz*nx*ny),

%B=[ones(size(A,1),buff,size(A,3)),A,ones(size(A,1),buff,size(A,3)).*2];
B=[ones(size(A,1),buff,size(A,3)),A];
%B=permute(B,[2 1 3]);

C=ones(size(B,1),size(B,2),size(B,3));
C(:,1:water,:)=2;

C(B==0)=0;

U=C(:);
save([ dir '/' spr '.bin.txt' ],'-ascii','U');
