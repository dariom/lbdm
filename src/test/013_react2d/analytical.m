NZ=20;
NX=38;

D=0.1;
Ceq=1.0;
C0=10;
KR=[0.01,0.1];

C(1:NZ,1:NX,1)=zeros(NZ,NX,1);

for cases=1:2;
kr=KR(cases);
for n=(1:100);
betan(n)=1;
err=100;
iter=1;
while((err**2)**0.5>10**-4);
iter=iter+1;
betan0=(atan(kr/(D*betan(n)))+pi()*(n-1))/NZ;
err=(betan0-betan(n))/betan(n);
betan(n)=betan0;
end;

Nn2(n)=(NZ/2*(1+(sin(2*betan(n)*NZ)/(2*betan(n)*NZ))));
A(n,:)=(sin(betan(n)*NZ)/(Nn2(n)*betan(n))*1/(cosh(betan(n)*NX))) .*cosh(betan(n).*([1:NX]-NX));
B(n,:)=cos(betan(n).*[0.5:NZ-0.5]);
C(1:NZ,1:NX,n+1) = C(:,:,n) + (A(n,:) .* B(n,:)');
S(1:NZ,1:NX,n)= (C0-Ceq).*C(:,:,n+1)+Ceq;
end;

M=[S(19,:,100)',S(14,:,100)'];
if (kr==0.01);
save('react_th_1.dat','-ascii','M');
else;
save('react_th_2.dat','-ascii','M');
end;

end;
