/* ! ATTENTION do not delete or add strange rows !
Just either comment or uncomment the "Capabilities"
and replace values of "Macros" with the desired ones */

/* ******************* */
/*  Capabilities here  */
/* ******************* */

#define DUMPHDF5
#define INASCII
//#define INHDF5

#define DPDLZ
//#define TWOPHASE
//#define THERMAL
//#define SCALAR
//#define REACTION
//#define MULTICOMP

//#define LBMRT
//#define GRAVITY
//#define NEIGHBOURING
//#define SPATIAL_WETTING
//#define SPATIAL_REACTION
//#define SPATIAL_EQUILIBRIUM
//#define REACTION_BOX
//#define MULTICOMP_GUO
//#define RANDOM_INPUT

//#define NON_NEWTONIAN
//#define HERSCHEL_BULCKLEY
//#define CONJUGATE_ON_REACT
//#define INSULATE_ON_ZERO

//#define SOURCE
//#define SOURCE_HET
//#define CONJUGATE
//#define EXOTHERMIC

//#define REBIRTH
//#define REBIRTH_STEADY
//#define REBIRTH_1PTO2P
//#define REBIRTH_0CTO1C
//#define REBIRTH_0TTO1T

//#define FREESLIP_XP
//#define FREESLIP_XM
//#define FREESLIP_YP
//#define FREESLIP_YM
//#define FREESLIP_ZP
//#define FREESLIP_ZM

//#define DENSITY_XP
//#define DENSITY_XM
//#define DENSITY_YP
//#define DENSITY_YM
//#define DENSITY_ZP
//#define DENSITY_ZM

//#define VELOCITY_XP
//#define VELOCITY_XM
//#define VELOCITY_YP
//#define VELOCITY_YM
//#define VELOCITY_ZP
//#define VELOCITY_ZM

//#define CONCENTRATION_XM
//#define CONCENTRATION_XP
//#define CONCENTRATION_YM
//#define CONCENTRATION_YP
//#define CONCENTRATION_ZM
//#define CONCENTRATION_ZP

//#define CONCENTRATION_EQ_XM
//#define CONCENTRATION_EQ_XP
//#define CONCENTRATION_EQ_YM
//#define CONCENTRATION_EQ_YP
//#define CONCENTRATION_EQ_ZM
//#define CONCENTRATION_EQ_ZP

//#define CONCENTRATION_HALF_XM
//#define CONCENTRATION_HALF_XP
//#define CONCENTRATION_HALF_YM
//#define CONCENTRATION_HALF_YP
//#define CONCENTRATION_HALF_ZM
//#define CONCENTRATION_HALF_ZP

//#define CONCENTRATION_INIT_2X
//#define COPY_SCA_THERM_BCS
//#define CONJUGATE_CORR
//#define EXOTHERMIC_LIN

//#define INNER_TIME_FLU_BCS
//#define INNER_TIME_TEM_BCS
//#define INNER_TIME_CON_BCS

//#define ACOUSTIC_PULSE
//#define ACOUSTIC_FORCEX

//#define ATAN_FORCE

/* ******************* */
/*  Macros for folks   */
/* ******************* */

// Name identifiers
#define NAME "TEST"
#define GEOM "test.txt"

// N-PROC
#define NP 2

// N-SIZE
#define NX 40
#define NY 2
#define NZ 40

// N-PROC PER EDGE
#define NEX 1
#define NEY 1
#define NEZ 2

// INIT DENSITY & dP/dL
#define RHO0 1.0
#define DPDL 0.0001

// Iteration max,chk,out
#define MAX_ITER 3000
#define CHK_ITER 1
#define OUT_ITER 500

// Q-speeds and t-relax
#define NQ 19
#define TAU 1.0

// RESIDUAL convergence
#define RESIDUAL -1
#define CLENGTH 10

// TWOPHASE parameters
#define GG 0.0
#define RHO2 1.0
#define RHOZ 1.0
#define RHOWALL 1.0

// MULTICOMP parameters
#define GR -9.0
#define GG2 0.0
#define RHO02 2.0
#define RHO22 0.1
#define RHOWALL2 0.0
#define TAUM2 1.0

// THERMAL parameters
#define TAUT 1.0
#define TINIT 1.0
#define TWALL 1.0
#define TSUB  1.0

// CONJUGATE parameters
#define TAU2 1.0
#define CP1 2.5
#define CP2 2.5

// SCALAR parameters
#define TAUC 1.0
#define CINIT 1.0
#define CINIT2 0.0
#define CINITL 0.0
#define CISHIFT 0.0

// INLET/OUTLET parameters
#define DIN  1.0234
#define DOUT 1.0
#define VIN  0.012676 
#define VOUT 0.0

// In/Out Scalar
#define CIN 1.0
#define CIN2 0.0
#define COUT 1.0
#define COUT2 0.0

// In/Out Thermal copy from scalar
#define CTIN 1.0
#define CTIN2 0.0
#define CTOUT 1.0
#define CTOUT2 0.0

// Non-Newtonian parameters
#define NN_KAPPA 0.009022
#define NN_YIELD 0.000036
#define NN_EXPON 0.9
#define NN_MTUNE 50000

// WAVE/INPUT parameters
#define AWAVE 0.001
#define FWAVE 4.00
#define RANDFQ 0.5
#define RANDVE 0.01
#define RANDSIZE 6

// REACTION parameters
#define A1 1
#define A2 1
#define A3 1

// REACTIVE box sizes
#define REBOX_XM 0
#define REBOX_XP 4
#define REBOX_YM 0
#define REBOX_YP 2
#define REBOX_ZM 0
#define REBOX_ZP 10

// EXOTHERMIC parameters
#define GAMMA1 1.0
#define GAMMA2 10.0
#define GAMMA3 1.0
#define GAMMA4 0.5
#define SCALE 32.4

// REBIRTH files
#define BIRTHPLACE "dump.BPLACE.h5"
#define BIRTHSTEADY "dump.BSTEADY.h5"
