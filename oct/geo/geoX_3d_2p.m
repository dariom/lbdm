
 function [liq_sol_ijk]=geoS_3d(name,vA)


 pp=1;

nx=160,
ny=160,
nz=160,

%A=load([ 'xtomo/scan-' name '/' name '.mat' ]);
A=dlmread([ '3D_txt/' name '.txt' ],[4096001,1,16384000,1]);
printf([ 'loaded ' name ]),
A=reshape(A,[nz nx ny]);
size(A),

if (vA==2); A=permute(A,[2 3 1]); end;
if (vA==3); A=permute(A,[3 1 2]); end;
   
%% coeff
porosity=sum(sum(sum(A,3),2),1)/(nz*nx*ny),
%pause(),

%figg=figure(1), 
%subplot(1,2,1), 
%isosurface(permute(A,[3,2,1])),
%pbaspect([nx,nz,ny]), view(3),
%camlight;
%xlim([1,nx]);
%ylim([1,nz]);
%zlim([1,ny]);
%subplot(1,2,2), 
%scatter(costheta_ran,phi_ran);
%xlim([0,1]);
%ylim([0,360]);
%
%
% pause(1), close(1),

%for y=1:ny;
%B(:,y,:)=A(:,:,y);
%end

%figure, imshow3D(B)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% indices
[isolid,jsolid,ksolid]=ind2sub(size(A),find(A==0));
[iliquid,jliquid,kliquid]=ind2sub(size(A),find(A==2));
Length=max(length(isolid),length(iliquid));
liq_sol_ijk=zeros(Length,7);
liq_sol_ijk(1:length(isolid),1)=isolid;
liq_sol_ijk(1:length(isolid),2)=jsolid;
liq_sol_ijk(1:length(isolid),3)=ksolid;
liq_sol_ijk(1:length(iliquid),4)=iliquid;
liq_sol_ijk(1:length(iliquid),5)=jliquid;
liq_sol_ijk(1:length(iliquid),6)=kliquid;

liq_sol_ijk(1:4,7)=[nz,nx,ny,porosity]';
