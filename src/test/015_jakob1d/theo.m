clear all
warning('off');

DT=10;
Lt=1000;
Lx=80;

rhl=5.8;
cp=5/2;
alpha=1/6;

jak=load('jakob.dat');

VT=jak(1:Lt/DT+1,1);

kappal=rhl.*cp.*alpha;

%%%%%% (1-2) %%%%%%

for u=1:2;

if (u==1);
j1=jak(1:Lt/DT+1,:);
elseif (u==2);
clear j1
j1=jak(Lt/DT+2:2*Lt/DT+2,:);
end;

Tsat=j1(:,7);
tl=j1(:,8);
tg=j1(:,6);

Twp=0.2-Tsat;
Typ=0.4-Tsat;

Tglp=tg-tl;

Tlp=tl-Tsat;
Tgp=tg-Tsat;


%ie=find(-Tlp<0,1)-1;
int0=[25:Lt/DT];

Tlp(isnan(Tlp))=0;
xx=[0:Lt/DT]';

[m p]=polyfit(xx(int0),log(-Tlp(int0)),1);
Tl=-exp(m(2)).*exp(xx.*m(1));

[m p]=polyfit(xx(int0),log(-Tgp(int0)),1);
Tg=-exp(m(2)).*exp(xx.*m(1));

[m p]=polyfit(xx(int0),log(-Twp(int0)),1);
Tw=-exp(m(2)).*exp(xx.*m(1));

[m p]=polyfit(xx(int0),log(Typ(int0)),1);
Ty=exp(m(2)).*exp(xx.*m(1));

[m p]=polyfit(xx(int0),log(Tglp(int0)),1);
Tgl=exp(m(2)).*exp(xx.*m(1));

%% chose power law
pow=1/2;

Xi=j1(:,2)-2;
Nl=j1(:,9);


jj=0; val=0;
for j=1:size(Xi,1);
  if (find(Xi>val));
  jj=jj+1;
  idx=find(Xi>val,1);
  Xj(jj)=Xi(idx);
  VJ(jj)=VT(idx);
  INT(jj)=idx;
  val=val+1;
  end;
end;

m(1)=sum(Xj./VJ.**0.5)./size(Xj,2);
ti=m(1).*VT.**(pow);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%int1=[2:Lt/DT/10];
%int2=[Lt/DT/10+1:Lt/DT+1];
%
%Qwall=j1(:,11).*kappal;
%xx=[0:Lt/DT]';
%[m p]=polyfit(log(xx(int1)),log(Qwall(int1)),1);
%Qw1=exp(m(2)).*xx.**m(1);
%
%%[m p]=polyfit(xx(int2),log(Qwall(int2)),1);
%%Qw2=exp(m(2)).*exp(xx.*m(1));
%%Qw(2:Lt/DT+1,1)=[Qw1(int1);Qw2(int2)];
%
%Qw=Qw1;
%
%Qw(1)=0;
%Qwall(1)=0;
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cp=5/2;
lambdavar=(-j1(2:end,5)-j1(2:end,4));
lambdanum=mean(-j1(2:end,5)+j1(2:end,4)),

lambda = 0.68;

for n=2:size(j1,1);

xt2(n)=(2.*alpha.*sum(-Tw(2:n),1).*DT.*(cp./lambda)./(1-((Tl(n)).*cp/lambda))).**0.5;
xt20(n)=(2.*alpha.*sum(-Tw(2:n),1).*DT.*(cp./lambda)).**0.5;

end;

%%% rooot %%%%%%%%%%%%%%%%%%

TW0=0.2;
TY0=0.4;
TM0=mean(Tsat);

it=0;
kki=lambda;
err=1;
while(~(err<0.001));
it=it+1;
kku= 1/sqrt(pi())* ( cp*(TM0-TW0)/(lambda*exp(kki**2)*erf(kki)) - cp*(TY0-TM0)/(lambda*exp(kki**2)*erfc(kki)) );
err=abs((kku-kki)/kki);
kki=kku;
if (it>200);
break;
end;
end;

xt4=-2.*kku.*sqrt(alpha.*VT);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%

M=[VT,ti,xt20'];
save('thickness.dat','-ascii','-append','M');

M=[VJ',Xj'];
save('thickness-pt.dat','-ascii','-append','M');

ERR=(xt20(INT)'-ti(INT))./(xt20(INT)');
save('error.dat','-ascii','-append','ERR');

end;
