dpl=-1e-6;


H=29*2;
d=29+2.5; % +2.5 because away from interface 
mu1=0.1/6;
mu2=2.3/6;

y=[-d+0.5:(H-d)];

% 0.5 prefactor necessary if you check Poiseuille
M=mu1/mu2;
u1=0.5/mu1*dpl.*(y.**2-y.*((H-d)**2-M*d**2)/((H-d)+M*d)-d*(H-d)*H*M/((H-d)+M*d));
u2=0.5/mu2*dpl.*(y.**2-y.*((H-d)**2-M*d**2)/((H-d)+M*d)-d*(H-d)*H/((H-d)+M*d));

U1=-dpl*H**2/12/mu1;

load('dump.CONV.h5');
ux=LBdm.ux;

figure(1),
hold on, plot(y./H,u2./U1,'-');
hold on, plot(y./H,u1./U1,'-');
ylim([0,0.5]);

hold on, plot(y./H,ux(2:end-1,1,1)./U1,'o');
saveas(gcf,'velocity.png','png');

yc=find(u2<u1,1);

y2=1:yc-1;
y1=yc:size(u1,2);


A=[u2(y2)';u1(y1)']./U1;
save('ux_profile.dat','-ascii','A');

A=[ux(2:end-1,1,1)]./U1;
save('ux_profile_lbdm.dat','-ascii','A');
