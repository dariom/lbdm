/* ! ATTENTION do not delete or add strange rows !
Just either comment or uncomment the "Capabilities" 
and replace values of "Macros" with the desired ones */

/* ******************* */
/*  Capabilities here  */
/* ******************* */

#define DPDLX

#define DUMPHDF5
#define INASCII

//#define LBMRT
//#define TWOPHASE
//#define THERMAL

//#define FREESLIP_XP
//#define FREESLIP_XM
//#define FREESLIP_YP
//#define FREESLIP_YM
//#define FREESLIP_ZP
#define FREESLIP_ZM

//#define GRAVITY

/* ******************* */
/*  Macros for folks   */
/* ******************* */

// Name identifiers
#define NAME "FREESLIP2D"
#define GEOM "fs2d.bin.txt"

// N-PROC
#define NP 2

// N-SIZE
#define NX 4
#define NY 2
#define NZ 20

// N-PROC PER EDGE
#define NEX 1
#define NEY 1
#define NEZ 2

// INIT DENSITY & dP/dL
#define RHO0 1.0
#define DPDL 0.00005

// Iteration max,chk,out
#define MAX_ITER 5000
#define CHK_ITER 1
#define OUT_ITER 200

// Q-speeds and t-relax
#define NQ 19
#define TAU 1.0

// RESIDUAL convergence
#define RESIDUAL 0.00001
#define CLENGTH 36

// TWOPHASE parameters
#define GG 0.0
#define RHO2 1.0
#define RHOZ 1.0
#define RHOWALL 1.0

// THERMAL parameters
#define TAUT 1.0    	
#define TINIT 1.0
#define TWALL 1.0
#define TSUB  1.0
