#!/bin/bash -e

echo " "
echo "#lbdm# Test 014 Thermal Poiseuille 2d - Nusselt"
echo " "

git checkout user-defined.c

if [ -f "*.bin.txt" ]; then
rm *.bin.txt
fi

if [ -f "dump.CONV.h5" ]; then
rm dump.CONV.h5
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

if [ -f "nu_lbdm.dat" ]; then
rm nu_lbdm.dat
fi

if [ -f "nusselt.png" ]; then
rm nusselt.png
fi

octave create_bin-txt.m
cp thpo2d.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..

make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/014_thpo2d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave nu_h5_thpo2d.m
rm dump.CONV.h5

###########

sed -i -e 's/define TAUT .*/define TAUT 0.7/g' user-defined.c

cp user-defined.c ../../user/
cd ../..

make superclean
make init
make 

if (mpirun -np 2 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/014_thpo2d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave nu_h5_thpo2d.m

awk 'NR>20&&NR<=40 {print ($1/7.54-1)}' nu_lbdm.dat > error.dat
awk 'NR>60&&NR<=80 {print ($1/7.54-1)}' nu_lbdm.dat >> error.dat
awk 'NR<=80 {if (($1*$1)^0.5<=0.05) print "ok"; else print "no" }' error.dat > errchk.dat

octave plot_nu.m

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/thpo2d.bin.txt

# clean local test
cd test/014_thpo2d/
git checkout user-defined.c
rm *.txt

########### 1) Check NaN 

for file in nu_lbdm.dat error.dat error_mrt.dat errchk.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 40 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
