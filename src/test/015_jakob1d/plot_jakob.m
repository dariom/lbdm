Lt=1000;
DT=10;

jak=load('jakob.dat');
th=load('thickness.dat');
thp=load('thickness-pt.dat');

ns=[0.077,0.107];

j1=jak(1:Lt/DT+1,:);
j2=jak(Lt/DT+2:2*Lt/DT+2,:);

VT=[0:DT:Lt];

ti1=th(1:Lt/DT+1,2);
vi1=th(1:Lt/DT+1,1);
xt21=th(1:Lt/DT+1,3);

vj1=thp(1:(end/2),1);
xj1=thp(1:(end/2),2);

ti2=th(Lt/DT+2:2*Lt/DT+2,2);
vi2=th(Lt/DT+2:2*Lt/DT+2,1);
xt22=th(Lt/DT+2:2*Lt/DT+2,3);

vj2=thp((end/2)+1:end,1);
xj2=thp((end/2)+1:end,2);

figure(1),
hold on, plot(VT,ti1,'--','color','b');
hold on, plot(VT,xt21,'-','color','r');
hold on, plot(vj1,xj1,'.','color','m');
xlim([0,1000])
ylim([0,20])
title('cond. (1) front thickness latent 0.68');
saveas(gcf,'front.1.png','png');

figure(2),
hold on, plot(VT,ti2,'--','color','b');
hold on, plot(VT,xt22,'-','color','r');
hold on, plot(vj2,xj2,'.','color','m');
xlim([0,1000])
ylim([0,20])
title('cond. (2) front thickness latent 0.68');
saveas(gcf,'front.2.png','png');
