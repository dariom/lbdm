for i=0:8:120;
load(sprintf('../../out/dump.REB1.%05d.h5',i));
if (i==0);
sol=double(LBdm.sol);
end;
ux=LBdm.ux;
tem=LBdm.tem;
ut(i/8+1)=sum(ux(:))./sum(sol(:));
tt(i/8+1)=sum(tem(:).*sol(:))./sum(sol(:));
end;

M=[ut(11:end)',tt(11:end)'];
save('data1.dat','-ascii','-append','M');
