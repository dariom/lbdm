for i=0:8:120;
load(sprintf('../../out/dump.REB3.%05d.h5',i));
if (i==0);
sol=double(LBdm.sol);
end;
con=LBdm.con;
tem=LBdm.tem;
ct(i/8+1)=sum(con(:))./sum(sol(:));
tt(i/8+1)=sum(tem(:).*sol(:))./sum(sol(:));
end;

M=[ct(11:end)',tt(11:end)'];
save('data1.dat','-ascii','-append','M');
