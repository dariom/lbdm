#include <stdio.h>
#include <mpi.h>
#include <math.h>
#include "mpi_open.h" 
#include "../var/shared.h"

int mpi_init(int *procoffz, int *procoffx, int *procoffy) {

    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Get the name of the processor
    char proc_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(proc_name, &name_len);

    // Print off a hello world message
    fprintf(stderr,"II: Proc %s, rank %d"
           " out of %d processors\n",
           proc_name, world_rank, world_size);

    // More definitions
    lx = NX/(int)NEX;
    ly = NY/(int)NEY;
    lz = NZ/(int)NEZ;

    lxp = lx+2;
    lyp = ly+2;
    lzp = lz+2;

    NXP = NX+2;
    NYP = NY+2;
    NZP = NZ+2;

    // Offsets
    int iproc = 0;

      for (iproc = 0; iproc < world_size; iproc++) {
      procoffz[iproc] = (int) lz*iproc - (int)(NZ*floor(iproc*lz/NZ));
      procoffx[iproc] = (int) lx*floor(iproc*lz/NZ) - (int)(NX*floor(iproc*lz/NZ*lx/NX));
      procoffy[iproc] = (int) ly*floor(iproc*lz/NZ*lx/NX) - (int)(NY*floor(iproc*lz/NZ*lx/NX*ly/NY));

        if (iproc==(int)world_rank) {
        fprintf(stderr,"II: Proc num: %d | off.z: %d off.x: %d off.y: %d\n",iproc,procoffz[iproc],procoffx[iproc],procoffy[iproc]);
	}
      }


    // Cartesian Grid
    int dims[3],periods[3],coords[3];
    dims[0] = (int)NEY; 
    dims[1] = (int)NEX; 
    dims[2] = (int)NEZ;
    periods[0] = 1;
    periods[1] = 1;
    periods[2] = 1;
    MPI_Dims_create (NP,3,dims);
    MPI_Cart_create (MPI_COMM_WORLD,3,dims,periods,0,&mpi_comm_cart);

    // Sub-communicators along X,Y,Z
    coords[0] = 1;
    coords[1] = 0;
    coords[2] = 0;
    MPI_Cart_sub (mpi_comm_cart, coords, &mpi_comm_y);
    coords[0] = 0;
    coords[1] = 1;
    coords[2] = 0;
    MPI_Cart_sub (mpi_comm_cart, coords, &mpi_comm_x);
    coords[0] = 0;
    coords[1] = 0;
    coords[2] = 1;
    MPI_Cart_sub (mpi_comm_cart, coords, &mpi_comm_z);

    MPI_Comm_rank (mpi_comm_z, &prz);
    MPI_Comm_rank (mpi_comm_x, &prx);
    MPI_Comm_rank (mpi_comm_y, &pry);

    MPI_Cart_shift (mpi_comm_x, 0, 1, &prxm, &prxp);
    MPI_Cart_shift (mpi_comm_y, 0, 1, &prym, &pryp);
    MPI_Cart_shift (mpi_comm_z, 0, 1, &przm, &przp);

    // MPI SendRecv Planes & info *dfdq

    /* int MPI_Type_contiguous(int count, MPI_Datatype oldtype,
    MPI_Datatype *newtype) */
    MPI_Type_contiguous (NQ, MPI_DOUBLE, &mpi_dfplane);
    MPI_Type_commit (&mpi_dfplane);

    /* int MPI_Type_vector(int count, int blocklength, int stride,
    MPI_Datatype oldtype, MPI_Datatype *newtype) */

    MPI_Type_vector (lx, lz, lzp, mpi_dfplane, &mpi_dfplane_y);
    MPI_Type_commit (&mpi_dfplane_y);

    MPI_Type_vector (lyp, lz, lzp*lxp, mpi_dfplane, &mpi_dfplane_x);
    MPI_Type_commit (&mpi_dfplane_x);

    MPI_Type_vector (lxp*lyp, 1, lzp, mpi_dfplane, &mpi_dfplane_z);
    MPI_Type_commit (&mpi_dfplane_z);

    // MPI SendRecv Planes *int
    MPI_Type_contiguous (1, MPI_INT, &mpi_intplane);
    MPI_Type_commit (&mpi_intplane);

    MPI_Type_vector (lx, lz, lzp, mpi_intplane, &mpi_intplane_y);
    MPI_Type_commit (&mpi_intplane_y);

    MPI_Type_vector (lyp, lz, lzp*lxp, mpi_intplane, &mpi_intplane_x);
    MPI_Type_commit (&mpi_intplane_x);

    MPI_Type_vector (lxp*lyp, 1, lzp, mpi_intplane, &mpi_intplane_z);
    MPI_Type_commit (&mpi_intplane_z);

    // MPI SendRecv Planes *macro
    MPI_Type_contiguous (1, MPI_DOUBLE, &mpi_maplane);
    MPI_Type_commit (&mpi_maplane);

    MPI_Type_vector (lx, lz, lzp, mpi_maplane, &mpi_maplane_y);
    MPI_Type_commit (&mpi_maplane_y);

    MPI_Type_vector (lyp, lz, lzp*lxp, mpi_maplane, &mpi_maplane_x);
    MPI_Type_commit (&mpi_maplane_x);

    MPI_Type_vector (lxp*lyp, 1, lzp, mpi_maplane, &mpi_maplane_z);
    MPI_Type_commit (&mpi_maplane_z);

}

int mpi_finalize() {

    // Finalize the MPI environment.
    MPI_Finalize();

}
