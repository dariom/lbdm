#ifndef GEOMETRY_H_
#define GEOMETRY_H_

int geometry_from_file();
int geometry_mpi();
int geometry_slip();

#endif
