R=[4];
LY=10;
LZ=10;

[xx yy zz]=meshgrid(1:80,1:LZ,1:LY);
S=(sqrt((yy-LY/2-0.5).**2+(zz-LZ/2-0.5).**2)<R);
A=int32(S);
A(:,1:15,:)=1;
A(:,16:40,:)=2;
U=uint8(A(:));
name=sprintf('wash3d.bin.txt');
dlmwrite(name,U);
