#!/bin/bash -e

echo " "
echo "#lbdm# Test 007 Thermal-Young-Laplace 3d"
echo " "

git checkout user-defined.c

if [ -f "dp-radius.dat" ]; then
rm dp-radius.dat 
fi

if [ -f "plot_dp-ra.png" ]; then
rm plot_dp-ra.png 
fi

if [ -f "dump.CONV.h5" ]; then
rm dump.CONV.h5
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

octave create_bin-txt.m

cp yl3d.1.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/007_thyoulap3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_yl3d_1.m

cp dump.CONV.h5 dump.1.h5

###########

cp yl3d.2.bin.txt ../../in/

sed -i -e 's/define NAME .*/define NAME "THYL3D2"/g' user-defined.c
sed -i -e 's/define GEOM .*/define GEOM "yl3d.2.bin.txt"/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/007_thyoulap3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_yl3d_2.m

cp dump.CONV.h5 dump.2.h5

###########

cp yl3d.3.bin.txt ../../in/

sed -i -e 's/define NAME .*/define NAME "THYL3D3"/g' user-defined.c
sed -i -e 's/define GEOM .*/define GEOM "yl3d.3.bin.txt"/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (3) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/007_thyoulap3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_yl3d_3.m

cp dump.CONV.h5 dump.3.h5

###########

gamma=`awk 'NR==1 {printf ($2/$1) }' dp-radius.dat`
echo ""
echo "Surface tension: " $gamma
echo ""

octave plot_dp-ra.m

awk -v g=$gamma 'NR<=3 {print (1-$2/($1*g))}' dp-radius.dat >> error.dat
awk 'NR<=3 {if (($1*$1)^0.5<=0.025) print "ok"; else print "no" }' error.dat > errchk.dat

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/yl3d.*.bin.txt

# clean local test
cd test/007_thyoulap3d/
git checkout user-defined.c
rm *.txt

########### 1) Check NaN 

for file in dp-radius.dat error.dat errchk.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 3 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
