    dir= '../src/out/SCAN33/'
    dirout= '../../f90_LPT/geo/'
    name='SCAN33.00770'
    t=[ 2 0 2 1 ];

    buffer=271;

    tau=1

    nu=1/3.*(tau-0.5);
    df=6;
    
for i=1:size(t,1)
    
    p=t(i,1); % e.g. porosity
    o=t(i,2); % e.g. main orientation
    r=t(i,3); % e.g. reynolds number
    v=t(i,4); % e.g. versor/direction 
    
    nameload=sprintf([ dir 'dump.' name '.h5' ]);
    nametxt=sprintf( '%d%d%d%d.txt',p,o,r,v);
    nametxtav=sprintf( '%d%d%d%d_av.txt',p,o,r,v);
    namestr=sprintf( '%d%d%d%d',p,o,r,v)
    
    load(nameload); %,'ux','uy','uz','Channel3D');
    ux=LBdm.ux;    
    uy=LBdm.uy;    
    uz=LBdm.uz;    
    Channel3D=double(LBdm.sol);    

    % freslip
    Channel3D([1:end],:,1)=0.0;
    Channel3D([1:end],:,end)=0.0;
    Channel3D(1,:,[1:end])=0.0;
    Channel3D(end,:,[1:end])=0.0;

    uxt=ux(:);
    uyt=uy(:);
    uzt=uz(:);
    cht=Channel3D(:);
    
    save([ dirout nametxt ],'-ascii','uxt','uyt','uzt','cht')
    
    
    % av values
    porosity=sum(sum(sum(Channel3D(2:end-1,buffer+1:end-buffer,2:end-1),3),2),1)/ ... 
        ((size(Channel3D,3)-2)*(size(Channel3D,2)-2*buffer)*(size(Channel3D,1)-2)),
    nu, df,
    av_x=mean(mean(mean(ux(2:end-1,buffer+1:end-buffer,2:end-1))))./porosity,
    av_y=mean(mean(mean(uy(2:end-1,buffer+1:end-buffer,2:end-1))))./porosity,
    av_z=mean(mean(mean(uz(2:end-1,buffer+1:end-buffer,2:end-1))))./porosity,
    re_x=av_x*df/nu,
    re_p=re_x/(1-porosity)
    
    M=[re_p,re_x,porosity,nu,df,av_x,av_y,av_z];
    
    
%     save([ '3D_txt/' nametxtav ],'-ascii','p','o','r','v', ... 
%         'porosity','nu','df','av_x','av_y','av_z','re_x');
    
    dlmwrite([ dirout nametxtav ],M,'delimiter',' ');
end

    
