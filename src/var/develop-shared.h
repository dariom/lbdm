/* ******************* */
/* Vars later defined  */
/* ******************* */

extern int var_init();
extern int twophase_init();
extern int chk_mrt_matrix();
extern int print_defs();

extern double CS2;

extern int NXP;
extern int NYP;
extern int NZP;

extern int lx;
extern int ly;
extern int lz;

extern int lxp;
extern int lyp;
extern int lzp;

extern double init_density;
extern double dpdl;

#ifdef LBMRT
extern double NUETA;
#endif

#ifdef TWOBELT
extern int lx2p;
extern int ly2p;
extern int lz2p;

extern int NX2P;
extern int NY2P;
extern int NZ2P;
#endif

#ifdef RANDOM_INPUT
extern int h;
extern float *randin;
#endif

extern int world_size;
extern int world_rank;

extern int *procoffx;
extern int *procoffy;
extern int *procoffz;

extern MPI_Datatype mpi_dfplane;
extern MPI_Datatype mpi_dfplane_x;
extern MPI_Datatype mpi_dfplane_y;
extern MPI_Datatype mpi_dfplane_z;

extern MPI_Datatype mpi_intplane;
extern MPI_Datatype mpi_intplane_x;
extern MPI_Datatype mpi_intplane_y;
extern MPI_Datatype mpi_intplane_z;

extern MPI_Datatype mpi_maplane;
extern MPI_Datatype mpi_maplane_x;
extern MPI_Datatype mpi_maplane_y;
extern MPI_Datatype mpi_maplane_z;

extern MPI_Comm mpi_comm_cart;
extern MPI_Comm mpi_comm_x;
extern MPI_Comm mpi_comm_y;
extern MPI_Comm mpi_comm_z;

extern int *dims, *coords, *periods;

extern int prx, pry, prz;
extern int prxm, prym, przm;
extern int prxp, pryp, przp;

extern double *w;
extern int *cx, *cy, *cz, *iq, *vq;
extern int *qsx, *qsy, *qsz;
extern int *iqsx, *iqsy, *iqsz;

extern int iter;
extern double *dpdlv;
extern double *wavev;

extern double retmp;
extern double *RECONV;
extern int CONVFLAG;
extern int CONVFLAGTP;
extern int ITERCONVTP;

/* ******************* */
/* Submacros for nerds */
/* ******************* */

# define ROOT(i) (i==0)

# define IDX( i, j, k ) ( (k) + (lz) * ( (i) + (lx) * (j) ) )
# define IDXP( i, j, k ) ( (k) + (lzp) * ( (i) + (lxp) * (j) ) )
# define IDXG( i, j, k ) ( (k) + (NZ) * ( (i) + (NX) * (j) ) )
# define IDXGP( i, j, k ) ( (k) + (NZP) * ( (i) + (NXP) * (j) ) )

typedef struct {
  double dq[NQ];
} dfdq;

typedef double macro;

typedef struct {
 int dq[NQ];
} I19;

typedef struct {
 double dq[NQ];
} D19;

typedef struct {
 double eq[10];
} D10;

extern I19 *MRT;
extern D10 *EQ;
extern I19 *EYE;
extern D19 *S;
extern D19 *iMRT;
