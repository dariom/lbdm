#!/bin/bash

if [ $# -eq 0 ]; then
echo "No argument supplied, which test?"
else

  if [ $1 = "all" ]; then
  echo "cleaning ALL tests"

  declare -a tes=( 	"000_po2d/"
  			"001_scps3d/"
			"002_youlap3d/"
			"003_rndmom3d/"
			"004_fslip3d/"
			"005_washb3d/"
			"006_settl3d/"
			"007_thyoulap3d"
			"009_bcs2d/"
			"010_trans2d/"
			"011_entropy1d/"
			"012_sca2d/"
			"013_react2d/"
			"014_thpo2d/"
			"015_jakob1d/"
			"016_rebirth2d/"
			"017_nnew2d/"
			"018_po2p2d/"
			"019_conj1d/"
		 )

  for t in "${tes[@]}"
    do

    echo "cleaning test # $t"

    if [[ $(git ls-files --others --exclude-standard $t) ]]; then
    git ls-files --others --exclude-standard $t | xargs rm
    else 
    echo "test directory already cleaned"
    fi

  done

  else 	

  echo "cleaning test # $1"

    if [[ $(git ls-files --others --exclude-standard $1) ]]; then
    git ls-files --others --exclude-standard $1 | xargs rm
    else
    echo "test directory already cleaned"
    fi

  fi
fi 



