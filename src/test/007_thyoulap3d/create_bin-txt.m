R=[9,10,11];

for i=1:3;
[xx yy zz]=meshgrid(1:40,1:40,1:40);
S=sqrt((xx-20.5).**2+(yy-20.5).**2+(zz-20.5).**2)<R(i);
A=int32(S);
A=A+1;
U=uint8(A(:));
name=sprintf('yl3d.%d.bin.txt',i);
dlmwrite(name,U);
end;
