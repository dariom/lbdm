#!/bin/bash

if [ $# -eq 0 ]; then
echo "No argument supplied, which test?"
else

  if [ $1 = "all" ]; then
  echo "checking ALL tests"

  declare -a tes=( 	"000_po2d/"
  			"001_scps3d/"
			"002_youlap3d/"
			"003_rndmom3d/"
			"004_fslip3d/"
			"005_washb3d/"
			"006_settl3d/"
			"007_thyoulap3d"
			"009_bcs2d/"
			"010_trans2d/"
			"011_entropy1d/"
			"012_sca2d/"
			"013_react2d/"
			"014_thpo2d/"
			"015_jakob1d/"
			"016_rebirth2d/"
			"017_nnew2d/"
			"018_po2p2d/"
			"019_conj1d/"
		 )

  CHEQ=0;

    for t in "${tes[@]}"
    do

    echo "checking test # $t"

      if [[ $(ls "${t}PASSED") ]]; then

      echo "test PASSED"
      CHEQ=$[CHEQ + 1]

      else

      echo "test NO-PASSED-APPROVAL"

      fi

    done

  else

  echo "checking test # $1"

    if [[ $(ls "${1}PASSED") ]]; then

    echo "test PASSED"

    else

    echo "ERROR: test NO-PASSED-APPROVAL"
    exit 1

    fi

  fi

  if [ $1 = "all" ]; then

    if [ $CHEQ -eq ${#tes[*]} ]; then

    echo "ALL tests PASSED! Jättebra!"

    else 

    echo "ERROR: some test has not PASSED"
    exit 1

    fi

  fi

fi  
