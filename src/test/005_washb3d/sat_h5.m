%close all
clear all

itmax=4000;
itout=100;

ang=72;
thr=1.3;

rhog=0.12;
rhol=2.4;
rhow=1.4; %deprecated

nu=1/6;

LX=80;
LZ=10;
LY=10;
LW=40;
RW=4;

gamma=0.1;
%ang=(rhow-rhol)/(rhog-rhol)*180,
ugamma=gamma*cos(pi()/180*ang)*2*RW/(nu*rhol*LW);

load('dump.ZERO.h5');
sol=double(LBdm.sol);

for i=1:itmax/itout+1;

name=sprintf('../../out/dump.WASH3D.%05d.h5',(i-1)*itout);
load(name);

rho=LBdm.rho;
sat(i)=sum(sum(sum(sol(:,end-LW+1:end,:).*(rho(:,end-LW+1:end,:)>thr),3),1),2)/sum(sum(sum(sol(:,end-LW+1:end,:),3),1),2);
h(i)=find(rho(LZ/2,:,LY/2)>thr,1,'last')-LW;
tph(i)=(i-1)*itout*ugamma/LW;

end;

M=[tph',sat'];
save('sat_lb.dat','-ascii','M');

yy=0.5.*tph.**0.5;
N=[tph',yy'];
save('sat_th.dat','-ascii','N');

