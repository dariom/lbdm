#ifndef MACRO_H_
#define MACRO_H_

int macro_nse();
int macro_nse_zero();
int macro_nse_rebirth();
int macro_twophase();
int periodic_macro();
int periodic_int();
int macro_forcing();
int macro_nse_inc();
int macro_nse_clean();
int macro_nse_dump();
int macro_nse_chk();
int macro_twophase_neigh();
int macro_twophase_inout();
int macro_twophase_slip();
int macro_mem_inner_bcs();
int macro_init_inner_bcs();
int macro_shear_rate();
int macro_exo_react();
int macro_exo_dump();
int macro_source();

#endif
