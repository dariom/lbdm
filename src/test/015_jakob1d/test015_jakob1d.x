#!/bin/bash -e

echo " "
echo "#lbdm# Test 015 Jakob number 1D"
echo " "

git checkout user-defined.c

if [ -f "jakob.dat" ]; then
rm jakob.dat 
fi

if [ -f "thickness.dat" ]; then
rm thickness.dat 
fi

if [ -f "thickness-pt.dat" ]; then
rm thickness-pt.dat 
fi

if [ -f "front.1.png" ]; then
rm front.1.png 
fi

if [ -f "front.2.png" ]; then
rm front.2.png 
fi

if [ -f "dump.CONV.h5" ]; then
rm dump.CONV.h5
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

octave create_bin-txt.m

cp jakob1d.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/015_jakob1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave jakob_h5.m

###########

sed -i -e 's/define RHO0 .*/define RHO0 2.75/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/015_jakob1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave jakob_h5.m

###########

octave theo.m
octave plot_jakob.m

awk 'NR<=30 {if (($1*$1)^0.5<=0.03) print "ok"; else print "no" }' error.dat > errchk.dat

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/jakob1d.bin.txt

# clean local test
cd test/015_jakob1d/
git checkout user-defined.c
rm *.txt

########### 1) Check NaN 

for file in thickness.dat error.dat errchk.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 30 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
