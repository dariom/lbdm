#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <stddef.h>
#include "micro.h"
#include "../var/shared.h"

#if (defined THERMAL && defined SCALAR && defined CONJUGATE_ON_REACT)
int micro_lbe_init(int *lsol, dfdq *f, dfdq *t, dfdq *c, macro *ux, macro *uy, macro *uz, macro *rho, macro *tem, macro *con, macro *lwet) {

#elif (defined THERMAL && defined SCALAR && !defined CONJUGATE_ON_REACT)
int micro_lbe_init(int *lsol, dfdq *f, dfdq *t, dfdq *c, macro *ux, macro *uy, macro *uz, macro *rho, macro *tem, macro *con) {

#elif (defined THERMAL || defined SCALAR)
int micro_lbe_init(int *lsol, dfdq *f, dfdq *t, macro *ux, macro *uy, macro *uz, macro *rho, macro *tem) {

#else
int micro_lbe_init(int *lsol, dfdq *f, macro *ux, macro *uy, macro *uz, macro *rho, int F) {
#endif

  #ifdef MULTICOMP
  if (F==1) {
  #endif

  if (ROOT(world_rank))
  fprintf(stderr,"II: Init microscopic quantities Lattice-Boltzmann\n");

  #ifdef MULTICOMP
  }
  #endif

  int idx=0, i=0, j=0, k=0, q=0, var=0;
  double origin_eqf=0., origin_eqt=0., origin_eqc;
  double origin_rho=0., origin_tem=0., origin_con=0.;
  double cinit=0.0, tinit=0.0;

  for (j=0; j<lyp; j++) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idx = IDXP(i,j,k);

	#if (!defined CONJUGATE)
        if (lsol[idx] == 1) {
	#elif (defined CONJUGATE && !defined CONJUGATE_ON_REACT)
        if (lsol[idx] == 1 || lsol[idx] == 0) {
	#elif (defined CONJUGATE && defined CONJUGATE_ON_REACT)
        if (lsol[idx] == 1 || lwet[idx] != 0.) {
	#endif

	// Definitions init
	#ifdef SCALAR

	#ifdef CONJUGATE
	if (lsol[idx] == 0) {
	cinit = 0.; } else {
	#endif

	cinit = (double)CINIT;

	#ifdef CONJUGATE
	}
	#endif

	#endif

	#ifdef THERMAL
	tinit = (double)TINIT;
	#endif

	//Init with double concentration
	#if (defined CONCENTRATION_INIT_2X || defined CONCENTRATION_INIT_2Y || defined CONCENTRATION_INIT_2Z)

	  #ifdef CONCENTRATION_INIT_2X
	  if ((i + (int)procoffx[world_rank] <= (int)(CINITL+CISHIFT)) && \
	      (i + (int)procoffx[world_rank] > (int)(CISHIFT))) {
	  #elif (defined CONCENTRATION_INIT_2Y)
	  if ((j + (int)procoffy[world_rank] <= (int)(CINITL+CISHIFT)) && \
	      (j + (int)procoffy[world_rank] > (int)(CISHIFT))) {
	  #elif (defined CONCENTRATION_INIT_2Z)
	  if ((k + (int)procoffz[world_rank] <= (int)(CINITL+CISHIFT)) && \
	      (k + (int)procoffz[world_rank] > (int)(CISHIFT))) {
	  #endif

	  #if (defined CONCENTRATION_INIT_2X && defined RANDOM_INPUT)
	  h = (int)(((CINITL+CISHIFT)-(i+(int)procoffx[world_rank]))*RANDFQ);
	  #elif (defined CONCENTRATION_INIT_2Y && defined RANDOM_INPUT)
	  h = (int)(((CINITL+CISHIFT)-(j+(int)procoffy[world_rank]))*RANDFQ);
	  #elif (defined CONCENTRATION_INIT_2Z && defined RANDOM_INPUT)
	  h = (int)(((CINITL+CISHIFT)-(k+(int)procoffz[world_rank]))*RANDFQ);
	  #endif

	  #ifdef RANDOM_INPUT
	  cinit = (double)randin[h];
	  #else
	  cinit = (double)CINIT2;
	  #endif
	  }
	#endif

	#ifdef REBIRTH

	  // TWOPHASE code: From one-phase to two-phase
	  #ifdef REBIRTH_1PTO2P
	  origin_rho = (double)RHO0;
	  #else
	  origin_rho = (double)rho[idx];
	  #endif

	  // SCALAR code: From zero-con to one-con
	  #ifdef REBIRTH_0CTO1C
	  origin_con = (double)(cinit * rho[idx]);
	  #else

	    #if (defined THERMAL && defined SCALAR)
	    origin_con = (double)(con[idx] * rho[idx]);

	    #elif (defined THERMAL || defined SCALAR)
	    origin_con = (double)(tem[idx] * rho[idx]);
	    #endif

	  #endif

	  // THERMAL code: From zero-tem to one-tem
	  #ifdef REBIRTH_0TTO1T

	    origin_tem = tinit * (double)(rho[idx]*(double)lsol[idx]+RHO0*(double)(1-lsol[idx]));

	  #else

	    #ifdef THERMAL

	      #ifndef TWOPHASE
	      origin_tem = (double)tem[idx] * (double)(rho[idx]*(double)lsol[idx]+RHO0*(1-lsol[idx]));
	      #else
	      origin_tem = tem[idx];
	      #endif

	    #else
	    origin_tem = tem[idx] * rho[idx];
            #endif

	  #endif

	#endif

          for (q=0; q<NQ; q++) {

	  #ifdef REBIRTH

	  #ifndef REBIRTH_STEADY
	  // Fluid: From equilibrium distribution
	  origin_eqf = origin_rho *w[q] * (1. +\
	  (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
	  (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
	  (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));
	  #endif

	    #ifdef THERMAL
	    // Temperature: From equilibrium distribution
	    origin_eqt = origin_tem *w[q] * (1. +\
	    (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
	    (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
	    (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));
	    #endif

	    #ifdef SCALAR
	    // Scalar: From equilibrium distribution
	    origin_eqc = origin_con *w[q] * (1. +\
	    (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
	    (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
	    (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));
	    #endif

	  #endif

          #if (defined DENSITY_XM && defined DENSITY_XP && !defined TWOPHASE)
          f[idx].dq[q] = (double)( (DOUT+(DIN-DOUT)*((double)(NX-i-procoffx[world_rank])+0.5)/(double)NX) * w[q] );
          #elif (defined DENSITY_YM && defined DENSITY_YP && !defined TWOPHASE)
          f[idx].dq[q] = (double)( (DOUT+(DIN-DOUT)*((double)(NY-j-procoffy[world_rank])+0.5)/(double)NY) * w[q] );
          #elif (defined DENSITY_ZM && defined DENSITY_ZP && !defined TWOPHASE)
          f[idx].dq[q] = (double)( (DOUT+(DIN-DOUT)*((double)(NZ-k-procoffz[world_rank])+0.5)/(double)NZ) * w[q] );

          #else

	    #ifndef REBIRTH_STEADY

	      #ifdef REBIRTH
	      f[idx].dq[q] = (double)( origin_eqf );
	      #else

	        #ifdef MULTICOMP
	        if (F == 2) {
                f[idx].dq[q] = (double)( RHO02 * w[q] );
	        } else {
	        #endif

		#ifdef CONJUGATE
		if (lsol[idx]==1) {
		#endif

              f[idx].dq[q] = (double)( RHO0 * w[q] );

		#ifdef CONJUGATE
		}
		#endif

	        #ifdef MULTICOMP
	        }
	        #endif

	      #endif

	    #endif

          #endif

          #if (defined THERMAL && defined SCALAR)

	    #ifdef REBIRTH
            t[idx].dq[q] = (double)( origin_eqt );
            c[idx].dq[q] = (double)( origin_eqc );
	    #else
            t[idx].dq[q] = (double)( tinit * w[q] );
            c[idx].dq[q] = (double)( cinit * w[q] );
	    #endif

	  #elif (defined THERMAL && !defined SCALAR)

	    #ifdef REBIRTH
            t[idx].dq[q] = (double)( origin_eqt );
	    #else
            t[idx].dq[q] = (double)( tinit * w[q] );
	    #endif

	  #elif (!defined THERMAL && defined SCALAR)

	    #ifdef REBIRTH
            t[idx].dq[q] = (double)( origin_eqc );
	    #else
            t[idx].dq[q] = (double)( cinit * w[q] );
	    #endif

          #endif

          }

        #ifdef TWOPHASE
        } else if (lsol[idx] == 2) {

          for (q=0; q<NQ; q++) {

	  #ifdef REBIRTH

	    // From one-phase to two-phase
	    #ifdef REBIRTH_1PTO2P
	    origin_rho = (double)RHO2;
	    #else
	    origin_rho = (double)rho[idx];
	    #endif

            // THERMAL code: From zero-tem to one-tem
            #if (defined THERMAL && defined REBIRTH_0TTO1T)
            origin_tem = tinit;
            #elif (defined THERMAL && !defined REBIRTH_0TTO1T)

	      #ifndef TWOPHASE
              origin_tem = (double)tem[idx]*origin_rho;
	      #else
	      origin_tem = tem[idx];
	      #endif

            #endif

	  // From equilibrium distribution
	  origin_eqf = origin_rho *w[q] * (1. +\
	  (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
	  (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
	  (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));

	    #ifdef THERMAL
	    // Temperature: From equilibrium distribution
	    origin_eqt = origin_tem *w[q] * (1. +\
	    (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
	    (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
	    (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));
	    #endif

	  #endif

          #ifdef REBIRTH
          f[idx].dq[q] = (double)( origin_eqf );
          #else

	    #ifdef MULTICOMP
	    if (F == 2) {
            f[idx].dq[q] = (double)( RHO22 * w[q] );
	    } else { 
	    #endif

          f[idx].dq[q] = (double)( RHO2 * w[q] );

	    #ifdef MULTICOMP
	    }
	    #endif

          #endif

	  #ifdef THERMAL

            #ifdef REBIRTH
            t[idx].dq[q] = (double)( origin_eqt );
            #else
            t[idx].dq[q] = (double)( tinit * w[q] );
            #endif

	  #endif

          }

	  #ifdef MULTICOMP
	  if (F == 2) {
	  #endif

        // Restore fluid in lsol
        lsol[idx] =  (int)1;

	  #ifdef MULTICOMP
	  }
	  #endif

	#endif

	// On solids we work on temperature
	// if (i) not-conjugate or (ii) conj_on_react
	#ifndef CONJUGATE
        } else if (lsol[idx] == 0) {
	#elif (defined CONJUGATE_ON_REACT)
        } else if (lsol[idx] == 0 && lwet[idx] == 0.) {
	#endif


          #if (defined THERMAL && (!defined CONJUGATE || \
	       defined CONJUGATE_ON_REACT))
          for (q=0; q<NQ; q++) {

	    #ifdef THERMAL_TEST
	    // Thermal test
            if (((int)prz == (int)NEZ-1) && (k==lz)) {
            t[idx].dq[q] = (double)(0.0);
	    } else {
            t[idx].dq[q] = (double)( TWALL * w[q] );
	    }

	    #else
            t[idx].dq[q] = (double)( TWALL * w[q] );
	    #endif

          }
          #endif

        #ifdef THERMAL
	} else if (lsol[idx] == 3) {

          for (q=0; q<NQ; q++) {
          t[idx].dq[q] = (double)( TSUB * w[q] );
          }

          lsol[idx] =  (int)0;

        #endif

        } else {

        fprintf(stderr,"EE: Error in some values of geo.bin.txt file\n");
        exit(1);

        }
      }
    }
  }
}

int periodic_df (dfdq *f) {

  // Pay attention: the order Y->X->Z (opposite to the code order) 
  // is essential to avoid info leakeage!!

  MPI_Status status1;

  /* int MPI_Sendrecv(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
     int dest, int sendtag, void *recvbuf, int recvcount,
     MPI_Datatype recvtype, int source, int recvtag,
     MPI_Comm comm, MPI_Status *status) */

  MPI_Sendrecv(f+lzp*lxp*ly+lzp+1    , 1, mpi_dfplane_y, pryp, 3, \
               f+lzp+1               , 1, mpi_dfplane_y, prym, 3, mpi_comm_y, &status1);
  MPI_Sendrecv(f+lzp*lxp+lzp+1       , 1, mpi_dfplane_y, prym, 4, \
               f+lzp*lxp*(ly+1)+lzp+1, 1, mpi_dfplane_y, pryp, 4, mpi_comm_y, &status1);

  MPI_Sendrecv(f+lzp*lx+1    , 1, mpi_dfplane_x, prxp, 1, \
               f+1           , 1, mpi_dfplane_x, prxm, 1, mpi_comm_x, &status1);
  MPI_Sendrecv(f+lzp+1       , 1, mpi_dfplane_x, prxm, 2, \
               f+lzp*(lx+1)+1, 1, mpi_dfplane_x, prxp, 2, mpi_comm_x, &status1);

  MPI_Sendrecv(f+lz  , 1, mpi_dfplane_z, przp, 5, \
               f     , 1, mpi_dfplane_z, przm, 5, mpi_comm_z, &status1);
  MPI_Sendrecv(f+1   , 1, mpi_dfplane_z, przm, 6, \
               f+lz+1, 1, mpi_dfplane_z, przp, 6, mpi_comm_z, &status1);

}

#ifndef REBIRTH_STEADY

#if (defined THERMAL && defined TWOPHASE)
int micro_lbe_eq(int *lsol, dfdq *teq, dfdq *feq, macro *rho, macro *ux, macro *uy, macro *uz, macro *tem, macro *psix, macro *psiy, macro *psiz) {
#elif (defined THERMAL && defined SCALAR)
int micro_lbe_eq(int *lsol, dfdq *ceq, dfdq *teq, dfdq *feq, macro *rho, macro *ux, macro *uy, macro *uz, macro *con, macro *tem) {
#elif (defined THERMAL || defined SCALAR)
int micro_lbe_eq(int *lsol, dfdq *teq, dfdq *feq, macro *rho, macro *ux, macro *uy, macro *uz, macro *tem) {
#else

#if (defined MULTICOMP && defined MULTICOMP_GUO)
int micro_lbe_eq(int *lsol, dfdq *feq, macro *rho, macro *ux, macro *uy, macro *uz, \
		dfdq *feq2, macro *rho2, macro *ux2, macro *uy2, macro *uz2) {
#elif (defined MULTICOMP && !defined MULTICOMP_GUO)
int micro_lbe_eq(int *lsol, dfdq *feq, macro *rho, macro *ux, macro *uy, macro *uz, \
		dfdq *feq2, macro *rho2, macro *ux2, macro *uy2, macro *uz2, \
		macro *psix, macro *psiy, macro *psiz, int F) {
#else
int micro_lbe_eq(int *lsol, dfdq *feq, macro *rho, macro *ux, macro *uy, macro *uz) {
#endif

#endif

#else

#if (defined THERMAL && defined SCALAR)
int micro_lbe_eq(int *lsol, dfdq *ceq, dfdq *teq, macro *ux, macro *uy, macro *uz, macro *con, macro *tem) {
#elif (defined THERMAL || defined SCALAR)
int micro_lbe_eq(int *lsol, dfdq *teq, macro *ux, macro *uy, macro *uz, macro *tem) {
#endif

#endif

  int idx=0, i=0, j=0, k=0, q=0;
  double C1=0.0;

  #ifdef MULTICOMP
  double uxc=0., uyc=0., uzc=0.;

    #ifndef MULTICOMP_GUO
    double tauc=0.;
    if (F==1) {
    tauc = TAU;
    } else {
    tauc = TAUM2;
    };
    #endif

  #endif

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

        idx = IDXP(i,j,k);

        if (lsol[idx] == 1) {

	#ifndef REBIRTH_STEADY

          for (q=0; q<NQ; q++) {

          #ifdef LBMRT
          feq[idx].dq[q] = rho[idx]*EQ[q].eq[0] + rho[idx]* ( \
          ux[idx]*EQ[q].eq[1] + uy[idx]*EQ[q].eq[2] + uz[idx]*EQ[q].eq[3] ) + pow(rho[idx],2.)* (\
          ux[idx]*ux[idx]*EQ[q].eq[4] + uy[idx]*uy[idx]*EQ[q].eq[5] + uz[idx]*uz[idx]*EQ[q].eq[6] + \
          ux[idx]*uy[idx]*EQ[q].eq[7] + uy[idx]*uz[idx]*EQ[q].eq[8] + ux[idx]*uz[idx]*EQ[q].eq[9] ) ;
          #else

            #if (defined THERMAL || defined SCALAR)

              #if (defined TWOPHASE) // && defined THERMAL)

              // [ZhangTian2008] two-way coupling with d0=1-2T
              C1 = (double)ceil((double)q/NQ);

              feq[idx].dq[q] = rho[idx]*w[q] * ( ((1.-C1)+tem[idx]*(3.*C1-2))*(double)(1./CS2) + \
              (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
              (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
              (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));
              #else

              feq[idx].dq[q] = rho[idx]*w[q] * (1. +\
              (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
              (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
              (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));
              #endif

            #else

	      #ifdef MULTICOMP
	      // global averaged & shifted velocity for multicomponent
	      uxc = (rho[idx]*ux[idx]+rho2[idx]*ux2[idx])/(double)(rho[idx]+rho2[idx]);
	      uyc = (rho[idx]*uy[idx]+rho2[idx]*uy2[idx])/(double)(rho[idx]+rho2[idx]);
	      uzc = (rho[idx]*uz[idx]+rho2[idx]*uz2[idx])/(double)(rho[idx]+rho2[idx]);

		#ifndef MULTICOMP_GUO
	        uxc += tauc * psix[idx] /(double)rho[idx];
	        uyc += tauc * psiy[idx] /(double)rho[idx];
	        uzc += tauc * psiz[idx] /(double)rho[idx];
		#endif

              feq[idx].dq[q] = rho[idx]*w[q] * (1. +\
              (double)(1./CS2)*(uxc*cx[q]+uyc*cy[q]+uzc*cz[q]) + \
              (double)(1./(2.*CS2*CS2))*pow(uxc*cx[q]+uyc*cy[q]+uzc*cz[q],2.) - \
              (double)(1./(2.*CS2))*(pow(uxc,2.)+pow(uyc,2.)+pow(uzc,2.)));

	      #else
              feq[idx].dq[q] = rho[idx]*w[q] * (1. +\
              (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
              (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
              (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));
              #endif

	    #endif

          #endif

          }

	#endif

	}

	#ifndef CONJUGATE
        if (lsol[idx] == 1) {
	#endif

        #if (defined THERMAL || defined SCALAR)

	  #if (defined TWOPHASE)
	  // We need the hydrodynamic velocity 
	  // for two-way coupling and successive dumping
	  ux[idx] += (-(double)TAU+0.5) * (double) (psix[idx]/rho[idx]);
	  uy[idx] += (-(double)TAU+0.5) * (double) (psiy[idx]/rho[idx]);
	  uz[idx] += (-(double)TAU+0.5) * (double) (psiz[idx]/rho[idx]);
	  #endif

          for (q=0; q<NQ; q++) {

          teq[idx].dq[q] = tem[idx]*w[q] * (1. + \
          (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
          (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
          (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));

	  #if (defined THERMAL && defined SCALAR)
          ceq[idx].dq[q] = con[idx]*w[q] * (1. + \
          (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
          (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
          (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));
	  #endif

          }
	
        #endif

	#ifndef CONJUGATE
        }
	#endif
      }
    }
  }
}

#ifndef REBIRTH_STEADY

#ifdef MULTICOMP
int micro_forcing(int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, \
		  macro *ux2, macro *uy2, macro *uz2, macro *rho2, \
		  macro *psix, macro *psiy, macro *psiz, dfdq *bf, int F) {
#elif (defined SOURCE_HET && (defined THERMAL && defined SCALAR))
int micro_forcing(int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, \
		  dfdq *bf, dfdq *bfs, macro *tem, macro *con, dfdq *t, dfdq *teq, \
		  dfdq *c, dfdq *ceq, macro *lwet, macro *st1, double *p_exo, int F) {
#elif (defined SOURCE_HET && (defined THERMAL || defined SCALAR))
int micro_forcing(int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, \
		  dfdq *bf, dfdq *bfs, macro *tem, dfdq *t, dfdq *teq, \
		  macro *lwet, macro *st1, double *p_exo, int F) {
#elif (defined SOURCE && (defined THERMAL && defined SCALAR))
int micro_forcing(int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, \
		  dfdq *bf, dfdq *bfs, macro *tem, macro *con, dfdq *t, dfdq *teq, \
		  dfdq *c, dfdq *ceq, macro *st1, int F) {
#elif (defined SOURCE && (defined THERMAL || defined SCALAR))
int micro_forcing(int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, \
		  dfdq *bf, dfdq *bfs, macro *tem, dfdq *t, dfdq *teq, \
		  macro *st1, int F) {
#elif (!defined SOURCE && (defined SCALAR || defined THERMAL))
int micro_forcing(int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, dfdq *bf, dfdq *bfs, int F) {
#else
int micro_forcing(int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, dfdq *bf, int F) {
#endif

#else

#if (defined SOURCE_HET && (defined THERMAL && defined SCALAR))
int micro_forcing(int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, \
		dfdq *bf, macro *tem, macro *con, dfdq *t, dfdq *teq, dfdq *c, dfdq *ceq, \
		macro *lwet, macro *st1, double *p_exo, int F) {
#elif (defined SOURCE_HET && (defined THERMAL || defined SCALAR))
int micro_forcing(int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, \
		dfdq *bf, macro *tem, dfdq *t, dfdq *teq, \
		macro *lwet, macro *st1, double *p_exo, int F) {
#elif (defined SOURCE && (defined THERMAL && defined SCALAR))
int micro_forcing(int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, \
		dfdq *bf, macro *tem, macro *con, dfdq *t, dfdq *teq, dfdq *c, dfdq *ceq, \
		macro *st1, int F) {
#elif (defined SOURCE && (defined THERMAL || defined SCALAR))
int micro_forcing(int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, \
		dfdq *bf, macro *tem, dfdq *t, dfdq *teq, \
		macro *st1, int F) {
#else
int micro_forcing(int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, dfdq *bf,int F) {
#endif

#endif

  int idx=0, i=0, j=0, k=0, q=0;
  double ramp = 0.;
  double force =0.;

  #ifdef CONJUGATE
  int idxp=0;
  double uu=0., qu=0.;
  double cp0=0., cpp=0., cpm=0.;
  double taua=0., cp1=0., cp2=0.;
  double qux=0., quy=0., quz=0.;
  #endif

  #if (defined EXOTHERMIC || defined CONJUGATE)
  int qc=0, ic=0, jc=0, kc=0, idxc=0;
  #endif

  #ifdef EXOTHERMIC
  double heat = 0.;
  #endif

  #if (defined MULTICOMP && defined MULTICOMP_GUO)
  double uxc=0., uyc=0., uzc=0;
  #endif

  #ifdef ATAN_FORCE
  ramp = atan((double)iter/(double)MAX_ITER*20.0) / (M_PI/2.0);
  #else
  ramp = 1.0;
  #endif

  // only at first iteration
  #if (defined REBIRTH_STEADY && !defined SOURCE)
  if (iter == 0) {
  #endif

  #ifdef EXOTHERMIC
  // initialize
  for (i = 0; i<4; i++)
  p_exo[i] = 0.;
  #endif

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

      #ifdef SOURCE
      if (F==1||F==2)
      st1[idx] = 0.;
      #endif

      #if (defined GRAVITY && defined SCALAR && defined THERMAL)
      force = (double)DPDL*con[idx];
      #elif (defined GRAVITY && defined SCALAR && !defined THERMAL)
      force = (double)DPDL*tem[idx];
      #elif (defined GRAVITY)
      force = (double)DPDL*rho[idx];
      #else
      force = (double)DPDL;
      #endif

	#ifndef CONJUGATE
	if (lsol[idx]==1) {

	#if (defined MULTICOMP && defined MULTICOMP_GUO)
        // global averaged & shifted velocity for multicomponent
        uxc = (rho[idx]*ux[idx]+rho2[idx]*ux2[idx])/(double)(rho[idx]+rho2[idx]);
        uyc = (rho[idx]*uy[idx]+rho2[idx]*uy2[idx])/(double)(rho[idx]+rho2[idx]);
        uzc = (rho[idx]*uz[idx]+rho2[idx]*uz2[idx])/(double)(rho[idx]+rho2[idx]);
        #endif

	  for (q=0; q<NQ; q++) {

	#else
        if (lsol[idx]==1) {
        taua=(double)TAUT;
        } else {
        taua=(double)TAU2;
        }

        // initialize
        qux=0.;
        quy=0.;
        quz=0.;

	for (q=0; q<NQ; q++) {
          if (lsol[idx]==1) {
	#endif

	    // post-eq (F==1)
	    if (F==1) {
	    #if (defined ACOUSTIC_FORCEX || defined ACOUSTIC_FORCEY || defined ACOUSTIC_FORCEZ)
	    bf[idx].dq[q] = 0.0;
	    #endif

	    #if (defined MULTICOMP && defined MULTICOMP_GUO)
	    // zeros on bf
	    bf[idx].dq[q] = 0.0;
	    #endif

	    // scalar calls bfs/bf for unsteady/steady
	    #if  (defined THERMAL || defined SCALAR)

	    #ifndef REBIRTH_STEADY
	    bfs[idx].dq[q] = ramp * w[q]*( \
            ((double)cx[q])*(double)(1./CS2) * force *(double)dpdlv[0] + \
            ((double)cy[q])*(double)(1./CS2) * force *(double)dpdlv[1] + \
            ((double)cz[q])*(double)(1./CS2) * force *(double)dpdlv[2]);

	    #else
	    bf[idx].dq[q] = ramp * w[q]*( \
            ((double)cx[q])*(double)(1./CS2) * force *(double)dpdlv[0] + \
            ((double)cy[q])*(double)(1./CS2) * force *(double)dpdlv[1] + \
            ((double)cz[q])*(double)(1./CS2) * force *(double)dpdlv[2]);
	    #endif

	    // scalar
	    #endif

	    // unsteady bf to fluid
	    #ifndef REBIRTH_STEADY

	    #if (defined ACOUSTIC_FORCEX || defined ACOUSTIC_FORCEY || defined ACOUSTIC_FORCEZ)
            bf[idx].dq[q] += w[q]*( \
            (((double)cx[q]-ux[idx])*(double)(1./CS2) + \
              ((double)cx[q]*ux[idx]+(double)cy[q]*uy[idx]+(double)cz[q]*uz[idx]) *\
	      (double)(1./pow(CS2,2.))*(double)cx[q])* \
              (double)AWAVE*(double)sin(2.0*M_PI*iter*(double)(FWAVE/MAX_ITER))*(double)wavev[0] + \
            (((double)cy[q]-uy[idx])*(double)(1./CS2) + \
              ((double)cx[q]*ux[idx]+(double)cy[q]*uy[idx]+(double)cz[q]*uz[idx]) *\
	      (double)(1./pow(CS2,2.))*(double)cy[q])* \
              (double)AWAVE*(double)sin(2.0*M_PI*iter*(double)(FWAVE/MAX_ITER))*(double)wavev[1] + \
            (((double)cz[q]-uz[idx])*(double)(1./CS2) + \
              ((double)cx[q]*ux[idx]+(double)cy[q]*uy[idx]+(double)cz[q]*uz[idx]) *\
	      (double)(1./pow(CS2,2.))*(double)cz[q])* \
              (double)AWAVE*(double)sin(2.0*M_PI*iter*(double)(FWAVE/MAX_ITER))*(double)wavev[2] );

	    #elif (defined MULTICOMP && defined MULTICOMP_GUO)
            bf[idx].dq[q] += w[q]*( \
            (((double)cx[q]-uxc)*(double)(1./CS2) + \
              ((double)cx[q]*uxc+(double)cy[q]*uyc+(double)cz[q]*uzc) *\
	      (double)(1./pow(CS2,2.))*(double)cx[q])* \
              (double)psix[idx] + \
            (((double)cy[q]-uyc)*(double)(1./CS2) + \
              ((double)cx[q]*uxc+(double)cy[q]*uyc+(double)cz[q]*uzc) *\
	      (double)(1./pow(CS2,2.))*(double)cy[q])* \
              (double)psiy[idx] + \
            (((double)cz[q]-uzc)*(double)(1./CS2) + \
              ((double)cx[q]*uxc+(double)cy[q]*uyc+(double)cz[q]*uzc) *\
	      (double)(1./pow(CS2,2.))*(double)cz[q])* \
              (double)psiz[idx] );

	    #else
            bf[idx].dq[q] = ramp * w[q]*( \
            (((double)cx[q]-ux[idx])*(double)(1./CS2) + \
             ((double)cx[q]*ux[idx]+(double)cy[q]*uy[idx]+(double)cz[q]*uz[idx]) *\
	      (double)(1./pow(CS2,2.))*(double)cx[q])* \
              force *(double)dpdlv[0] + \
            (((double)cy[q]-uy[idx])*(double)(1./CS2) + \
             ((double)cx[q]*ux[idx]+(double)cy[q]*uy[idx]+(double)cz[q]*uz[idx]) *\
	      (double)(1./pow(CS2,2.))*(double)cy[q])* \
              force *(double)dpdlv[1] + \
            (((double)cz[q]-uz[idx])*(double)(1./CS2) + \
             ((double)cx[q]*ux[idx]+(double)cy[q]*uy[idx]+(double)cz[q]*uz[idx]) *\
	      (double)(1./pow(CS2,2.))*(double)cz[q])* \
              force *(double)dpdlv[2] );
            #endif

	    #endif
	    } // F==1

	  // q-loop (nconj) / idx=1 (conj)
          }

	  #ifdef CONJUGATE
	  if (F==1) {
	  // calculate the local (approximate) diffusive flux

	    #ifdef CONJUGATE_ON_REACT
	    if (lsol[idx]!=0||lwet[idx]!=0.) {
	    #endif

	  qux += (double)(1.-(double)(1./(2.*taua)))*(t[idx].dq[q]-teq[idx].dq[q])*(double)cx[q];
	  quy += (double)(1.-(double)(1./(2.*taua)))*(t[idx].dq[q]-teq[idx].dq[q])*(double)cy[q];
	  quz += (double)(1.-(double)(1./(2.*taua)))*(t[idx].dq[q]-teq[idx].dq[q])*(double)cz[q];

	    #ifdef CONJUGATE_ON_REACT
	    }
	    #endif

	  }
	  #endif

	  // after streaming (F==3)
	  // density correction at the fluid-solid boundaries
	  #ifdef CONJUGATE_CORR
	  if (F==3) {

	    if (q>0) {
            ic=(int)i-cx[q];
            jc=(int)j-cy[q];
            kc=(int)k-cz[q];
            qc=(int)iq[q];

            idxc = IDXP(ic,jc,kc);

	      #ifdef CONJUGATE_ON_REACT
	      if (lwet[idx]!=0.||lwet[idxc]!=0.) {
	      #endif

	      if (lsol[idx]!=lsol[idxc]) {
	      t[idx].dq[q] = t[idx].dq[q] * \
			     ((double)lsol[idx]*rho[idx]+(double)(1-lsol[idx])*RHO0) / \
			     ((double)lsol[idxc]*rho[idxc]+(double)(1-lsol[idxc])*RHO0);
	      }

	      #ifdef CONJUGATE_ON_REACT
	      }
	      #endif

	    }
	  }
	  #endif

	  #ifdef EXOTHERMIC
	  // pre-eq (F==2)
	  if (F==2 && iter>0) {

	    if (q>0) {
	    // Heat release
	    // at the surf: (J/m3) q = (rho_solv*cp_solv+(cs*mw)*cp_solu)* Delta T
	    // along a link: (J/m3/s) dq/dt = S/V*D*dc/dn *dH_sto = dc/dt*dHsto
	    // Gamma3 = dH_sto*c0/(cp_solv*rho_solv*T0)
	    // Gamma4 = c0*mw/rho_solv*cp_solu/cp_solv
	    // Gamma5 = dH_sto*c0/(cp_cat*rho_cat*T0)
	    // for each link: dT/dt = Gamma3*dc/(1+Gamma4*cs))
	    // for each link: dT/dt = Gamma5*dc
	    // Surface conc/flux (Huang J.Comp.Phys.2015):
	    // cs = 1/(2wq)*(c(t+dt,q)+c(t,qc))
	    // flux dc = 1/3/(2wq)*(c(t,qc)-c(t+dt,q))

            ic=(int)i-cx[q];
            jc=(int)j-cy[q];
            kc=(int)k-cz[q];
            qc=(int)iq[q];

            idxc = IDXP(ic,jc,kc);

              // Only when border reaction
              if (((lwet[idxc]!=0.)&&(lsol[idx]==1)) || \
		   ((lwet[idx]!=0.)&&(lsol[idxc]==1))) {

                // heat release dc in c*rho units (mol/m3)
		if (lsol[idx]==1) {
                heat = 0.5*GAMMA3*(-c[idx].dq[q]+ceq[idx].dq[qc])*(1./(6.*w[q])) / \
                       (double)(1.+GAMMA4*0.5/(w[q])*(c[idx].dq[q]+ceq[idx].dq[qc]));

		// half add to source liquid
	        heat = heat*293.15/20.;
	        st1[idx] += heat;

	        // Proc sums
	        p_exo[0] = 1./(6.*w[q])*(-c[idx].dq[q]+ceq[idx].dq[qc]) + p_exo[0];
	        p_exo[1] = 1./(2.*w[q])*(c[idx].dq[q]+ceq[idx].dq[qc]) + p_exo[1];
	        p_exo[2] = p_exo[2] + CP1*heat;
	        p_exo[3] = p_exo[3] + 1.;

                // heat release dc in c*rho units (mol/m3)
		} else {
                heat = 0.5*(double)(CP1/CP2*GAMMA3)* \
		       (-c[idxc].dq[qc]+ceq[idxc].dq[q])*(1./(6.*w[q]));

		// half add to source solid
	        heat = heat*293.15/20.;
	        st1[idx] += heat;

		// Proc sums
	        p_exo[2] = p_exo[2] + CP2*heat;
		}
	      }
	    }
	  }
	  #endif

	// idx=1|0 (nconj) / q-loop (conj)
	}

        // Source term corrected from Karani & Huber PRE E 91, 023304 (2015)
        #ifdef CONJUGATE
	if (F==1) {

	cp1 = (double)(CP1);
	cp2 = (double)(CP2);

        // center
        #ifdef CONJUGATE_ON_REACT
        cp0 = (double)(cp2*(double)(lwet[idx]!=0.)+(double)cp1*(double)(lwet[idx]==0.));
        #else
        cp0 = (double)(cp2*(double)(1-lsol[idx])+(double)cp1*(double)lsol[idx]);
        #endif

	  for (q=1; q<=6; q++) {

	  idxp = IDXP(i+cx[q],j+cy[q],k+cz[q]);
	  uu = ux[idx]*cx[q]*cx[q] + uy[idx]*cy[q]*cy[q] + uz[idx]*cz[q]*cz[q];
	  qu = qux*cx[q]*cx[q] + quy*cy[q]*cy[q] + quz*cz[q]*cz[q];

	  cp1 = (double)(CP1);
	  cp2 = (double)(CP2);

	  // qplus-minus
          #ifdef CONJUGATE_ON_REACT
          cpp = (double)lsol[idx]*(cp2*(double)(lwet[idxp]!=0.)+cp0*(double)(lwet[idxp]==0.))\
              + (double)(1-lsol[idx])*(cp1*(double)lsol[idxp]+cp0*(double)(1-lsol[idxp]));
          #else
          cpp = (double)(cp2*(double)(1-lsol[idxp])+cp1*(double)lsol[idxp]);
          #endif

          // update with flux with cp gradient
          st1[idx] += 0.5*(double)(cp0*(qu))* \
                (1./((double)cp0)-(2./((double)(cp0+cpp))))*-2.* \
		(cx[q]+cy[q]+cz[q]);

	  }
	}
        #endif
        // conjugate
      }
    }
  }

  // only at first iteration
  #if (defined REBIRTH_STEADY && !defined SOURCE)
  }
  #endif

}

#ifndef REBIRTH_STEADY

#if (defined THERMAL && defined SCALAR && defined SOURCE_HET)
int micro_lbe_collision(int *lsol, dfdq *f, dfdq *t, dfdq *c, \
		dfdq *feq, dfdq *teq, dfdq *ceq, dfdq *bf, dfdq *bfs, \
		macro *tem, macro *con, macro *st1, macro *st2, \
		macro *ux, macro *uy, macro *uz) {
#elif (defined THERMAL && defined SCALAR && defined SOURCE)
int micro_lbe_collision(int *lsol, dfdq *f, dfdq *t, dfdq *c, \
		dfdq *feq, dfdq *teq, dfdq *ceq, dfdq *bf, dfdq *bfs, \
		macro *tem, macro *con, macro *st1, \
		macro *ux, macro *uy, macro *uz) {
#elif ((defined THERMAL || defined SCALAR) && defined SOURCE_HET)
int micro_lbe_collision(int *lsol, dfdq *f, dfdq *t, \
		dfdq *feq, dfdq *teq, dfdq *bf, dfdq *bfs, \
		macro *tem, macro *st1, macro *st2, \
		macro *ux, macro *uy, macro *uz) {
#elif ((defined THERMAL || defined SCALAR) && defined SOURCE)
int micro_lbe_collision(int *lsol, dfdq *f, dfdq *t, \
		dfdq *feq, dfdq *teq, dfdq *bf, dfdq *bfs, \
		macro *tem, macro *st1, \
		macro *ux, macro *uy, macro *uz) {
#elif (defined THERMAL && defined SCALAR && !defined SOURCE)
int micro_lbe_collision(int *lsol, dfdq *f, dfdq *t, dfdq *c, \
		dfdq *feq, dfdq* teq, dfdq *ceq, dfdq *bf, dfdq *bfs, \
		macro *tem, macro *con) {
#elif ((defined THERMAL || defined SCALAR ) && !defined SOURCE)
int micro_lbe_collision(int *lsol, dfdq *f, dfdq *t, \
		dfdq *feq, dfdq *teq, dfdq *bf, dfdq *bfs, macro *tem) {
#elif (defined NON_NEWTONIAN)
int micro_lbe_collision(int *lsol, dfdq *f, dfdq *feq, dfdq *bf, I19 *MRT, macro *taun, macro *rho) {
#else
int micro_lbe_collision(int *lsol, dfdq *f, dfdq *feq, dfdq *bf, I19 *MRT, int Fc) {
#endif

#else

#if (defined THERMAL && defined SCALAR && !defined SOURCE)
int micro_lbe_collision(int *lsol, dfdq *t, dfdq *c, dfdq* teq, dfdq *ceq, dfdq *bfs, macro *tem, macro *con) {
#elif ((defined THERMAL || defined SCALAR) && !defined SOURCE)
int micro_lbe_collision(int *lsol, dfdq *t, dfdq *teq, dfdq *bfs, macro *tem) {
#elif (defined THERMAL && defined SCALAR && defined SOURCE_HET)
int micro_lbe_collision(int *lsol, dfdq *t, dfdq *c, dfdq* teq, dfdq *ceq, dfdq *bfs, macro *tem, macro *con, macro *st1, macro *st2, macro *ux, macro *uy, macro *uz) {
#elif (defined THERMAL && defined SCALAR && defined SOURCE)
int micro_lbe_collision(int *lsol, dfdq *t, dfdq *c, dfdq* teq, dfdq *ceq, dfdq *bfs, macro *tem, macro *con, macro *st1, macro *ux, macro *uy, macro *uz) {
#elif ((defined THERMAL || defined SCALAR) && defined SOURCE_HET)
int micro_lbe_collision(int *lsol, dfdq *t, dfdq* teq, dfdq *bfs, macro *tem, macro *st1, macro *st2, macro *ux, macro *uy, macro *uz) {
#elif ((defined THERMAL || defined SCALAR) && defined SOURCE)
int micro_lbe_collision(int *lsol, dfdq *t, dfdq* teq, dfdq *bfs, macro *tem, macro *st1, macro *ux, macro *uy, macro *uz) {
#endif

#endif

  int i=0, j=0, k=0, idx=0; 
  int q1=0, q2=0;
  double temp=0.;

  #ifdef MULTICOMP
  double tauc=0.;
  if (Fc==1) {
  tauc = TAU;
  } else {
  tauc = TAUM2;
  }
  #endif

  #ifdef CONJUGATE
  double taua=0.;
  #endif

  #ifdef SOURCE
  double st=0.;
  #endif

  #if (defined LBMRT && !defined REBIRTH_STEADY)
  // (19xntot)
  dfdq *A, *C, *D, *E, *F;
  A=(dfdq *)calloc(lxp*lyp*lzp, sizeof(dfdq)); 
  C=(dfdq *)calloc(lxp*lyp*lzp, sizeof(dfdq)); 
  D=(dfdq *)calloc(lxp*lyp*lzp, sizeof(dfdq)); 
  E=(dfdq *)calloc(lxp*lyp*lzp, sizeof(dfdq)); 
  F=(dfdq *)calloc(lxp*lyp*lzp, sizeof(dfdq));

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);

        if (lsol[idx] == 1) {
          for (q1=0; q1<NQ; q1++) {
            for (q2=0; q2<NQ; q2++) {
  
            A[idx].dq[q1] += (double)MRT[q1].dq[q2] * bf[idx].dq[q2];
            D[idx].dq[q1] += (double)MRT[q1].dq[q2] * f[idx].dq[q2];

            }
          }
        }
      }
    }
  }

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);

        if (lsol[idx] == 1) {
          for (q1=0; q1<NQ; q1++) {
            for (q2=0; q2<NQ; q2++) {

            C[idx].dq[q1] += ((double)EYE[q1].dq[q2] - 0.5 * S[q1].dq[q2]) * A[idx].dq[q2];
            E[idx].dq[q1] += S[q1].dq[q2] * (D[idx].dq[q2] - feq[idx].dq[q2]);

            }
          }
        }
      }
    }
  }

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);

        if (lsol[idx] == 1) {
          for (q1=0; q1<NQ; q1++) {
            for (q2=0; q2<NQ; q2++) {

            F[idx].dq[q1] += iMRT[q1].dq[q2] * (E[idx].dq[q2]-C[idx].dq[q2]);

            }
          }
        }
      }
    }
  }


  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);

        if (lsol[idx] == 1) {
          for (q1=0; q1<NQ; q1++) {

          f[idx].dq[q1] = f[idx].dq[q1] - F[idx].dq[q1];

          }
        }
      }
    }
  }

  free(A);
  free(C);
  free(D);
  free(E);
  free(F);

  #endif

  #if (!defined LBMRT || (defined LBMRT && (defined THERMAL || defined SCALAR)))

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);

      #ifdef SOURCE_HET
      st = st1[idx] + st2[idx];
      #elif (defined SOURCE)
      st = st1[idx];
      #endif

        #ifndef CONJUGATE
        if (lsol[idx] == 1) {
          for (q1=0; q1<NQ; q1++) {

	// Invert order for conjugate ht
	#else
        for (q1=0; q1<NQ; q1++) {
          if (lsol[idx] == 1) {
	#endif

	  // Non-Newtonian fluids (rebirth not implemented?)
	  #ifdef NON_NEWTONIAN
          f[idx].dq[q1] = (1.-(double)(1./taun[idx])) * f[idx].dq[q1] + \
                              (double)(1./taun[idx]) * feq[idx].dq[q1] + \
                          (1.-(double)(1./(2.*taun[idx]))) * bf[idx].dq[q1] ;
	  #else

	  #if (!defined LBMRT && !defined REBIRTH_STEADY)

	  #ifdef MULTICOMP
          f[idx].dq[q1] = (1.-(double)(1./tauc)) * f[idx].dq[q1] + \
                              (double)(1./tauc) * feq[idx].dq[q1] + \
                          (1.-(double)(1./(2.*tauc))) * bf[idx].dq[q1] ;
	  #else
          f[idx].dq[q1] = (1.-(double)(1./TAU)) * f[idx].dq[q1] + \
                              (double)(1./TAU) * feq[idx].dq[q1] + \
                          (1.-(double)(1./(2.*TAU))) * bf[idx].dq[q1] ;
	  #endif

	  #endif

	  #endif

	  #ifdef CONJUGATE
	  // Diffusivity
	  taua = (double)TAUT;
	  } else {
	  taua = (double)TAU2;
	  }
	  #endif

	  // Only thermal
	  #if (defined THERMAL && !defined SCALAR)

	  #ifdef TWOPHASE
	  temp = 1.;
	  #else
	  temp = tem[idx];
	  #endif

	    #if (!defined CONJUGATE && !defined SOURCE)
            t[idx].dq[q1] = (1.-(double)(1./TAUT)) * t[idx].dq[q1] + \
                                (double)(1./TAUT) * teq[idx].dq[q1] + \
                            (1.-(double)(1./(2.*TAUT))) * temp * bfs[idx].dq[q1] ;

	    #elif (!defined CONJUGATE && defined SOURCE)
            t[idx].dq[q1] = (1.-(double)(1./TAUT)) * t[idx].dq[q1] + \
                                (double)(1./TAUT) * teq[idx].dq[q1] + \
                            (1.-(double)(1./(2.*TAUT))) * ( w[q1]*st * \
			     (1.+(cx[q1]*ux[idx]+cy[q1]*uy[idx]+cz[q1]*uz[idx])/(double)CS2) + \
			     temp * bfs[idx].dq[q1]);

	    #elif (defined CONJUGATE && defined SOURCE)
            t[idx].dq[q1] = (1.-(double)(1./taua)) * t[idx].dq[q1] + \
                                (double)(1./taua) * teq[idx].dq[q1] + \
                            (1.-(double)(1./(2.*taua))) * ( w[q1]*st * \
			     (1.+(cx[q1]*ux[idx]+cy[q1]*uy[idx]+cz[q1]*uz[idx])/(double)CS2) + \
			     temp * bfs[idx].dq[q1]);

	    #endif

	  #endif

	  // Only scalar
	  #if (!defined THERMAL && defined SCALAR)
          t[idx].dq[q1] = (1.-(double)(1./TAUC)) * t[idx].dq[q1] + \
                              (double)(1./TAUC) * teq[idx].dq[q1] + \
                          (1.-(double)(1./(2.*TAUC))) * (tem[idx]*bfs[idx].dq[q1] );
	  #endif

	  // Both thermal and scalar
	  #if (defined THERMAL && defined SCALAR)

	  #ifdef CONJUGATE
	  if (lsol[idx]==1) {
	  #endif

          c[idx].dq[q1] = (1.-(double)(1./TAUC)) * c[idx].dq[q1] + \
                              (double)(1./TAUC) * ceq[idx].dq[q1] + \
                          (1.-(double)(1./(2.*TAUC))) * ( con[idx] * bfs[idx].dq[q1] );

	  #ifdef CONJUGATE
	  }
	  #endif

	  #ifdef TWOPHASE
	  temp = 1.;
	  #else
	  temp = tem[idx];
	  #endif

	    #if (!defined CONJUGATE && !defined SOURCE)
            t[idx].dq[q1] = (1.-(double)(1./TAUT)) * t[idx].dq[q1] + \
                                (double)(1./TAUT) * teq[idx].dq[q1] + \
                            (1.-(double)(1./(2.*TAUT))) * ( temp * bfs[idx].dq[q1] ) ;

	    #elif (defined CONJUGATE && defined SOURCE)
            t[idx].dq[q1] = (1.-(double)(1./taua)) * t[idx].dq[q1] + \
                                (double)(1./taua) * teq[idx].dq[q1] + \
                            (1.-(double)(1./(2.*taua))) * ( w[q1]*st * \
			     (1.+(cx[q1]*ux[idx]+cy[q1]*uy[idx]+cz[q1]*uz[idx])/(double)CS2) + \
			      temp * bfs[idx].dq[q1] );

	    #elif (defined CONJUGATE && !defined SOURCE)
            t[idx].dq[q1] = (1.-(double)(1./taua)) * t[idx].dq[q1] + \
                                (double)(1./taua) * teq[idx].dq[q1] + \
                            (1.-(double)(1./(2.*taua))) * ( temp * bfs[idx].dq[q1] );

	    #elif (!defined CONJUGATE && defined SOURCE)
            t[idx].dq[q1] = (1.-(double)(1./TAUT)) * t[idx].dq[q1] + \
                                (double)(1./TAUT) * teq[idx].dq[q1] + \
                            (1.-(double)(1./(2.*TAUT))) * ( w[q1]*st * \
			     (1.+(cx[q1]*ux[idx]+cy[q1]*uy[idx]+cz[q1]*uz[idx])/(double)CS2) + \
			      temp * bfs[idx].dq[q1] );
	    #endif

	  #endif

	  #ifndef CONJUGATE
          }
	  #endif

        }
      }
    }
  }

  #endif

}

#ifndef REBIRTH_STEADY

#if (defined THERMAL && defined SCALAR && defined SPATIAL_REACTION)
int micro_lbe_streaming(int *lsol, dfdq *f, dfdq *t, dfdq *c, dfdq *feq, dfdq *teq, dfdq *ceq, double *lwet) {
#elif (defined THERMAL && defined SCALAR && !defined SPATIAL_REACTION)
int micro_lbe_streaming(int *lsol, dfdq *f, dfdq *t, dfdq *c, dfdq *feq, dfdq *teq, dfdq *ceq) {
#elif ((defined THERMAL || defined SCALAR) && defined SPATIAL_REACTION)
int micro_lbe_streaming(int *lsol, dfdq *f, dfdq *t, dfdq *feq, dfdq *teq, double *lwet) {
#elif ((defined THERMAL || defined SCALAR) && !defined SPATIAL_REACTION)
int micro_lbe_streaming(int *lsol, dfdq *f, dfdq *t, dfdq *feq, dfdq *teq) {
#else
int micro_lbe_streaming(int *lsol, dfdq *f, dfdq *feq) {
#endif

#else

#if (defined THERMAL && defined SCALAR && defined SPATIAL_REACTION)
int micro_lbe_streaming(int *lsol, dfdq *t, dfdq *c, dfdq *teq, dfdq *ceq, double *lwet) {
#elif (defined THERMAL && defined SCALAR && !defined SPATIAL_REACTION)
int micro_lbe_streaming(int *lsol, dfdq *t, dfdq *c, dfdq *teq, dfdq *ceq) {
#elif ((defined THERMAL || defined SCALAR) && defined SPATIAL_REACTION)
int micro_lbe_streaming(int *lsol, dfdq *t, dfdq *teq, double *lwet) {
#elif ((defined THERMAL || defined SCALAR) && !defined SPATIAL_REACTION)
int micro_lbe_streaming(int *lsol, dfdq *t, dfdq *teq) {
#endif

#endif

  // Implemented with bounce-back (No scan required)
  int idx=0, i=0, j=0, k=0, q=0;
  int idxc=0, ic=0, jc=0, kc=0, qc=0;

  // 1) zero at lsol=0 (can I avoid it?) 
  for (j=0; j<lyp; j++) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idx=IDXP(i,j,k);

        for (q=0; q<NQ; q++) {

	#ifndef REBIRTH_STEADY
        // Fluid dist.fun
        feq[idx].dq[q] = f[idx].dq[q] * (double)lsol[idx];
	#endif

        // Temperature/Scalar dist.fun
	#if (defined THERMAL && defined SCALAR)
        ceq[idx].dq[q] = c[idx].dq[q] * (double)lsol[idx];
	#elif (!defined THERMAL && defined SCALAR)
        teq[idx].dq[q] = t[idx].dq[q] * (double)lsol[idx];
	#endif

	#ifdef THERMAL

	  #ifdef THERMAL_TEST
	  // Thermal test
          if (((int)prz == (int)NEZ-1) && (k==lz)) {
          teq[idx].dq[q] = t[idx].dq[q] * (double)lsol[idx];
	  } else {
          teq[idx].dq[q] = t[idx].dq[q];
	  }

	  #else
          teq[idx].dq[q] = t[idx].dq[q];
	  #endif

	#endif

	}
      }
    }
  }

  // 2) Stream and bounce on the basis of lsol=0|1
  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);

        #ifndef CONJUGATE
	if (lsol[idx] == 1) {

	  for (q=1; q<NQ; q++) {

	  ic=(int)i-cx[q];
	  jc=(int)j-cy[q];
	  kc=(int)k-cz[q];
	  qc=(int)iq[q];
  
	  idxc= IDXP(ic,jc,kc);

	// Invert order for conjugate ht
	#else
        for (q=1; q<NQ; q++) {

	ic=(int)i-cx[q];
	jc=(int)j-cy[q];
	kc=(int)k-cz[q];
	qc=(int)iq[q];
  
	idxc= IDXP(ic,jc,kc);
  
	  if (lsol[idx] == 1) {

	#endif

	  #ifndef REBIRTH_STEADY
          // Fluid dist.fun
          f[idx].dq[q] = feq[idxc].dq[q] * (double)lsol[idx] + \
  	                 feq[idx].dq[qc] * (double)(1-lsol[idxc]);
	  #endif

          // Temperature/Scalar dist.fun + REACTION
	  #if (defined THERMAL && defined SCALAR)

	    // Huang J.Comp.Phys 310(2016) 26-44
	    // with inverted coeff. A1,A2
            #ifdef REACTION
            double  q_tmp;
            q_tmp = (double)A1 / (3.0 * (double)(CS2*((double)TAUC-0.5)));
	    // This is q=a1/(3*D)

	      // Reaction inside box
	      #ifdef REACTION_BOX

	      // Get local position of the reaction box
	      int local_xm = (int)(REBOX_XM - procoffx[world_rank]);
	      int local_xp = (int)(REBOX_XP - procoffx[world_rank]);
	      int local_ym = (int)(REBOX_YM - procoffy[world_rank]);
	      int local_yp = (int)(REBOX_YP - procoffy[world_rank]);
	      int local_zm = (int)(REBOX_ZM - procoffz[world_rank]);
	      int local_zp = (int)(REBOX_ZP - procoffz[world_rank]);

                if ((i>=local_xm+1) && (i<=local_xp) && \
                    (j>=local_ym+1) && (j<=local_yp) && \
                    (k>=local_zm+1) && (k<=local_zp)) {

		c[idx].dq[q] = ceq[idxc].dq[q] * (double)lsol[idx] + \
			       (double)(1-lsol[idxc]) * (ceq[idx].dq[qc] * \
			       (-A2 + q_tmp) / (A2 + q_tmp) + 2.0 * w[q] * A3 / (double)(A2 + q_tmp)) ;
		} else {
                c[idx].dq[q] = ceq[idxc].dq[q] * (double)lsol[idx] + \
			       ceq[idx].dq[qc] * (double)(1-lsol[idxc]);
		}

	      // with a spatially distributed reaction
	      #elif defined SPATIAL_REACTION
	      c[idx].dq[q] = ceq[idxc].dq[q] * (double)lsol[idx] + \
			     (double)(1-lsol[idxc]) * (ceq[idx].dq[qc] * \
			     (-lwet[idxc] + q_tmp) / (lwet[idxc] + q_tmp) \
			     + 2.0 * w[q] * A3 / (double)(lwet[idxc] + q_tmp) \
			     * (int)(lwet[idxc]!=0.)) ;

	      // no box and no spatial reaction..
	      #else

	      c[idx].dq[q] = ceq[idxc].dq[q] * (double)lsol[idx] + \
			     (double)(1-lsol[idxc]) * (ceq[idx].dq[qc] * \
			     (-A2 + q_tmp) / (A2 + q_tmp) + 2.0 * w[q] * A3 / (double)(A2 + q_tmp)) ;

	      #endif

	    // no reaction ..
            #else

            c[idx].dq[q] = ceq[idxc].dq[q] * (double)lsol[idx] + \
			   ceq[idx].dq[qc] * (double)(1-lsol[idxc]);
            #endif

	  #elif (!defined THERMAL && defined SCALAR)

            #ifdef REACTION
            double  q_tmp;
            q_tmp = (double)A1 / (3.0 * (double)(CS2*((double)TAUC-0.5))); // This is a1/(3*D)

	      // Reaction inside box
	      #ifdef REACTION_BOX

	      // Get local position of the reaction box
	      int local_xm = (int)(REBOX_XM - procoffx[world_rank]);
	      int local_xp = (int)(REBOX_XP - procoffx[world_rank]);
	      int local_ym = (int)(REBOX_YM - procoffy[world_rank]);
	      int local_yp = (int)(REBOX_YP - procoffy[world_rank]);
	      int local_zm = (int)(REBOX_ZM - procoffz[world_rank]);
	      int local_zp = (int)(REBOX_ZP - procoffz[world_rank]);

                if ((i>=local_xm+1) && (i<=local_xp) && \
                    (j>=local_ym+1) && (j<=local_yp) && \
                    (k>=local_zm+1) && (k<=local_zp)) {

                t[idx].dq[q] = teq[idxc].dq[q] * (double)lsol[idx] + \
			      (double)(1-lsol[idxc]) * (teq[idx].dq[qc] * \
			      (-A2 + q_tmp) / (A2 + q_tmp) + 2.0 * w[q] * A3 / (double)(A2 + q_tmp)) ;
		} else {
                t[idx].dq[q] = teq[idxc].dq[q] * (double)lsol[idx] + \
			       teq[idx].dq[qc] * (double)(1-lsol[idxc]);
		}

	      // with a spatially distributed reaction rate k
	      #elif (defined SPATIAL_REACTION && !defined SPATIAL_EQUILIBRIUM)
	      t[idx].dq[q] = teq[idxc].dq[q] * (double)lsol[idx] + \
			     (double)(1-lsol[idxc]) * (teq[idx].dq[qc] * \
			     (-lwet[idxc] + q_tmp) / (lwet[idxc] + q_tmp) \
			     + 2.0 * w[q] * A3 / (double)(lwet[idxc] + q_tmp) \
			     * (int)(lwet[idxc]!=0.)) ;

	      // with a spatially distributed reactive equilibrium ceq
	      #elif (defined SPATIAL_REACTION && defined SPATIAL_EQUILIBRIUM)
	      t[idx].dq[q] = teq[idxc].dq[q] * (double)lsol[idx] + \
			     (double)(1-lsol[idxc]) * (teq[idx].dq[qc] * \
			     (-(A2*(int)(lwet[idxc]>=0.)) + q_tmp) / \
			     ( (A2*(int)(lwet[idxc]>=0.)) + q_tmp) \
			     + 2.0 * w[q] * (lwet[idxc]*A2) / (double)(A2 + q_tmp) \
			     * (int)(lwet[idxc]>=0.)) ;

	      // no box ..
	      #else

              t[idx].dq[q] = teq[idxc].dq[q] * (double)lsol[idxc] + \
			     (double)(1-lsol[idxc]) * (teq[idx].dq[qc] * \
			     (-A2 + q_tmp) / (A2 + q_tmp) + 2.0 * w[q] * A3 / (double)(A2 + q_tmp)) ;

	      #endif

	    // no reaction ..
            #else

            t[idx].dq[q] = teq[idxc].dq[q] * (double)lsol[idx] + \
			   teq[idx].dq[qc] * (double)(1-lsol[idxc]);
            #endif

	  #endif

	  #ifdef CONJUGATE
	  }
	  #endif

	  #if (defined THERMAL && !defined CONJUGATE)

	    #ifdef THERMAL_TEST
	    // Thermal test
	    if (((int)prz == (int)NEZ-1) && (k==lz-1)) {
	    t[idx].dq[q] = teq[idxc].dq[q] * (double)lsol[idx] + \
			   teq[idx].dq[qc] * (double)(1-lsol[idxc]);
	    } else {
	    t[idx].dq[q] = teq[idxc].dq[q] * (double)lsol[idxc] + \
	                ( -teq[idx].dq[qc] + 2.0*teq[idxc].dq[q] ) * (double)(1-lsol[idxc]);
	    }

	    // Dirilecht or streaming
	    #else
            t[idx].dq[q] = teq[idxc].dq[q] * (double)lsol[idxc] + \
		        ( -teq[idx].dq[qc] + 2.0*teq[idxc].dq[q] ) * (double)(1-lsol[idxc]);

	    #endif

	  // In this case we perform streaming everywhere
	  #elif (defined THERMAL && defined CONJUGATE)

	    #ifdef CONJUGATE_ON_REACT
	    if (lsol[idx] != 0 || lwet[idx] != 0.) {
	    #endif

	    #ifdef INSULATE_ON_ZERO
            t[idx].dq[q] = teq[idxc].dq[q] * \
			   (double)(1-(lsol[idxc]==0)*(lwet[idxc]==0.)) + \
			   teq[idx].dq[qc] * \
			   (double)((lsol[idxc]==0)*(lwet[idxc]==0.));
	    #else
            t[idx].dq[q] = teq[idxc].dq[q];
	    #endif

	    #ifdef CONJUGATE_ON_REACT
	    }
	    #endif

	  #endif

	  #ifndef CONJUGATE
          }
	  #endif
        }
      }
    }
  }
}

#ifndef REBIRTH_STEADY

#if (defined THERMAL && defined SCALAR)
int micro_lbe_slip(int *lsol, dfdq *f, dfdq *t, dfdq *c) {
#elif (defined THERMAL || defined SCALAR)
int micro_lbe_slip(int *lsol, dfdq *f, dfdq *t) {
#else
int micro_lbe_slip(int *lsol, dfdq *f) {
#endif

#else

#if (defined THERMAL && defined SCALAR)
int micro_lbe_slip(int *lsol, dfdq *t, dfdq *c) {
#elif (defined THERMAL || defined SCALAR)
int micro_lbe_slip(int *lsol, dfdq *t) {
#endif

#endif

  int idxe=0, idxi=0, q=0, i=0, j=0, k=0;
  int iq=0, jq=0, kq=0;
  double hydro=0.0;

  #ifndef REBIRTH_STEADY

  #if defined FREESLIP_XP
  //Check if processor is involved
  if ((int)prx == (int)NEX-1) {

    for (j=0; j<lyp; j++) {
      for (k=0; k<lzp; k++) {

      idxe=IDXP(lxp-1,j,k);
      idxi=IDXP(lxp-2,j,k);

        if (lsol[idxe]==1) {

          for (q=0; q<5; q++) {
          f[idxe].dq[qsx[q+5]] = f[idxi].dq[qsx[q]];

          }
        }
      }
    }
  }
  #endif

  #if defined FREESLIP_XM
  //Check if processor is involved
  if ((int)prx == (int)0) {

    for (j=0; j<lyp; j++) {
      for (k=0; k<lzp; k++) {

      idxe=IDXP(0,j,k);
      idxi=IDXP(1,j,k);

        if (lsol[idxe]==1) {
          for (q=0; q<5; q++) {
          f[idxe].dq[qsx[q]] = f[idxi].dq[qsx[q+5]];

          }
        }
      }
    }
  }
  #endif

  #if defined FREESLIP_YP
  //Check if processor is involved
  if ((int)pry == (int)NEY-1) {

    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idxe=IDXP(i,lyp-1,k);
      idxi=IDXP(i,lyp-2,k);

        if (lsol[idxe]==1) {
          for (q=0; q<5; q++) {
          f[idxe].dq[qsy[q+5]] = f[idxi].dq[qsy[q]];

          }
        }
      }
    }
  }
  #endif

  #if defined FREESLIP_YM
  //Check if processor is involved
  if ((int)pry == (int)0) {

    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idxe=IDXP(i,0,k);
      idxi=IDXP(i,1,k);

        if (lsol[idxe]==1) {
          for (q=0; q<5; q++) {
          f[idxe].dq[qsy[q]] = f[idxi].dq[qsy[q+5]];

          }
        }
      }
    }
  }
  #endif

  #if defined FREESLIP_ZP
  //Check if processor is involved
  if ((int)prz == (int)NEZ-1) {

    for (i=0; i<lxp; i++) {
      for (j=0; j<lyp; j++) {

      idxe=IDXP(i,j,lzp-1);
      idxi=IDXP(i,j,lzp-2);

        if (lsol[idxe]==1) {
          for (q=0; q<5; q++) {
          f[idxe].dq[qsz[q+5]] = f[idxi].dq[qsz[q]];

          }
        }
      }
    }
  }
  #endif

  #if defined FREESLIP_ZM
  //Check if processor is involved
  if ((int)prz == (int)0) {

    for (i=0; i<lxp; i++) {
      for (j=0; j<lyp; j++) {

      idxe=IDXP(i,j,0);
      idxi=IDXP(i,j,1);

        if (lsol[idxe]==1) {
          for (q=0; q<5; q++) {
          f[idxe].dq[qsz[q]] = f[idxi].dq[qsz[q+5]];

          }
        }
      }
    }
  }
  #endif

  #endif

  // Cumulative
  // Concentration & Thermal equilibrium boundaries OR Freeslips
  #if (defined THERMAL || defined SCALAR)

  #if (defined FREESLIP_XM && !defined CONCENTRATION_EQ_XM)
  //Check if processor is involved
  if ((int)prx == (int)0) {

    i = 1;
    for (j=1; j<=ly; j++) {
      for (k=1; k<=lz; k++) {

      idxi=IDXP(i,j,k);

        if (lsol[idxi]==1) {
          for (q=0; q<5; q++) {

	  iq = i + (int)cx[qsx[q+5]];
	  jq = j + (int)cy[qsx[q+5]];
	  kq = k + (int)cz[qsx[q+5]];

	  idxe=IDXP(iq,jq,kq);

            if (lsol[idxe]==1) {

	    t[idxe].dq[iqsx[q+5]] = t[idxi].dq[qsx[q+5]] * 1.;

              #if (defined THERMAL && defined SCALAR)
	      c[idxe].dq[iqsx[q+5]] = c[idxi].dq[qsx[q+5]] * 1.;
	      #endif

	    }
	  }
	}
      }
    }
  }
  #endif

  #if (defined FREESLIP_YM && !defined CONCENTRATION_EQ_YM)
  //Check if processor is involved
  if ((int)pry == (int)0) {

    j = 1;
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idxi=IDXP(i,j,k);

        if (lsol[idxi]==1) {
	  for (q=0; q<5; q++) {

	  iq = i + (int)cx[qsy[q+5]];
	  jq = j + (int)cy[qsy[q+5]];
	  kq = k + (int)cz[qsy[q+5]];

	  idxe=IDXP(iq,jq,kq);

            if (lsol[idxe]==1) {

	    t[idxe].dq[iqsy[q+5]] = t[idxi].dq[qsy[q+5]] * 1.;

              #if (defined THERMAL && defined SCALAR)
	      c[idxe].dq[iqsy[q+5]] = c[idxi].dq[qsy[q+5]] * 1.;
	      #endif

	    }
	  }
	}
      }
    }
  }
  #endif

  #if (defined FREESLIP_ZM && !defined CONCENTRATION_EQ_ZM)
  //Check if processor is involved
  if ((int)prz == (int)0) {

    k = 1;
    for (j=1; j<=ly; j++) {
      for (i=1; i<=lx; i++) {

      idxi=IDXP(i,j,k);

        if (lsol[idxi]==1) {
	  for (q=0; q<5; q++) {

	  iq = i + (int)cx[qsz[q+5]];
	  jq = j + (int)cy[qsz[q+5]];
	  kq = k + (int)cz[qsz[q+5]];

	  idxe=IDXP(iq,jq,kq);

            if (lsol[idxe]==1) {

	    t[idxe].dq[iqsz[q+5]] = t[idxi].dq[qsz[q+5]] * 1.;

              #if (defined THERMAL && defined SCALAR)
	      c[idxe].dq[iqsz[q+5]] = c[idi].dq[qsz[q+5]] * 1.;
	      #endif

	    }
	  }
	}
      }
    }
  }
  #endif

  #if (defined FREESLIP_XP && !defined CONCENTRATION_EQ_XP)
  //Check if processor is involved
  if ((int)prx == (int)NEX-1) {

    i = lx;
    for (j=1; j<=ly; j++) {
      for (k=1; k<=lz; k++) {

      idxi=IDXP(i,j,k);

        if (lsol[idxi]==1) {
          for (q=0; q<5; q++) {

	  iq = i + (int)cx[qsx[q]];
	  jq = j + (int)cy[qsx[q]];
	  kq = k + (int)cz[qsx[q]];

	  idxe=IDXP(iq,jq,kq);

            if (lsol[idxe]==1) {

	    t[idxe].dq[iqsx[q]] = t[idxi].dq[qsx[q]] * 1.;

              #if (defined THERMAL && defined SCALAR)
	      c[idxe].dq[iqsx[q]] = c[idxi].dq[qsx[q]] * 1.;
	      #endif

	    }
	  }
	}
      }
    }
  }
  #endif

  #if (defined FREESLIP_YP && !defined CONCENTRATION_EQ_YP)
  //Check if processor is involved
  if ((int)pry == (int)NEY-1) {

    j = ly;
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idxi=IDXP(i,j,k);

        if (lsol[idxi]==1) {
	  for (q=0; q<5; q++) {

	  iq = i + (int)cx[qsy[q]];
	  jq = j + (int)cy[qsy[q]];
	  kq = k + (int)cz[qsy[q]];

	  idxe=IDXP(iq,jq,kq);

            if (lsol[idxe]==1) {

	    t[idxe].dq[iqsy[q]] = t[idxi].dq[qsy[q]] * 1.;

              #if (defined THERMAL && defined SCALAR)
	      c[idxe].dq[iqsy[q]] = c[idxi].dq[qsy[q]] * 1.;
	      #endif

	    }
	  }
	}
      }
    }
  }
  #endif

  #if (defined FREESLIP_ZP && !defined CONCENTRATION_EQ_ZP)
  //Check if processor is involved
  if ((int)prz == (int)NEZ-1) {

    k = lz;
    for (j=1; j<=ly; j++) {
      for (i=1; i<=lx; i++) {

      idxi=IDXP(i,j,k);

        if (lsol[idxi]==1) {
	  for (q=0; q<5; q++) {

	  iq = i + (int)cx[qsz[q]];
	  jq = j + (int)cy[qsz[q]];
	  kq = k + (int)cz[qsz[q]];

	  idxe=IDXP(iq,jq,kq);

            if (lsol[idxe]==1) {

	    t[idxe].dq[iqsz[q]] = t[idxi].dq[qsz[q]] * 1.;

              #if (defined THERMAL && defined SCALAR)
	      c[idxe].dq[iqsz[q]] = c[idxi].dq[qsz[q]] * 1.;
	      #endif

	    }
	  }
	}
      }
    }
  }
  #endif

  #endif

  // Boring 12-cases edges AS zero for:
  // * REACTION with FREESLIP or
  // * THERMAL with FREESLIP
  #if (defined REACTION || defined THERMAL)

  // 1st
  #if (defined FREESLIP_XP && defined FREESLIP_YP)
  if ( (prx==(int)NEX-1) && (pry==(int)NEY-1) ) {
    for (k=0; k<lzp; k++) {

    idxe=IDXP(lx+1,ly+1,k);
    idxi=IDXP(lx,ly,k);

    #ifndef REBIRTH_STEADY
    f[idxe].dq[10] = f[idxi].dq[7];
    #endif
    t[idxe].dq[10] = t[idxi].dq[7];

      #if (defined THERMAL && defined SCALAR)
      c[idxe].dq[10] = c[idxi].dq[7];
      #endif
    }
  }
  #endif

  // 2nd
  #if (defined FREESLIP_XP && defined FREESLIP_YM)
  if ( (prx==(int)NEX-1) && (pry==0) ) {
    for (k=0; k<lzp; k++) {

    idxe=IDXP(lx+1,0,k);
    idxi=IDXP(lx,1,k);

    #ifndef REBIRTH_STEADY
    f[idxe].dq[8] = f[idxi].dq[9];
    #endif
    t[idxe].dq[8] = t[idxi].dq[9];

      #if (defined THERMAL && defined SCALAR)
      c[idxe].dq[8] = c[idxi].dq[9];
      #endif
    }
  }
  #endif

  // 3rd
  #if (defined FREESLIP_XM && defined FREESLIP_YP)
  if ( (prx==0) && (pry==(int)NEY-1) ) {
    for (k=0; k<lzp; k++) {

    idxe=IDXP(0,ly+1,k);
    idxi=IDXP(1,ly,k);

    #ifndef REBIRTH_STEADY
    f[idxe].dq[9] = f[idxi].dq[8];
    #endif
    t[idxe].dq[9] = t[idxi].dq[8];

      #if (defined THERMAL && defined SCALAR)
      c[idxe].dq[9] = c[idxi].dq[8];
      #endif
    }
  }
  #endif

  // 4th
  #if (defined FREESLIP_XM && defined FREESLIP_YM)
  if ( (prx==0) && (pry==0) ) {
    for (k=0; k<lzp; k++) {

    idxe=IDXP(0,0,k);
    idxi=IDXP(1,1,k);

    #ifndef REBIRTH_STEADY
    f[idxe].dq[7] = f[idxi].dq[10];
    #endif
    t[idxe].dq[7] = t[idxi].dq[10];

      #if (defined THERMAL && defined SCALAR)
      c[idxe].dq[7] = c[idxi].dq[10];
      #endif
    }
  }
  #endif

  // 5th
  #if (defined FREESLIP_XP && defined FREESLIP_ZP)
  if ( (prx==(int)NEX-1) && (prz==(int)NEZ-1) ) {
    for (j=0; j<lyp; j++) {

    idxe=IDXP(lx+1,j,lz+1);
    idxi=IDXP(lx,j,lz);

    #ifndef REBIRTH_STEADY
    f[idxe].dq[14] = f[idxi].dq[11];
    #endif
    t[idxe].dq[14] = t[idxi].dq[11];

      #if (defined THERMAL && defined SCALAR)
      c[idxe].dq[14] = c[idxi].dq[11];
      #endif
    }
  }
  #endif

  // 6th
  #if (defined FREESLIP_XP && defined FREESLIP_ZM)
  if ( (prx==(int)NEX-1) && (prz==0) ) {
    for (j=0; j<lyp; j++) {

    idxe=IDXP(lx+1,j,0);
    idxi=IDXP(lx,j,1);

    #ifndef REBIRTH_STEADY
    f[idxe].dq[12] = f[idxi].dq[13];
    #endif
    t[idxe].dq[12] = t[idxi].dq[13];

      #if (defined THERMAL && defined SCALAR)
      c[idxe].dq[12] = c[idxi].dq[13];
      #endif
    }
  }
  #endif

  // 7th
  #if (defined FREESLIP_XM && defined FREESLIP_ZP)
  if ( (prx==0) && (prz==(int)NEZ-1) ) {
    for (j=0; j<lyp; j++) {

    idxe=IDXP(0,j,lz+1);
    idxi=IDXP(1,j,lz);

    #ifndef REBIRTH_STEADY
    f[idxe].dq[13] = f[idxi].dq[12];
    #endif
    t[idxe].dq[13] = t[idxi].dq[12];

      #if (defined THERMAL && defined SCALAR)
      c[idxe].dq[13] = c[idxi].dq[12];
      #endif
    }
  } 

 #endif

  // 8th
  #if (defined FREESLIP_XM && defined FREESLIP_ZM)
  if ( (prx==0) && (prz==0) ) {
    for (j=0; j<lyp; j++) {

    idxe=IDXP(0,j,0);
    idxi=IDXP(1,j,1);

    #ifndef REBIRTH_STEADY
    f[idxe].dq[11] = f[idxi].dq[14];
    #endif
    t[idxe].dq[11] = t[idxi].dq[14];

      #if (defined THERMAL && defined SCALAR)
      c[idxe].dq[11] = c[idxi].dq[14];
      #endif
    }
  }
  #endif

  // 9th
  #if (defined FREESLIP_YP && defined FREESLIP_ZP)
  if ( (pry==(int)NEY-1) && (prz==(int)NEZ-1) ) {
    for (i=0; i<lxp; i++) {

    idxe=IDXP(i,ly+1,lz+1);
    idxi=IDXP(i,ly,lz);

    #ifndef REBIRTH_STEADY
    f[idxe].dq[18] = f[idxi].dq[15];
    #endif
    t[idxe].dq[18] = t[idxi].dq[15];

      #if (defined THERMAL && defined SCALAR)
      c[idxe].dq[18] = c[idxi].dq[15];
      #endif
    }
  }
  #endif

  // 10th
  #if (defined FREESLIP_YP && defined FREESLIP_ZM)
  if ( (pry==(int)NEY-1) && (prz==0) ) {
    for (i=0; i<lxp; i++) {

    idxe=IDXP(i,ly+1,0);
    idxi=IDXP(i,ly,1);

    #ifndef REBIRTH_STEADY
    f[idxe].dq[16] = f[idxi].dq[17];
    #endif
    t[idxe].dq[16] = t[idxi].dq[17];

      #if (defined THERMAL && defined SCALAR)
      c[idxe].dq[16] = c[idxi].dq[17];
      #endif
    }
  }
  #endif

  // 11th
  #if (defined FREESLIP_YM && defined FREESLIP_ZP)
  if ( (pry==0) && (prz==(int)NEZ-1) ) {
    for (i=0; i<lxp; i++) {

    idxe=IDXP(i,0,lz+1);
    idxi=IDXP(i,1,lz);

    #ifndef REBIRTH_STEADY
    f[idxe].dq[17] = f[idxi].dq[16];
    #endif
    t[idxe].dq[17] = t[idxi].dq[16];

      #if (defined THERMAL && defined SCALAR)
      c[idxe].dq[17] = c[idxi].dq[16];
      #endif
    }
  }
  #endif

  // 12th
  #if (defined FREESLIP_YM && defined FREESLIP_ZM)
  if ( (pry==0) && (prz==0) ) {
    for (i=0; i<lxp; i++) {

    idxe=IDXP(i,0,0);
    idxi=IDXP(i,1,1);

    #ifndef REBIRTH_STEADY
    f[idxe].dq[15] = f[idxi].dq[18];
    #endif
    t[idxe].dq[15] = t[idxi].dq[18];

      #if (defined THERMAL && defined SCALAR)
      c[idxe].dq[15] = c[idxi].dq[18];
      #endif
    }
  }
  #endif

  #endif

}

#ifndef REBIRTH_STEADY

#if (defined THERMAL && defined SCALAR)
int micro_lbe_inout(int *lsol, dfdq *f, dfdq *t, dfdq *c, macro *rho, macro *ux, macro *uy, macro *uz, macro *tem, macro *con) {
#elif (defined THERMAL || defined SCALAR)
int micro_lbe_inout(int *lsol, dfdq *f, dfdq *t, macro *rho, macro *ux, macro *uy, macro *uz, macro *tem) {
#else
int micro_lbe_inout(int *lsol, dfdq *f, macro *rho, macro *ux, macro *uy, macro *uz) {
#endif

#else

#if (defined THERMAL && defined SCALAR)
int micro_lbe_inout(int *lsol, dfdq *t, dfdq *c, macro *rho, macro *ux, macro *uy, macro *uz, macro *tem, macro *con) {
#elif (defined THERMAL || defined SCALAR)
int micro_lbe_inout(int *lsol, dfdq *t, macro *rho, macro *ux, macro *uy, macro *uz, macro *tem) {
#endif

#endif

  int q=0, i=0, j=0, k=0, idx=0;
  int ckk=0, kbi=0, kai=0;
  double var=0.0;
  double hydro=0.0;
  double ctin=0., ctin2=0.;
  double ctout=0., ctout2=0.;
  double origin_rho=0.;

  // Inlet
  #if (defined DENSITY_XM || defined DENSITY_YM || defined DENSITY_ZM || \
       defined VELOCITY_XM || defined VELOCTITY_YM || defined VELOCITY_ZM || \
       defined CONCENTRATION_XM || defined CONCENTRATION_HALF_XM || \
       defined CONCENTRATION_YM || defined CONCENTRATION_HALF_YM || \
       defined CONCENTRATION_ZM || defined CONCENTRATION_HALF_ZM )

  #if (defined DENSITY_XM || defined VELOCITY_XM || \
       defined CONCENTRATION_XM || defined CONCENTRATION_HALF_XM )
  //Check if processor is involved
  if ((int)prx == (int)0) {

    i = 1;
    for (j=1; j<=ly; j++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_YM || defined VELOCITY_YM || \
         defined CONCENTRATION_YM || defined CONCENTRATION_HALF_YM )
  //Check if processor is involved
  if ((int)pry == (int)0) {

    j = 1;
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_ZM || defined VELOCITY_ZM || \
         defined CONCENTRATION_ZM || defined CONCENTRATION_HALF_ZM )
  //Check if processor is involved
  if ((int)prz == (int)0) {

    k = 1;
    for (j=1; j<=ly; j++) {
      for (i=1; i<=lx; i++) {

  #endif

      idx=IDXP(i,j,k);
      var = 0.0;

      // set the density
      origin_rho = rho[idx]*(double)lsol[idx]+(double)(1-lsol[idx])*RHO0;

	#ifndef CONJUGATE
        if (lsol[idx]==1) {
	#endif

        #if (defined DENSITY_XM || defined DENSITY_YM || defined DENSITY_ZM || \
	     defined VELOCITY_XM || defined VELOCITY_YM || defined VELOCITY_ZM)

          // Momentum correction
          for (q=0; q<NQ; q++) {

          #if (defined DENSITY_XM || defined VELOCITY_XM)
          ckk = cx[q];
          #elif (defined DENSITY_YM || defined VELOCITY_YM)
          ckk = cy[q];
          #elif (defined DENSITY_ZM || defined VELOCITY_ZM)
          ckk = cz[q];
          #endif

          kbi = (int)(1-(int)pow(ckk*ckk,0.5));
          kai = (int)(1-(int)floor(1.+(double)(ckk/2.)));
          var += (double)kbi*f[idx].dq[q] + 2.*(double)kai*f[idx].dq[q];

          }

        // Hydrodynamic velocity
        #ifdef DENSITY_XM
        ux[idx] = +1.0-(double)(var/DIN);
        #elif defined DENSITY_YM
        uy[idx] = +1.0-(double)(var/DIN);
        #elif defined DENSITY_ZM
        uz[idx] = +1.0-(double)(var/DIN);

        // Density from constant hydrodynamic velocity 
        #elif (defined VELOCITY_XM || defined VELOCITY_YM || defined VELOCITY_ZM)
        rho[idx] = (double)(var/(+1.0-VIN));
        #endif

          // Distribution functions correction [Zou He PoF 1997]
          for (q=0; q<5; q++) {

          #ifdef DENSITY_XM
          f[idx].dq[qsx[q]] = f[idx].dq[iq[qsx[q]]] + 2.*w[qsx[q]]/(double)CS2*DIN*ux[idx];
          #elif defined DENSITY_YM
          f[idx].dq[qsy[q]] = f[idx].dq[iq[qsy[q]]] + 2.*w[qsy[q]]/(double)CS2*DIN*uy[idx];
          #elif defined DENSITY_ZM
          f[idx].dq[qsz[q]] = f[idx].dq[iq[qsz[q]]] + 2.*w[qsz[q]]/(double)CS2*DIN*uz[idx];

          #elif defined VELOCITY_XM
          f[idx].dq[qsx[q]] = f[idx].dq[iq[qsx[q]]] + 2.*w[qsx[q]]/(double)CS2*rho[idx]*VIN;
          #elif defined VELOCITY_YM
          f[idx].dq[qsy[q]] = f[idx].dq[iq[qsy[q]]] + 2.*w[qsy[q]]/(double)CS2*rho[idx]*VIN;
          #elif defined VELOCITY_ZM
          f[idx].dq[qsz[q]] = f[idx].dq[iq[qsz[q]]] + 2.*w[qsz[q]]/(double)CS2*rho[idx]*VIN;

          #endif

          }

	#endif

        #if (defined CONCENTRATION_XM || defined CONCENTRATION_HALF_XM || \
             defined CONCENTRATION_YM || defined CONCENTRATION_HALF_YM || \
             defined CONCENTRATION_ZM || defined CONCENTRATION_HALF_ZM )

          for (q=0; q<NQ; q++) {

	  // Hydrodynamic contribution
	  hydro = origin_rho * \
		  (1.+(double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
		  (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
		  (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));

          #if (defined THERMAL && defined SCALAR)

	    #ifdef CONCENTRATION_XM

	      #ifdef RANDOM_INPUT
	      // Access global random vector with local index h
	      c[idx].dq[q] = w[q]*(double)randin[h] * hydro;
	      #else
	      c[idx].dq[q] = w[q]*(double)CIN * hydro;
	      #endif

	    #elif defined CONCENTRATION_HALF_XM // over z
	    int kg = (int)procoffz[world_rank] + (int)k;

	      if (kg <= (int)(NZ/2)) {
	      c[idx].dq[q] = w[q]*(double)CIN * hydro;
	      } else {
	      c[idx].dq[q] = w[q]*(double)CIN2 * hydro; 
	      }

	    #endif

	    #ifdef CONCENTRATION_YM

	      #ifdef RANDOM_INPUT
	      // Access global random vector with local index h
	      c[idx].dq[q] = w[q]*(double)randin[h] * hydro;
	      #else
	      c[idx].dq[q] = w[q]*(double)CIN * hydro;
	      #endif

	    #elif defined CONCENTRATION_HALF_YM // over x
	    int kg = (int)procoffx[world_rank] + (int)i;

	      if (kg <= (int)(NX/2)) {
	      c[idx].dq[q] = w[q]*(double)CIN * hydro;
	      } else {
	      c[idx].dq[q] = w[q]*(double)CIN2 * hydro;
	      }

	    #endif

	    #ifdef CONCENTRATION_ZM

	      #ifdef RANDOM_INPUT
	      // Access global random vector with local index h
	      c[idx].dq[q] = w[q]*(double)randin[h] * hydro;
	      #else
	      c[idx].dq[q] = w[q]*(double)CIN * hydro;
	      #endif

	    #elif defined CONCENTRATION_HALF_ZM // over y
	    int kg = (int)procoffy[world_rank] + (int)j;

	      if (kg <= (int)(NY/2)) {
	      c[idx].dq[q] = w[q]*(double)CIN * hydro;
	      } else {
	      c[idx].dq[q] = w[q]*(double)CIN2 * hydro;
	      }

	    #endif

	  #endif

          #if ((defined SCALAR && !defined THERMAL) || (defined THERMAL && !defined SCALAR) \
	    || (defined SCALAR && defined THERMAL && defined COPY_SCA_THERM_BCS))

	    // Copy scalar to thermal bcs
	    #if defined COPY_SCA_THERM_BCS
	    ctin = CTIN;
	    ctin2 = CTIN2;
	    #else

	      #ifdef RANDOM_INPUT
	      // Access global random vector with local index h
	      ctin = (double)randin[h];
	      #else
	      ctin = CIN;
	      #endif

	    ctin2 = CIN2;
	    #endif

	    #ifdef CONCENTRATION_XM
            t[idx].dq[q] = w[q] * ctin * hydro;

	    #elif defined CONCENTRATION_HALF_XM
	    int kg = (int)procoffz[world_rank] + (int)k;

	      if (kg <= (int)(NZ/2)) {
              t[idx].dq[q] = w[q] * ctin * hydro;
	      } else {
              t[idx].dq[q] = w[q] * ctin2 * hydro;
	      }

	    #endif 

	    #ifdef CONCENTRATION_YM
            t[idx].dq[q] = w[q] * ctin * hydro;

	    #elif defined CONCENTRATION_HALF_YM // over x
	    int kg = (int)procoffx[world_rank] + (int)i;

	      if (kg <= (int)(NX/2)) {
              t[idx].dq[q] = w[q] * ctin * hydro;
	      } else {
              t[idx].dq[q] = w[q] * ctin2 * hydro;
	      }

	    #endif 

	    #ifdef CONCENTRATION_ZM
            t[idx].dq[q] = w[q] * ctin * hydro;

	    #elif defined CONCENTRATION_HALF_ZM // over y
	    int kg = (int)procoffy[world_rank] + (int)j;

	      if (kg <= (int)(NY/2)) {
              t[idx].dq[q] = w[q] * ctin * hydro;
	      } else {
              t[idx].dq[q] = w[q] * ctin2 * hydro;
	      }

	    #endif 

	  #endif

          }

	#endif

	#ifndef CONJUGATE
        }
	#endif
      }
    }
  }
  #endif

  // Outlet
  #if (defined DENSITY_XP || defined DENSITY_YP || defined DENSITY_ZP || \
       defined VELOCITY_XP || defined VELOCTITY_YP || defined VELOCITY_ZP || \
       defined CONCENTRATION_XP || defined CONCENTRATION_HALF_XP || \
       defined CONCENTRATION_YP || defined CONCENTRATION_HALF_YP || \
       defined CONCENTRATION_ZP || defined CONCENTRATION_HALF_ZP )

  #if (defined DENSITY_XP || defined VELOCITY_XP || \
       defined CONCENTRATION_XP || defined CONCENTRATION_HALF_XP )
  //Check if processor is involved
  if ((int)prx == (int)NEX-1) {

    i = lx;
    for (j=1; j<=ly; j++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_YP || defined VELOCITY_YP || \
         defined CONCENTRATION_YP || defined CONCENTRATION_HALF_YP )
  //Check if processor is involved
  if ((int)pry == (int)NEY-1) {

    j = ly;
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_ZP || defined VELOCITY_ZP || \
         defined CONCENTRATION_ZP || defined CONCENTRATION_HALF_ZP )
  //Check if processor is involved
  if ((int)prz == (int)NEZ-1) {

    k = lz;
    for (j=1; j<=ly; j++) {
      for (i=1; i<=lx; i++) {

  #endif

      idx=IDXP(i,j,k);
      var = 0.0;

      // set density
      origin_rho = rho[idx]*(double)lsol[idx]+(double)(1-lsol[idx])*RHO0;

	#ifndef CONJUGATE
        if (lsol[idx]==1) {
	#endif

        #if (defined DENSITY_XP || defined DENSITY_YP || defined DENSITY_ZP ||\
	     defined VELOCITY_XP || defined VELOCITY_YP || defined VELOCITY_ZP)

          // Momentum correction
          for (q=0; q<NQ; q++) {

          #if (defined DENSITY_XP || defined VELOCITY_XP)
          ckk = cx[q];
          #elif (defined DENSITY_YP || defined VELOCITY_YP)
          ckk = cy[q];
          #elif (defined DENSITY_ZP || defined VELOCITY_ZP)
          ckk = cz[q];
          #endif

          kbi = (int)(1-(int)pow(ckk*ckk,0.5));
          kai = (int)(1-(int)floor(1.-(double)(ckk/2.)));
          var += (double)kbi*f[idx].dq[q] + 2.*(double)kai*f[idx].dq[q];

          }

        // Hydrodynamic velocity
        #ifdef DENSITY_XP
        ux[idx] = -1.0+(double)(var/DOUT);
        #elif defined DENSITY_YP
        uy[idx] = -1.0+(double)(var/DOUT);
        #elif defined DENSITY_ZP
        uz[idx] = -1.0+(double)(var/DOUT);

        // Density from constant hydrodynamic velocity 
        #elif (defined VELOCITY_XP || defined VELOCITY_YP || defined VELOCITY_ZP)
        rho[idx] = (double)(var/(+1.0+VOUT));
        #endif

          // Distribution functions correction [Zou He PoF 1997]
          for (q=0; q<5; q++) {

          #ifdef DENSITY_XP
          f[idx].dq[iq[qsx[q]]] = f[idx].dq[qsx[q]] - 2*w[qsx[q]]/(double)CS2*DOUT*ux[idx];
          #elif defined DENSITY_YP
          f[idx].dq[iq[qsy[q]]] = f[idx].dq[qsy[q]] - 2*w[qsy[q]]/(double)CS2*DOUT*uy[idx];
          #elif defined DENSITY_ZP
          f[idx].dq[iq[qsz[q]]] = f[idx].dq[qsz[q]] - 2*w[qsz[q]]/(double)CS2*DOUT*uz[idx];

          #elif defined VELOCITY_XP
          f[idx].dq[qsx[q]] = f[idx].dq[iq[qsx[q]]] + 2.*w[qsx[q]]/(double)CS2*rho[idx]*VOUT;
          #elif defined VELOCITY_YP
          f[idx].dq[qsy[q]] = f[idx].dq[iq[qsy[q]]] + 2.*w[qsy[q]]/(double)CS2*rho[idx]*VOUT;
          #elif defined VELOCITY_ZP
          f[idx].dq[qsz[q]] = f[idx].dq[iq[qsz[q]]] + 2.*w[qsz[q]]/(double)CS2*rho[idx]*VOUT;

          #endif

          }

	#endif

        #if (defined CONCENTRATION_XP || defined CONCENTRATION_HALF_XP || \
             defined CONCENTRATION_YP || defined CONCENTRATION_HALF_YP || \
             defined CONCENTRATION_ZP || defined CONCENTRATION_HALF_ZP )

          for (q=0; q<NQ; q++) {

	  // Hydrodynamic contribution
	  hydro = origin_rho * \
		  (1.+(double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
		  (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
		  (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));

          #if (defined THERMAL && defined SCALAR)

	    #ifdef CONCENTRATION_XP
	    c[idx].dq[q] = w[q]*(double)COUT * hydro;

	    #elif defined CONCENTRATION_HALF_XP // over z
	    int kg = (int)procoffz[world_rank] + (int)k;

	      if (kg <= (int)(NZ/2)) {
	      c[idx].dq[q] = w[q]*(double)COUT * hydro;
	      } else {
	      c[idx].dq[q] = w[q]*(double)COUT2 * hydro;
	      }

	    #endif

	    #ifdef CONCENTRATION_YP
	    c[idx].dq[q] = w[q]*(double)COUT * hydro;

	    #elif defined CONCENTRATION_HALF_YP // over x
	    int kg = (int)procoffx[world_rank] + (int)i;

	      if (kg <= (int)(NX/2)) {
	      c[idx].dq[q] = w[q]*(double)COUT * hydro;
	      } else {
	      c[idx].dq[q] = w[q]*(double)COUT2 * hydro;
	      }

	    #endif

	    #ifdef CONCENTRATION_ZP
	    c[idx].dq[q] = w[q]*(double)COUT * hydro;

	    #elif defined CONCENTRATION_HALF_ZP // over y
	    int kg = (int)procoffy[world_rank] + (int)j;

	      if (kg <= (int)(NY/2)) {
	      c[idx].dq[q] = w[q]*(double)COUT * hydro;
	      } else {
	      c[idx].dq[q] = w[q]*(double)COUT2 * hydro;
	      }

	    #endif

	  #endif

          #if ((defined SCALAR && !defined THERMAL) || (defined THERMAL && !defined SCALAR) \
	    || (defined SCALAR && defined THERMAL && defined COPY_SCA_THERM_BCS))

	    // Copy scalar to thermal bcs
	    #if defined COPY_SCA_THERM_BCS
	    ctout = CTOUT;
	    ctout2 = CTOUT2;
	    #else
	    ctout = COUT;
	    ctout2 = COUT2;
	    #endif

	    #ifdef CONCENTRATION_XP
	    t[idx].dq[q] = w[q] * ctout * hydro;

	    #elif defined CONCENTRATION_HALF_XP // over z
	    int kg = (int)procoffz[world_rank] + (int)k;

	      if (kg <= (int)(NZ/2)) {
	      t[idx].dq[q] = w[q] * ctout * hydro;
	      } else {
	      t[idx].dq[q] = w[q] * ctout2 * hydro;
	      }

	    #endif 

	    #ifdef CONCENTRATION_YP
	    t[idx].dq[q] = w[q] * ctout * hydro;

	    #elif defined CONCENTRATION_HALF_YP // over x
	    int kg = (int)procoffx[world_rank] + (int)i;

	      if (kg <= (int)(NX/2)) {
	      t[idx].dq[q] = w[q] * ctout * hydro;
	      } else {
	      t[idx].dq[q] = w[q] * ctout2 * hydro;
	      }

	    #endif 

	    #ifdef CONCENTRATION_ZP
	    t[idx].dq[q] = w[q] * ctout * hydro;

	    #elif defined CONCENTRATION_HALF_ZP // over y
	    int kg = (int)procoffy[world_rank] + (int)j;

	      if (kg <= (int)(NY/2)) {
	      t[idx].dq[q] = w[q] * ctout * hydro;
	      } else {
	      t[idx].dq[q] = w[q] * ctout2* hydro;
	      }

	    #endif 

	  #endif

          }

	#endif

	#ifndef CONJUGATE
        }
	#endif
      }
    }
  }

  #endif

  // Cumulative. Based on Huang et al 2011 BC5 J.Phys.A with non-zero vel. 
  // Concentration & Thermal equilibrium boundaries OR Freeslips
  #if (defined THERMAL || defined SCALAR)

  int idx1=0, idx2=0, idx3=0;

  #if (defined CONCENTRATION_EQ_XM)
  //Check if processor is involved
  if ((int)prx == (int)0) {

    i = 1;
    for (j=1; j<=ly; j++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);
      idx1 = IDXP(i+1,j,k);

      // set the density
      origin_rho = rho[idx]*(double)lsol[idx]+(double)(1-lsol[idx])*RHO0;

        #ifndef CONJUGATE
        if (lsol[idx]==1) {
	#endif

          for (q=0; q<NQ; q++) {

	  // Hydrodynamic contribution
	  hydro = origin_rho*(1.+(double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
		  (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
		  (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));

          #if (defined THERMAL && defined SCALAR)
	  c[idx].dq[q] = con[idx1]*w[q] * hydro;

	    #ifdef COPY_SCA_THERM_BCS
	    t[idx].dq[q] = tem[idx1]*w[q] * hydro;
	    #endif

	  #elif (defined SCALAR || defined THERMAL)
	  t[idx].dq[q] = tem[idx1]*w[q] * hydro;
	  #endif
	  }

        #ifndef CONJUGATE
	}
	#endif
      }
    }
  }
  #endif

  #if (defined CONCENTRATION_EQ_YM)
  //Check if processor is involved
  if ((int)pry == (int)0) {

    j = 1;
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);
      idx2 = IDXP(i,j+1,k);

      // set the density
      origin_rho = rho[idx]*(double)lsol[idx]+(double)(1-lsol[idx])*RHO0;

        #ifndef CONJUGATE
        if (lsol[idx]==1) {
	#endif

          for (q=0; q<NQ; q++) {

	  // Hydrodynamic contribution
	  hydro = origin_rho*(1.+(double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
		  (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
		  (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));

          #if (defined THERMAL && defined SCALAR)
	  c[idx].dq[q] = con[idx2]*w[q] * hydro;

	    #ifdef COPY_SCA_THERM_BCS
	    t[idx].dq[q] = tem[idx2]*w[q] * hydro;
	    #endif

	  #elif (defined SCALAR || defined THERMAL)
	  t[idx].dq[q] = tem[idx2]*w[q] * hydro;
	  #endif
	  }

        #ifndef CONJUGATE
	}
	#endif
      }
    }
  }
  #endif

  #if (defined CONCENTRATION_EQ_ZM)
  //Check if processor is involved
  if ((int)prz == (int)0) {

    k = 1;
    for (j=1; j<=ly; j++) {
      for (i=1; i<=lx; i++) {

      idx=IDXP(i,j,k);
      idx3 = IDXP(i,j,k+1);

      // set the density
      origin_rho = rho[idx]*(double)lsol[idx]+(double)(1-lsol[idx])*RHO0;

        #ifndef CONJUGATE
        if (lsol[idx]==1) {
	#endif

          for (q=0; q<NQ; q++) {

	  // Hydrodynamic contribution
	  hydro = origin_rho*(1.+(double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
		  (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
		  (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));

          #if (defined THERMAL && defined SCALAR)
	  c[idx].dq[q] = con[idx3]*w[q] * hydro;

	    #ifdef COPY_SCA_THERM_BCS
	    t[idx].dq[q] = tem[idx3]*w[q] * hydro;
	    #endif

	  #elif (defined SCALAR || defined THERMAL)
	  t[idx].dq[q] = tem[idx3]*w[q] * hydro;
	  #endif
	  }

        #ifndef CONJUGATE
	}
	#endif
      }
    }
  }
  #endif

  #if (defined CONCENTRATION_EQ_XP)
  //Check if processor is involved
  if ((int)prx == (int)NEX-1) {

    i = lx;
    for (j=1; j<=ly; j++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);
      idx1 = IDXP(i-1,j,k);

      // set the density
      origin_rho = rho[idx]*(double)lsol[idx]+(double)(1-lsol[idx])*RHO0;

        #ifndef CONJUGATE
        if (lsol[idx]==1) {
	#endif

          for (q=0; q<NQ; q++) {

	  // Hydrodynamic contribution
	  hydro = origin_rho*(1.+(double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
		  (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
		  (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));

          #if (defined THERMAL && defined SCALAR)
	  c[idx].dq[q] = con[idx1]*w[q] * hydro;

	    #ifdef COPY_SCA_THERM_BCS
	    t[idx].dq[q] = tem[idx1]*w[q] * hydro;
	    #endif

	  #elif (defined SCALAR || defined THERMAL)
	  t[idx].dq[q] = tem[idx1]*w[q] * hydro;
	  #endif
	  }

	#ifndef CONJUGATE
	}
	#endif
      }
    }
  }
  #endif

  #if (defined CONCENTRATION_EQ_YP)
  //Check if processor is involved
  if ((int)pry == (int)NEY-1) {

    j = ly;
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);
      idx2 = IDXP(i,j-1,k);

      // set the density
      origin_rho = rho[idx]*(double)lsol[idx]+(double)(1-lsol[idx])*RHO0;

        #ifndef CONJUGATE
        if (lsol[idx]==1) {
	#endif

          for (q=0; q<NQ; q++) {

	  // Hydrodynamic contribution
	  hydro = origin_rho*(1.+(double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
		  (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
		  (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));

          #if (defined THERMAL && defined SCALAR)
	  c[idx].dq[q] = con[idx2]*w[q] * hydro;

	    #ifdef COPY_SCA_THERM_BCS
	    t[idx].dq[q] = tem[idx2]*w[q] * hydro;
	    #endif

	  #elif (defined SCALAR || defined THERMAL)
	  t[idx].dq[q] = tem[idx2]*w[q] * hydro;
	  #endif
	  }

        #ifndef CONJUGATE
	}
	#endif
      }
    }
  }
  #endif

  #if (defined CONCENTRATION_EQ_ZP)
  //Check if processor is involved
  if ((int)prz == (int)NEZ-1) {

    k = lz;
    for (j=1; j<=ly; j++) {
      for (i=1; i<=lx; i++) {

      idx=IDXP(i,j,k);
      idx3 = IDXP(i,j,k-1);

      // set the density
      origin_rho = rho[idx]*(double)lsol[idx]+(double)(1-lsol[idx])*RHO0;

        #ifndef CONJUGATE
        if (lsol[idx]==1) {
	#endif

          for (q=0; q<NQ; q++) {

	  // Hydrodynamic contribution
	  hydro = origin_rho*(1.+(double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
		  (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
		  (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));

          #if (defined THERMAL && defined SCALAR)
	  c[idx].dq[q] = con[idx3]*w[q] * hydro;

	    #ifdef COPY_SCA_THERM_BCS
	    t[idx].dq[q] = tem[idx3]*w[q] * hydro;
	    #endif

	  #elif (defined SCALAR || defined THERMAL)
	  t[idx].dq[q] = tem[idx3]*w[q] * hydro;
	  #endif
	  }

        #ifndef CONJUGATE
	}
	#endif
      }
    }
  }
  #endif

  #endif

}

int micro_lbe_inner_bcs (int *lsol, dfdq *f, int **innerCells, int innerCellsNr, int iter) {

  int i=0, idx=0, q=0;
  int iC=0, jC=0, kC=0;
  int ic=0, jc=0, kc=0, qc=0, idxc=0;

  for (i = 0; i < innerCellsNr; ++i) {

  iC = innerCells[i][0];
  jC = innerCells[i][1];
  kC = innerCells[i][2];
  idx = IDXP(iC, jC, kC);

  // Apply now time-dependent BCs

    // Acoustic pulse
    #ifdef ACOUSTIC_PULSE
    for (q=0; q<NQ; q++)
    f[idx].dq[q] = (double)( (RHO0+AWAVE*(double)sin(2.0*M_PI*iter*(double)(FWAVE/MAX_ITER))) * w[q] );
    #endif

    // Example of inner bcs correction
    #ifdef DOSOMETHING
    for (q=1; q<NQ; q++) {
    ic=(int)iC+cx[q];
    jc=(int)jC+cy[q];
    kc=(int)kC+cz[q];

    qc=(int)iq[q];
    idxc= IDXP(ic,jc,kc);

    // Do something

    }

    #endif

  }
}

int micro_lbe_chk(dfdq *f, int *lsol, char *flag) {

  int idx=0, i=0, j=0, k=0, q=0;
  FILE *fileout;
  char filename[128];

  sprintf(filename,"out/%s.chk.%d.dat",flag,world_rank);
  fileout = fopen(filename,"w");

  for (j=0; j<lyp; j++) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      q = 0;
      idx = IDXP(i,j,k);
      fprintf(fileout,"%d %d %d %g %d\n",i,j,k,f[idx].dq[q],lsol[idx]);

      }
    }
  }

  fclose(fileout);

}
