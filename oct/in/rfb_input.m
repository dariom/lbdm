function dPdL=rfb_input(REfore)

%% Spaziante

xn=0.000008; % [m/DLB]
cv=1.6;         % [mol/l] 
Icell=110;      % [C/s=A] cell current
F=96000;        % [C/mol] Faraday constant
SOCmin=0.25;    % [%/100]
SOCmax=0.95;    % [%/100]
%a_factor=afA;  % [1]
N_membr=11;     % [1]
N_chann=6;      % [1]

% ---> calc [m^3/s]

Qcell=  Icell/(cv*F*SOCmin)/1000;

% Gas channel inputs (Vanadium)
Qpm=Qcell/N_membr;
rho_h0=1350; mu_h0=5*10^-3;
pory_pred=0.9; Apm=0.09*0.003*pory_pred;
upm_av=Qpm/Apm; df0=50*10**(-6);
Re=upm_av*df0*rho_h0/mu_h0,
df=df0/xn,

%%%% for Reynolds=0.1
if (REfore==0.1);
dPdL=300*Qpm
end;

%% Proxima
