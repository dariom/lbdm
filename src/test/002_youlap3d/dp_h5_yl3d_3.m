load('dump.CONV.h5');

rho=LBdm.rho;
rr=rho(20,:,20);

pp=rr.*1/3-5.5/6.*(1-exp(-rr)).**2;

R=11;
DP=(pp(20)-pp(1));

M=[2/R,DP];

save('dp-radius.dat','-ascii','-append','M');
