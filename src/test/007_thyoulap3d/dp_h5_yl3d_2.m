load('dump.CONV.h5');

rho=LBdm.rho;
rr=rho(20,:,20);
tem=LBdm.tem;
tt=tem(20,:,20);

pp=rr.*tt-9.5/6.*(exp(-1./rr)).**2;

R=10;
DP=(pp(20)-pp(1));

M=[2/R,DP];

save('dp-radius.dat','-ascii','-append','M');
