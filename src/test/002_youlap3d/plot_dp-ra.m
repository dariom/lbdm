dat=load('dp-radius.dat');

graphics_toolkit ("gnuplot");
setenv ("GNUTERM", "qt");

figure, 
plot(dat(:,1),dat(:,2),'o');
hold on, 
plot([0,dat(1,1)],[0,dat(1,2)],'-');

saveas(gcf,'plot_dp-ra.png','png');
