#!/bin/bash -e

echo " "
echo "#lbdm# Test 011 Entropy 1d"
echo " "

git checkout user-defined.c

if [ -f "DT.dat" ]; then
rm DT.dat 
fi

if [ -f "latentheat.dat" ]; then
rm latentheat.dat 
fi

if [ -f "maxwell.png" ]; then
rm maxwell.png 
fi

if [ -f "maxwell1.png" ]; then
rm maxwell1.png 
fi

if [ -f "maxwell2.png" ]; then
rm maxwell2.png 
fi

if [ -f "entropic.png" ]; then
rm entropic.png 
fi

if ls *.h5 1> /dev/null 2>&1; then
rm *.h5
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

octave create_bin-txt.m

cp entro1d.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 1 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/011_entropy1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_entro1d.m

cp dump.CONV.h5 dump.G95.h5

###########

sed -i -e 's/define NAME .*/define NAME "ENTRO1D2"/g' user-defined.c
sed -i -e 's/TINIT .*/TINIT 0.202/g' user-defined.c
sed -i -e 's/RHO2 .*/RHO2 4.9683/g' user-defined.c
sed -i -e 's/RHO0 .*/RHO0 0.36/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 1 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/011_entropy1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_entro1d.m

###########

sed -i -e 's/define NAME .*/define NAME "ENRTO1D3"/g' user-defined.c
sed -i -e 's/TINIT .*/TINIT 0.204/g' user-defined.c
sed -i -e 's/RHO2 .*/RHO2 4.91245/g' user-defined.c
sed -i -e 's/RHO0 .*/RHO0 0.36/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 1 ./lbdm); then
echo "Run (3) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/011_entropy1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_entro1d.m

###########

sed -i -e 's/define NAME .*/define NAME "ENRTO1D4"/g' user-defined.c
sed -i -e 's/TINIT .*/TINIT 0.206/g' user-defined.c
sed -i -e 's/RHO2 .*/RHO2 4.8575/g' user-defined.c
sed -i -e 's/RHO0 .*/RHO0 0.36/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 1 ./lbdm); then
echo "Run (4) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/011_entropy1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_entro1d.m

###########

sed -i -e 's/define NAME .*/define NAME "ENRTO1D5"/g' user-defined.c
sed -i -e 's/TINIT .*/TINIT 0.208/g' user-defined.c
sed -i -e 's/RHO2 .*/RHO2 4.8042/g' user-defined.c
sed -i -e 's/RHO0 .*/RHO0 0.36/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 1 ./lbdm); then
echo "Run (5) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/011_entropy1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_entro1d.m

###########

sed -i -e 's/define NAME .*/define NAME "ENRTO1D6"/g' user-defined.c
sed -i -e 's/TINIT .*/TINIT 0.210/g' user-defined.c
sed -i -e 's/RHO2 .*/RHO2 4.7510/g' user-defined.c
sed -i -e 's/RHO0 .*/RHO0 0.36/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 1 ./lbdm); then
echo "Run (6) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/011_entropy1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_entro1d.m

###########

sed -i -e 's/define NAME .*/define NAME "ENTRO1D7"/g' user-defined.c
sed -i -e 's/GG .*/GG 4.0/g' user-defined.c
sed -i -e 's/TINIT .*/TINIT 0.150/g' user-defined.c
sed -i -e 's/RHO2 .*/RHO2 2.22/g' user-defined.c
sed -i -e 's/RHO0 .*/RHO0 0.50/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 1 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/011_entropy1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_entro1d.m

cp dump.CONV.h5 dump.G40.h5

###########

sed -i -e 's/define NAME .*/define NAME "ENTRO1D8"/g' user-defined.c
sed -i -e 's/TINIT .*/TINIT 0.152/g' user-defined.c
sed -i -e 's/RHO2 .*/RHO2 2.11/g' user-defined.c
sed -i -e 's/RHO0 .*/RHO0 0.50/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 1 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/011_entropy1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_entro1d.m

###########

sed -i -e 's/define NAME .*/define NAME "ENRTO1D9"/g' user-defined.c
sed -i -e 's/TINIT .*/TINIT 0.154/g' user-defined.c
sed -i -e 's/RHO2 .*/RHO2 1.98/g' user-defined.c
sed -i -e 's/RHO0 .*/RHO0 0.50/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 1 ./lbdm); then
echo "Run (3) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/011_entropy1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_entro1d.m

###########

sed -i -e 's/define NAME .*/define NAME "ENRTO1D10"/g' user-defined.c
sed -i -e 's/TINIT .*/TINIT 0.156/g' user-defined.c
sed -i -e 's/RHO2 .*/RHO2 1.78/g' user-defined.c
sed -i -e 's/RHO0 .*/RHO0 0.50/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 1 ./lbdm); then
echo "Run (4) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/011_entropy1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_entro1d.m

###########

sed -i -e 's/define NAME .*/define NAME "ENRTO1D11"/g' user-defined.c
sed -i -e 's/TINIT .*/TINIT 0.158/g' user-defined.c
sed -i -e 's/RHO2 .*/RHO2 1.58/g' user-defined.c
sed -i -e 's/RHO0 .*/RHO0 0.50/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 1 ./lbdm); then
echo "Run (5) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/011_entropy1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_entro1d.m

###########

sed -i -e 's/define NAME .*/define NAME "ENRTO3D12"/g' user-defined.c
sed -i -e 's/TINIT .*/TINIT 0.160/g' user-defined.c
sed -i -e 's/RHO2 .*/RHO2 1.38/g' user-defined.c
sed -i -e 's/RHO0 .*/RHO0 0.50/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 1 ./lbdm); then
echo "Run (6) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/011_entropy1d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave dp_h5_entro1d.m

##########

##########

octave entropy.m

gamma1=`awk 'NR==1 {printf ($1) }' latentheat.dat`
gamma2=`awk 'NR==1 {printf ($2) }' latentheat.dat`
echo ""
echo "Latent Heat(s): " $gamma1 $gamma2
echo ""

octave maxwell2.m 

awk 'NR>=1&&NR<=6 {if (($1*$1)^0.5<=0.33) print "ok"; else print "no" }' error.dat > errchk.dat
awk 'NR>=7&&NR<=12 {if (($1*$1)^0.5<=0.02) print "ok"; else print "no" }' error.dat >> errchk.dat
awk 'NR>=13&&NR<=18 {if (($1*$1)^0.5<=0.03) print "ok"; else print "no" }' error.dat >> errchk.dat
awk 'NR>=19&&NR<=24 {if (($1*$1)^0.5<=0.01) print "ok"; else print "no" }' error.dat >> errchk.dat

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/entro1d.bin.txt

# clean local test
cd test/011_entropy1d/
git checkout user-defined.c
rm *.txt

########### 1) Check NaN 

for file in DT.dat error.dat errchk.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 24 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
