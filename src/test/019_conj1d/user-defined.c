/* ! ATTENTION do not delete or add strange rows !
Just either comment or uncomment the "Capabilities" 
and replace values of "Macros" with the desired ones */

/* ******************* */
/*  Capabilities here  */
/* ******************* */

#define DUMPHDF5
#define INASCII

#define DPDLX

#define THERMAL
#define CONJUGATE
#define SOURCE

#define FREESLIP_XP
#define CONCENTRATION_XM
#define CONCENTRATION_XP

//#define REBIRTH
//#define REBIRTH_STEADY
//#define REBIRTH_0TTO1T

#define CONJUGATE_CORR

/* ******************* */
/*  Macros for folks   */
/* ******************* */

// Name identifiers
#define NAME "CONJ1D"
#define GEOM "conj1d.bin.txt"

// N-PROC
#define NP 2

// N-SIZE
#define NX 100
#define NY 1
#define NZ 1

// N-PROC PER EDGE
#define NEX 2
#define NEY 1
#define NEZ 1

// INIT DENSITY & dP/dL
#define RHO0 1.0
#define DPDL 0.0

// Iteration max,chk,out
#define MAX_ITER 16000
#define CHK_ITER 1
#define OUT_ITER 800

// Q-speeds and t-relax
#define NQ 19
#define TAU 1.0

// RESIDUAL convergence
#define RESIDUAL -1
#define CLENGTH 8

// TWOPHASE parameters
#define GG 4.0
#define RHO2 2.22
#define RHOZ 1.0
#define RHOWALL 0.0

// THERMAL parameters
#define TAUT 1.5
#define TINIT 0.0
#define TWALL 1.0
#define TSUB  0.2

// CONJUGATE parameters
#define TAU2 1.0
#define CP1 1.0
#define CP2 1.0

// SCALAR parameters
#define TAUC 1.0
#define CINIT 1.0
#define CINIT2 10.0
#define CINITL 10

// INLET/OUTLET parameters
#define DIN  1.0
#define DOUT 1.0
#define VIN  0.0 
#define VOUT 0.0

// In/Out Scalar
#define CIN 1.0
#define CIN2 0.0
#define COUT 0.0
#define COUT2 0.0

// ACOUSTIC parameters
#define AWAVE 0.001
#define FWAVE 4.00

// REACTION parameters
#define A1 1
#define A2 1
#define A3 1

// REACTIVE box sizes
#define REBOX_XM 0
#define REBOX_XP 4
#define REBOX_YM 0
#define REBOX_YP 2
#define REBOX_ZM 0
#define REBOX_ZP 10

// REBIRTH file
#define BIRTHSTEADY "dump.PART.10000.h5"
#define BIRTHPLACE "dump.PART.00000.h5"
