#!/bin/bash -e

echo " "
echo "#lbdm# Test 010 Transient Flow Shrinkage 2d"
echo " "

git checkout user-defined.c

if [ -f "*.bin.txt" ]; then
rm *.bin.txt
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "continuity.png" ]; then
rm continuity.png
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

if ls *.h5 1> /dev/null 2>&1; then
rm *.h5 
fi

octave create_bin-txt.m
cp trans2d.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..

make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/010_trans2d/

cp ../../out/dump.TRANS2D.*.h5 .

octave qm_h5_trans2d.m

awk 'NR>10&&NR<=50 {if (($1*$1)^0.5<=0.3) print "ok"; else print "no" }' error.dat > errchk.dat

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/trans2d.bin.txt

# clean local test
cd test/010_trans2d/
git checkout user-defined.c
rm *.txt
#specifc for this test
rm *.h5

########### 1) Check NaN 

for file in error.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 40 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
