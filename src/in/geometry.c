#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <unistd.h>
#include "hdf5.h"
#include "geometry.h"
#include "../var/shared.h"

#ifdef INASCII

#if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
int geometry_from_file(int *gsol, double *gwet) {
#else
int geometry_from_file(int *gsol) {
#endif

  FILE *filein;
  char filename[128];
  double solid=0.0;
  int count=0;
  
  // Must be in Z,X,Y order!!
  sprintf(filename,"in/%s",GEOM);

  if (access(filename,F_OK) != -1 ) {
    if (ROOT(world_rank))
    fprintf(stderr,"II: I found the %s file. Reading it.\n",filename);

  filein = fopen(filename,"r");
    while( fscanf(filein,"%lf\n",&solid)!=EOF && count<(NX*NY*NZ)) {
    gsol[count]=(int)solid;
    count++;
    }

  fclose(filein);

  } else {
  fprintf(stderr,"EE: I cannot find the %s file.\n",filename);
  exit(1);
  }

  #if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
  double wetti=0.0;
  count = 0;

  sprintf(filename,"in/%s.wet",GEOM);

  if (access(filename,F_OK) != -1 ) {
    if (ROOT(world_rank))
    fprintf(stderr,"II: I found the %s wetting file. Reading it.\n",filename);

  filein = fopen(filename,"r");
    while( fscanf(filein,"%lf\n",&wetti)!=EOF && count<(NX*NY*NZ)) {
    gwet[count]=(double)wetti;
    count++;
    }

  fclose(filein);

  } else {
  fprintf(stderr,"EE: I cannot find the %s wetting file.\n",filename);
  exit(1);
  }

  #endif

}

#endif

#ifdef INASCII

#if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
int geometry_mpi(int *gsol, int *lsol, double *gwet, double *lwet) {
#else
int geometry_mpi(int *gsol, int *lsol) {
#endif

#elif (defined INHDF5)

#if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
int geometry_mpi(int *lsol, double *lwet) {
#else
int geometry_mpi(int *lsol) {
#endif

#endif

  #ifdef INASCII
  if (ROOT(world_rank))
  fprintf(stderr,"II: Start to decompose MPI domain.\n");


  int i=0, j=0, k=0;
  int idxg=0, idxl=0;
  int offx=0, offy=0, offz=0;
  
  offx = (int) procoffx[world_rank];
  offy = (int) procoffy[world_rank];
  offz = (int) procoffz[world_rank];

  for (j=0; j<NY; j++) {
    for (i=0; i<NX; i++) {
      for (k=0; k<NZ; k++) {
        if ( i>=offx && i<offx+lx && j>=offy && j<offy+ly && k>=offz && k<offz+lz ) { 

        // Single BELT +1
        idxg = IDXG(i,j,k); 
        idxl = IDXP(i-offx+1,j-offy+1,k-offz+1);

          if ( i==offx && j==offy && k==offz ) 
          fprintf(stderr,"II: Mpi decomposition proc: %d\n",world_rank);

        lsol[idxl]=gsol[idxg];

        #if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
        lwet[idxl]=gwet[idxg];
        #endif

        }
      }
    }
  }

  #elif (defined INHDF5)
  if (ROOT(world_rank))
  fprintf(stderr,"II: Start to decompose MPI domain.\n");

  FILE *filein;
  char filename[128];

  sprintf(filename,"in/%s",GEOM);

  if (access(filename,F_OK) != -1 ) {
    if (ROOT(world_rank))
    fprintf(stderr,"II: I found the %s file. Reading it.\n",filename);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  hid_t file_id;
  hid_t hdf5_status;
  hid_t dataset_id, dataset_id2;
  hid_t plist_id, efilespace, edimespace;
  hid_t ret, string_type, xfer_plist;
  hsize_t efile_3d[3], edime_3d[3];
  hsize_t estart_3d[3], ecount_3d[3], estride_3d[3], eblock_3d[3];
  hsize_t dstart_3d[3], dcount_3d[3], dstride_3d[3], dblock_3d[3];
  herr_t status;

  H5E_auto_t old_func;
  void *old_client_data;

  H5Eget_auto (H5E_DEFAULT, &old_func, &old_client_data);
  H5Eset_auto (H5E_DEFAULT, old_func, old_client_data);

  efile_3d[0] = NY;
  efile_3d[1] = NX;
  efile_3d[2] = NZ;

  edime_3d[0] = lyp;
  edime_3d[1] = lxp;
  edime_3d[2] = lzp;

  estart_3d[0] = 1;
  estart_3d[1] = 1;
  estart_3d[2] = 1;
  estride_3d[0] = 1;
  estride_3d[1] = 1;
  estride_3d[2] = 1;
  eblock_3d[0] = ly;
  eblock_3d[1] = lx;
  eblock_3d[2] = lz;
  ecount_3d[0] = 1;
  ecount_3d[1] = 1;
  ecount_3d[2] = 1;

  dstart_3d[0] = procoffy[world_rank];
  dstart_3d[1] = procoffx[world_rank];
  dstart_3d[2] = procoffz[world_rank];
  dstride_3d[0] = 1;
  dstride_3d[1] = 1;
  dstride_3d[2] = 1;
  dblock_3d[0] = ly;
  dblock_3d[1] = lx;
  dblock_3d[2] = lz;
  dcount_3d[0] = 1;
  dcount_3d[1] = 1;
  dcount_3d[2] = 1;

  plist_id = H5Pcreate (H5P_FILE_ACCESS);
  hdf5_status = H5Pset_fapl_mpio (plist_id, MPI_COMM_WORLD, MPI_INFO_NULL);

  file_id = H5Fopen (filename, H5F_ACC_RDONLY, plist_id);

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id = H5Dopen (file_id, "sol", H5P_DEFAULT);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  // status = H5Dread (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dread (dataset_id, H5T_NATIVE_INT, edimespace, efilespace, xfer_plist, lsol);

  H5Dclose (dataset_id);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  #if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id2 = H5Dopen (file_id, "wet", H5P_DEFAULT);

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dread (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dread (dataset_id2, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, lwet);

  H5Dclose (dataset_id2);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  #endif

  H5Fclose (file_id);
  H5Pclose (plist_id);

  #endif

#if (defined INASCII || defined INHDF5)
}
#endif

#if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
int geometry_slip(int *lsol, macro *lwet) {
#else
int geometry_slip(int *lsol) {
#endif

  int idxe=0, idxi=0, i=0, j=0, k=0;

  #if ( (defined FREESLIP_XP || defined FREESLIP_XM) ||\
       ((defined SPATIAL_WETTING || defined SPATIAL_REACTION) &&\
	(defined CONCENTRATION_XM || defined CONCENTRATION_XP ||\
	 defined CONCENTRATION_EQ_XM || defined CONCENTRATION_EQ_XP ||\
	 defined CONCENTRATION_HALF_XM || defined CONCENTRATION_HALF_XP)) ||\
       ((defined CONJUGATE) &&\
	(defined CONCENTRATION_XM || defined CONCENTRATION_XP ||\
	 defined CONCENTRATION_EQ_XM || defined CONCENTRATION_EQ_XP ||\
	 defined CONCENTRATION_HALF_XM || defined CONCENTRATION_HALF_XP)) )

  // Overwrite top boundary
  if (prx == (int)NEX-1) {
    for (j=0; j<lyp; j++) {
      for (k=0; k<lzp; k++) {

      idxi=IDXP(lxp-2,j,k);
      idxe=IDXP(lxp-1,j,k);
      lsol[idxe] = lsol[idxi];

      #if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
      lwet[idxe] = lwet[idxi];
      #endif

      }
    }
  }

  // Overwrite low boundary
  if (prx == 0) {
    for (j=0; j<lyp; j++) {
      for (k=0; k<lzp; k++) {

      idxi=IDXP(1,j,k);
      idxe=IDXP(0,j,k);
      lsol[idxe] = lsol[idxi];

      #if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
      lwet[idxe] = lwet[idxi];
      #endif

      }
    }
  }

  #endif

  #if ( (defined FREESLIP_YP || defined FREESLIP_YM) ||\
       ((defined SPATIAL_WETTING || defined SPATIAL_REACTION) &&\
	(defined CONCENTRATION_YM || defined CONCENTRATION_YP ||\
	 defined CONCENTRATION_EQ_YM || defined CONCENTRATION_EQ_YP ||\
	 defined CONCENTRATION_HALF_YM || defined CONCENTRATION_HALF_YP)) ||\
       ((defined CONJUGATE) &&\
	(defined CONCENTRATION_YM || defined CONCENTRATION_YP ||\
	 defined CONCENTRATION_EQ_YM || defined CONCENTRATION_EQ_YP ||\
	 defined CONCENTRATION_HALF_YM || defined CONCENTRATION_HALF_YP)) )

  // Overwrite top boundary
  if (pry == (int)NEY-1) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idxi=IDXP(i,lyp-2,k);
      idxe=IDXP(i,lyp-1,k);
      lsol[idxe] = lsol[idxi];

      #if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
      lwet[idxe] = lwet[idxi];
      #endif

      }
    }
  }

  // Overwrite low boundary
  if (pry == 0) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idxi=IDXP(i,1,k);
      idxe=IDXP(i,0,k);
      lsol[idxe] = lsol[idxi];

      #if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
      lwet[idxe] = lwet[idxi];
      #endif

      }
    }
  }

  #endif

  #if ( (defined FREESLIP_ZP || defined FREESLIP_ZM) ||\
       ((defined SPATIAL_WETTING || defined SPATIAL_REACTION) &&\
	(defined CONCENTRATION_ZM || defined CONCENTRATION_ZP ||\
	 defined CONCENTRATION_EQ_ZM || defined CONCENTRATION_EQ_ZP ||\
	 defined CONCENTRATION_HALF_ZM || defined CONCENTRATION_HALF_ZP)) ||\
       ((defined CONJUGATE) &&\
	(defined CONCENTRATION_ZM || defined CONCENTRATION_ZP ||\
	 defined CONCENTRATION_EQ_ZM || defined CONCENTRATION_EQ_ZP ||\
	 defined CONCENTRATION_HALF_ZM || defined CONCENTRATION_HALF_ZP)) )

  // Overwrite top boundary
  if (prz == (int)NEZ-1) {
    for (j=0; j<lyp; j++) {
      for (i=0; i<lxp; i++) {

      idxi=IDXP(i,j,lzp-2);
      idxe=IDXP(i,j,lzp-1);
      lsol[idxe] = lsol[idxi];

      #if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
      lwet[idxe] = lwet[idxi];
      #endif

      }
    }
  }

  // Overwrite low boundary
  if (prz == 0) {
    for (j=0; j<lyp; j++) {
      for (i=0; i<lxp; i++) {

      idxi=IDXP(i,j,1);
      idxe=IDXP(i,j,0);
      lsol[idxe] = lsol[idxi];

      #if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
      lwet[idxe] = lwet[idxi];
      #endif

      }
    }
  }

  #endif

  // Boring 12-cases edges at zero,
  // I wait everyone to avoid overwriting
  MPI_Barrier(MPI_COMM_WORLD);

  // If REACTION or THERMAL activated it is done somewhere-else (depracted)
  // #if (!defined REACTION && !defined THERMAL)

  // 1st
  #if (defined FREESLIP_XP && defined FREESLIP_YP)
  if ( (prx==(int)NEX-1) && (pry==(int)NEY-1) ) {
    for (k=0; k<lzp; k++) { 
    idxe=IDXP(lxp-1,lyp-1,k);
    lsol[idxe]=0;
    }
  }
  #endif

  // 2nd
  #if (defined FREESLIP_XP && defined FREESLIP_YM)
  if ( (prx==(int)NEX-1) && (pry==0) ) {
    for (k=0; k<lzp; k++) { 
    idxe=IDXP(lxp-1,0,k);
    lsol[idxe]=0;
    }
  }
  #endif

  // 3rd
  #if (defined FREESLIP_XM && defined FREESLIP_YP)
  if ( (prx==0) && (pry==(int)NEY-1) ) {
    for (k=0; k<lzp; k++) { 
    idxe=IDXP(0,lyp-1,k);
    lsol[idxe]=0;
    }
  }
  #endif

  // 4th
  #if (defined FREESLIP_XM && defined FREESLIP_YM)
  if ( (prx==0) && (pry==0) ) {
    for (k=0; k<lzp; k++) { 
    idxe=IDXP(0,0,k);
    lsol[idxe]=0;
    }
  }
  #endif

  // 5th
  #if (defined FREESLIP_XP && defined FREESLIP_ZP)
  if ( (prx==(int)NEX-1) && (prz==(int)NEZ-1) ) {
    for (j=0; j<lyp; j++) { 
    idxe=IDXP(lxp-1,j,lzp-1);
    lsol[idxe]=0;
    }
  }
  #endif

  // 6th
  #if (defined FREESLIP_XP && defined FREESLIP_ZM)
  if ( (prx==(int)NEX-1) && (prz==0) ) {
    for (j=0; j<lyp; j++) { 
    idxe=IDXP(lxp-1,j,0);
    lsol[idxe]=0;
    }
  }
  #endif

  // 7th
  #if (defined FREESLIP_XM && defined FREESLIP_ZP)
  if ( (prx==0) && (prz==(int)NEZ-1) ) {
    for (j=0; j<lyp; j++) { 
    idxe=IDXP(0,j,lzp-1);
    lsol[idxe]=0;
    }
  }
  #endif

  // 8th
  #if (defined FREESLIP_XM && defined FREESLIP_ZM)
  if ( (prx==0) && (prz==0) ) {
    for (j=0; j<lyp; j++) { 
    idxe=IDXP(0,j,0);
    lsol[idxe]=0;
    }
  }
  #endif

  // 9th
  #if (defined FREESLIP_YP && defined FREESLIP_ZP)
  if ( (pry==(int)NEY-1) && (prz==(int)NEZ-1) ) {
    for (i=0; i<lxp; i++) { 
    idxe=IDXP(i,lyp-1,lzp-1);
    lsol[idxe]=0;
    }
  }
  #endif

  // 10th
  #if (defined FREESLIP_YP && defined FREESLIP_ZM)
  if ( (pry==(int)NEY-1) && (prz==0) ) {
    for (i=0; i<lxp; i++) { 
    idxe=IDXP(i,lyp-1,0);
    lsol[idxe]=0;
    }
  }
  #endif

  // 11th
  #if (defined FREESLIP_YM && defined FREESLIP_ZP)
  if ( (pry==0) && (prz==(int)NEZ-1) ) {
    for (i=0; i<lxp; i++) { 
    idxe=IDXP(i,0,lzp-1);
    lsol[idxe]=0;
    }
  }
  #endif

  // 12th
  #if (defined FREESLIP_YM && defined FREESLIP_ZM)
  if ( (pry==0) && (prz==0) ) {
    for (i=0; i<lxp; i++) { 
    idxe=IDXP(i,0,0);
    lsol[idxe]=0;
    }
  }
  #endif

  //#endif

}
