zh=load('../001_scps3d/zickhomsy.dat');
lb=load('drag_lbdm.dat');

%% good second order polyfit
[a,b]=polyfit(zh(:,1).**(1/3)./(1-zh(:,1)),zh(:,2),2);

x=(lb(:,1)).**(1/3)./(1-lb(:,1));
y=x.**2.*a(1)+x.*a(2)+a(3);

M=[lb(:,1),y];

save('zickhomsy_fitted.dat','-ascii','M');

xx=[0.3:0.01:0.65];
yy=xx.**2.*a(1)+xx.*a(2)+a(3);

graphics_toolkit ("gnuplot");
setenv ("GNUTERM", "qt");

figure,
plot(zh(:,1).**(1/3)./(1-zh(:,1)),zh(:,2),'o');
hold on,
plot(lb(:,1).**(1/3)./(1-lb(:,1)),lb(:,2),'x');
hold on,
plot(xx,yy,'-');

saveas(gcf,'proofval.png','png');

