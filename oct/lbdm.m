
% %%% : this is a Lattice Boltmann Model 3D MRT D3Q19 single-phase : %%%

					%%%%%% DECLARE IF %%%%%%%%%%%%%

				 	name='output'
					geoname='in/pipe.bin.txt'
					
					%% use of body force ??
					bf=1;

					% restart
					restart=0;



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % 1 %%%% SET-UP Model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if restart==0;

tic
g_density=1.0;

% %% FORCING & INPUTS %%%%%%%

l=6; % charct.length
addpath('geo:in');

% Set a Pressure Gradients ..
dPdL = 1e-5;
Fvect = [1 0 0];

% %%% TIME relaxation %%%%%%%

nueta=5/57; %0.0878;
tau=1.5;
Sxplus=1/tau;
Sxminus=(nueta/(1/Sxplus-0.5)+0.5)^-1;

C_ux0=0.3.*g_density;

% %%% SEVERAL GEOMETRIES %%%%

input=dlmread(geoname);

% %% Build GEO %%%%%%%%%%%%%%

Nr=14;
Mc=40;
Ld=14;

% porosity ... etc
geoname, name,
Channel3D=reshape(input,[Nr,Mc,Ld]);

% %%% PROPERTIES %%%%%%%%%%

% sound speed
cs2=1/3; %?

% fluid mean density 
sizemp=sum(sum(sum(Channel3D,1),2),3);

% others
omega=tau.^-1;
cP_visco=(tau-0.5)*cs2*g_density;
Lky_visco=cP_visco/g_density; % lattice kinematic viscosity


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%











% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %% 2 %%% DESCRIPTION of tHe Model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% time relaxation PARAMETERS
%	  +   +   -   +   +    +    -
%	[Sx1,Sx2,Sx4,Sx9,Sx13.Sx10,Sx16]
Sx = ... 
	[Sxplus,Sxplus,Sxminus,Sxplus,Sxplus,Sxplus,Sxminus];


% expected Re
REt=1;


% %%% DIRECTIONS %%%%%%%

%	z		C_xzy =[x ,y ,z ]
%	^ y		       [+j,+k,-i]
%	|/
%	o--> x          

%	


% x & y & z components of velocities
N_c=19; % number of directions

% versors D3Q19
C_x=[0  1 -1  0  0  0  0  1 -1  1 -1  1 -1  1 -1  0  0  0  0 ]; 
C_y=[0  0  0  1 -1  0  0  1  1 -1 -1  0  0  0  0  1 -1  1 -1 ];
C_z=[0  0  0  0  0  1 -1  0  0  0  0  1  1 -1 -1  1  1 -1 -1 ];
C=[C_x;C_y;C_z];


% %% BOUNCE BACK SCHEME %%%%

% after collision the fluid elements densities f are sent back to the
% lattice node they come from with opposite direction
% indices opposite to 1:8 for fast inversion after bounce
%	  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
ic_op = [ 1  3  2  5  4  7  6 11 10  9  8 15 14 13 12 19 18 17 16] ;
ic_sy = [ 1  2  3  4  5  7  6  8  9 10 11 14 15 12 13 18 19 16 17] ; 
%   i.e. 4 is opposite to 2 etc.

% %% PERIODIC BOUNDARY %%%%%

xi2=[Mc , 1:Mc , 1]; % Period Bound Cond
zi2=[Nr , 1:Nr , 1];
yi2=[Ld , 1:Ld , 1];


% %% INITIALIZATION %%%%%%%%%

% directional weights (density weights)
% and potential weights
w0=12/36 ; w1=2/36 ; w2=1/36;
W=[w0 w1 w1 w1 w1 w1 w1 w2 w2 w2 w2 w2 w2 w2 w2 w2 w2 w2 w2];

% M matrix
M  =	[ 1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1 ;
	-30 -11 -11 -11 -11 -11 -11   8   8   8   8   8   8   8   8   8   8   8   8 ;
	 12  -4  -4  -4  -4  -4  -4   1   1   1   1   1   1   1   1   1   1   1   1 ;
	  0   1  -1   0   0   0   0   1  -1   1  -1   1  -1   1  -1   0   0   0   0 ;
	  0  -4   4   0   0   0   0   1  -1   1  -1   1  -1   1  -1   0   0   0   0 ;
	  0   0   0   1  -1   0   0   1   1  -1  -1   0   0   0   0   1  -1   1  -1 ;
	  0   0   0  -4   4   0   0   1   1  -1  -1   0   0   0   0   1  -1   1  -1 ;
          0   0   0   0   0   1  -1   0   0   0   0   1   1  -1  -1   1   1  -1  -1 ;
	  0   0   0   0   0  -4   4   0   0   0   0   1   1  -1  -1   1   1  -1  -1 ;
	  0   2   2  -1  -1  -1  -1   1   1   1   1   1   1   1   1  -2  -2  -2  -2 ;
	  0  -4  -4   2   2   2   2   1   1   1   1   1   1   1   1  -2  -2  -2  -2 ;
	  0   0   0   1   1  -1  -1   1   1   1   1  -1  -1  -1  -1   0   0   0   0 ;
	  0   0   0  -2  -2   2   2   1   1   1   1  -1  -1  -1  -1   0   0   0   0 ;
	  0   0   0   0   0   0   0   1  -1  -1   1   0   0   0   0   0   0   0   0 ;
	  0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   1  -1  -1   1 ;
	  0   0   0   0   0   0   0   0   0   0   0   1  -1  -1   1   0   0   0   0 ;
	  0   0   0   0   0   0   0   1  -1   1  -1  -1   1  -1   1   0   0   0   0 ;
	  0   0   0   0   0   0   0  -1  -1   1   1   0   0   0   0   1  -1   1  -1 ;
	  0   0   0   0   0   0   0   0   0   0   0   1   1  -1  -1  -1  -1   1   1 ];

% S matrix
S  = diag([0,Sx(1),Sx(2),0,Sx(3),0,Sx(3),0,Sx(3),Sx(4),Sx(5),Sx(4),Sx(5),Sx(6),Sx(6),Sx(6),Sx(7),Sx(7),Sx(7)]);

% EQ matrix
rd=g_density;
we=3;%0;
wej=-11/2;%-475/63;
wxx=-1/2;%0;

EQ = zeros(N_c,10);
EQ(1,1)=1; 
EQ(2,1)=-11; EQ(2,5)=19/rd; EQ(2,6)=19/rd; EQ(2,7)=19/rd;	
EQ(3,1)=we; EQ(3,5)=wej/rd; EQ(3,6)=wej/rd; EQ(3,7)=wej/rd; %% BGK
EQ(4,2)=1; EQ(6,3)=1; EQ(8,4)=1;
EQ(5,2)=-2/3; EQ(7,3)=-2/3; EQ(9,4)=-2/3;
EQ(10,5)=2/rd; EQ(10,6)=-1/(rd); EQ(10,7)=-1/(rd);
EQ(11,5)=2*wxx/rd; EQ(11,6)=-wxx/rd; EQ(11,7)=-wxx/rd;
EQ(12,6)=1/rd; EQ(12,7)=-1/rd;
EQ(13,6)=wxx/rd; EQ(13,7)=-wxx/rd; 
EQ(14,8)=1/rd; 
EQ(15,9)=1/rd;
EQ(16,10)=1/rd;

% c constants (sound speed related)
cs2=cs2; cs2x2=2*cs2; cs4x2=2*cs2.^2;
f1=1/cs2; f3=1/cs2x2; f2=1/cs4x2;
f1=3.; f2=4.5; f3=1.5; % coef. of the f equil.

% multi-reflection costant
kFpc=0; kb1=1+kFpc; kb0=0-kFpc; 
kubm2=0; kbm1=0; kubm1=0-kFpc;

% declarative statemets
f=zeros(Nr,Mc,Ld,N_c); % array of fluid density distribution
force=zeros(Nr,Mc,Ld,N_c);
feq=zeros(Nr,Mc,Ld,N_c); % f at equilibriumrho=zeros(Nr,Mc,Ld); % macro-scopic density

% dimensionless velocities
ux=zeros(Nr,Mc,Ld);   uy=zeros(Nr,Mc,Ld); uz=zeros(Nr,Mc,Ld);


% %% EXTERNAL FORCES %%%%%%

%	1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
% cForce=[0  1 -1  0  0  0  0  1 -1  1 -1  1 -1  1 -1];

if bf==1;
fGradx(:,:,:,1) = dPdL.*Channel3D*Fvect(1); %sum(force.*cForce,2); % 0
fGradx(:,:,:,2) = dPdL.*Channel3D*Fvect(2);
fGradx(:,:,:,3) = dPdL.*Channel3D*Fvect(3);
else;
fGradx= [0,0,0];
end;


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%












% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %% 3 %%% While .. MAIN TIME EVOLUTION LOOP %%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



StopFlag=false; % i.e. logical(0)

% Inputs
Max_Iter=50; % max allowed number of iteration
Check_Iter=1; Output_Every=10; % frequency check & output
Cur_Iter=0; % current iteration counter inizialization
toler=10^(-4); % tollerance to declare convegence
Max_Time=3600; % maximum sim time

Cond_path=[]; % recording values of the convergence criterium
density_path=[]; % recording aver. density values for convergence
Condition2=toler; Iter_conv2=0; cond2=0;
Iter_Partial=Inf;
saved=0;

for ic=1:N_c
f(:,:,:,ic)=1/N_c.*g_density.*Channel3D(:,:,:);
end

tic
end % end if restart=0

% restart from partial
if restart==1;
tic
toler=10^(-7);
StopFlag=false;
Condition2=toler; Iter_conv2=0; cond2=3;
Max_Iter=1000000;
end;



						while(~StopFlag)
    
Cur_Iter=Cur_Iter+1 % iteration counter update

% init velocities and density
ux=zeros(Nr,Mc,Ld); uy=zeros(Nr,Mc,Ld); uz=zeros(Nr,Mc,Ld);
rho=sum(f,4);


	%%% UPDATE VEL %%%%%%%%%%  

	for ic=2:1:N_c
	C_yic=C_y(ic); C_xic=C_x(ic); C_zic=C_z(ic);	
 
	if Cur_Iter > 0  % if start from a value
           
    	ux(:,:,:)=f(:,:,:,ic)*C_xic+ux(:,:,:);
	uy(:,:,:)=f(:,:,:,ic)*C_yic+uy(:,:,:);
	uz(:,:,:)=f(:,:,:,ic)*C_zic+uz(:,:,:);
	
    	else

    	for y=1:Ld;
	ux(:,:,y)=repmat(ux00(:,1,y),[1,Mc]).*Channel3D(:,:,y);
	uy(:,:,y)=repmat(uy00(:,1,y),[1,Mc]).*Channel3D(:,:,y);
	uz(:,:,y)=repmat(uz00(:,1,y),[1,Mc]).*Channel3D(:,:,y);
	end
    	
	end

	end

	% add force
	ux=ux+0.5.*fGradx(:,:,:,1);
	uy=uy+0.5.*fGradx(:,:,:,2);
	uz=uz+0.5.*fGradx(:,:,:,3);


% % UPDATE F EQ and RHO %%

% Equilibrium distribution
feq=zeros(Nr,Mc,Ld,N_c);	

	if Cur_Iter > 0 % if start from a value
	
	for ic=1:1:N_c
        
	feq(:,:,:,ic)=rho.*EQ(ic,1) +ux.*EQ(ic,2) +uy.*EQ(ic,3) ...
	+uz.*EQ(ic,4) +(ux.^2).*EQ(ic,5) ...
        +(uy.^2).*EQ(ic,6) +(uz.^2).*EQ(ic,7) ... 
	+(ux.*uy).*EQ(ic,8) +(uy.*uz).*EQ(ic,9) ... 
	+(ux.*uz).*EQ(ic,10) ;

	end

	else

	for y=1:Ld; for ic=1:1:N_c
	feq(:,:,y,ic)=repmat(feq00(:,1,y,ic),[1,Mc]).*Channel3D(:,:,y);
	f(:,:,y,ic)=repmat(f00(:,1,y,ic),[1,Mc]).*Channel3D(:,:,y);
	end; end

	end

rho=rho.*Channel3D;
% temp rho+1 to avoid NaN on boundaries
ux=ux./(rho+1-Channel3D); 
uy=uy./(rho+1-Channel3D); 
uz=uz./(rho+1-Channel3D);



% %% COLLISION %%%%% 

	% add body force
	for ic=1:N_c;	
        C_yic=C_y(ic); C_xic=C_x(ic); C_zic=C_z(ic);

	force(:,:,:,ic) = W(ic).* ...
	( ((C_xic-ux).*f1+(C_xic.*ux).*(2*f2*C_xic)).*fGradx(:,:,:,1) ...
 	+ ((C_yic-uy).*f1+(C_yic.*uy).*(2*f2*C_yic)).*fGradx(:,:,:,2) ...
	+ ((C_zic-uz).*f1+(C_zic.*uz).*(2*f2*C_zic)).*fGradx(:,:,:,3) ...
	);

	end;

f   = reshape(permute(f,[4,1,2,3]),N_c,Nr*Mc*Ld);
feq = reshape(permute(feq,[4,1,2,3]),N_c,Nr*Mc*Ld);

	f   = f - inv(M)*(S*(M*f-feq) - (eye(N_c)-0.5.*S)*(M*( ...
	reshape(permute(force,[4,1,2,3]),N_c,Nr*Mc*Ld)   )));	

f   = permute(reshape(f,N_c,Nr,Mc,Ld),[2,3,4,1]);
feq = permute(reshape(feq,N_c,Nr,Mc,Ld),[2,3,4,1]);
	

% %% STREAM %%%%%%%%

% Forward Propagation step & 
% Bounce Back (collision fluid with obstacles)
   
feq = f; % temp storage of f in feq

        for ic=2:1:N_c,
        ic2=ic_op(ic); % velocity opposite to ic for BB
        C_yic = C_y(ic); C_xic = C_x(ic); C_zic=C_z(ic);

	f(:,:,:,ic) = circshift(feq(:,:,:,ic),[-C_zic,C_xic,C_yic]).*(Channel3D) ... 
	+ kb1.*( feq(:,:,:,ic2) ...
	).*circshift((1-Channel3D(:,:,:)),[-C_zic,C_xic,C_yic]) ;

	end ; %  for ic direction

	% multireflection still NOT working!
%	C_yic2 = C_y(ic2); C_xic2= C_x(ic2); C_zic2=C_z(ic2);
%	- circshift((1-Channel3D(:,:,:)),[-C_zic,C_xic,C_yic])
%	+ kb0.*circshift(feq(:,:,:,ic2),[-C_zic2,C_xic2,C_yic2]) ...
%	+ kbm1.*circshift(feq(:,:,:,ic2),2.*[-C_zic2,C_xic2,C_yic2]) ...
%	+ kubm2.*circshift(feq(:,:,:,ic),[-C_zic2,C_xic2,C_yic2]) ...
%	+ kubm1.*feq(:,:,:,ic)...


% %% BBs %%%

%f=zouhebbss_3d(ijk_io,u_ioWg,u_ioEg,u_ioSl,u_ioSg,f,g_density,l_density, Cur_Iter,dP,bf,Iter_Partial);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ends of Forward Propagation step &  Bounce Back Sections

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%












% %%%%%%%%% CONVERGENCE ? %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % Convergence check on velocity values
    
if (mod(Cur_Iter,Check_Iter)==0) ; % every 'Check_Iter' iterations

	% averaging velocity (in the wet area)
	rho=sum(f,4); % density
	ux=((rho+1-Channel3D).*ux+0.0.*fGradx(:,:,:,1))./(rho+1-Channel3D); %-0.5.*fGradx

	Af=sum(sum(Channel3D,2),3); 
	ux_da=(sum(sum(ux.*Channel3D,2),3))./Af;
	ux_da(isnan(ux_da))=0; 
	av_vel=mean(ux_da);

	rho0=g_density;

	REtp0 = abs(av_vel)*(l)/cP_visco*rho0;

	REtp=REtp0;
	Condition=abs( abs(REt/REtp )-1); % should --> 0        
	REt=REtp; % time t & t+1 
 

	%%% TOLERANCE BREAK %%%

        if (Cur_Iter > Max_Iter || isnan(REt) || (size(Condition2,2)>2) )
            StopFlag=true;
            display( 'Stop iteration: iteration exeeding the max allowed or NaN value of Re or ... Yuppi converged!' )
            display( ['Current iteration: ',num2str(Cur_Iter),...
                ' Max Number of iter: ',num2str(Max_Iter),...
		' Reynolds numberrrr: ',num2str(REt) ] )
            break % Terminate execution of WHILE ..
        end
	
	if (Condition < toler) ;
	if    (Cur_Iter == Iter_conv2+Check_Iter) ;	    
	      cond2=cond2+1,
	else; cond2=1; end;
	Condition2(cond2)=Condition;
	Iter_conv2=Cur_Iter;
	end

        time=toc;

end


if ( (time > Max_Time) && (saved==0) );
printf('\n Max Time reached .. I m sorry \n\n');
spr=sprintf([ name '.mat' ]);
save( [ 'out/' spr '.part.mat' ],'ux','uy','uz','Channel3D','rho','convergence','Condition2','name')
StopFlag=true;
saved=1,
break
end

% %% outputs every iteration output

    if (mod(Cur_Iter,Output_Every)==0) ;

	convergence(Cur_Iter/Output_Every)=Condition,
	vec_Iter=[Output_Every:Output_Every:Cur_Iter];
		
    end % every

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
end %  End main time Evolution Loop

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




% TIME

if restart==1;
toc;
TOC=TOC+toc;
else
toc; TOC=toc
end;

% saving
if (saved==0);
spr=sprintf([ name '.mat']);
save( [ 'out/' spr ],'ux','uy','uz','Channel3D','rho','convergence','Condition2','name')
saved=1,
end;

