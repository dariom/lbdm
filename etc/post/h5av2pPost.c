#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "hdf5.h"

  #define NX (210)
  #define NY (210)
  #define NZ (140)

  #define IDX(i,j,k)  (k + NZ*(i + NX*(j)));

/* This guy reads 1 two-phase flow input file and nodally average it
 * on space with the aim of canceling out staggering invariants.
 * Refs: Ladd, JFM 271(1994) & Guo, Phil.Trans.R.Soc.A 369(2011) */

char *fname=NULL;
char *dirout=NULL;
char *fnameout=NULL;

void readfile (int *sol, double *ux, double *uy, double *uz) {
  hid_t file_id, group_id, dataset_id;  /* identifiers */
  hid_t property_id, plist_id, efilespace;
  hsize_t efile_3d[3];
  herr_t status;

  fprintf (stderr, "(1) reading %s \n", fname);

  file_id = H5Fopen (fname, H5F_ACC_RDWR, H5P_DEFAULT);
  group_id = H5Gopen (file_id, "/LBdm", H5P_DEFAULT);

  dataset_id = H5Dopen (group_id, "sol", H5P_DEFAULT);
  status = H5Dread (dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, sol);
  status = H5Dclose (dataset_id);

  dataset_id = H5Dopen (group_id, "ux", H5P_DEFAULT);
  status = H5Dread (dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, ux);
  status = H5Dclose (dataset_id);

  dataset_id = H5Dopen (group_id, "uy", H5P_DEFAULT);
  status = H5Dread (dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, uy);
  status = H5Dclose (dataset_id);

  dataset_id = H5Dopen (group_id, "uz", H5P_DEFAULT);
  status = H5Dread (dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, uz);
  status = H5Dclose (dataset_id);

  status = H5Gclose (group_id);
  status = H5Fclose (file_id);

}

void writefile (int *soli, double *uxi, double *uyi, double *uzi) {
  hid_t file_id, group_id, dataset_id;  /* identifiers */
  hid_t property_id, plist_id, efilespace;
  hsize_t efile_3d[3];
  herr_t status;
  char fcompleteout[1024];

  fprintf(stderr,"(3) writing %s \n",fnameout);
  sprintf(fcompleteout,"%s%s",dirout,fnameout);

  efile_3d[0] = NY;
  efile_3d[1] = NX;
  efile_3d[2] = NZ;
  efilespace = H5Screate_simple (3, efile_3d, NULL);

  file_id = H5Fcreate (fcompleteout, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  group_id = H5Gcreate (file_id,"/LBdm", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  dataset_id = H5Dcreate (group_id, "soli", H5T_NATIVE_INT, efilespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Dwrite (dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, soli);
  status = H5Dclose (dataset_id);

  dataset_id = H5Dcreate (group_id, "uxi", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Dwrite (dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, uxi);
  status = H5Dclose (dataset_id);

  dataset_id = H5Dcreate (group_id, "uyi", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Dwrite (dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, uyi);
  status = H5Dclose (dataset_id);

  dataset_id = H5Dcreate (group_id, "uzi", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Dwrite (dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, uzi);
  status = H5Dclose (dataset_id);

  status = H5Gclose (group_id);
  status = H5Fclose (file_id);
}


void main (int argc, char **argv) {

  int i, j, k, ip, jp, kp;
  int idx0, idx1, idx2, idx3, idx4;
  int idx5, idx6, idx7;
  int *sol, *soli;
  double *ux, *uy, *uz;
  double *uxi, *uyi, *uzi;
  
  sol = (int *) calloc (NX*NY*NZ,sizeof(int));

  ux = (double *) calloc (NX*NY*NZ,sizeof(double));
  uy = (double *) calloc (NX*NY*NZ,sizeof(double));
  uz = (double *) calloc (NX*NY*NZ,sizeof(double));

  uxi = (double *) calloc (NX*NY*NZ,sizeof(double));
  uyi = (double *) calloc (NX*NY*NZ,sizeof(double));
  uzi = (double *) calloc (NX*NY*NZ,sizeof(double));

  soli = (int *) calloc (NX*NY*NZ,sizeof(int));

  if (argc != 4) {
    fprintf (stderr,
             "Error! Usage is %s <filename> <dirout> <fnameout>\n",
             argv[0]);
    exit (2);
  }

  fname = argv[1];
  dirout = argv[2];
  fnameout = argv[3];

  readfile (sol,ux,uy,uz);

  fprintf (stderr, "(2) calculating for half time, output: %s \n",fnameout);

  for (k = 0; k < NZ; k++) {
    for (j = 0; j < NY; j++) {
      for (i = 0; i < NX; i++) {

      if (i==NX-1) {
      ip = 0; } else { ip = i+1; }

      if (j==NY-1) {
      jp = 0; } else { jp = j+1; }

      if (k==NZ-1) {
      kp = 0; } else { kp = k+1; }

	idx0 = IDX(i,j,k);

	idx1 = IDX(ip,j,k);
	idx2 = IDX(i,jp,k);
	idx3 = IDX(i,j,kp);

	idx4 = IDX(ip,jp,k);
	idx5 = IDX(ip,j,kp);
	idx6 = IDX(i,jp,kp);

	idx7 = IDX(ip,jp,kp);

      uxi[idx0] = 0.5*(0.5*(0.5*(ux[idx1]*sol[idx1]+ux[idx0]*sol[idx0])+0.5*(ux[idx4]*sol[idx4]+ux[idx2]*sol[idx2]))+ \
		       0.5*(0.5*(ux[idx5]*sol[idx5]+ux[idx3]*sol[idx3])+0.5*(ux[idx7]*sol[idx7]+ux[idx6]*sol[idx6])) );

      uyi[idx0] = 0.5*(0.5*(0.5*(uy[idx1]*sol[idx1]+uy[idx0]*sol[idx0])+0.5*(uy[idx4]*sol[idx4]+uy[idx2]*sol[idx2]))+ \
		       0.5*(0.5*(uy[idx5]*sol[idx5]+uy[idx3]*sol[idx3])+0.5*(uy[idx7]*sol[idx7]+uy[idx6]*sol[idx6])) );

      uzi[idx0] = 0.5*(0.5*(0.5*(uz[idx1]*sol[idx1]+uz[idx0]*sol[idx0])+0.5*(uz[idx4]*sol[idx4]+uz[idx2]*sol[idx2]))+ \
		       0.5*(0.5*(uz[idx5]*sol[idx5]+uz[idx3]*sol[idx3])+0.5*(uz[idx7]*sol[idx7]+uz[idx6]*sol[idx6])) );

      }  
    }
  }

  writefile (soli,uxi,uyi,uzi);

  free(sol);
  free(rho);

  free(ux);
  free(uy);
  free(uz);

  free(uxi);
  free(uyi);
  free(uzi);

  fprintf(stderr,"(*) done.\n");

}
