#include<stdio.h>
#include<stdlib.h>
#include<math.h>
//#include "fftw.h"
//#include "rfftw.h"
#include "hdf5.h"

  // -> SOL(WET)
  #define SOLWET

  #define NX (20) 
  #define NY (2)
  #define NZ (20)
 
  // It seems that h5create does it with zyx order .. 
  #define IDX(i,j,k)  (k + (i + j * (NX) ) * (NZ) );

char *fdir=NULL,*fnewname=NULL,*forigname=NULL;

void readtopoi (int * f) {
  int i, j, k, count;
  float idx1;
  char fname[100];
  FILE *fread;

  // .......................................

  sprintf (fname, "%s", forigname);
  fprintf (stderr, "(1) read %s \n", fname);

  /* Open an existing file. */
  fread=fopen(fname,"r");
  count=0; 

  while ( fscanf(fread,"%g\n",&idx1)!=EOF && count<(NX*NY*NZ) ) {
  f[count]=(int)idx1;
  count++;
  }
}

#ifdef SOLWET
void readtopod (double * f) {
  int i, j, k, count;
  float idx1;
  char fname[100];
  FILE *fread;

  // .......................................

  sprintf (fname, "%s.wet", forigname);
  fprintf (stderr, "(1.2) read %s \n", fname);

  /* Open an existing file. */
  fread=fopen(fname,"r");
  count=0; 

  while ( fscanf(fread,"%g\n",&idx1)!=EOF && count<(NX*NY*NZ) ) {
  f[count]=(double)idx1;
  count++;
  }
}
#endif

#ifdef SOL
void writetopo (int * f) {
#elif (defined SOLWET)
void writetopo (int * f, double * f1) {
#endif

  hid_t file_id, group_id, dataset_id;  /* identifiers */
  hid_t property_id, plist_id, efilespace;
  hsize_t efile_3d[3];
  herr_t status;
  char fname[128];

  sprintf (fname, "%s%s.h5", fdir, fnewname);
  fprintf (stderr, "(2) write %s \n", fname);

  efile_3d[0] = NY;
  efile_3d[1] = NX;
  efile_3d[2] = NZ;
  efilespace = H5Screate_simple (3, efile_3d, NULL);

  file_id = H5Fcreate (fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  dataset_id = H5Dcreate (file_id, "/sol", H5T_NATIVE_INT, efilespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Dwrite (dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, f);
  status = H5Dclose (dataset_id);

  #ifdef SOLWET
  dataset_id = H5Dcreate (file_id, "/wet", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Dwrite (dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, f1);
  status = H5Dclose (dataset_id);
  #endif

  status = H5Fclose (file_id);
  printf("(*) done!\n");

}

void zeroi (int * f) {
  int i;
  for (i = 0; i < NX * NY * NZ; i++) {
    f[i] = 0;
  }
}

void zerod (double * f) {
  int i;
  for (i = 0; i < NX * NY * NZ; i++) {
    f[i] = 0.0;
  }
}

void main (int argc, char **argv) {

  int idx, i, j, k;
  int count;

  int *f;  
  f = (int *) malloc (sizeof (int) * NX * NY * NZ);

  #ifdef SOLWET
  double *f1;
  f1 = (double *) malloc (sizeof (double) * NX * NY * NZ);
  #endif

  if (argc != 4) {
    fprintf (stderr,
             "Error! Usage is %s ORIGINAL-name out-dir XXXX-name \n",
             argv[0]);
    exit (2);
  }

  fdir=argv[2];
  forigname=argv[1];
  fnewname=argv[3];
  printf("*original file: %s\n*dirout: %s\n*name-out: %s\n\n",forigname,fdir,fnewname);

  zeroi (f);
  readtopoi (f);
  #ifdef SOLWET
  zerod (f1);
  readtopod (f1);
  #endif

  #ifdef SOL
  writetopo (f);
  #elif (defined SOLWET)
  writetopo (f,f1);
  #endif

}
