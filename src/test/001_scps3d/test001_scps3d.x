#!/bin/bash -e

echo " "
echo "#lbdm# Test 001 Single-Centered Packed Spheres 3d"
echo " "

git checkout user-defined.c

if [ -f "*.bin.txt" ]; then
rm *.bin.txt
fi

if [ -f "dump.CONV.h5" ]; then
rm dump.CONV.h5 
fi

if [ -f "dump.ZERO.h5" ]; then
rm dump.ZERO.h5 
fi

if [ -f "zickhomsy_fitted.dat" ]; then
rm zickhomsy_fitted.dat 
fi

if [ -f "proofval.png" ]; then
rm proofval.png 
fi

if [ -f "drag_lbdm.dat" ]; then
rm drag_lbdm.dat 
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

octave create_bin-txt.m
cp scps3d.1.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 4 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/001_scps3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5
cp ../../out/dump.*.00000.h5 dump.ZERO.h5

octave drag_h5_scps3d_1.m

#####

cp scps3d.2.bin.txt ../../in/

sed -i -e 's/#define DPDLZ/#define DPDLY/g' user-defined.c
sed -i -e 's/define GEOM .*/define GEOM "scps3d.2.bin.txt"/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 4 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/001_scps3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5
cp ../../out/dump.*.00000.h5 dump.ZERO.h5

octave drag_h5_scps3d_2.m

#####

cp scps3d.3.bin.txt ../../in/

sed -i -e 's/#define DPDLY/#define DPDLX/g' user-defined.c
sed -i -e 's/define GEOM .*/define GEOM "scps3d.3.bin.txt"/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 4 ./lbdm); then
echo "Run (3) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/001_scps3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5
cp ../../out/dump.*.00000.h5 dump.ZERO.h5

octave drag_h5_scps3d_3.m

#####

octave fit_zickhomsy.m

paste drag_lbdm.dat zickhomsy_fitted.dat | awk 'NR<=3 {print ((($2-$4)/$4)^2)^0.5 }' > error.dat 
awk 'NR<=3 {if ($1<=0.08) print "ok"; else print "no" }' error.dat > errchk.dat

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

#####

# clean source
cd ../..
make clean
rm in/scps3d.1.bin.txt
rm in/scps3d.2.bin.txt
rm in/scps3d.3.bin.txt

# clean local test
cd test/001_scps3d/
git checkout user-defined.c
rm *.txt

########### 1) Check NaN 

for file in zickhomsy_fitted.dat drag_lbdm.dat error.dat errchk.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 3 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
