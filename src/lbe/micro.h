#ifndef MICRO_H_
#define MICRO_H_

int micro_lbe_init();
int periodic_df();
int micro_lbe_chk();
int micro_lbe_eq();
int micro_forcing();
int micro_lbe_collision();
int micro_lbe_streaming();
int micro_lbe_slip();
int micro_lbe_inout();
int micro_lbe_inner_bcs();
int micro_exo_heat();

#endif 
