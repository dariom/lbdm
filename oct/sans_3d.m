 

                                           %%% MOMENTUM BALANCE %%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

g_nu=(tau-0.5)*cs2;
% g_density=rho0; %%% not precise!!

threshold=1.5*(g_density);
d_rho=Channel3D; %(rho<threshold).*Channel3D;
t_rho=Channel3D;

h0=1;%find(d_rho(:,round(Mc/2),Ld/2),1,'first');
h2=160;%find(d_rho(:,round(Mc/2),Ld/2),1,'last');

Af=sum(sum(d_rho,2),3);
L=size(d_rho,2);
H0=size(d_rho,1);
W0=size(d_rho,3);
H=H0-h0+1;

hr1=find(Af==max(Af),1,'first');
hr2=find(Af==max(Af),1,'last');

h3=find(d_rho(Nr/2,round(Mc/2),:),1,'first');
h4=find(d_rho(Nr/2,round(Mc/2),:),1,'last');

g_rh=sum(sum(sum(d_rho,2),1),3)/(2*L*W0);

Ar=Af./L;

%% n x z %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% SV=ones(Nr,Mc+2,Ld+2);
% SV(:,:,2:Ld+1)=[ones(Nr,1,Ld),t_rho,ones(Nr,1,Ld)];
%(:,2:Mc+1,2:Ld+1)
% Sv_t=bwperim(Channel3D,6);
%Sv_t(hr1+1:hr2-1,[1,L],[h3+1:h4-1])=0;
% Sv_t([2:ch_h+1],[1,L],[Ld/2-ch_w/2+1:Ld/2+ch_w/2])=0;
% Sv_t([ch_h+2:Nr-1],[1,L],:)=0;
Sv=bwperim(Channel3D,6);%.*(Sv_t);
nxyz=geocollxyz(d_rho,Sv,0,0,0);

%% UZ UY UX (d_rho-nxyz) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ux0=ux; uy0=uy; uz0=uz;

% x oriented;
ux=uxi; uy=uyi; uz=uzi;
% y oriented;
%ux=uyi; uy=uzi; uz=uxi;
% z oriented;
%ux=uxi; uy=uyi; uz=uzi;

c1=-11/6; c2=3; c3=-3/2; c4=1/3; d0=1;
e1=245/56; e2=-735/504; e3=735/1400; e4=-5/56;  % IV  - 4 nodi
%e1=75/20; e2=-25/30; e3=3/20; e4=0;	 	% III - 3 nodi
%e1=3; e2=-1/3; e3=0; e4=0;	 		% II - 2 nodi

UZ=(circshift(ux,[1,0,0])-circshift(ux,[-1,0,0]))./2.*(d_rho-abs(nxyz(:,:,:,3)))+ (circshift(c4.*ux,[3,0,0])+circshift(c3.*ux,[2,0,0])+circshift(c2.*ux,[1,0,0])+c1.*ux)./d0.* (nxyz(:,:,:,3)==1)+ (circshift(-c4.*ux,[-3,0,0])+circshift(-c3.*ux,[-2,0,0])+ circshift(-c2.*ux,[-1,0,0])-c1.*ux)./d0.*(nxyz(:,:,:,3)==-1);

UY=(circshift(ux,[0,0,-1])-circshift(ux,[0,0,1]))./2.*(d_rho-abs(nxyz(:,:,:,2)))+ (circshift(c4.*ux,[0,0,-3])+circshift(c3.*ux,[0,0,-2])+circshift(c2.*ux,[0,0,-1])+c1.*ux)./d0.* (nxyz(:,:,:,2)==1)+ (circshift(-c4.*ux,[0,0,3])+circshift(-c3.*ux,[0,0,2])+ circshift(-c2.*ux,[0,0,1])-c1.*ux)./d0.*(nxyz(:,:,:,2)==-1);

UX=(circshift(ux,[0,-1,0])-circshift(ux,[0,1,0]))./2.*(d_rho-abs(nxyz(:,:,:,1)))+ (circshift(c4.*ux,[0,-3,0])+circshift(c3.*ux,[0,-2,0])+circshift(c2.*ux,[0,-1,0])+c1.*ux)./d0.* (nxyz(:,:,:,1)==1)+ (circshift(-c4.*ux,[0,3,0])+circshift(-c3.*ux,[0,2,0])+ circshift(-c2.*ux,[0,1,0])-c1.*ux)./d0.*(nxyz(:,:,:,1)==-1);

UZbb=(circshift(e4.*ux,[3,0,0])+circshift(e3.*ux,[2,0,0])+circshift(e2.*ux,[1,0,0])+e1.*ux).* (nxyz(:,:,:,3)==1)+ (circshift(-e4.*ux,[-3,0,0])+circshift(-e3.*ux,[-2,0,0])+ circshift(-e2.*ux,[-1,0,0])-e1.*ux).*(nxyz(:,:,:,3)==-1);

UYbb=(circshift(e4.*ux,[0,0,-3])+circshift(e3.*ux,[0,0,-2])+circshift(e2.*ux,[0,0,-1])+e1.*ux).* (nxyz(:,:,:,2)==1)+ (circshift(-e4.*ux,[0,0,3])+circshift(-e3.*ux,[0,0,2])+ circshift(-e2.*ux,[0,0,1])-e1.*ux).*(nxyz(:,:,:,2)==-1);

UXbb=(circshift(e4.*ux,[0,-3,0])+circshift(e3.*ux,[0,-2,0])+circshift(e2.*ux,[0,-1,0])+e1.*ux).* (nxyz(:,:,:,1)==1)+ (circshift(-e4.*ux,[0,3,0])+circshift(-e3.*ux,[0,2,0])+ circshift(-e2.*ux,[0,1,0])-e1.*ux).*(nxyz(:,:,:,1)==-1);

%% pressure %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dPdL0=dPdL*afA(1);%*Gr;

if bf==0; for X=1:L;
rhok(:,X,:)=rho(:,X,:)-dPdL0*(L-X)/cs2;
end; else; rhok=rho; end;

Geff=0; Phi=0;
P=(rhok.*cs2+cs2/2*Geff.*Phi.^2).*(d_rho);%
%P=Pi;

p_da=sum(sum(P,3),2)./Af;
rho_da=sum(sum(rho,2),3)./Af;
p_ti=zeros(Nr,Mc,Ld);

%% tau_tot (d_rho) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Z=h0:H; Zh=H0-Z+1-1; % ¡¡ dPdL is -dPdL !!
tau_tot(Z)=dPdL0/g_density*(sum(Ar(Zh:-1:h0)));
end

nL=sum(sum(sum(abs(nxyz(:,:,:,3)),3),2),1)/L;
tau_tot2=dPdL0/g_density*g_rh;
u_star2=sqrt(tau_tot2);
u_star1=sqrt(dPdL0/g_density*ch_h);

%% axis Z0 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 tmp=abs(sum(sum(UZ(zmin:ch_h+zmin-1,:),2),3));
 [toll,Zh0]=min(tmp); Zh0=Zh0+zmin-1;
 Zh0=1;%Nr/2+1;
 Z0=H0-Zh0+1;
 %tau_slip=tau_tot2;
 Zhf=Zh0+2;
 tau_slip=tau_tot(H0-Zhf+1)*Zh0/Zhf;

%% tau_f (d_rho) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Z=h0:H; Zh=H0-Z+1; 
UZ_da(Z)=sum(sum(UZ(Zh,:),2),3)./Af(Zh);
UZ_da_Ar(Z)=UZ_da(Z).*Ar(Zh);
UY_da(Z)=sum(sum(UY(Zh,:),2),3)./Af(Zh);
UY_da_Ar(Z)=UY_da(Z).*Ar(Zh);
UX_da(Z)=sum(sum(UX(Zh,:),2),3)./Af(Zh);
UX_da_Ar(Z)=UX_da(Z).*Ar(Zh);
end

for Z=h0:H; Zh=H0-Z+1;
tau_fx(Z)=-g_nu.*(UZ_da_Ar(Z0)-UZ_da_Ar(Z));
tau_fx2(Z)=-g_nu.*(UY_da_Ar(Z0)-UY_da_Ar(Z));
end

%% tau_dv (Sv) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Z=h0:H; Zhx=H0-Z+1; 
visc_derx3(Z)=g_nu*sum(sum((UZbb(Zhx,:,:).*nxyz(Zhx,:,:,3)),3),2);
end
for Z=h0:H; Zhx=H0-Z+1; 
visc_derx2(Z)=g_nu*sum(sum((UYbb(Zhx,:,:).*nxyz(Zhx,:,:,2)),3),2);
end
for Z=h0:H; Zhx=H0-Z+1; 
visc_derx1(Z)=g_nu*sum(sum((UXbb(Zhx,:,:).*nxyz(Zhx,:,:,1)),3),2);
end

visc_derx1(Z0:H)=0; visc_derx2(Z0:H)=0; visc_derx3(Z0:H)=0;

for Z=h0:H;
tau_dvx1(Z)=sum(visc_derx1(Z:Z0))/L;
tau_dvx2(Z)=sum(visc_derx2(Z:Z0))/L;
tau_dvx3(Z)=sum(visc_derx3(Z:Z0))/L;
end 

%% tau_dp (Sv) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Z=h0:H; Zh=H0-Z+1;
p_ti(Zh,:)=P(Zh,:)-p_da(Zh);
end

for Z=h0:H; Zhx=H0-Z+1;
pressx(Z)=sum(sum(P(Zhx,:,:).*nxyz(Zhx,:,:,1),3),2);
end

for Z=h0:H;
tau_dpx(Z)=-sum(pressx(Z:Z0))/(L*g_density);
end

%%% tau_p (d_rho) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Z=h0:H; Zh=H0-Z+1;
p_da_Ar(Z)=p_da(Zh).*Ar(Zh);
end

for Z=h0:H;
tau_pz(Z)=1/g_density*(p_da_Ar(Z0)-p_da_Ar(Z));
end

%%% u_da u_ti (d_rho) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ux_da=sum(sum(ux.*d_rho,3),2)./Af;
uz_da=sum(sum(uz.*d_rho,3),2)./Af;

for Z=h0:H; Zh=H0-Z+1;
ux_ti(Zh,:,:)=ux(Zh,:,:).*d_rho(Zh,:,:)-ux_da(Zh);
uz_ti(Zh,:,:)=uz(Zh,:,:).*d_rho(Zh,:,:)-uz_da(Zh);
uxz_ti_da(Z)=sum(sum(ux_ti(Zh,:,:).*uz_ti(Zh,:,:),3),2)./Af(Zh);
uxz_ti_da_Ar(Z)=uxz_ti_da(Z).*Ar(Zh);
end

for Z=h0:H;
tau_fix(Z)=-(uxz_ti_da_Ar(Z0)-uxz_ti_da_Ar(Z));
end

%%% tau_a %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ux_da=sum(sum(ux.*d_rho,2),3)./Af;
uz_da=sum(sum(uz.*d_rho,2),3)./Af;

U_daZ=(circshift(ux_da,[1,0])-circshift(ux_da,[-1,0]))./2;

for Z=h0:H; Zh=H0-Z+1;
tau_a(Z)=-sum(uz_da(Zh:Zh0).*Ar(Zh:Zh0).*U_daZ(Zh:Zh0));
end 


%%% BALANCE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ff='-';
tau_x=tau_dpx+tau_dvx1+tau_dvx2+tau_dvx3+tau_fx+tau_fix;

%%%%%%

	figure(4),
    
  	subplot(1,3,1),
	hold off,
	plot((tau_tot(h0:H)./tau_tot2-tau_slip/tau_tot2)./W0,[0.5*1/(H-h0+1):1/(H-h0+1):1],'color','black');
	%plot(2*tau_tot1./tau_tot2,0,'o','color','red');
	
	hold on, plot(tau_fx(h0:H)./tau_tot2./W0,[0.5*1/(H-h0+1):1/(H-h0+1):1],ff,'color','green');
	hold on, plot(tau_dvx1(h0:H)./tau_tot2./W0,[0.5*1/(H-h0+1):1/(H-h0+1):1],ff,'color','blue');
	hold on, plot(tau_dvx2(h0:H)./tau_tot2./W0,[0.5*1/(H-h0+1):1/(H-h0+1):1],ff,'color','cyan');
	hold on, plot(tau_dvx3(h0:H)./tau_tot2./W0,[0.5*1/(H-h0+1):1/(H-h0+1):1],ff,'color','yellow');
	hold on, plot(tau_dpx(h0:H)./tau_tot2./W0,[0.5*1/(H-h0+1):1/(H-h0+1):1],ff,'color','magenta');
	hold on, plot(tau_fix(h0:H)./tau_tot2./W0,[0.5*1/(H-h0+1):1/(H-h0+1):1],ff,'color','red');
%	hold on, plot(tau_fx2(h0:H)./tau_tot2./W0,[0.5*1/(H-h0+1):1/(H-h0+1):1],ff,'color','yellow');
	hold on, plot(tau_x(h0:H)./tau_tot2./W0,[0.5*1/(H-h0+1):1/(H-h0+1):1],ff,'linewidth',2,'color','r');
	xlim([-0.15,2.0]);
    	ylim([0,1.0]);
    
    	subplot(1,3,2), hold off,
        hold on, plot(tau_fix(h0:H)./tau_tot2./W0,[0.5*1/(H-h0+1):1/(H-h0+1):1],ff,'color','red');
        %plot(vec_Iter,log10(convergence),'color','b');

	subplot(1,3,3), 
	hold off, plot(ux_da(2:Nr-1)./sqrt(cs2),[2:Nr-1],'-','color','g');	
	%hold on, plot(u_profile./sqrt(cs2),[2:ch_h+1],'o-','color','r');
	
	pause(), close(4)

