load('dump.CONV.h5');
con=LBdm.con;

x1=3;
x2=8;

A=[con(x1,2:end-1,1)',con(x2,2:end-1,1)'];
save('react_lbdm_2.dat','-ascii','A');

sol=load('react_th_2.dat');
figure(1),
plot(sol,'-');
hold on, plot(A,'o');
saveas(gcf,'profile_2.png','png');
