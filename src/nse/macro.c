#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include "hdf5.h"
#include "macro.h"
#include "../var/shared.h"

#ifndef REBIRTH_STEADY

#if (defined THERMAL && defined SCALAR)
int macro_nse (dfdq *f, macro *ux, macro *uy, macro *uz, macro *rho, dfdq *t, macro *tem, dfdq *c, macro *con) {
#elif (defined THERMAL || defined SCALAR)
int macro_nse (dfdq *f, macro *ux, macro *uy, macro *uz, macro *rho, dfdq *t, macro *tem) {
#else
int macro_nse (dfdq *f, macro *ux, macro *uy, macro *uz, macro *rho) {
#endif

#else

#if (defined THERMAL && defined SCALAR)
int macro_nse (dfdq *t, macro *tem, dfdq *c, macro *con) {
#elif (defined THERMAL || defined SCALAR)
int macro_nse (dfdq *t, macro *tem) {
#endif

#endif
    
  int idx=0, i=0, j=0, k=0, q=0;

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

        for (q=0; q<NQ; q++) {

	#ifndef REBIRTH_STEADY
        ux[idx] += f[idx].dq[q] * cx[q]; 	
        uy[idx] += f[idx].dq[q] * cy[q]; 	
        uz[idx] += f[idx].dq[q] * cz[q]; 	
        rho[idx] += f[idx].dq[q];
	#endif

        #if (defined THERMAL && defined SCALAR)
        con[idx] += c[idx].dq[q];
	#endif

        #if (defined THERMAL || defined SCALAR)
        tem[idx] += t[idx].dq[q];
	#endif

        }
      }
    }
  }
}

#ifndef REBIRTH_STEADY

#if (defined THERMAL && defined SCALAR)
int macro_nse_zero (macro *ux, macro *uy, macro *uz, macro *rho, macro *tem, macro *con) {
#elif (defined THERMAL || defined SCALAR)
int macro_nse_zero (macro *ux, macro *uy, macro *uz, macro *rho, macro *tem) {
#else
int macro_nse_zero (macro *ux, macro *uy, macro *uz, macro *rho) {
#endif

#else

#if (defined THERMAL && defined SCALAR)
int macro_nse_zero (macro *tem, macro *con) {
#elif (defined THERMAL || defined SCALAR)
int macro_nse_zero (macro *tem) {
#endif

#endif
    
  int idx=0, i=0, j=0, k=0, q=0;

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

      #ifndef REBIRTH_STEADY
      ux[idx]=0.0;
      uy[idx]=0.0;
      uz[idx]=0.0;
      rho[idx]=0.0;
      #endif

      #if (defined THERMAL && defined SCALAR)
      con[idx]=0.0;
      #endif

      #if (defined THERMAL || defined SCALAR)
      tem[idx]=0.0;
      #endif

      }
    }
  }
}

int periodic_macro (double *rho) {

  MPI_Status status1;

  /* int MPI_Sendrecv(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
     int dest, int sendtag, void *recvbuf, int recvcount,
     MPI_Datatype recvtype, int source, int recvtag,
     MPI_Comm comm, MPI_Status *status) */


  MPI_Sendrecv(rho+lzp*lxp*ly+lzp+1    , 1, mpi_maplane_y, pryp, 3, \
               rho+lzp+1               , 1, mpi_maplane_y, prym, 3, mpi_comm_y, &status1);
  MPI_Sendrecv(rho+lzp*lxp+lzp+1       , 1, mpi_maplane_y, prym, 4, \
               rho+lzp*lxp*(ly+1)+lzp+1, 1, mpi_maplane_y, pryp, 4, mpi_comm_y, &status1);

  MPI_Sendrecv(rho+lzp*lx+1    , 1, mpi_maplane_x, prxp, 1, \
               rho+1           , 1, mpi_maplane_x, prxm, 1, mpi_comm_x, &status1);
  MPI_Sendrecv(rho+lzp+1       , 1, mpi_maplane_x, prxm, 2, \
               rho+lzp*(lx+1)+1, 1, mpi_maplane_x, prxp, 2, mpi_comm_x, &status1);

  MPI_Sendrecv(rho+lz  , 1, mpi_maplane_z, przp, 5, \
               rho     , 1, mpi_maplane_z, przm, 5, mpi_comm_z, &status1);
  MPI_Sendrecv(rho+1   , 1, mpi_maplane_z, przm, 6, \
               rho+lz+1, 1, mpi_maplane_z, przp, 6, mpi_comm_z, &status1);

}

int periodic_int (int *lsol) {

  MPI_Status status1;

  /* int MPI_Sendrecv(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
     int dest, int sendtag, void *recvbuf, int recvcount,
     MPI_Datatype recvtype, int source, int recvtag,
     MPI_Comm comm, MPI_Status *status) */

  MPI_Sendrecv(lsol+lzp*lxp*ly+lzp+1    , 1, mpi_intplane_y, pryp, 3, \
               lsol+lzp+1               , 1, mpi_intplane_y, prym, 3, mpi_comm_y, &status1);
  MPI_Sendrecv(lsol+lzp*lxp+lzp+1       , 1, mpi_intplane_y, prym, 4, \
               lsol+lzp*lxp*(ly+1)+lzp+1, 1, mpi_intplane_y, pryp, 4, mpi_comm_y, &status1);

  MPI_Sendrecv(lsol+lzp*lx+1    , 1, mpi_intplane_x, prxp, 1, \
               lsol+1           , 1, mpi_intplane_x, prxm, 1, mpi_comm_x, &status1);
  MPI_Sendrecv(lsol+lzp+1       , 1, mpi_intplane_x, prxm, 2, \
               lsol+lzp*(lx+1)+1, 1, mpi_intplane_x, prxp, 2, mpi_comm_x, &status1);

  MPI_Sendrecv(lsol+lz  , 1, mpi_intplane_z, przp, 5, \
               lsol     , 1, mpi_intplane_z, przm, 5, mpi_comm_z, &status1);
  MPI_Sendrecv(lsol+1   , 1, mpi_intplane_z, przm, 6, \
               lsol+lz+1, 1, mpi_intplane_z, przp, 6, mpi_comm_z, &status1);

}

#if (defined TWOPHASE && defined NON_NEWTONIAN)
int macro_forcing (int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *psix, macro *psiy, macro *psiz, macro *taun) {
#elif (defined TWOPHASE && !defined NON_NEWTONIAN)
int macro_forcing (int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *psix, macro *psiy, macro *psiz) {
#elif (defined GRAVITY && defined SCALAR)
int macro_forcing (int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *con) {
#else
int macro_forcing (int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho) {
#endif

  int idx=0, i=0, j=0, k=0;
  double ramp = 0.0;

  #ifdef ATAN_FORCE
  ramp = atan((double)iter/(double)MAX_ITER*20.0) / (M_PI/2.0);
  #else
  ramp = 1.0;
  #endif

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

        if (lsol[idx] == 1) {

        #ifdef GRAVITY

	  // Buoyancy on scalar
	  #ifdef SCALAR
          ux[idx] += 0.5 * (con[idx]) *(double) DPDL * (double) dpdlv[0];
          uy[idx] += 0.5 * (con[idx]) *(double) DPDL * (double) dpdlv[1];
          uz[idx] += 0.5 * (con[idx]) *(double) DPDL * (double) dpdlv[2];
          #else

          ux[idx] += 0.5 * rho[idx] *(double) DPDL * (double) dpdlv[0];
          uy[idx] += 0.5 * rho[idx] *(double) DPDL * (double) dpdlv[1];
          uz[idx] += 0.5 * rho[idx] *(double) DPDL * (double) dpdlv[2];
	  #endif

        #else
        ux[idx] += 0.5 * ramp * (double) DPDL * (double) dpdlv[0];
        uy[idx] += 0.5 * ramp * (double) DPDL * (double) dpdlv[1];
        uz[idx] += 0.5 * ramp * (double) DPDL * (double) dpdlv[2];
        #endif  

        #if (defined TWOPHASE && !defined MULTICOMP)

	  // non-Newtonian
	  #ifdef NON_NEWTONIAN
	  if (iter==0)
	  taun[idx]=TAU;

          ux[idx] += (double)taun[idx] * (double) psix[idx] * (double)lsol[idx];
          uy[idx] += (double)taun[idx] * (double) psiy[idx] * (double)lsol[idx];
          uz[idx] += (double)taun[idx] * (double) psiz[idx] * (double)lsol[idx];

	  // Newtonian
	  #else
          ux[idx] += (double)TAU * (double) psix[idx] * (double)lsol[idx];
          uy[idx] += (double)TAU * (double) psiy[idx] * (double)lsol[idx];
          uz[idx] += (double)TAU * (double) psiz[idx] * (double)lsol[idx];
          #endif

	#elif (defined TWOPHASE && defined MULTICOMP && defined MULTICOMP_GUO)
        ux[idx] += 0.5 * (double) psix[idx] * (double)lsol[idx];
        uy[idx] += 0.5 * (double) psiy[idx] * (double)lsol[idx];
        uz[idx] += 0.5 * (double) psiz[idx] * (double)lsol[idx];
	#endif

	#if (defined ACOUSTIC_FORCEX || defined ACOUSTIC_FORCEY || defined ACOUSTIC_FORCEZ)
        ux[idx] += 0.5 * (double) AWAVE * (double)sin(2.0*M_PI*iter*(double)(FWAVE/MAX_ITER)) * (double) wavev[0];
        uy[idx] += 0.5 * (double) AWAVE * (double)sin(2.0*M_PI*iter*(double)(FWAVE/MAX_ITER)) * (double) wavev[1];
        uz[idx] += 0.5 * (double) AWAVE * (double)sin(2.0*M_PI*iter*(double)(FWAVE/MAX_ITER)) * (double) wavev[2];
	#endif

        }
      }
    }
  }
}

#ifdef SOURCE

#ifdef SOURCE_HET
int macro_source (macro *tem, macro *st1, macro *st2) {
#else
int macro_source (macro *tem, macro *st1) {
#endif

  int idx=0, i=0, j=0, k=0;

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

      #ifdef SOURCE_HET
      tem[idx] = tem[idx] + 0.5 * (st1[idx]+st2[idx]);
      #else
      tem[idx] = tem[idx] + 0.5 * st1[idx];
      #endif

      }
    }
  }
}
#endif

#if (defined THERMAL && defined SCALAR)
int macro_nse_inc (int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *tem, macro *con, int F) {
#elif (defined THERMAL || defined SCALAR)
int macro_nse_inc (int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *tem, int F) {
#else
int macro_nse_inc (int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, int F) {
#endif

  int i=0, j=0, k=0, idx=0;
  
  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

      #ifndef REBIRTH_STEADY
      if (F == 1) {
      rho[idx] = (double)(rho[idx]*lsol[idx]);
      ux[idx]  = (double)ux[idx]/((double)rho[idx]+(double)(1-lsol[idx]));
      uy[idx]  = (double)uy[idx]/((double)rho[idx]+(double)(1-lsol[idx]));
      uz[idx]  = (double)uz[idx]/((double)rho[idx]+(double)(1-lsol[idx]));
      }
      #endif

      #if (defined THERMAL && defined SCALAR)
      if (F == 2)
      con[idx]  = (double)con[idx]/((double)rho[idx]+(double)(1-lsol[idx])*RHO0);
      if (F == 0)
      con[idx]  = (double)con[idx]*((double)rho[idx]+(double)(1-lsol[idx])*RHO0);
      #endif

      #if ((defined THERMAL && !defined TWOPHASE) || defined SCALAR)
      if (F == 2)
      tem[idx]  = (double)tem[idx]/((double)rho[idx]+(double)(1-lsol[idx])*(double)RHO0);
      if (F == 0)
      tem[idx]  = (double)tem[idx]*((double)rho[idx]+(double)(1-lsol[idx])*RHO0);
      #endif

      }
    }
  }
}

#ifdef NON_NEWTONIAN
int macro_nse_clean (int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *psix, macro *psiy, macro *psiz, macro *taun, int iter) {
#else
int macro_nse_clean (int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *psix, macro *psiy, macro *psiz, double C) {
#endif

  int i=0, j=0, k=0, idx=0;
  
  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

      #ifdef NON_NEWTONIAN
      if (iter==0)
      taun[idx]=TAU;

      // Purging from Van der Waals Forces (non-N)
      ux[idx] += (-taun[idx]+0.5) * (double) (psix[idx]/((double)rho[idx]+(double)(1-lsol[idx]))) * (double)lsol[idx];
      uy[idx] += (-taun[idx]+0.5) * (double) (psiy[idx]/((double)rho[idx]+(double)(1-lsol[idx]))) * (double)lsol[idx];
      uz[idx] += (-taun[idx]+0.5) * (double) (psiz[idx]/((double)rho[idx]+(double)(1-lsol[idx]))) * (double)lsol[idx];

      #else
      // Purging from Van der Waals Forces
      ux[idx] += C * (double) (psix[idx]/((double)rho[idx]+(double)(1-lsol[idx]))) * (double)lsol[idx];
      uy[idx] += C * (double) (psiy[idx]/((double)rho[idx]+(double)(1-lsol[idx]))) * (double)lsol[idx];
      uz[idx] += C * (double) (psiz[idx]/((double)rho[idx]+(double)(1-lsol[idx]))) * (double)lsol[idx];
      #endif

      }
    }
  }
}

#ifdef SPATIAL_WETTING
int macro_twophase (int *lsol, double *lwet, macro *rho, macro *psix, macro *psiy, macro *psiz) {
#elif (defined MULTICOMP)
int macro_twophase (int *lsol, macro *rho, macro *rho2, macro *psix, macro *psiy, macro *psiz, int F) {
#else
int macro_twophase (int *lsol, macro *rho, macro *psix, macro *psiy, macro *psiz) {
#endif

  /* Psudopotential forcing will lead to EOS of the kind:
  pressure P = rho (T|cs2) + G cs2 /2 psi**2  */

  int i=0, j=0, k=0, idx=0;
  int q=0, idxq=0, idxiq=0;
  int nq=0;
  double G0=0.;
 
  //Temporary allocate PSI
  macro *PSI;
  PSI=(macro *)calloc(lxp*lyp*lzp, sizeof(macro));

  #ifdef MULTICOMP
  macro *PSI2;
  PSI2=(macro *)calloc(lxp*lyp*lzp, sizeof(macro));
  #endif

  #ifdef MULTICOMP
  double rhowallc=0.0;
  if (F==2) {
  G0 = GG2;
  rhowallc = RHOWALL2;
  } else {
  rhowallc = RHOWALL;
  #endif

  // Setup the non-mutual interaction
  G0 = GG;

  #ifdef MULTICOMP
  }
  #endif

  for (j=0; j<lyp; j++) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idx = IDXP(i,j,k);

        // Set fictious density at walls
        if (lsol[idx] == 0) {

	// If we haven't already averaged
        #ifndef NEIGHBOURING

	  // Wetting defines rho
	  #ifdef SPATIAL_WETTING
          rho[idx] = (double)lwet[idx];
	  #else

	    #ifdef MULTICOMP
	    rho2[idx] = (double)rhowallc/(double)GR;
	    #else
            rho[idx] = (double)RHOWALL;
	    #endif

	  #endif

        #endif

        }

      // Determine PSI Pseudopotential and zero xyz force
      #ifdef THERMAL
      PSI[idx] = (double) exp(-1./(double)rho[idx]);
      #else

	#ifdef MULTICOMP
	PSI[idx] = (double)rho[idx];
	PSI2[idx] = (double)rho2[idx];
	#else

        PSI[idx] = sqrt(RHOZ) * (double)( 1.-exp( -(double)rho[idx]/(double)RHOZ) );
	#endif

      #endif

      psix[idx] = 0.0;
      psiy[idx] = 0.0;
      psiz[idx] = 0.0;

	#if (defined MULTICOMP && defined NEIGHBOURING)
        if (F==2) {
	#endif

        // Restore zero density at walls
        if (lsol[idx] == 0)
	rho[idx] = 0.0;

	#ifdef MULTICOMP 
        // Restore zero density at walls
        if (lsol[idx] == 0)
	rho2[idx] = 0.0;
	#endif

	#if (defined MULTICOMP && defined NEIGHBOURING)
	};
	#endif

      }
    }
  }

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

	if (lsol[idx] == 1) {

          for (nq=0; nq<(int)(NQ-1)/2; nq++) {
          q=(int)vq[nq];

          idxq = IDXP(i+cx[q],j+cy[q],k+cz[q]);
          idxiq = IDXP(i+cx[iq[q]],j+cy[iq[q]],k+cz[iq[q]]);

	  #ifdef MULTICOMP
	  //xyz Forcing on rho1 the basis of PSI(rho1),PSI(rho2)
	  psix[idx] += PSI[idx] * ((double)GR * (double)w[q] * PSI2[idxq] * (double)cx[q] + \
				   (double)GR * (double)w[iq[q]] * PSI2[idxiq] * (double)cx[iq[q]]);
	  psiy[idx] += PSI[idx] * ((double)GR * (double)w[q] * PSI2[idxq] * (double)cy[q] + \
				   (double)GR * (double)w[iq[q]] * PSI2[idxiq] * (double)cy[iq[q]]);
	  psiz[idx] += PSI[idx] * ((double)GR * (double)w[q] * PSI2[idxq] * (double)cz[q] + \
				   (double)GR * (double)w[iq[q]] * PSI2[idxiq] * (double)cz[iq[q]]);

	  //xyz Forcing on the basis of PSI(rho) * lsol
	  psix[idx] += PSI[idx] * ((double)G0 * (double)w[q] * PSI[idxq] * lsol[idxq] * (double)cx[q] + \
				   (double)G0 * (double)w[iq[q]] * PSI[idxiq] * (double)cx[iq[q]]);
	  psiy[idx] += PSI[idx] * ((double)G0 * (double)w[q] * PSI[idxq] * lsol[idxq] * (double)cy[q] + \
				   (double)G0 * (double)w[iq[q]] * PSI[idxiq] * (double)cy[iq[q]]);
	  psiz[idx] += PSI[idx] * ((double)G0 * (double)w[q] * PSI[idxq] * lsol[idxq] * (double)cz[q] + \
				   (double)G0 * (double)w[iq[q]] * PSI[idxiq] * (double)cz[iq[q]]);
	  #else

	  //xyz Forcing on the basis of PSI(rho)
	  psix[idx] += PSI[idx] * ((double)G0 * (double)w[q] * PSI[idxq] * (double)cx[q] + \
				   (double)G0 * (double)w[iq[q]] * PSI[idxiq] * (double)cx[iq[q]]);
	  psiy[idx] += PSI[idx] * ((double)G0 * (double)w[q] * PSI[idxq] * (double)cy[q] + \
				   (double)G0 * (double)w[iq[q]] * PSI[idxiq] * (double)cy[iq[q]]);
	  psiz[idx] += PSI[idx] * ((double)G0 * (double)w[q] * PSI[idxq] * (double)cz[q] + \
				   (double)G0 * (double)w[iq[q]] * PSI[idxiq] * (double)cz[iq[q]]);
	  #endif
          }
        }
      }
    }
  }

  free(PSI);
  #ifdef MULTICOMP
  free(PSI2);
  #endif

}

#ifdef SPATIAL_WETTING
int macro_twophase_neigh (int *lsol, macro *rho, macro *lwet) {
#else

#ifdef MULTICOMP
int macro_twophase_neigh (int *lsol, macro *rho, macro *rho2, int F) {
#else
int macro_twophase_neigh (int *lsol, macro *rho) {
#endif

#endif

  // Running over available neighbours
  // once we set psi for wall/fluid nodes.

  int i=0, j=0, k=0, idx=0;
  int q=0, idxq=0, idxiq=0;
  int nq=0;

  int countne=0;
  int inq=0, jnq=0, knq=0;
  int iniq=0, jniq=0, kniq=0;
  double rhone=0.0;

  #ifdef MULTICOMP
  double rhowallc=0.;
  if (F==1) {
  rhowallc=-RHOWALL;
  } else {
  rhowallc=-RHOWALL2;
  }
  #endif

  #ifdef MULTICOMP
  double rhonee=0.;
  #endif

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

        if (lsol[idx] == 0) { 
        countne=0;
        rhone=0.0;

	#ifdef MULTICOMP
        rhonee=0.0;
	#endif

          //Pair of opposites directions
          for (nq=0; nq<(int)(NQ-1)/2; nq++) {
          q=(int)vq[nq];

          inq = i+cx[q];
          jnq = j+cy[q];
          knq = k+cz[q];

          iniq = i+cx[iq[q]];
          jniq = j+cy[iq[q]];
          kniq = k+cz[iq[q]];

            // Check if it's inside domain [q] 
            if ((inq>=0&&inq<lxp)&&(jnq>=0&&jnq<lyp)&&(knq>=0&&knq<lzp)) {

            idxq = IDXP(inq,jnq,knq);
  
              // And if it's fluid [q]
              if (lsol[idxq] == 1) {
              countne++;
              rhone += rho[idxq];

	      #ifdef MULTICOMP
	      rhonee += rho[idxq]*rho[idxq];
	      #endif
              }
            }

            // Check if it's inside domain [iq]
            if ((iniq>=0&&iniq<lxp)&&(jniq>=0&&jniq<lyp)&&(kniq>=0&&kniq<lzp)) {

            idxiq = IDXP(iniq,jniq,kniq);
  
              // And if it's fluid [iq]
              if (lsol[idxiq] == 1) {
              countne++;
              rhone += rho[idxiq];

	      #ifdef MULTICOMP
	      rhonee += rho[idxiq]*rho[idxiq];
	      #endif
              }
            }
          }

          if (countne>0) {

	  // Wetting defines PSI
	  #ifdef SPATIAL_WETTING
          rho[idx] = (double)(rhone/(double)countne) + (double)lwet[idx];
	  #else

	    // Fictious density
	    #ifdef MULTICOMP
            rho2[idx] = (double)(rhonee-rhone*rhone/(double)countne)/(double)countne * (double)rhowallc;
	    #else
            rho[idx] = (double)(rhone/(double)countne) + (double)RHOWALL;
	    #endif

	   // minimum
	   //if (rho[idx] < 0.001)
	   //rho[idx] = 0.001;

	  #endif

          }
        }
      }
    }
  }
} 

int macro_twophase_inout (int iter, int *lsol, macro *psix, macro *psiy, macro *psiz) {

  int q=0, i=0, j=0, k=0, idx=0, idx1=0;

  // Inlet
  #if (defined DENSITY_XM || defined DENSITY_YM || defined DENSITY_ZM || \
       defined VELOCITY_XM || defined VELOCTITY_YM || defined VELOCITY_ZM)

  #if (defined DENSITY_XM || defined VELOCITY_XM)
  //Check if processor is involved
  if ((int)prx == (int)0) {

    i = 0;
    for (j=1; j<=ly; j++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_YM || defined VELOCITY_YM)
  //Check if processor is involved
  if ((int)pry == (int)0) {

    j = 0;
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_ZM || defined VELOCITY_ZM)
  //Check if processor is involved
  if ((int)prz == (int)0) {

    k = 0;
    for (j=1; j<=ly; j++) {
      for (i=1; i<=lx; i++) {

  #endif

      idx = IDXP(i,j,k);

      #if (defined DENSITY_XM || defined VELOCITY_XM)
      idx1 = IDXP(i+1,j,k);
      #elif (defined DENSITY_YM || defined VELOCITY_YM)
      idx1 = IDXP(i,j+1,k);
      #elif (defined DENSITY_ZM || defined VELOCITY_ZM)
      idx1 = IDXP(i,j,k+1);
      #endif

      psix[idx1] = 0.0;
      psiy[idx1] = 0.0;
      psiz[idx1] = 0.0;

      // Correction required at first step if BCs not symmetric
      if (iter == 0)
      lsol[idx] = lsol[idx1];

      }
    }
  }

  #endif

  // Outlet
  #if (defined DENSITY_XP || defined DENSITY_YP || defined DENSITY_ZP || \
       defined VELOCITY_XP || defined VELOCTITY_YP || defined VELOCITY_ZP)

  #if (defined DENSITY_XP || defined VELOCITY_XP)
  //Check if processor is involved
  if ((int)prx == (int)NEX-1) {

    i = lx+1;
    for (j=1; j<=ly; j++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_YP || defined VELOCITY_YP)
  //Check if processor is involved
  if ((int)pry == (int)NEY-1) {

    j = ly+1;
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_ZP || defined VELOCITY_ZP)
  //Check if processor is involved
  if ((int)prz == (int)NEZ-1) {

    k = lz+1;
    for (j=1; j<=ly; j++) {
      for (i=1; i<=lx; i++) {

  #endif

      idx = IDXP(i,j,k);

      #if (defined DENSITY_XP || defined VELOCITY_XP)
      idx1 = IDXP(i-1,j,k);
      #elif (defined DENSITY_YP || defined VELOCITY_YP)
      idx1 = IDXP(i,j-1,k);
      #elif (defined DENSITY_ZP || defined VELOCITY_ZP)
      idx1 = IDXP(i,j,k-1);
      #endif

      psix[idx1] = 0.0;
      psiy[idx1] = 0.0;
      psiz[idx1] = 0.0;

      // Correction required at first step if BCs not symmetric
      if (iter == 0)
      lsol[idx] = lsol[idx1];

      }
    }
  }

  #endif

}

int macro_twophase_slip (macro *rho) {

  int q=0, i=0, j=0, k=0, idx=0, idx1=0;

  // Inlet
  #if (defined FREESLIP_XM || defined FREESLIP_YM || defined FREESLIP_ZM)

  #if (defined FREESLIP_XM)
  //Check if processor is involved
  if ((int)prx == (int)0) {

    i = 0;
    for (j=0; j<=ly+1; j++) {
      for (k=0; k<=lz+1; k++) {

  #elif (defined FREESLIP_YM)
  //Check if processor is involved
  if ((int)pry == (int)0) {

    j = 0;
    for (i=0; i<=lx+1; i++) {
      for (k=0; k<=lz+1; k++) {

  #elif (defined FREESLIP_ZM)
  //Check if processor is involved
  if ((int)prz == (int)0) {

    k = 0;
    for (j=0; j<=ly+1; j++) {
      for (i=0; i<=lx+1; i++) {

  #endif

      idx = IDXP(i,j,k);

      #if (defined FREESLIP_XM)
      idx1 = IDXP(i+1,j,k);
      #elif (defined FREESLIP_YM)
      idx1 = IDXP(i,j+1,k);
      #elif (defined FREESLIP_ZM)
      idx1 = IDXP(i,j,k+1);
      #endif

      rho[idx] = rho[idx1];

      }
    }
  }

  #endif

  // Outlet
  #if (defined FREESLIP_XP || defined FREESLIP_YP || defined FREESLIP_ZP)

  #if (defined FREESLIP_XP)
  //Check if processor is involved
  if ((int)prx == (int)NEX-1) {

    i = lx+1;
    for (j=0; j<=ly+1; j++) {
      for (k=0; k<=lz+1; k++) {

  #elif (defined FREESLIP_YP)
  //Check if processor is involved
  if ((int)pry == (int)NEY-1) {

    j = ly+1;
    for (i=0; i<=lx+1; i++) {
      for (k=0; k<=lz+1; k++) {

  #elif (defined FREESLIP_ZP)
  //Check if processor is involved
  if ((int)prz == (int)NEZ-1) {

    k = lz+1;
    for (j=0; j<=ly+1; j++) {
      for (i=0; i<=lx+1; i++) {

  #endif

      idx = IDXP(i,j,k);

      #if (defined FREESLIP_XP)
      idx1 = IDXP(i-1,j,k);
      #elif (defined FREESLIP_YP)
      idx1 = IDXP(i,j-1,k);
      #elif (defined FREESLIP_ZP)
      idx1 = IDXP(i,j,k-1);
      #endif

      rho[idx] = rho[idx1];

      }
    }
  }

  #endif

}

int macro_mem_inner_bcs (int *lsol, int *innerCellsNr) {

  int idx=0, i=0, j=0, k=0, q=0;
  *innerCellsNr = 0;

  // Start counting how many cells in each type inner boundary
  // Inner BCs should be declared on the geometry txt file
  // See HELPGEO

  for (j = 1; j < (lyp-1); j++) {
    for (i = 1; i < (lxp - 1); i++) {
      for (k = 1; k < (lzp - 1); k++) {

      idx = IDXP(i, j, k);

	// Value 4 for Inner BC
	if (lsol[idx] == 4)
        ++(*innerCellsNr);

      }
    }
  }

  fprintf(stderr, "II: Init INNER_TIME_BCS. I have found innerCellsNr: %d\n", *innerCellsNr);

}

int macro_init_inner_bcs (int *lsol, int **innerCells) {

  int i0=0, idx=0, i=0, j=0, k=0;

  for (j = 1; j < (lyp -1); j++) {
    for (i = 1; i < (lxp -1); i++) {
      for (k = 1; k < (lzp -1); k++) {

      idx = IDXP(i, j, k);

	// Value 4 for Inner BC
	if (lsol[idx] == 4) {
        innerCells[i0][0] = i;
        innerCells[i0][1] = j;
        innerCells[i0][2] = k;

	// Restore (assuming) fluid
	lsol[idx]=1;

	// Count
	++i0;

	}
      }
    }
  }
}

#ifdef NON_NEWTONIAN
int macro_shear_rate (int *lsol, macro *rho, macro *taun, macro *stress, dfdq *f, dfdq *feq, int iter) {

  int i=0, j=0, k=0, q=0, idx=0;
  double D2=0., Sxx=0., Syy=0., Szz=0.;
  double Sxy=0., Sxz=0., Syz=0.;
  double mueff=0., shear=0.;
  double mueffmin=0.;

  // define starting iter & density
  int itero=100;
  double rhoo=0.87*RHO2;

  for (j = 1; j < (lyp -1); j++) {
    for (i = 1; i < (lxp -1); i++) {
      for (k = 1; k < (lzp -1); k++) {

      idx = IDXP(i, j, k);

        // Applies to specific nodes
        #ifdef TWOPHASE
        if ((lsol[idx] == 1) && (rho[idx] > rhoo)) {
        #else
        if (lsol[idx] == 1) {
	#endif

	  if (iter<itero) {
	  taun[idx] = TAU;
	  } else {

            for (q=0; q<NQ; q++) {

	    // Strain rate tensor
	    Sxx  += (double)(-1./(2.*rho[idx]*CS2*taun[idx])) * (cx[q]*cx[q]) * (f[idx].dq[q]-feq[idx].dq[q]);
	    Syy  += (double)(-1./(2.*rho[idx]*CS2*taun[idx])) * (cy[q]*cy[q]) * (f[idx].dq[q]-feq[idx].dq[q]);
	    Szz  += (double)(-1./(2.*rho[idx]*CS2*taun[idx])) * (cz[q]*cz[q]) * (f[idx].dq[q]-feq[idx].dq[q]);
	    Sxy  += (double)(-1./(2.*rho[idx]*CS2*taun[idx])) * (cx[q]*cy[q]) * (f[idx].dq[q]-feq[idx].dq[q]);
	    Sxz  += (double)(-1./(2.*rho[idx]*CS2*taun[idx])) * (cx[q]*cz[q]) * (f[idx].dq[q]-feq[idx].dq[q]);
	    Syz  += (double)(-1./(2.*rho[idx]*CS2*taun[idx])) * (cy[q]*cz[q]) * (f[idx].dq[q]-feq[idx].dq[q]);

	    }

	  // Dii
          D2 = (Sxx*Sxx + 2*Sxy*Sxy + 2*Sxz*Sxz + \
                Syy*Syy + 2*Syz*Syz + Szz*Szz) ;

          // Shear rate
          shear = (double)pow(2.*D2,0.5);
	  stress[idx] = (double)shear;

          // To null
          Sxx=0.0; Syy=0.0; Szz=0.0;
          Sxy=0.0; Sxz=0.0; Syz=0.0;

          #ifdef HERSCHEL_BULCKLEY
          // J Boyd et al 2006 J. Phys. A: Math. Gen. 39 14241
          mueff = (double)(NN_YIELD/shear) * (double)(1.-(exp(-(double)NN_MTUNE*shear))) + \
                  (double)(NN_KAPPA*pow(shear,(double)(NN_EXPON-1.)));

          // New relaxation time
          taun[idx] = (double)(mueff/(rho[idx]*CS2))+0.5;
	  #endif

	  }


	#ifdef TWOPHASE
	} else if ((lsol[idx] == 1) && (rho[idx] <= rhoo)) {

	  // To gas phase
	  if (iter<itero) {
	  taun[idx] = TAU;
	  } else {
	  taun[idx] = ((double)(NN_KAPPA/(RHO2*CS2))+0.5);
	  }

	#endif
	}
      }
    }
  }
}

#endif

#ifdef EXOTHERMIC
int macro_exo_react(int *lsol, macro *tem, macro *lwet) {

  int i=0, j=0, k=0, idx=0;
  double tkel=0.;

  // Eyring equation (transm. coeff=1)
  // Gamma1 = T0 kb/h exp(dS/R-dH/R/T0) / (D/Scale^2)
  // Gamma2 = dH / (RT0)
  // k = Gamma1*T*exp(Gamma2(1-1/T))
  // 6/Scale = Spec.surf material

  // Include ghost nodes
  for (j=0; j<lyp; j++) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idx=IDXP(i,j,k);

        // Update reaction with temperature
        if (lsol[idx]==0 && lwet[idx]!=0.) {

	  #ifdef EXOTHERMIC_LIN
	  if (tem[idx]>1.) {
	  #else
	  if (tem[idx]>0.) {
	  #endif

	  tkel = (tem[idx]*20.+273.15)/293.15;

	  #ifdef EXOTHERMIC_LIN
          lwet[idx] = GAMMA1*tem[idx]*(1.+(GAMMA2*(1.-1./tem[idx]))) \
		      * (double)CS2*(TAUC-0.5)/SCALE/6.;
	  #else
          lwet[idx] = GAMMA1*tkel*exp(GAMMA2*(1.-1./tkel)) \
		      * (double)CS2*(TAUC-0.5)/SCALE/6.;
	  #endif

	  } else {
	  #ifdef EXOTHERMIC_LIN
	  lwet[idx] = GAMMA1 \
		      * (double)CS2*(TAUC-0.5)/SCALE/6.;
	  #else
	  lwet[idx] = 0.;
	  #endif
	  }
	}
      }
    }
  }
}
#endif

#ifndef REBIRTH_STEADY
int macro_nse_chk (int iter, int *lsol, macro *rho, macro *ux, macro *uy, macro *uz) {
#else
int macro_nse_chk (int iter, int *lsol, macro *tem) {
#endif

  int itchk = 0;
  double residual = 0.0;
  double re = 0.0, re_all=0.0;
  int i=0, j=0, k=0, idx=0;
  int sizep = 0, sizep_all=0;
  char fname[128];
  FILE *fileconv;

  if (ROOT(world_rank)) {
    sprintf(fname,"out/conv_re.dat");
    fileconv = fopen(fname,"a");
  }

  // Single procs
  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);
 
        if (lsol[idx] == 1) {
        sizep += (int)lsol[idx];

	#ifndef REBIRTH_STEADY

        #if (  (defined DPDLX && !defined DPDLY && !defined DPDLZ) ||\
	       (defined ACOUSTIC_FORCEX && !defined ACOUSTIC_FORCEY && !defined ACOUSTIC_FORCEZ) )
        re += (double)(ux[idx]*(double)CLENGTH/(CS2*(TAU-0.5))); 
        #elif ((defined DPDLY && !defined DPDLX && !defined DPDLZ) ||\
	       (defined ACOUSTIC_FORCEY && !defined ACOUSTIC_FORCEX && !defined ACOUSTIC_FORCEZ) )
        re += (double)(uy[idx]*(double)CLENGTH/(CS2*(TAU-0.5))); 
        #elif ((defined DPDLZ && !defined DPDLY && !defined DPDLX) ||\
	       (defined ACOUSTIC_FORCEZ && !defined ACOUSTIC_FORCEY && !defined ACOUSTIC_FORCEX) )
        re += (double)(uz[idx]*(double)CLENGTH/(CS2*(TAU-0.5))); 
        #else
        re += (double)(sqrt(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)) * \
              (double)CLENGTH/(CS2*(TAU-0.5)));
        #endif

	#else
        re += (double)(tem[idx]);
	#endif

        }
      }
    }
  }

  // All procs
  MPI_Allreduce (&re, &re_all, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce (&sizep, &sizep_all, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  re_all = (double)(re_all/sizep_all);
  residual = (double)(sqrt(pow((re_all-retmp)/retmp,2.)));
  retmp = re_all;

  if (ROOT(world_rank)) {
    if (iter % OUT_ITER == 0) {
    #ifndef REBIRTH_STEADY
    fprintf(stderr,"II: CHK at iter %d Re = %g\n",iter,re_all);
    fprintf(stderr,"II: CHK at iter %d Residual = %g\n",iter,residual);
    #else
    #ifdef THERMAL
    fprintf(stderr,"II: CHK at iter %d Tem = %g\n",iter,re_all);
    #else
    fprintf(stderr,"II: CHK at iter %d Con = %g\n",iter,re_all);
    #endif
    fprintf(stderr,"II: CHK at iter %d Residual = %g\n",iter,residual);
    #endif
    }

  fprintf(fileconv,"%g %g\n",re_all,residual);
  fclose(fileconv);

  // Check
  #ifndef REBIRTH_STEADY
  itchk = (int)(iter/CHK_ITER);
  RECONV[itchk-1] = residual;

    if (isnan(re_all)) {

    fprintf(stderr,"EE: Reynolds NaN Error.\n");
    exit(1); 

    }

    if (residual <= RESIDUAL) {

    // Eventually modify for better convergence
    #ifdef TWOPHASE
    if ((int)iter>50) {
      if ((int)ITERCONVTP+CHK_ITER==(int)iter) { 
      CONVFLAGTP++;
        if ((int)CONVFLAGTP>2) {
        fprintf(stderr,"\nII: Converged! Yuppi!\n\n");
        CONVFLAG=1;
        }
      }
    ITERCONVTP = (int)iter;
    }
    #else

    fprintf(stderr,"\nII: Converged! Yuppi!\n\n");
    CONVFLAG=1;

    #endif

    }

  #endif

  }

  #ifndef REBIRTH_STEADY
  // Broadcast the convergence
  MPI_Bcast(&CONVFLAG, 1, MPI_INT, 0, MPI_COMM_WORLD);
  #endif

}

#ifndef REBIRTH_STEADY

#ifdef NON_NEWTONIAN

#if (defined THERMAL && defined SCALAR)
int macro_nse_dump (int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *tem, macro *con, macro *stress) {
#elif (defined THERMAL || defined SCALAR)
int macro_nse_dump (int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *tem, macro *stress) {
#else
int macro_nse_dump (int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *stress) {
#endif

#else

#if (defined THERMAL && defined SCALAR && defined EXOTHERMIC)
int macro_nse_dump (int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *tem, macro *con, macro *lwet) {
#elif (defined THERMAL && defined SCALAR && !defined EXOTHERMIC)
int macro_nse_dump (int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *tem, macro *con) {
#elif (defined THERMAL || defined SCALAR)
int macro_nse_dump (int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *tem) {
#else
int macro_nse_dump (int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, int R) {
#endif

#endif

#else

#if (defined THERMAL && defined SCALAR && defined EXOTHERMIC)
int macro_nse_dump (int iter, macro *tem, macro *con, macro *lwet) {
#elif (defined THERMAL && defined SCALAR && !defined EXOTHERMIC)
int macro_nse_dump (int iter, macro *tem, macro *con) {
#elif (defined THERMAL || defined SCALAR)
int macro_nse_dump (int iter, macro *tem) {
#endif

#endif

  #ifdef MULTICOMP
  if (R==1) {
  #endif

  if (ROOT(world_rank))
  fprintf(stderr,"II: DUMPING macroscopic quantities Navier-Stokes at iter %d\n",iter); 

  #ifdef MULTICOMP
  }
  #endif

  int idx=0, i=0, j=0, k=0;
  FILE *fileout;
  char filename[128];

  // depreacted
  #ifdef DUMPASCII
  sprintf(filename,"out/dump.%s.%05d.p_%d.dat",NAME,iter,world_rank);
  fileout = fopen(filename,"w");

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);
      fprintf(fileout,"%g %g %g %g %d\n",ux[idx],uy[idx],uz[idx],rho[idx],lsol[idx]);

      }
    }
  }

  fclose(fileout);
  #endif

  
  #ifdef DUMPHDF5

  hid_t file_id;
  hid_t group_id; 
  hid_t hdf5_status;
  hid_t dataset_id, dataset_id_2, dataset_id_3, dataset_id_4, dataset_id_5;
  hid_t plist_id, efilespace, edimespace;
  hid_t ret, string_type, xfer_plist;
  hsize_t efile_3d[3], edime_3d[3];
  hsize_t estart_3d[3], ecount_3d[3], estride_3d[3], eblock_3d[3];
  hsize_t dstart_3d[3], dcount_3d[3], dstride_3d[3], dblock_3d[3];
  herr_t status;

  H5E_auto_t old_func;
  void *old_client_data;

  H5Eget_auto (H5E_DEFAULT, &old_func, &old_client_data);
  H5Eset_auto (H5E_DEFAULT, old_func, old_client_data);

  sprintf (filename, "out/dump.%s.%05d.h5",NAME,iter);

  efile_3d[0] = NY;
  efile_3d[1] = NX;
  efile_3d[2] = NZ;

  edime_3d[0] = lyp;
  edime_3d[1] = lxp;
  edime_3d[2] = lzp;

  estart_3d[0] = 1;
  estart_3d[1] = 1;
  estart_3d[2] = 1;
  estride_3d[0] = 1;
  estride_3d[1] = 1;
  estride_3d[2] = 1;
  eblock_3d[0] = ly;
  eblock_3d[1] = lx;
  eblock_3d[2] = lz;
  ecount_3d[0] = 1;
  ecount_3d[1] = 1;
  ecount_3d[2] = 1;

  dstart_3d[0] = procoffy[world_rank];
  dstart_3d[1] = procoffx[world_rank];
  dstart_3d[2] = procoffz[world_rank];
  dstride_3d[0] = 1;
  dstride_3d[1] = 1;
  dstride_3d[2] = 1;
  dblock_3d[0] = ly;
  dblock_3d[1] = lx;
  dblock_3d[2] = lz;
  dcount_3d[0] = 1;
  dcount_3d[1] = 1;
  dcount_3d[2] = 1;

  plist_id = H5Pcreate (H5P_FILE_ACCESS);
  hdf5_status = H5Pset_fapl_mpio (plist_id, MPI_COMM_WORLD, MPI_INFO_NULL); 

  #ifdef MULTICOMP
  // Open for multicomp
  if (R==2) {
  file_id = H5Fopen (filename, H5F_ACC_RDWR, plist_id);
  group_id = H5Gopen (file_id, "/LBdm", H5P_DEFAULT);
  } else {
  #endif

  file_id = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
  group_id = H5Gcreate (file_id, "/LBdm", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  #ifdef MULTICOMP
  }
  #endif

  #ifndef REBIRTH_STEADY
  // Dump RHO

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  #ifdef MULTICOMP
  // Rho (2) for multicomp
  if (R==2) {
  dataset_id = H5Dcreate (group_id, "rho2", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  } else {
  #endif

  dataset_id = H5Dcreate (group_id, "rho", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  #ifdef MULTICOMP
  }
  #endif

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  // status = H5Dwrite (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dwrite (dataset_id, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, rho);

  H5Dclose (dataset_id);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  // Dump UX

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  #ifdef MULTICOMP
  // Ux (2) for multicomp
  if (R==2) {
  dataset_id_2 = H5Dcreate (group_id, "ux2", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);
  } else {
  #endif

  dataset_id_2 = H5Dcreate (group_id, "ux", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);

  #ifdef MULTICOMP
  }
  #endif

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dwrite (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dwrite (dataset_id_2, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, ux);

  H5Dclose (dataset_id_2);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  // Dump UY

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  #ifdef MULTICOMP
  // Uy (2) for multicomp
  if (R==2) {
  dataset_id_3 = H5Dcreate (group_id, "uy2", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);
  } else {
  #endif

  dataset_id_3 = H5Dcreate (group_id, "uy", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);

  #ifdef MULTICOMP
  }
  #endif

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dwrite (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dwrite (dataset_id_3, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, uy);

  H5Dclose (dataset_id_3);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  // Dump UZ

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  #ifdef MULTICOMP
  // Uz (2) for multicomp
  if (R==2) {
  dataset_id_4 = H5Dcreate (group_id, "uz2", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);
  } else {
  #endif

  dataset_id_4 = H5Dcreate (group_id, "uz", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);

  #ifdef MULTICOMP
  }
  #endif

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dwrite (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dwrite (dataset_id_4, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, uz);

  H5Dclose (dataset_id_4);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  #ifdef MULTICOMP
  if ( R==1 ) {
  #endif

  // Dump SOL
  if ( iter == 0 ) {

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_5 = H5Dcreate (group_id, "sol", H5T_NATIVE_INT, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dwrite (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dwrite (dataset_id_5, H5T_NATIVE_INT, edimespace, efilespace, xfer_plist, lsol);

  H5Dclose (dataset_id_5);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);
  }

  #ifdef MULTICOMP
  }
  #endif

  // unSteady
  #endif

  // Avoid when multicomp (2)
  #ifndef MULTICOMP

  // Dump SCAL
  #if (defined THERMAL && defined SCALAR)

  hid_t dataset_id_7;

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_7 = H5Dcreate (group_id, "con", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dwrite (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dwrite (dataset_id_7, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, con);

  H5Dclose (dataset_id_7);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  // Dump Exothermic reaction
  #ifdef EXOTHERMIC

  hid_t dataset_id_9;

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_9 = H5Dcreate (group_id, "wet", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dwrite (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dwrite (dataset_id_9, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, lwet);

  H5Dclose (dataset_id_9);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);
  #endif

  // end thermal & scalar
  #endif

  // Dump TEMP or SCAL
  #if (defined THERMAL || defined SCALAR)

  hid_t dataset_id_6;

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  #ifdef THERMAL
  dataset_id_6 = H5Dcreate (group_id, "tem", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);
  #elif (defined SCALAR && !defined THERMAL)
  dataset_id_6 = H5Dcreate (group_id, "con", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);
  #endif

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dwrite (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dwrite (dataset_id_6, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, tem);

  H5Dclose (dataset_id_6);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  #endif

  // Dump SHEAR STRESS
  #if (defined NON_NEWTONIAN)

  hid_t dataset_id_8;

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_8 = H5Dcreate (group_id, "shear", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dwrite (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dwrite (dataset_id_8, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, stress);

  H5Dclose (dataset_id_8);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  #endif

  // Avoid when multicomp
  #endif

  // Closing end

  H5Gclose (group_id);
  H5Fclose (file_id);
  H5Pclose (plist_id);

  #endif

  MPI_Barrier(MPI_COMM_WORLD);

}

#ifdef REBIRTH

#if (defined THERMAL && defined SCALAR && defined EXOTHERMIC)
int macro_nse_rebirth (macro *ux, macro *uy, macro *uz, macro *rho, macro *tem, macro *con, macro *lwet) {

#elif (defined THERMAL && defined SCALAR && !defined EXOTHERMIC)
int macro_nse_rebirth (macro *ux, macro *uy, macro *uz, macro *rho, macro *tem, macro *con) {

#elif (defined THERMAL || defined SCALAR)
int macro_nse_rebirth (macro *ux, macro *uy, macro *uz, macro *rho, macro *tem) {

#else
int macro_nse_rebirth (macro *ux, macro *uy, macro *uz, macro *rho) {
#endif

  if (ROOT(world_rank))
  fprintf(stderr,"II: Revive macroscopic quantities Navier-Stokes\n");

  int idx=0, i=0, j=0, k=0;
  FILE *fileout;
  char filename[128];
  char filename_s[128];

  MPI_Barrier(MPI_COMM_WORLD);

  hid_t file_id;
  hid_t group_id;
  hid_t hdf5_status;
  hid_t dataset_id, dataset_id_2, dataset_id_3, dataset_id_4, dataset_id_5;
  hid_t plist_id, efilespace, edimespace;
  hid_t ret, string_type, xfer_plist;
  hsize_t efile_3d[3], edime_3d[3];
  hsize_t estart_3d[3], ecount_3d[3], estride_3d[3], eblock_3d[3];
  hsize_t dstart_3d[3], dcount_3d[3], dstride_3d[3], dblock_3d[3];
  herr_t status;

  H5E_auto_t old_func;
  void *old_client_data;

  H5Eget_auto (H5E_DEFAULT, &old_func, &old_client_data);
  H5Eset_auto (H5E_DEFAULT, old_func, old_client_data);

  sprintf (filename, "in/%s",BIRTHPLACE);

  #ifdef REBIRTH_STEADY
  sprintf (filename_s, "in/%s",BIRTHSTEADY);
  #endif

  efile_3d[0] = NY;
  efile_3d[1] = NX;
  efile_3d[2] = NZ;

  edime_3d[0] = lyp;
  edime_3d[1] = lxp;
  edime_3d[2] = lzp;

  estart_3d[0] = 1;
  estart_3d[1] = 1;
  estart_3d[2] = 1;
  estride_3d[0] = 1;
  estride_3d[1] = 1;
  estride_3d[2] = 1;
  eblock_3d[0] = ly;
  eblock_3d[1] = lx;
  eblock_3d[2] = lz;
  ecount_3d[0] = 1;
  ecount_3d[1] = 1;
  ecount_3d[2] = 1;

  dstart_3d[0] = procoffy[world_rank];
  dstart_3d[1] = procoffx[world_rank];
  dstart_3d[2] = procoffz[world_rank];
  dstride_3d[0] = 1;
  dstride_3d[1] = 1;
  dstride_3d[2] = 1;
  dblock_3d[0] = ly;
  dblock_3d[1] = lx;
  dblock_3d[2] = lz;
  dcount_3d[0] = 1;
  dcount_3d[1] = 1;
  dcount_3d[2] = 1;

  plist_id = H5Pcreate (H5P_FILE_ACCESS);
  hdf5_status = H5Pset_fapl_mpio (plist_id, MPI_COMM_WORLD, MPI_INFO_NULL);

  #ifdef REBIRTH_STEADY
  file_id = H5Fopen (filename_s, H5F_ACC_RDONLY, plist_id);
  #else
  file_id = H5Fopen (filename, H5F_ACC_RDONLY, plist_id);
  #endif

  group_id = H5Gopen (file_id, "/LBdm", H5P_DEFAULT);

  // Revive RHO

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id = H5Dopen (group_id, "rho", H5P_DEFAULT);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  // status = H5Dread (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dread (dataset_id, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, rho);

  H5Dclose (dataset_id);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  // Revive UX

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_2 = H5Dopen (group_id, "ux", H5P_DEFAULT);

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dread (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dread (dataset_id_2, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, ux);

  H5Dclose (dataset_id_2);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  // Revive UY

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_3 = H5Dopen (group_id, "uy", H5P_DEFAULT);

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dread (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dread (dataset_id_3, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, uy);

  H5Dclose (dataset_id_3);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  // Revive UZ

  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_4 = H5Dopen (group_id, "uz", H5P_DEFAULT);

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dread (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dread (dataset_id_4, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, uz);

  H5Dclose (dataset_id_4);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  #if (defined REBIRTH_STEADY && !defined REBIRTH_0CTO1C && !defined REBIRTH_0TTO1T)
  H5Gclose (group_id);
  H5Fclose (file_id);
  file_id = H5Fopen (filename, H5F_ACC_RDONLY, plist_id);
  group_id = H5Gopen (file_id, "/LBdm", H5P_DEFAULT);
  #endif

  // Revive SCAL
  #if (defined THERMAL && defined SCALAR && !defined REBIRTH_0CTO1C)

  hid_t dataset_id_7;

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_7 = H5Dopen (group_id, "con", H5P_DEFAULT);

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dread (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dread (dataset_id_7, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, con);

  H5Dclose (dataset_id_7);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  #endif

  // Revive TEMP or SCAL
  #if ((defined THERMAL && !defined REBIRTH_0TTO1T && !defined SCALAR) || \
       (defined SCALAR && !defined REBIRTH_0CTO1C && !defined THERMAL) || \
       (defined THERMAL && defined SCALAR && !defined REBIRTH_0TTO1T))

  hid_t dataset_id_6;

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  #ifdef THERMAL
  dataset_id_6 = H5Dopen (group_id, "tem", H5P_DEFAULT);
  #else
  dataset_id_6 = H5Dopen (group_id, "con", H5P_DEFAULT);
  #endif

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dread (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dread (dataset_id_6, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, tem);

  H5Dclose (dataset_id_6);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  #endif

  // revive exo
  #if (defined EXOTHERMIC && !defined REBIRTH_0TTO1T && !defined REBIRTH_0CTO1C)

  hid_t dataset_id_9;

  // From support.hdfgroup.org/HDF5/PHDF5/
  // dataspace_id = H5Screate_simple (rank, dims, maxdims)
  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_9 = H5Dopen (group_id, "wet", H5P_DEFAULT);

  // status = h5sselect_hyperslab_f(space_id, operator, start, count, hdferr, stride, block)
  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  // status = H5Dread (set_id, mem_type_id, mem_space_id, file_space_id, xfer_prp, buf);
  status = H5Dread (dataset_id_9, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, lwet);

  H5Dclose (dataset_id_9);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  #endif

  H5Gclose (group_id);
  H5Fclose (file_id);
  H5Pclose (plist_id);

}

#endif

#ifdef SOURCE_HET
int macro_exo_dump(double *p_exo, int iter) {

  int world_size;
  int world_rank;
  int p=0;

  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  if (ROOT(world_rank)) {

  // root processor prints
  MPI_Status status;

  double summ=0.;
  double sumh=0.;
  double sums=0;
  int sumt=0;

  double *data;
  data = (double *) calloc (4,sizeof(double));

    if (world_size>1) {
      for (p=1; p<world_size; p++) {
      // int MPI_Recv(void *buf, int count, MPI_Datatype datatype,
      //              int source, int tag, MPI_Comm comm, MPI_Status *status)
      MPI_Recv(&data[0], 4, MPI_DOUBLE,p,0,MPI_COMM_WORLD,&status);

      summ += data[0];
      sumh += data[1];
      sums += data[2];
      sumt += (int)data[3];

      summ += p_exo[0];
      sumh += p_exo[1];
      sums += p_exo[2];
      sumt += (int)p_exo[3];
      }

    } else {
    summ = p_exo[0];
    sumh = p_exo[1];
    sums = p_exo[2];
    sumt = p_exo[3];
    }

  FILE *file;
  char fname[128];
  sprintf (fname, "out/%s_exo.dat",NAME);
  file = fopen(fname,"a");
  fprintf(file,"%d %g %g %g %d\n",iter,summ,sumh,sums,(int)sumt);
  fclose(file);

  } else {
  // int MPI_Send(const void *buf, int count, MPI_Datatype datatype,
  //              int dest, int tag, MPI_Comm comm)
  MPI_Send(&p_exo[0], 4, MPI_DOUBLE,0,0,MPI_COMM_WORLD);

  }
}
#endif
