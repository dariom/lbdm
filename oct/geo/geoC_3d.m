function [liq_sol_ijk]=geoC_3d(n,dfA,dvA)

df=dfA*n;
dv=dvA*n;
hf=df+dv;

nx=hf;
ny=hf;
nz=hf;

ncarb_z=hf/(df+dv);
ncarb_y=hf/(df+dv);
ncarb_x=hf/(df+dv);

disp('¡check INTEGERS!');
ncarb_x, ncarb_y, ncarb_z,
forecast_porosity=1-1/(dv+df)^3*1/6*pi()*df^3
pause(0),

wS=0; wN=0;

sp=dv/n;
ss=nx/(ncarb_x*n)-sp;
nz=nz+wN+wS;

A=ones(nz,nx,ny);

%A(nz-10:nz-wS,nx/2-5:nx/2+5,ny/2-5:ny/2+5)=2;

%%% carbon square
%for ncx=1:ncarb_x
%for ncy=1:ncarb_y
%for ncz=1:ncarb_z
%ycarb=(sp+ss)*n/2+(ncy-1)*(sp+ss)*n;
%zcarb=(sp+ss/2)*n+(ncz-1)*(sp+ss)*n+wS;
%xcarb=(sp+ss)*n/2+(ncx-1)*(sp+ss)*n;
%A(nz-zcarb-ss*n/2+1:nz-zcarb+ss*n/2,xcarb-ss*n/2+1:xcarb+ss*n/2,ycarb-ss*n/2+1:ycarb+ss*n/2)=0;
%end; end; end;


%% carbon circle
for ncz=1:ncarb_z*2; fct=factor(ncz); if fct~=2;
for ncy=1:ncarb_y;
for ncx=1:ncarb_x;
ycarb=(sp+ss)*n/2+(ncy-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+(ncz-1)/2*(sp+ss)*n+wS;
xcarb=(sp+ss)*n/2+(ncx-1)*(sp+ss)*n+1;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ycarb+y-0.5)=0;
end; end; end; end; else;
for ncy=1:ncarb_y-1;
for ncx=1:ncarb_x-1;
ycarb=(sp+ss)*n+(ncy-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+(ncz-1)/2*(sp+ss)*n+wS;
xcarb=(sp+ss)*n+(ncx-1)*(sp+ss)*n+1;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ycarb+y-0.5)=0;
end; end; end; end;
for ncy=1:ncarb_y-1;
ycarb=(sp+ss)*n+(ncy-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+(ncz-1)/2*(sp+ss)*n+wS;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,1:ryr,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,1:ryr,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,ycarb+y-0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,ycarb+y-0.5)=0;
end; end; end;
for ncx=1:ncarb_x-1;
xcarb=(sp+ss)*n+(ncx-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+(ncz-1)/2*(sp+ss)*n+wS;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,1+y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,1+y-0.5)=0;
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ny-y+0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ny-y+0.5)=0;
% angle
A(nz-zcarb-z+1,1:ryr,1+y-0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,1+y-0.5)=0;
A(nz-zcarb-z+1,1:ryr,ny-y+0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,ny-y+0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,1+y-0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,1+y-0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,ny-y+0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,ny-y+0.5)=0;
end; end; end;
end; end;


%%%%%%%%%%%%% correction %%%%%%%%%%%%%%%%%%%%

for ncz=[-0.5]%,0.5*ncarb_z]
for ncy=1:ncarb_y-1;
for ncx=1:ncarb_x-1;
ycarb=(sp+ss)*n+(ncy-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+ncz*(sp+ss)*n+wS;
xcarb=(sp+ss)*n+(ncx-1)*(sp+ss)*n+1;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ycarb+y-0.5)=0;
end; end; end; end;
for ncy=1:ncarb_y-1;
ycarb=(sp+ss)*n+(ncy-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+ncz*(sp+ss)*n+wS;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,1:ryr,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,1:ryr,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,ycarb+y-0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,ycarb-y-0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,ycarb-y-0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,ycarb+y-0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,ycarb+y-0.5)=0;
end; end; end;
for ncx=1:ncarb_x-1;
xcarb=(sp+ss)*n+(ncx-1)*(sp+ss)*n+1;
zcarb=(sp)*n/2+ncz*(sp+ss)*n+wS;
for z=1:ss*n/2; rz=ss*n/2+0.5-z; rx=sqrt((ss*n/2)^2-rz^2); rxr=round(rx);
for y=0.5:1:rxr-0.5; ry=(sqrt(rx^2-(y)^2)); ryr=round(ry);
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,1+y-0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,1+y-0.5)=0;
A(nz-zcarb-z+1,xcarb-ryr:xcarb+ryr-1,ny-y+0.5)=0;
A(nz-zcarb-ss*n+z,xcarb-ryr:xcarb+ryr-1,ny-y+0.5)=0;
% angle
A(nz-zcarb-z+1,1:ryr,1+y-0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,1+y-0.5)=0;
A(nz-zcarb-z+1,1:ryr,ny-y+0.5)=0;
A(nz-zcarb-ss*n+z,1:ryr,ny-y+0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,1+y-0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,1+y-0.5)=0;
A(nz-zcarb-z+1,nx-ryr+1:nx,ny-y+0.5)=0;
A(nz-zcarb-ss*n+z,nx-ryr+1:nx,ny-y+0.5)=0;
end; end; end;
end;

A=A(1:nz,:,:);
% x1=(0.5*ncarb_z*(sp+ss)*n+sp*n/2+wS+ss*n);
% x2=(0.5*ncarb_z*(sp+ss)*n+sp*n/2+wS+ss*n/2);
% A(nz-x1:nz-x2,:,:)=1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ratio
%ch_h=nz-wN-wS-hf;
%find(1-A(wN+1:nz,sp*n/2+ss*n/2,sp*n/2+ss*n/2),1)-1-sp*n/2;
%chpm_ratio=ch_h/(ny-wS-wN-ch_h);
%ch_w=wy;
%chla_ratio=ch_w/(ny-ch_w);
%porosity=sum(sum(sum(A(ch_h+wN:nz-wS,:,:),3),2),1)/((nz-ch_h-wS-wN)*nx*ny);

ch_h=0;
ch_w=0;
chla_ratio=0;
chpm_ratio=0;
porosity=sum(sum(sum(A,3),2),1)/(nx*ny*nz);
concentration=1-porosity,

%% boundaries
%A([wN,nz-wS+1:nz],[1:nx,1:nx],[1:ny,1:ny])=0;
%A([wN:ch_h+wN],[1:nx,1:nx],[1:ny/2-ch_w/2,ny/2+ch_w/2+1:ny])=0;
%A([wN+1:ch_h+wN],[1:nx],[(ny-ch_w)/2+1:(ny-ch_w)/2+ch_w])=1;

%% double layer channel
%A=repmat(A,[1,1,2]);
%ny=2*ny;

% figure(1), isosurface(A,0); 
% pbaspect([nx,nz,ny])
% xlim([0,nx]);
% ylim([0,ny]);
% zlim([0,nz]);
% %pause(), close(1);
 
%% indices
[isolid,jsolid,ksolid]=ind2sub(size(A),find(A==0));
Length=max(length(isolid),1);
liq_sol_ijk=zeros(Length,4);
liq_sol_ijk(1:length(isolid),1)=isolid;
liq_sol_ijk(1:length(isolid),2)=jsolid;
liq_sol_ijk(1:length(isolid),3)=ksolid;

liq_sol_ijk(1:4,4)=[nz,nx,ny,porosity]';
