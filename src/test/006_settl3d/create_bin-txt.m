A=ones(40,100,40);

[yy zz] = meshgrid(1:40,1:40);
S=(sqrt((yy-20.5).**2+(zz-20.5).**2)<19);

for i=1:100;
for j=1:40;
A(:,i,j)=int32(S(:,j));
end; end;

[zz xx yy]=meshgrid(1:100,1:40,1:40);
S=(sqrt((xx-20.5).**2+(yy-20.5).**2+(zz-20.5).**2)<10);

for i=1:100;
for j=1:40;
for k=1:40;
if (S(k,i,j)==1);
A(k,i,j)=2;
end; end;end;end;

U=uint8(A(:));
dlmwrite('settl3d.pipe.bin.txt',U);
