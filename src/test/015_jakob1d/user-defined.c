/* ! ATTENTION do not delete or add strange rows !
Just either comment or uncomment the "Capabilities" 
and replace values of "Macros" with the desired ones */

/* ******************* */
/*  Capabilities here  */
/* ******************* */

//#define DPDLX

#define DUMPHDF5
#define INASCII

//#define LBMRT
#define TWOPHASE
#define THERMAL
#define THERMAL_TEST

//#define FREESLIP_XP
//#define FREESLIP_XM
//#define FREESLIP_YP
//#define FREESLIP_YM
//#define FREESLIP_ZP
//#define FREESLIP_ZM

//#define GRAVITY
#define NEIGHBOURING

/* ******************* */
/*  Macros for folks   */
/* ******************* */

// Name identifiers
#define NAME "JAKOB1D"
#define GEOM "jakob1d.bin.txt"

// N-PROC
#define NP 2

// N-SIZE
#define NX 2
#define NY 2
#define NZ 400

// N-PROC PER EDGE
#define NEX 1
#define NEY 1
#define NEZ 2

// INIT DENSITY & dP/dL
#define RHO0 2.35
#define DPDL 0.00

// Iteration max,chk,out
#define MAX_ITER 1000
#define CHK_ITER 1
#define OUT_ITER 10

// Q-speeds and t-relax
#define NQ 19
#define TAU 1.0

// RESIDUAL convergence
#define RESIDUAL 0.0
#define CLENGTH 10

// TWOPHASE parameters
#define GG 9.5
#define RHO2 6.1
#define RHOZ 1.0
#define RHOWALL +0.00

// THERMAL parameters
#define TAUT 1.0    	
#define TINIT 0.40
#define TWALL 0.40
#define TSUB 0.20

