#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <mpi.h>
#include <unistd.h>
#include <time.h>
#include "../mpi/mpi_open.h"
#include "../var/shared.h"
#include "../in/geometry.h"
#include "../nse/macro.h"
#include "../lbe/micro.h"

int main(void)
{

// Set memory limit
setmemlimit();

// Allocate offsets of procs
procoffx = (int *) calloc (NP, sizeof(int));
procoffy = (int *) calloc (NP, sizeof(int));
procoffz = (int *) calloc (NP, sizeof(int));

// Init open-MPI
mpi_init(procoffz,procoffx,procoffy);

// Get world rank and size
MPI_Comm_size(MPI_COMM_WORLD, &world_size);
MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

// Open time.name file
if (ROOT(world_rank)) {
FILE *ftime = NULL;
ftime = fopen("time.name","w");

// Get and print local time
time_t rawtime;
struct tm * timeinfo;
time (&rawtime);
timeinfo = localtime (&rawtime);
fprintf(ftime,"Local time for sim named %s: \n %s\n",NAME,asctime(timeinfo)); 
fclose(ftime); }

// Three-dimensional
dpdlv = (double *) calloc (3, sizeof(double));
wavev = (double *) calloc (3, sizeof(double));

// Convergence array allocation
RECONV = (double *) calloc ((int)MAX_ITER/CHK_ITER, sizeof(double));

// Allocate weights, discrete speeds and MRT
w = (double *) calloc (NQ, sizeof(double)); 
cx = (int *) calloc (NQ, sizeof(int)); 
cy = (int *) calloc (NQ, sizeof(int)); 
cz = (int *) calloc (NQ, sizeof(int)); 
iq = (int *) calloc (NQ, sizeof(int)); 
vq = (int *) calloc ((NQ-1)/2, sizeof(int)); 

// Allocate free-slip q-speed (uptonow I allocate anyway)
qsx = (int *) calloc (10, sizeof(int)); 
qsy = (int *) calloc (10, sizeof(int)); 
qsz = (int *) calloc (10, sizeof(int)); 

// Allocate inverse free-slip q-speed (uptonow I allocate anyway)
iqsx = (int *) calloc (10, sizeof(int));
iqsy = (int *) calloc (10, sizeof(int));
iqsz = (int *) calloc (10, sizeof(int));

// Allocate MRT only for D3Q19
#ifdef LBMRT
S = (D19 *) calloc (NQ, sizeof(D19));
iMRT = (D19 *) calloc (NQ, sizeof(D19));
MRT = (I19 *) calloc (NQ, sizeof(I19));
EQ = (D10 *) calloc (NQ, sizeof(D10));
EYE = (I19 *) calloc (NQ, sizeof(I19));
#endif

// Something to init?  
#ifdef LBMRT
var_init(w,cx,cy,cz,iq,vq,qsx,qsy,qsz,iqsx,iqsy,iqsz,dpdlv,wavev,MRT,iMRT,EYE,S,EQ);
chk_mrt_matrix(MRT,iMRT,EYE,S,EQ);
#else
var_init(w,cx,cy,cz,iq,vq,qsx,qsy,qsz,iqsx,iqsy,iqsz,dpdlv,wavev);
#endif

#ifdef RANDOM_INPUT
// Read and allocate random_input
randin = (float *) calloc (RANDSIZE,sizeof(float));
float readin=0.0;
FILE *ftime = NULL;

  // Access
  if (access("in/rand.input",F_OK) != -1 ) {
  ftime = fopen("in/rand.input","r");
    while( fscanf(ftime,"%f\n",&readin)!=EOF && h<RANDSIZE) {
    randin[h]=(float)readin;
    h++;
    }
  fclose(ftime);

    // Message
    if (ROOT(world_rank))
    fprintf(stderr,"II: I found the rand.input file, size %d. Reading it.\n",h);
  h = 0;

  // Not found
  } else {
    if (ROOT(world_rank)) {
    fprintf(stderr,"EE: I cannot find the rand.input file.\n");
    exit(1);
    }
  }
#endif

// Init message?
if (ROOT(world_rank))
fprintf(stderr,"II: LBdm on start: please refer to webpage https://gitlab.com/dariom/lbdm \n");

// Fluid Init?
if (ROOT(world_rank))
fprintf(stderr,"II: LB fluid parameters: fl.relaxation tau(U) = %g, initial rho0 = %g\n",TAU,RHO0);

// Two-phase to init?
#if (defined TWOPHASE && !defined SPATIAL_WETTING && !defined MULTICOMP)
if (ROOT(world_rank))
fprintf(stderr,"II: Shan-Chen parameters: G = %g, coeff.rho0 = %g, wall density = %g\n",GG,RHOZ,RHOWALL);
#endif

// Two-phase spatial wetting to init?
#if (defined TWOPHASE && defined SPATIAL_WETTING && !defined MULTICOMP)
if (ROOT(world_rank))
fprintf(stderr,"II: Shan-Chen parameters: G = %g, coeff.rho0 = %g, wall density is collected from file.\n",GG,RHOZ);
#endif

// Two-phase multicomponent
#if (defined TWOPHASE && defined MULTICOMP)
if (ROOT(world_rank))
fprintf(stderr,"II: Shan-Chen multicompoent parameters: G11 = %g, G22 = %g, G12 = %g\n",GG,GG2,GR);
fprintf(stderr,"II: Shan-Chen multicompoent parameters: tau1 = %g, tau2 = %g\n",TAU,TAUM2);
fprintf(stderr,"II: Shan-Chen multicompoent parameters: wall1 = %g, wall2 = %g\n",RHOWALL,RHOWALL2);
#endif

// Thermal to init?
#ifdef THERMAL
if (ROOT(world_rank)) { 
fprintf(stderr,"II: Thermal parameters: th.relaxation tau(T) = %g, initial temp = %g\n",TAUT,TINIT);
fprintf(stderr,"II: Thermal parameters: wall temp = %g, subcooled temp = %g\n",TWALL,TSUB); }
#endif

// Thermal to init?
#ifdef CONJUGATE
if (ROOT(world_rank)) {
fprintf(stderr,"II: Conjugate heat transfer parameters: tau1 = %g, cp1 = %g, temp fluid = %g\n",TAUT,CP1,TINIT);
fprintf(stderr,"II: Conjugate heat transfer parameters: tau2 = %g, cp2 = %g, temp solid1 = %g\n",TAU2,CP2,TWALL); }
#endif

// Scalar to init?
#ifdef SCALAR
if (ROOT(world_rank))
fprintf(stderr,"II: Scalar parameters: sc.relaxation tau(C) = %g, initial conc = %g\n",TAUC,CINIT);
#endif

// Acoustic Init?
#if (defined ACOUSTIC_PULSE || defined ACOUSTIC_FORCEX || ACOUSTIC_FROCEY || ACOUSTIC_FORCEZ)
if (ROOT(world_rank))
fprintf(stderr,"II: LB acoustic parameters: wave amplitude = %g, frequency = %g\n",AWAVE,FWAVE);
#endif

// Reaction to init?
#if (defined REACTION && !defined SPATIAL_REACTION)
if (ROOT(world_rank))
fprintf(stderr,"II: Reaction parameters: Diffusion = %g, react.rate kr = %g, Eq.conc Ceq = %g\n",A1,A2,(double)(A3/A2));
#endif

// Reaction to spatially init?
#if (defined REACTION && defined SPATIAL_REACTION)
if (ROOT(world_rank))
fprintf(stderr,"II: Reaction parameters: Diffusion = %g, Eq.conc Ceq = %g, reac.rate spatial\n",A1,(double)(A3/A2));
#endif

// Check definitions
print_defs();

// Check processors number
if ((int)world_size!=(int)NP) {
fprintf(stderr,"EE: Error, number of procs mismatch!\n .. in fact world_size is %d while NP is %d\n",world_size,NP);
exit(1);
}

// Print procs geometry
if (ROOT(world_rank)) {
fprintf(stderr,"II: Global | NX:%d NY:%d NZ:%d\n",NX,NY,NZ);
fprintf(stderr,"II:  Local | lx:%d ly:%d lz:%d\n",lx,ly,lz);
}

// Print procs geometry
fprintf(stderr,"II: Proc num: %d | prx: %d pry: %d prz: %d\n",world_rank,prx,pry,prz);
fprintf(stderr,"II: Proc num: %d | prxm: %d prym: %d przm: %d\n",world_rank,prxm,prym,przm);
fprintf(stderr,"II: Proc num: %d | prxp: %d pryp: %d przp: %d\n",world_rank,prxp,pryp,przp);

// Print Name
FILE *fprname=NULL;
fprname = fopen("sim.name","w");
fprintf(fprname,"%s",NAME);
fclose(fprname);

// DECLARE
dfdq *f;
dfdq *feq;
#if (defined THERMAL && defined SCALAR)
dfdq *c;
dfdq *ceq;
#endif
#if (defined THERMAL || defined SCALAR)
dfdq *t;
dfdq *teq;
#ifndef REBIRTH_STEADY
dfdq *bfs;
#endif
#endif
dfdq *bf;
macro *ux;
macro *uy;
macro *uz;
macro *rho;
#ifdef TWOPHASE
macro *psix;
macro *psiy;
macro *psiz;
#endif
#ifdef MULTICOMP
dfdq *f2;
dfdq *feq2;
dfdq *bf2;
macro *rho2;
macro *ux2;
macro *uy2;
macro *uz2;
macro *psix2;
macro *psiy2;
macro *psiz2;
#endif
#if (defined THERMAL && defined SCALAR)
macro *con;
#endif
#if (defined THERMAL || defined SCALAR)
macro *tem;
#endif
#if defined SOURCE
macro *st1;
#endif
#if defined SOURCE_HET
macro *st2;
#endif
#if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
double *gwet;
double *lwet;
#endif
#ifdef NON_NEWTONIAN
macro *taun;
macro *stress;
#endif
#if (defined INNER_TIME_FLU_BCS || defined INNER_TIME_TEM_BCS || defined INNER_TIME_CON_BCS)
int **innerCells;
int innerCellsNr;
#endif
#ifdef SOURCE_HET
double *p_exo;
#endif
int *gsol;
int *lsol;

// ALLOCATE main micro-vars
f = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));
feq = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));
bf = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));

#if (defined THERMAL && defined SCALAR)
// ALLOCATE main micro-vars for Scalar
c = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));
ceq = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));
#endif

#if (defined THERMAL || defined SCALAR)
// ALLOCATE main micro-vars for Temperature or Scalar
t = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));
teq = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));
#ifndef REBIRTH_STEADY
bfs = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));
#endif
#endif

// ALLOCATE main macro-vars
ux = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
uy = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
uz = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
rho = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));

#ifdef TWOPHASE
// ALLOCATE macro two-phase vars
psix = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
psiy = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
psiz = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
#endif

#ifdef MULTICOMP
// ALLOCATE multicomponent micro vars
f2 = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));
feq2 = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));
bf2 = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));

// ALLOCATE multicomponent macro vars
ux2 = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
uy2 = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
uz2 = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
rho2 = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));

// ALLOCATE multicomponent macro two-phase vars
psix2 = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
psiy2 = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
psiz2 = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
#endif

#if (defined THERMAL && defined SCALAR)
// ALLOCATE macro scalar vars
con = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
#endif

#if (defined THERMAL || defined SCALAR)
// ALLOCATE macro thermal vars
tem = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
#endif

#if (defined SOURCE)
// ALLOCATE macro source var
st1 = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
#endif

#if (defined SOURCE_HET)
// ALLOCATE macro source var
st2 = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
#endif

#if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
// ALLOCATE macro spatial wetting/reaction var
#ifdef INASCII
gwet = (double *) calloc (NX*NY*NZ,sizeof(double));
#endif
lwet = (double *) calloc ((lxp)*(lyp)*(lzp),sizeof(double));
#endif

#ifdef NON_NEWTONIAN
// ALLOCATE macro spatial shear-related viscosity
taun = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
stress = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
#endif

#ifdef SOURCE_HET
// Allocate dumping values
p_exo = (double *) calloc (4,sizeof(double));
#endif

// ALLOCATE geometry GlobalSolid and LocalSolid
#ifdef INASCII
gsol = (int *) calloc (NX*NY*NZ,sizeof(int));
#endif
lsol = (int *) calloc (lxp*lyp*lzp,sizeof(int));

// Init Global Geometry
#ifdef INASCII

  #if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
  geometry_from_file(gsol, gwet);
  #else
  geometry_from_file(gsol);
  #endif

  // Divide Geometry in Local MPI Geometry
  #if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
  geometry_mpi(gsol, lsol, gwet, lwet);
  free(gsol);
  free(gwet);
  #else
  geometry_mpi(gsol, lsol);
  free(gsol);
  #endif

// Input HDF5 only need local allocation
#elif (defined INHDF5)

  // Divide Geometry in Local MPI Geometry
  #if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
  geometry_mpi(lsol, lwet);
  #else
  geometry_mpi(lsol);
  #endif

#endif

#if (defined INNER_TIME_FLU_BCS || defined INNER_TIME_TEM_BCS || defined INNER_TIME_CON_BCS)
// Setmem for inner time-dependent BCs
macro_mem_inner_bcs(lsol,&innerCellsNr);

// Allocate inner BCs
innerCells = (int **)calloc(innerCellsNr, sizeof(int*));
  int i;
  for (i=0; i<innerCellsNr; ++i)
  innerCells[i] = (int *) calloc(3, sizeof(int));

// Init Inner time-dependent BCs
macro_init_inner_bcs(lsol,innerCells);
#endif

#if (defined THERMAL && defined SCALAR)

  #if (defined REBIRTH && defined SOURCE_HET)
  // Rebirth with exothermic
  macro_nse_rebirth(ux,uy,uz,rho,tem,con,lwet);

  #elif (defined REBIRTH && !defined SOURCE_HET)
  // Rebirth without exothermic
  macro_nse_rebirth(ux,uy,uz,rho,tem,con);
  #endif

  #ifdef CONJUGATE_ON_REACT
  // Init fluid df, scalar df and temperature df with react_conj
  micro_lbe_init(lsol,f,t,c,ux,uy,uz,rho,tem,con,lwet);
  #else

  // Init fluid df, scalar df and temperature df
  micro_lbe_init(lsol,f,t,c,ux,uy,uz,rho,tem,con);
  #endif

#elif (defined THERMAL || defined SCALAR)

  #ifdef REBIRTH
  // Rebirth for fluid velocity
  macro_nse_rebirth(ux,uy,uz,rho,tem);
  #endif

// Init fluid df and scalar/temperature df
micro_lbe_init(lsol,f,t,ux,uy,uz,rho,tem);
#else

  #ifdef REBIRTH
  // Rebirth for fluid velocity
  macro_nse_rebirth(ux,uy,uz,rho);
  #endif

// Init fluid df
micro_lbe_init(lsol,f,ux,uy,uz,rho,1);

  #ifdef MULTICOMP
  // Init fluid df2
  micro_lbe_init(lsol,f2,ux2,uy2,uz2,rho2,2);
  #endif

#endif

// Periodic Boundary Conditions dfdq necessary for aligning input geometry
#if (defined THERMAL && defined SCALAR)
periodic_df(c);

#elif (defined THERMAL || defined SCALAR)
periodic_df(t);
#endif

periodic_df(f);

// Periodic Boundary Conditions lsol
periodic_int(lsol);

#ifdef MULTICOMP
// Periodic Boundary Conditions dfdq for multicomp
periodic_df(f2);
#endif

#ifdef RANDOM_INPUT
// Shift (global) random index after initialization
h = (int)(CINITL*RANDFQ)-1;
#endif

// Boundary concentrations needs macroscopic copies (double if we revive)
#ifndef REBIRTH_STEADY

  #if (defined THERMAL && defined SCALAR)
  // Zeros
  macro_nse_zero(ux,uy,uz,rho,tem,con);

  // Compute init macro for concentration bcs
  macro_nse(f,ux,uy,uz,rho,t,tem,c,con);

  // Incomp
  macro_nse_inc(lsol,ux,uy,uz,rho,tem,con,1);

  // Incomp
  macro_nse_inc(lsol,ux,uy,uz,rho,tem,con,2);

  #elif (defined THERMAL || defined SCALAR)
  // Zeros
  macro_nse_zero(ux,uy,uz,rho,tem);

  // Compute init macro for concentration bcs
  macro_nse(f,ux,uy,uz,rho,t,tem);

  // Incomp
  macro_nse_inc(lsol,ux,uy,uz,rho,tem,1);

  // Incomp
  macro_nse_inc(lsol,ux,uy,uz,rho,tem,2);
  #endif

#else

  #if (defined THERMAL && defined SCALAR)
  // Zeros
  macro_nse_zero(tem,con);

  // Compute init macro for concentration bcs
  macro_nse(t,tem,c,con);

  // Incomp
  macro_nse_inc(lsol,ux,uy,uz,rho,tem,con,2);

  #elif (defined THERMAL || defined SCALAR)
  // Zeros
  macro_nse_zero(tem);

  // Compute init macro for concentration bcs
  macro_nse(t,tem);

  // Incomp
  macro_nse_inc(lsol,ux,uy,uz,rho,tem,2);
  #endif

#endif

#if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
// Periodic Boundary Conditions lwet
periodic_macro(lwet);
#endif

// Initial conditions depend on REBIRTH

// Freeslip correction of lsol
#if (defined SPATIAL_WETTING || defined SPATIAL_REACTION)
geometry_slip(lsol,lwet);
#else
geometry_slip(lsol);
#endif

// Unsteady restart
#ifndef REBIRTH_STEADY

#if (defined THERMAL && defined SCALAR && (!defined REBIRTH || \
    (defined REBIRTH_01TO1C && defined REBIRTH_0TTO1T)))
// Inlet/Outlet boundary conditions with Temperature and Scalar
micro_lbe_inout(lsol,f,t,c,rho,ux,uy,uz,tem,con);
#endif

#if ((defined THERMAL && !defined SCALAR && (!defined REBIRTH || defined REBIRTH_0TTO1T)) || \
     (defined SCALAR && !defined THERMAL && (!defined REBIRTH || defined REBIRTH_0CTO1C)))
// Inlet/Outlet boundary conditions with Temperature
micro_lbe_inout(lsol,f,t,rho,ux,uy,uz,tem);
#endif

#if (!defined THERMAL && !defined SCALAR && !defined REBIRTH)
// Inlet/Outlet boundary conditions
micro_lbe_inout(lsol,f,rho,ux,uy,uz);
#endif

// Steady restart
#else

#if (defined THERMAL && defined SCALAR)
// Inlet/Outlet boundary conditions for Temperature and Scalar
micro_lbe_inout(lsol,t,c,rho,ux,uy,uz,tem,con);

#elif (defined THERMAL || defined SCALAR)
// Inlet/Outlet boundary conditions for Temperature
micro_lbe_inout(lsol,t,rho,ux,uy,uz,tem);
#endif

#endif

#ifdef INNER_TIME_FLU_BCS
// Inner time-dependent BCs Fluid
micro_lbe_inner_bcs(lsol,f,innerCells,innerCellsNr,0);
#endif

// Start clock
double tic = 0.0;
tic = MPI_Wtime();

/******************************/
/****        LOOOOP        ****/
/******************************/

if (ROOT(world_rank))
fprintf(stderr,"\nII: Inside LB time-LOOOOP .... %s\n\n",NAME);

  for (iter=0; iter<MAX_ITER+1; iter++) {

  // Solve the fluid
  #ifndef REBIRTH_STEADY

  #if (defined THERMAL && defined SCALAR)
  // Navier-Stokes vars, Temperature and Scalar at zero
  macro_nse_zero(ux,uy,uz,rho,tem,con);

  #elif (defined THERMAL || defined SCALAR)
  // Navier-Stokes vars and Temperature or Scalar at zero
  macro_nse_zero(ux,uy,uz,rho,tem);

  #else
  // Zero

  #ifdef MULTICOMP
  // Navier-Stokes multicomp vars at zero
  macro_nse_zero(ux2,uy2,uz2,rho2);
  #endif

  // Navier-Stokes vars at zero
  macro_nse_zero(ux,uy,uz,rho);
  #endif

  #if (defined THERMAL && defined SCALAR)
  // Navier-Stokes macroscopic vars, Temperature and Scalar
  macro_nse(f,ux,uy,uz,rho,t,tem,c,con);

  #elif (defined THERMAL || defined SCALAR)
  // Navier-Stokes macroscopic vars and Temperature or Scalar
  macro_nse(f,ux,uy,uz,rho,t,tem);

  #else
  // Nse

  #ifdef MULTICOMP
  // Navier-Stokes multicomp vars
  macro_nse(f2,ux2,uy2,uz2,rho2);
  #endif

  // Navier-Stokes macroscopic vars
  macro_nse(f,ux,uy,uz,rho);
  #endif

  #ifdef TWOPHASE
  // Periodic rho for two-phase
  periodic_macro(rho);

  // Correction for freeslip
  macro_twophase_slip(rho);

  #ifdef MULTICOMP
  // Periodic rho for multicomp
  periodic_macro(rho2);

  // Correction for freeslip
  macro_twophase_slip(rho2);
  #endif

  #if (defined NEIGHBOURING && defined SPATIAL_WETTING)
  // Averaging rho in the nearest nodes spatially wet
  macro_twophase_neigh(lsol,rho,lwet);

  #elif (defined NEIGHBOURING && !defined SPATIAL_WETTING)
  // Neighbouring

  #ifdef MULTICOMP
  // Averaging rho in the nearest nodes multic_1
  macro_twophase_neigh(lsol,rho,rho2,1);

  // Averaging rho in the nearest nodes multic_1
  macro_twophase_neigh(lsol,rho2,rho,2);
  #else

  // Averaging rho in the nearest nodes
  macro_twophase_neigh(lsol,rho);
  #endif

  // Neigh
  #endif

  #ifdef NEIGHBOURING
  // Neighbouring periodic

  #ifdef MULTICOMP
  // Periodic rho2 after averaging
  periodic_macro(rho2);
 
  // Correction for freeslip
  macro_twophase_slip(rho2);
  #endif

  // Periodic rho after averaging
  periodic_macro(rho);

  // Correction for freeslip
  macro_twophase_slip(rho);
  #endif

  #ifdef SPATIAL_WETTING
  // Pseudopoential for two-phase, spatial wetting
  macro_twophase(lsol,lwet,rho,psix,psiy,psiz);

  #elif (defined MULTICOMP)
  // Pseudopoential for multicomp, rho1 uniform wetting
  macro_twophase(lsol,rho,rho2,psix,psiy,psiz,1);

  // Pseudopoential for multicomp, rho2 uniform wetting
  macro_twophase(lsol,rho2,rho,psix2,psiy2,psiz2,2);
  #else

  // Pseudopoential for two-phase, uniform wetting
  macro_twophase(lsol,rho,psix,psiy,psiz);
  #endif

  #ifdef MULTICOMP
  // Periodic correction for Inout BCs
  macro_twophase_inout(iter,lsol,psix2,psiy2,psiz2);
  #endif

  // Periodic correction for Inout BCs
  macro_twophase_inout(iter,lsol,psix,psiy,psiz);
  #endif

  #ifdef EXOTHERMIC
  // Get values at borders
  periodic_df(c);

  // Compute forcing microscopic & heterogeneous source
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,bfs,tem,con,t,teq,c,ceq,lwet,st2,p_exo,2);
  #endif

  // Avoid at t0 if rebirth
  #ifdef REBIRTH
  if ( iter != 0 ) {
  #endif

  #if (defined TWOPHASE && defined NON_NEWTONIAN && !defined MULTICOMP)
  // Add forcing macroscopic (1) for two-phase (non-N)
  macro_forcing(iter,lsol,ux,uy,uz,rho,psix,psiy,psiz,taun);

  #elif (defined TWOPHASE && !defined NON_NEWTONIAN)
  // Add forcing macroscopic (1) for two-phase
  macro_forcing(iter,lsol,ux,uy,uz,rho,psix,psiy,psiz);

  #ifdef MULTICOMP
  // Add forcing macroscopic (1) for multic_2
  macro_forcing(iter,lsol,ux2,uy2,uz2,rho2,psix2,psiy2,psiz2);
  #endif

  #elif (defined GRAVITY && defined SCALAR)
  // Add forcing macroscopic (1) for buoyancy
  macro_forcing(iter,lsol,ux,uy,uz,rho,tem);

  #else
  // Add forcing macroscopic (1)
  macro_forcing(iter,lsol,ux,uy,uz,rho);
  #endif

  #ifdef SOURCE_HET
  // Adding macro source term
  macro_source(tem,st1,st2);

  #elif (defined SOURCE)
  // Adding macro source term
  macro_source(tem,st1);
  #endif

  // Close if rebirth
  #ifdef REBIRTH
  }
  #endif

  #if (defined THERMAL && defined SCALAR)
  // Thermal and scalar compressible correction
  macro_nse_inc(lsol,ux,uy,uz,rho,tem,con,1);

  #elif (defined THERMAL || defined SCALAR)
  // Scalar compressible correction
  macro_nse_inc(lsol,ux,uy,uz,rho,tem,1);

  #else
  // Navier-Stokes compressible correction
  macro_nse_inc(lsol,ux,uy,uz,rho,1);
  #endif

  #ifdef MULTICOMP 
  // Navier-Stokes compressible correction for multicomp
  macro_nse_inc(lsol,ux2,uy2,uz2,rho2);
  #endif
  
  #if (defined REBIRTH && defined TWOPHASE && !defined REBIRTH_1PTO2P)
  // Correction for two-phase rebirth at first iter
  if ( iter == 0 )
  macro_nse_clean(lsol,ux,uy,uz,rho,psix,psiy,psiz,(double)TAU-0.5);
  #endif

  #if (defined THERMAL && defined TWOPHASE)
  // Lattice-Boltzmann equilibrium distribution with Temperature and Twophase
  micro_lbe_eq(lsol,teq,feq,rho,ux,uy,uz,tem,psix,psiy,psiz);

  #elif (defined THERMAL && defined SCALAR)
  // Lattice-Boltzmann equilibrium distribution with Temperature and Scalar
  micro_lbe_eq(lsol,ceq,teq,feq,rho,ux,uy,uz,con,tem);
  
  #elif (defined THERMAL || defined SCALAR)
  // Lattice-Boltzmann equilibrium distribution with Temperature or Scalar
  micro_lbe_eq(lsol,teq,feq,rho,ux,uy,uz,tem);

  #else
  // Equilibrium

  #if (defined MULTICOMP && defined MULTICOMP_GUO)
  // Lattice-Boltzmann equilibrium distribution for multicomp_1
  micro_lbe_eq(lsol,feq,rho,ux,uy,uz,feq2,rho2,ux2,uy2,uz2);

  // Lattice-Boltzmann equilibrium distribution for multicomp_2
  micro_lbe_eq(lsol,feq2,rho2,ux2,uy2,uz2,feq,rho,ux,uy,uz);

  #elif (defined MULTICOMP && !defined MULTICOMP_GUO)
  // Lattice-Boltzmann equilibrium distribution for multicomp_1
  micro_lbe_eq(lsol,feq,rho,ux,uy,uz,feq2,rho2,ux2,uy2,uz2,psix,psiy,psiz,1);

  // Lattice-Boltzmann equilibrium distribution for multicomp_2
  micro_lbe_eq(lsol,feq2,rho2,ux2,uy2,uz2,feq,rho,ux,uy,uz,psix2,psiy2,psiz2,2);

  #else
  // Lattice-Boltzmann equilibrium distribution
  micro_lbe_eq(lsol,feq,rho,ux,uy,uz);
  #endif

  // Equilibrium
  #endif

  #if (defined TWOPHASE && !defined THERMAL && defined NON_NEWTONIAN && !defined MULTICOMP)
  // Navier-Stokes two-phase hydrodynamic velocities (non-N)
  macro_nse_clean(lsol,ux,uy,uz,rho,psix,psiy,psiz,taun,iter);

  #elif (defined TWOPHASE && !defined THERMAL && !defined NON_NEWTONIAN && !defined MULTICOMP)
  // Navier-Stokes two-phase hydrodynamic velocities
  macro_nse_clean(lsol,ux,uy,uz,rho,psix,psiy,psiz,-(double)TAU+0.5);

  #elif (defined TWOPHASE && !defined THERMAL && !defined NON_NEWTONIAN && defined MULTICOMP && !defined MULTICOMP_GUO)
  // Navier-Stokes two-phase hydrodynamic velocities
  macro_nse_clean(lsol,ux,uy,uz,rho,psix,psiy,psiz,0.5);
  #endif

  // Navier-Stokes compressible correction (2)
  #if (defined THERMAL && defined SCALAR)
  macro_nse_inc(lsol,ux,uy,uz,rho,tem,con,2);

  #elif (defined THERMAL || defined SCALAR)
  macro_nse_inc(lsol,ux,uy,uz,rho,tem,2);
  #endif

  // Check Convergence
  if ((iter % CHK_ITER == 0) && ( iter != 0 ))
  macro_nse_chk(iter,lsol,rho,ux,uy,uz);

  // No initial dump
  #ifdef NO_INITDUMP
  if ( iter > 0 ) {
  #endif

  #ifdef NON_NEWTONIAN
  // non-Newtonian

  #if (defined THERMAL && defined SCALAR)
  // Dump Navier-Stokes quantities, Temperature and Scalar
  if ( iter % OUT_ITER == 0 )
  macro_nse_dump(iter,lsol,ux,uy,uz,rho,tem,con,stress);

  #elif (defined THERMAL || defined SCALAR)
  // Dump Navier-Stokes quantities and Temperature or Scalar
  if ( iter % OUT_ITER == 0 )
  macro_nse_dump(iter,lsol,ux,uy,uz,rho,tem,stress);

  #else
  // Dump Navier-Stokes quantities
  if ( iter % OUT_ITER == 0 )
  macro_nse_dump(iter,lsol,ux,uy,uz,rho,stress);
  #endif

  #if (defined THERMAL && defined SCALAR)
  // Dump again if converged (flag iter+1) with Temperature and Scalar
  if ( CONVFLAG == 1 ) {
  MPI_Barrier(MPI_COMM_WORLD);
  macro_nse_dump(iter+1,lsol,ux,uy,uz,rho,tem,con,stress);
  break; }

  #elif (defined THERMAL || defined SCALAR)
  // Dump again if converged (flag iter+1) with Temperature or Scalar
  if ( CONVFLAG == 1 ) {
  MPI_Barrier(MPI_COMM_WORLD);
  macro_nse_dump(iter+1,lsol,ux,uy,uz,rho,tem,stress);
  break; }

  #else
  // Dump again if converged (flag iter+1)
  if ( CONVFLAG == 1 ) {
  MPI_Barrier(MPI_COMM_WORLD);
  macro_nse_dump(iter+1,lsol,ux,uy,uz,rho,stress);
  break; }
  #endif

  #else
  // non-Newtonian

  #if (defined THERMAL && defined SCALAR && defined SOURCE_HET)
  // Dump Navier-Stokes quantities, Temperature and Scalar
  if ( iter % OUT_ITER == 0 )
  macro_nse_dump(iter,lsol,ux,uy,uz,rho,tem,con,lwet);

  #elif (defined THERMAL && defined SCALAR && !defined SOURCE_HET)
  // Dump Navier-Stokes quantities, Temperature and Scalar
  if ( iter % OUT_ITER == 0 )
  macro_nse_dump(iter,lsol,ux,uy,uz,rho,tem,con);

  #elif (defined THERMAL || defined SCALAR)
  // Dump Navier-Stokes quantities and Temperature or Scalar
  if ( iter % OUT_ITER == 0 )
  macro_nse_dump(iter,lsol,ux,uy,uz,rho,tem);

  #else
  // Dump Navier-Stokes quantities
  if ( iter % OUT_ITER == 0 )
  macro_nse_dump(iter,lsol,ux,uy,uz,rho,1);

  #ifdef MULTICOMP
  // Dump Navier-Stokes quantities for multicomp (after)
  if ( iter % OUT_ITER == 0 )
  macro_nse_dump(iter,lsol,ux2,uy2,uz2,rho2,2);
  #endif

  // Dump
  #endif

  #ifdef SOURCE_HET
  // Dump exothermic global
  if ( iter % OUT_ITER == 0 )
  macro_exo_dump(p_exo,iter);
  #endif

  // No initial dump
  #ifdef NO_INITDUMP
  }
  #endif

  #if (defined THERMAL && defined SCALAR)
  // Dump again if converged (flag iter+1) with Temperature and Scalar
  if ( CONVFLAG == 1 ) {
  MPI_Barrier(MPI_COMM_WORLD);
  macro_nse_dump(iter+1,lsol,ux,uy,uz,rho,tem,con);
  break; }

  #elif (defined THERMAL || defined SCALAR)
  // Dump again if converged (flag iter+1) with Temperature or Scalar
  if ( CONVFLAG == 1 ) {
  MPI_Barrier(MPI_COMM_WORLD);
  macro_nse_dump(iter+1,lsol,ux,uy,uz,rho,tem);
  break; }

  #else
  // Dump again if converged (flag iter+1)
  if ( CONVFLAG == 1 ) {
  MPI_Barrier(MPI_COMM_WORLD);
  macro_nse_dump(iter+1,lsol,ux,uy,uz,rho,1);

  #ifdef MULTICOMP 
  // Dump again if converged (flag iter+1) for multicomp (after)
  macro_nse_dump(iter+1,lsol,ux2,uy2,uz2,rho2,2);
  #endif

  break; }
  #endif

  // Newtonian
  #endif

  #ifdef NON_NEWTONIAN
  // Non_Newtonian shear rate
  macro_shear_rate(lsol,rho,taun,stress,f,feq,iter);
  #endif

  #ifdef MULTICOMP
  // Add forcing microscopic (2) for multicomp_1
  micro_forcing(iter,lsol,ux,uy,uz,rho,ux2,uy2,uz2,rho2,psix,psiy,psiz,bf,1);

  // Add forcing microscopic (2) for multicomp_2
  micro_forcing(iter,lsol,ux2,uy2,uz2,rho2,ux,uy,uz,rho,psix2,psiy2,psiz2,bf2,1);

  #elif (defined SOURCE_HET && (defined THERMAL && defined SCALAR))
  // Add forcing microscopic (2) for source het
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,bfs,tem,con,t,teq,c,ceq,lwet,st1,p_exo,1);

  #elif (defined SOURCE_HET && (defined THERMAL || defined SCALAR))
  // Add forcing microscopic (2) for source het
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,bfs,tem,t,teq,lwet,st1,p_exo,1);

  #elif (defined SOURCE && (defined THERMAL && defined SCALAR))
  // Add forcing microscopic (2) for source hom
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,bfs,tem,con,t,teq,c,ceq,st1,1);

  #elif (defined SOURCE && (defined THERMAL || defined SCALAR))
  // Add forcing microscopic (2) for source hom
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,bfs,tem,t,teq,st1,1);

  #elif (!defined SOURCE && (defined SCALAR || defined THERMAL))
  // Add forcing microscopic (2) for gravity con
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,bfs,1);

  #else
  // Add forcing microscopic (2)
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,1);
  #endif

  #if (defined THERMAL && defined SCALAR && defined SOURCE_HET)
  // COLLISION Scalar Source
  micro_lbe_collision(lsol,f,t,c,feq,teq,ceq,bf,bfs,tem,con,st1,st2,ux,uy,uz);

  #elif (defined THERMAL && defined SCALAR && defined SOURCE)
  // COLLISION Scalar Source
  micro_lbe_collision(lsol,f,t,c,feq,teq,ceq,bf,bfs,tem,con,st1,ux,uy,uz);

  #elif ((defined THERMAL || defined SCALAR) && defined SOURCE_HET)
  // COLLISION Temperature or Scalar Source
  micro_lbe_collision(lsol,f,t,feq,teq,bf,bfs,tem,st1,st2,ux,uy,uz);

  #elif ((defined THERMAL || defined SCALAR) && defined SOURCE)
  // COLLISION Temperature or Scalar Source
  micro_lbe_collision(lsol,f,t,feq,teq,bf,bfs,tem,st1,ux,uy,uz);

  #elif (defined THERMAL && defined SCALAR && !defined SOURCE)
  // COLLISION Scalar
  micro_lbe_collision(lsol,f,t,c,feq,teq,ceq,bf,bfs,tem,con);

  #elif ((defined THERMAL || defined SCALAR) && !defined SOURCE)
  // COLLISION Temperature or Scalar
  micro_lbe_collision(lsol,f,t,feq,teq,bf,bfs,tem);

  #elif (defined NON_NEWTONIAN)
  // COLLISION Newtonian
  micro_lbe_collision(lsol,f,feq,bf,MRT,taun,rho);

  #else
  // Collision

  #ifdef MULTICOMP
  // COLLISION Newtonian for multicomp
  micro_lbe_collision(lsol,f2,feq2,bf2,MRT,2);
  #endif
  
  // COLLISION Newtonian
  micro_lbe_collision(lsol,f,feq,bf,MRT,1);
  #endif

  #if (defined THERMAL && defined SCALAR)
  // Periodic Scalar
  periodic_df(c);
  #endif

  #if (defined THERMAL || defined SCALAR)
  // Periodic Temperature or Scalar
  periodic_df(t);
  #endif

  #ifdef MULTICOMP
  // Periodic for mutlicomp
  periodic_df(f2);
  #endif

  // Periodic
  periodic_df(f);

  #if (defined THERMAL && defined SCALAR)
  // Free-slip Temperature and Scalar
  micro_lbe_slip(lsol,f,t,c);

  #elif (defined THERMAL || defined SCALAR)
  // Free-slip Temperature or Scalar
  micro_lbe_slip(lsol,f,t);

  #else
  // Freeslip

  #ifdef MULTICOMP
  // Free-slip for multicomp
  micro_lbe_slip(lsol,f2);
  #endif

  // Free-slip (uptonow I called it anyway)
  micro_lbe_slip(lsol,f);
  #endif

  #ifdef EXOTHERMIC
  // Exothermic reaction
  macro_exo_react(lsol,tem,lwet);

  // Periodic
  periodic_macro(lwet);

  // Correction (temporary)
  geometry_slip(lsol,lwet);
  #endif

  #if (defined THERMAL && defined SCALAR && defined SPATIAL_REACTION)
  // STREAMING Scalar and Temperature with spatial Reaction
  micro_lbe_streaming(lsol,f,t,c,feq,teq,ceq,lwet);

  #elif (defined THERMAL && defined SCALAR && !defined SPATIAL_REACTION)
  // STREAMING Scalar and Temperature
  micro_lbe_streaming(lsol,f,t,c,feq,teq,ceq);

  #elif ((defined THERMAL || defined SCALAR) && defined SPATIAL_REACTION)
  // STREAMING Scalar or Temperature with spatial Reaction
  micro_lbe_streaming(lsol,f,t,feq,teq,lwet);

  #elif ((defined THERMAL || defined SCALAR) && !defined SPATIAL_REACTION)
  // STREAMING Scalar or Temperature with spatial Reaction
  micro_lbe_streaming(lsol,f,t,feq,teq);

  #else
  // Streaming

  #ifdef MULTICOMP
  // STREAMING for mutlicomp
  micro_lbe_streaming(lsol,f2,feq2);
  #endif

  // STREAMING
  micro_lbe_streaming(lsol,f,feq);
  #endif

  #ifdef CONJUGATE_CORR
  // Get values at borders
  periodic_macro(rho);

  #if (defined SOURCE_HET && (defined THERMAL && defined SCALAR))
  // Correction microscopic (2) for source hom
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,bfs,tem,con,t,teq,c,ceq,lwet,st1,p_exo,3);

  #elif (defined SOURCE_HET && (defined THERMAL || defined SCALAR))
  // Correction microscopic (2) for source hom
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,bfs,tem,t,teq,lwet,st1,p_exo,3);

  #elif (defined THERMAL && defined SCALAR)
  // Correction microscopic (2) for source hom
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,bfs,tem,con,t,teq,c,ceq,st1,3);

  #elif (defined THERMAL && !defined SCALAR)
  // Correction microscopic (2) for source hom
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,bfs,tem,t,teq,st1,3);
  #endif

  // Conjug_corr
  #endif

  #ifdef RANDOM_INPUT
  // Random input update (next iteration)
  h=(int)((float)((1+iter*RANDVE+CINITL)*RANDFQ-.5*RANDFQ));
  #endif

  #if (defined THERMAL && defined SCALAR)
  // Inlet/Outlet boundary conditions with Temperature and Scalar
  micro_lbe_inout(lsol,f,t,c,rho,ux,uy,uz,tem,con);

  #elif (defined THERMAL || defined SCALAR)
  // Inlet/Outlet boundary conditions with Temperature
  micro_lbe_inout(lsol,f,t,rho,ux,uy,uz,tem);

  #else
  // Inout

  #ifdef MULTICOMP
  // Inlet/Outlet boundary conditions for multicomp
  micro_lbe_inout(lsol,f2,rho2,ux2,uy2,uz2);
  #endif

  // Inlet/Outlet boundary conditions
  micro_lbe_inout(lsol,f,rho,ux,uy,uz);
  #endif

  #ifdef INNER_TIME_FLU_BCS
  // Inner time-dependent BCs Fluid
  micro_lbe_inner_bcs(lsol,f,innerCells,innerCellsNr,iter);
  #endif

  #ifdef INNER_TIME_TEM_BCS
  // Inner time-dependent BCs Temperature
  micro_lbe_inner_bcs(lsol,t,innerCells,innerCellsNr,iter);
  #endif

  #ifdef INNER_TIME_CON_BCS
  // Inner time-dependent BCs Scalar
  micro_lbe_inner_bcs(lsol,c,innerCells,innerCellsNr,iter);
  #endif

  // **********************************
  // **********************************
  // Revive from steady fluid condition
  #else

  #if (defined THERMAL && defined SCALAR)
  // Navier-Stokes vars, Temperature and Scalar at zero
  macro_nse_zero(tem,con);

  #elif (defined THERMAL || defined SCALAR)
  // Navier-Stokes vars and Temperature or Scalar at zero
  macro_nse_zero(tem);
  #endif

  #if (defined THERMAL && defined SCALAR)
  // Navier-Stokes macroscopic vars, Temperature and Scalar
  macro_nse(t,tem,c,con);

  #elif (defined THERMAL || defined SCALAR)
  // Navier-Stokes macroscopic vars and Temperature or Scalar
  macro_nse(t,tem);
  #endif

// Init fluid df and scalar/temperature df
  #ifdef EXOTHERMIC
  // Get values at borders
  periodic_df(c);
  #endif

  #ifdef EXOTHERMIC
  // Add forcing microscopic (2) for source het
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,tem,con,t,teq,c,ceq,lwet,st2,p_exo,2);
  #endif

  #ifdef SOURCE_HET
  // Adding macro source term
  macro_source(tem,st1,st2);

  #elif (defined SOURCE)
  // Adding macro source term
  macro_source(tem,st1);
  #endif

  #if (defined THERMAL && defined SCALAR)
  // Lattice-Boltzmann equilibrium distribution with Temperature and Scalar
  micro_lbe_eq(lsol,ceq,teq,ux,uy,uz,con,tem);

  #elif (defined THERMAL || defined SCALAR)
  // Lattice-Boltzmann equilibrium distribution with Temperature or Scalar
  micro_lbe_eq(lsol,teq,ux,uy,uz,tem);
  #endif

  #if (defined THERMAL && defined SCALAR)
  // Compressible correction
  macro_nse_inc(lsol,ux,uy,uz,rho,tem,con,2);

  #elif (defined THERMAL || defined SCALAR)
  // Compressible correction
  macro_nse_inc(lsol,ux,uy,uz,rho,tem,2);
  #endif

// Init fluid df and scalar/temperature df
  // Check Convergence
  if ((iter % CHK_ITER == 0) && ( iter != 0 ))
  macro_nse_chk(iter,lsol,tem);

  // No initial dump
  #ifdef NO_INITDUMP
  if ( iter > 0 ) {
  #endif

  #if (defined THERMAL && defined SCALAR && defined EXOTHERMIC)
  // Dump Navier-Stokes quantities, Temperature and Scalar
  if ( iter % OUT_ITER == 0 )
  macro_nse_dump(iter,tem,con,lwet);

  #elif (defined THERMAL && defined SCALAR && !defined EXOTHERMIC)
  // Dump Navier-Stokes quantities, Temperature and Scalar
  if ( iter % OUT_ITER == 0 )
  macro_nse_dump(iter,tem,con);

  #elif (defined THERMAL || defined SCALAR)
  // Dump Navier-Stokes quantities and Temperature or Scalar
  if ( iter % OUT_ITER == 0 )
  macro_nse_dump(iter,tem);
  #endif

  #ifdef SOURCE_HET
  // Dump exothermic global
  if ( iter % OUT_ITER == 0 )
  macro_exo_dump(p_exo,iter);
  #endif

  // No initial dump
  #ifdef NO_INITDUMP
  }
  #endif

  #if (defined SOURCE_HET && (defined THERMAL && defined SCALAR))
  // Compute forcing microscopic & heterogeneous source
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,tem,con,t,teq,c,ceq,lwet,st1,p_exo,1);

  #elif (defined SOURCE_HET && (defined THERMAL || defined SCALAR))
  // Compute forcing microscopic & heterogeneous source
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,tem,t,teq,lwet,st1,p_exo,1);

  #elif (defined SOURCE && (defined THERMAL && defined SCALAR))
  // Compute forcing microscopic & homogeneous source
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,tem,con,t,teq,c,ceq,st1,1);

  #elif (defined SOURCE && (defined THERMAL || defined SCALAR))
  // Compute forcing microscopic & homogeneous source
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,tem,t,teq,st1,1);

  #else
  // Compute forcing microscopic
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,1);
  #endif

  #if (defined THERMAL && defined SCALAR && !defined SOURCE)
  // COLLISION Scalar (no MRT)
  micro_lbe_collision(lsol,t,c,teq,ceq,bf,tem,con);

  #elif ((defined THERMAL || defined SCALAR) && !defined SOURCE)
  // COLLISION Temperature or Scalar (no MRT)
  micro_lbe_collision(lsol,t,teq,bf,tem);

  #elif (defined THERMAL && defined SCALAR && defined SOURCE_HET)
  // COLLISION Scalar (no MRT) Source
  micro_lbe_collision(lsol,t,c,teq,ceq,bf,tem,con,st1,st2,ux,uy,uz);

  #elif (defined THERMAL && defined SCALAR && defined SOURCE)
  // COLLISION Scalar (no MRT) Source
  micro_lbe_collision(lsol,t,c,teq,ceq,bf,tem,con,st1,ux,uy,uz);

  #elif ((defined THERMAL || defined SCALAR) && defined SOURCE_HET)
  // COLLISION Temperature (no MRT) Conjugate
  micro_lbe_collision(lsol,t,teq,bf,tem,st1,st2,ux,uy,uz);

  #elif ((defined THERMAL || defined SCALAR) && defined SOURCE)
  // COLLISION Temperature (no MRT) Conjugate
  micro_lbe_collision(lsol,t,teq,bf,tem,st1,ux,uy,uz);
  #endif

  #if (defined THERMAL && defined SCALAR)
  // Periodic Scalar
  periodic_df(c);
  #endif

  #if (defined THERMAL || defined SCALAR)
  // Periodic Temperature or Scalar
  periodic_df(t);
  #endif

  #if (defined THERMAL && defined SCALAR)
  // Free-slip Temperature and Scalar
  micro_lbe_slip(lsol,t,c);

  #elif (defined THERMAL || defined SCALAR)
  // Free-slip Temperature or Scalar
  micro_lbe_slip(lsol,t);
  #endif

  #ifdef EXOTHERMIC
  // Exothermic reaction
  macro_exo_react(lsol,tem,lwet);

  // Periodic
  periodic_macro(lwet);

  // Correction (temporary)
  geometry_slip(lsol,lwet);
  #endif

  #if (defined THERMAL && defined SCALAR && defined SPATIAL_REACTION)
  // STREAMING Scalar and Temperature with Reaction
  micro_lbe_streaming(lsol,t,c,teq,ceq,lwet);

  #elif (defined THERMAL && defined SCALAR && !defined SPATIAL_REACTION)
  // STREAMING Scalar and Temperature
  micro_lbe_streaming(lsol,t,c,teq,ceq);

  #elif ((defined THERMAL || defined SCALAR) && defined SPATIAL_REACTION)
  // STREAMING Scalar or Temperature with Reaction
  micro_lbe_streaming(lsol,t,teq,lwet);

  #elif ((defined THERMAL || defined SCALAR) && !defined SPATIAL_REACTION)
  // STREAMING Scalar or Temperature
  micro_lbe_streaming(lsol,t,teq);
  #endif

  #ifdef CONJUGATE_CORR
  // Get values at borders
  periodic_macro(rho);

  #if (defined SOURCE_HET && (defined THERMAL && defined SCALAR))
  // Correction microscopic (2) for source hom
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,tem,con,t,teq,c,ceq,lwet,st1,p_exo,3);

  #elif (defined SOURCE_HET && (defined THERMAL || defined SCALAR))
  // Correction microscopic (2) for source hom
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,tem,t,teq,lwet,st1,p_exo,3);

  #elif (defined THERMAL && defined SCALAR)
  // Correction microscopic (2) for source hom
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,tem,con,t,teq,c,ceq,st1,3);

  #elif (defined THERMAL && !defined SCALAR)
  // Correction microscopic (2) for source hom
  micro_forcing(iter,lsol,ux,uy,uz,rho,bf,tem,t,teq,st1,3);
  #endif

  // Conjug_corr
  #endif

  #ifdef RANDOM_INPUT
  // Random input update
  h=(int)((float)((1+iter*RANDVE+CINITL)*RANDFQ-.5*RANDFQ));
  #endif

  #if (defined THERMAL && defined SCALAR)
  // Inlet/Outlet boundary conditions with Temperature and Scalar
  micro_lbe_inout(lsol,t,c,rho,ux,uy,uz,tem,con);

  #elif (defined THERMAL || defined SCALAR)
  // Inlet/Outlet boundary conditions with Temperature
  micro_lbe_inout(lsol,t,rho,ux,uy,uz,tem);
  #endif

  #endif
  } // End of LOOOOP

// MPI Barrier 
MPI_Barrier(MPI_COMM_WORLD);

// Stop clock
double toc = 0.0;
toc = MPI_Wtime() - tic;

// Print sim time
if (ROOT(world_rank)) {
FILE *ftime = NULL;
ftime = fopen("time.name","a");
fprintf(ftime,"Total Loop simulation time:\n %g [s]\n",toc);
fclose(ftime); }

// Checks
//micro_lbe_chk(bf,lsol,"df"); 

// Finalize open-MPI
mpi_finalize();

// Freeeeeeee
free(procoffx);
free(procoffy);
free(procoffz);

free(dpdlv);
free(RECONV);

free(w);
free(cx);
free(cy);
free(cz);
free(iq);
free(vq);

free(qsx);
free(qsy);
free(qsz);

free(iqsx);
free(iqsy);
free(iqsz);

#ifdef LBMRT
free(S);
free(iMRT);
free(MRT);
free(EQ);
free(EYE);
#endif

free(f);
free(feq);
free(bf);

free(ux);
free(uy);
free(uz);
free(rho);

#if (defined THERMAL && defined SCALAR)
free(c);
free(ceq);
free(con);
#endif

free(lsol);

#if (defined THERMAL || defined SCALAR)
free(t);
free(teq);
free(tem);
#endif

#ifdef TWOPHASE
free(psix);
free(psiy);
free(psiz);
#endif

#if (defined INNER_TIME_FLU_BCS || defined INNER_TIME_TEM_BCS || defined INNER_TIME_CON_BCS)
  for (i=0; i<innerCellsNr; ++i)
  free(innerCells[i]);
free(innerCells);
#endif

}
