load('dump.CONV.h5');
tem=LBdm.tem;
ux=LBdm.ux;

pr=1;

x=[81:160];
x0=80.5;

%%%%% (-4/3 (xwall) +3/2 (+0.5dx) -1/6 (+1.5dx))/0.5dx
dtdz=(-4/3*tem(1,x,1)+3/2*tem(2,x,1)-1/6*tem(3,x,1))*2;
Tb=sum(ux(2:end-1,x,1).*tem(2:end-1,x,1),1)./sum(ux(2:end-1,x,1),1);

nu=2*18.*dtdz./(Tb-0.2);

xe=1:40;
M=nu(xe)';
save('nu_lbdm.dat','-ascii','M','-append');
