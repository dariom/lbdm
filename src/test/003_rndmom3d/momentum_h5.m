load('dump.ZERO.h5');
sol=double(LBdm.sol);

load('dump.CONV.h5');

ux=LBdm.ux;
rho=LBdm.rho;

ERR_RHO=abs(max(rho(:))-1),

MOM=sum(sum(rho.*ux.*sol,1),3);
ERR=abs(1-MOM./mean(MOM));
M=[MOM',ERR'];
save('error.dat','-ascii','-append','M');
