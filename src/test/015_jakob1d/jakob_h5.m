warning('off');

%%%% threshold
load('dump.CONV.h5');
rho=LBdm.rho;
ith=find((rho(:,1,1)./rho(30,1,1)-1)>0.09,1,'last');
sh=rho(ith,1,1),

DT=10;
Lt=1000;
VT=[0:DT:Lt];

rhl=5.80;
rhg=0.19;
cp=5/2;
alpha=1/6;

load('../../out/dump.JAKOB1D.00000.h5');
sol=double(LBdm.sol);

n=0;
for t=VT;
n=n+1;

load(sprintf('../../out/dump.JAKOB1D.%05d.h5',t));
rho=LBdm.rho;
tem=LBdm.tem;

tg(n)=sum(tem(:).*(rho(:)<sh).*sol(:))./sum(sol(:).*(rho(:)<sh));
tl(n)=sum(tem(:).*(rho(:)>=sh).*sol(:))./sum(sol(:).*(rho(:)>=sh));

ng(n)=sum((rho(:)<sh).*sol(:))./sum(sol(:));
nl(n)=sum((rho(:)>=sh).*sol(:))./sum(sol(:));

rg(n)=sum(rho(:).*(rho(:)<sh).*sol(:))./sum(sol(:).*(rho(:)<sh));
rl(n)=sum(rho(:).*(rho(:)>=sh).*sol(:))./sum(sol(:).*(rho(:)>=sh));

dtdz(n)=(-4/3*tem(1,1,1)+3/2*tem(2,1,1)-1/6*tem(3,1,1))*2;
Tb(n)=sum(tem(:,1,1).*(rho(:,1,1)<sh).*sol(:,1,1))./sum((rho(:,1,1)<sh).*sol(:,1,1));
nu(n)=2*78.*dtdz(n)./(0.4-0.2);

xi(n)=find(rho(2:end,1,1)<sh,1)+1;
%xi(n)=100-find(rho(100:-1:1,1,1)>sh,1)+2;

if (xi(n)>1);
 if (xi(n)>3);
 dtm(n)=-3/2*tem(xi(n),1,1)+2*tem(xi(n)-1,1,1)-1/2*tem(xi(n)-2,1,1);
 else
 dtm(n)=-1*tem(xi(n),1,1)+1*tem(xi(n)-1,1,1);
 end;
dtp(n)=-3/2*tem(xi(n),1,1)+2*tem(xi(n)+1,1,1)-1/2*tem(xi(n)+2,1,1);
dth(n)=-1/2*tem(xi(n)-1,1,1)-1/2*tem(xi(n)+1,1,1);
end;

tsat(n)=tem(xi(n),1,1);

end;

%% chose power law
pow=1/2;

ie=find(xi==xi(end)-3,1);
int=[2:ie];
[m p]=polyfit(VT(int)'.**(pow),xi(int)'-1.5,1);
vi=pow.*m(1).*VT.**(pow-1);
lh=cp*alpha.*(rhg.*dtp-rhl.*dtm)./(rhl.*vi);
lh1=cp*alpha.*(rhg.*dtp)./(rhl.*vi);
lh2=cp*alpha.*(rhl.*dtm)./(rhl.*vi);

%  1   2   3   4    5    6   7     8   9   10  11    12  13
M=[VT',xi',vi',lh1',lh2',tg',tsat',tl',nl',ng',dtdz',rl',rg'];
save('jakob.dat','-ascii','-append','M');

%T=[tem(1:30,1,1),rho(1:30,1,1)];
%save('temrho.dat','-ascii','-append','T');
