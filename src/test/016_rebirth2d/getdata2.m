for i=0:8:40;
load(sprintf('../../out/dump.REB2.%05d.h5',i));
if (i==0);
sol=double(LBdm.sol);
end;
ux=LBdm.ux;
tem=LBdm.tem;
ut2(i/8+1)=sum(ux(:))./sum(sol(:));
tt2(i/8+1)=sum(tem(:).*sol(:))./sum(sol(:));
end;

M=[ut2(1:end)',tt2(1:end)'];
save('data2.dat','-ascii','-append','M');
