#!/bin/bash -e

echo " "
echo "#lbdm# Test 000 Poiseuille Parallel 2d"
echo " "

git checkout user-defined.c

if [ -f "*.bin.txt" ]; then
rm *.bin.txt
fi

if [ -f "dump.CONV.h5" ]; then
rm dump.CONV.h5
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

if [ -f "ux_profile_lbdm.dat" ]; then
rm ux_profile_lbdm.dat
fi

octave create_bin-txt.m
cp po2d.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..

make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/000_po2d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave ux_h5_po2d.m

paste ux_profile.dat ux_profile_lbdm.dat | awk 'NR<=18 {print ($1/$2-1)}' > error.dat 
awk 'NR<=18 {if (($1*$1)^0.5<=0.0055) print "ok"; else print "no" }' error.dat > errchk.dat

###########

## We ll check FASTER viscosity independent sims tau=1.5 
sed -i -e 's/\/\/#define LBMRT/#define LBMRT/g' user-defined.c
sed -i -e 's/define DPDL .*/define DPDL 0.00015625/g' user-defined.c
sed -i -e 's/define TAU .*/define TAU 1.5/g' user-defined.c

cp user-defined.c ../../user/
cd ../..

make superclean
make init
make 

if (mpirun -np 2 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/000_po2d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave ux_h5_po2d_mrt.m

paste ux_profile.dat ux_profile_lbdm_mrt.dat | awk 'NR<=18 {print ($1/$2-1)}' > error_mrt.dat 
awk 'NR>=1 && NR<=2 {if (($1*$1)^0.5<=0.018) print "ok"; else print "no" }' error_mrt.dat >> errchk.dat
awk 'NR>=3 && NR<=16 {if (($1*$1)^0.5<=0.0046) print "ok"; else print "no" }' error_mrt.dat >> errchk.dat
awk 'NR>=17 && NR<=18 {if (($1*$1)^0.5<=0.018) print "ok"; else print "no" }' error_mrt.dat >> errchk.dat

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/po2d.bin.txt

# clean local test
cd test/000_po2d/
git checkout user-defined.c
rm *.txt

########### 1) Check NaN 

for file in ux_profile.dat ux_profile_lbdm_mrt.dat ux_profile_lbdm.dat error.dat error_mrt.dat errchk.dat prova9.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 36 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
