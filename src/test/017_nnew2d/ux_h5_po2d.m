load([ 'dump.CONV.h5' ]);
uxl=LBdm.ux;

V =  0.038879;
for i=1:18;
up(i,1)=mean(mean(uxl(i+1,:,:)))./V;
end;

dat=[(0.5:1:17.5)'./18,up];

save('ux_profile_lbdm.dat','-ascii','dat');
