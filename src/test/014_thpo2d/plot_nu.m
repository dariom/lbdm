nu=load('nu_lbdm.dat');

graphics_toolkit ("gnuplot");
setenv ("GNUTERM", "qt");

figure,  plot([1 40],[7.54 7.54],'-');
hold on, plot(nu(1:40),'-o');
hold on, plot(nu(41:80),'-x');

ylim([5,10]);
xlim([0,40]);

saveas(gcf,'nusselt.png','png');


