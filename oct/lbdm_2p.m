
%%%% : this is a Lattice Boltmann Model 3D MRT D3Q19 single-phase : %%%

					%%%%%% DECLARE IF %%%%%%%%%%%%%

				 	name='8888'
					geoname='geoB_3d_2p'
					
					% �� use of body force ??
					bf=1;

					% restart
					restart=0;

%%% looop sims
 
AfA=[0,0,1;
    10,0,0]; 

for fA=[1];
for vA=[1];

afA=AfA(fA,:);
dfA=8,dvA=12
rA=1.5,ncA=50,oA=1,


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1 %%%% SET-UP Model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if restart==0;

	% close all,
	clear -x name geoname bf restart afA AfA fA vA oA rA ncA dfA dvA;

tic

% Absolute liquid-gas
g_density=0.6;
l_density=3.0;

% wall-densities
w_density=1;
phb_density=1;
z_density=1;

% initial-density
i_density=g_density;

% Intermolecular
Gb=+2.5;        % (-) small density to big density
Gb1=-7.5;      % (-) wettable
Gb2=Gb1;
Geff=Gb1+Gb;


%%% FORCING & INPUTS %%%%%%%

n=2; %resolution
df=dfA; %check

addpath('geo:in:two');

% Set a Reynolds ..
Re=0.1;
%rfb_input=str2func([ 'rfb_input' ]);
%dPdL=rfb_input(Re,n);

% Customize
dPdL=10**-4;

if (geoname=='geoS_3d_2p');
spr=sprintf('%1.0f%1.0f%1.0f%1.0f',[n,oA,fA,vA]);
name=spr,
end;

%%%% TIME relaxation %%%%%%%

tau=1.0;
C_ux0=0.3.*i_density;

%%%% SEVERAL GEOMETRIES %%%%

sprgeo=sprintf([ geoname ]);
geoN=str2func(sprgeo);

% Remember to put the right inputs

if (geoname=='geoX_3d_2p');
[liq_sol_ijk]=geoN(name,vA); 

elseif (geoname=='geoC_3d_2p');
[liq_sol_ijk]=geoN(n,dfA,dvA);

elseif (geoname=='geoB_3d_2p');
[liq_sol_ijk]=geoN(n,dfA,dvA);

elseif (geoname=='geoS_3d_2p');
[liq_sol_ijk]=geoN(n,rA,ncA,oA,vA,fA,name,80);

end;

%%% Build GEO %%%%%%%%%%%%%%

Nr=liq_sol_ijk(1,7); Mc=liq_sol_ijk(2,7); Ld=liq_sol_ijk(3,7);
porosity=liq_sol_ijk(4,7);

% porosity ... etc
geoname, name, porosity,

liq_sol_ijk=[liq_sol_ijk(:,1:6);zeros(1,6)];
solidLength=find(liq_sol_ijk(:,1)==0,1)-1;
liquidLength=find(liq_sol_ijk(:,4)==0,1)-1;

Channel3D=ones(Nr,Mc,Ld);
ChannelPhases=ones(Nr,Mc,Ld);
ChannelPhases(:,:,:)=i_density;

for ijksolid=1:solidLength
isolid=liq_sol_ijk(ijksolid,1);
jsolid=liq_sol_ijk(ijksolid,2);
ksolid=liq_sol_ijk(ijksolid,3);
Channel3D(isolid,jsolid,ksolid)=0;
ChannelPhases(isolid,jsolid,ksolid)=0;
end

for ijkliquid=1:liquidLength
iliquid=liq_sol_ijk(ijkliquid,4);
jliquid=liq_sol_ijk(ijkliquid,5);
kliquid=liq_sol_ijk(ijkliquid,6);
ChannelPhases(iliquid,jliquid,kliquid)=l_density;
end


%%% memfree
clear liq_sol_ijk

%%% BBs and GEOs properties %%%

wW=sum(sum(Channel3D(:,1,:),1),3); 
wE=sum(sum(Channel3D(:,Mc,:),1),3); 
wS=sum(sum(ChannelPhases(Nr,:,:)==i_density,2),3);
wL=sum(sum(ChannelPhases(Nr,:,:)==l_density,2),3);
wmin=min(min(sum(Channel3D(:,:,:),1)));

ijk_io=zeros(wW,12);
[ijk_io(1:wW,1),ijk_io(1:wW,2),ijk_io(1:wW,3)]= ind2sub([Nr,1,Ld],find(Channel3D(:,1,:))); ijk_io(1:wW,2)=1;
[ijk_io(1:wE,4),ijk_io(1:wE,5),ijk_io(1:wE,6)]= ind2sub([Nr,1,Ld],find(Channel3D(:,Mc,:))); ijk_io(1:wE,5)=Mc;

[ijk_io(1:wL,7),ijk_io(1:wL,8),ijk_io(1:wL,9)]= ind2sub([1,Mc,Ld],find(ChannelPhases(Nr,:,:)==l_density)); ijk_io(1:wL,7)=Nr;
[ijk_io(1:wS,10),ijk_io(1:wS,11),ijk_io(1:wS,12)]= ind2sub([1,Mc,Ld],find(ChannelPhases(Nr,:,:)==i_density)); ijk_io(1:wS,10)=Nr;

zmin=1;%find(Channel3D(:,round(Mc/2),Ld/2),1);
zzP=Nr/2;
wall=0;

%%%% PROPERTIES %%%%%%%%%%

% sound speed
cs2=1/3; %?

% fluid mean density 
sizemp=sum(sum(sum(Channel3D,1),2),3);
mean_density=sum(sum(sum(ChannelPhases,2),1),3)/sizemp;

omega=tau.^-1;
cP_visco=(tau-0.5)*cs2*mean_density;
g_visco=(tau-0.5)*cs2*i_density;
Lky_visco=cP_visco/mean_density; % lattice kinematic viscosity


%%%% Pressure Gradient %%% �2D!

dch=Nr; % ???
dlmp=sum(ChannelPhases(Nr,:,Ld/2)==l_density,2);

ux_fin_max=dPdL.*(dch./2).^2.*(1/(8*cP_visco));
ux_av_in=2/3.*ux_fin_max; % Poiseuille
dP=dPdL*(Mc-1);

%  linear vel .. inizialization
u_in_ch=Re*Lky_visco/dch;
u_in_pm=dPdL*n^2/g_visco;
u_out_ch=u_in_ch;
u_out_pm=u_in_pm;
w_in=0;
w_out=0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%











%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% 2 %%% DESCRIPTION of tHe Model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%% analytic solution Poiseuille %%%%

% west & east
 yWch_profile=([-(dch/2):+(dch/2)-1]+0.5);
 u_profile=u_in_ch.*3/2.*(1-(yWch_profile/(dch/2)).^2);

% south
u_ioSl=zeros(Mc,Ld);
u_ioSg=repmat([i_density+dP/cs2.*(Mc-1:-1:0)./(Mc-1)]',1,Ld);

% west
u_ioWg=zeros(Nr,Ld);
u_ioEg=zeros(Nr,Ld);

REt=Re;
rholt=i_density;
nliqt=0;


%%%% DIRECTIONS %%%%%%%

%	z		C_xzy =[x ,y ,z ]
%	^ y		       [+j,+k,-i]
%	|/
%	o--> x          

%	

% x & y & z components of velocities
N_c=15; % number of directions

% versors D3Q15
C_x=[1 -1 0 0 0 0 1 -1 1 -1 1 -1 1 -1 0];
C_y=[0 0 1 -1 0 0 1 -1 1 -1 -1 1 -1 1 0];
C_z=[0 0 0 0 1 -1 1 -1 -1 1 1 -1 -1 1 0];
C=[C_x;C_y;C_z];


%%% BOUNCE BACK SCHEME %%%%

% after collision the fluid elements densities f are sent back to the
% lattice node they come from with opposite direction
% indices opposite to 1:8 for fast inversion after bounce
ic_op = [2 1 4 3 6 5 8 7 10 9 12 11 14 13]; %   i.e. 4 is opposite to 2 etc.


%%% PERIODIC BOUNDARY %%%%%

xi2=[Mc , 1:Mc , 1]; % Period Bound Cond
zi2=[Nr , 1:Nr , 1];
yi2=[Ld , 1:Ld , 1];


%%% INITIALIZATION %%%%%%%%%

% directional weights (density weights)
% and potential weights
w0=16/72 ; w1=8/72 ; w2=1/72;
u1=4/45*cs2; u2=1/21*cs2; u3=2/105*cs2; u4=5/504*cs2;
u5=1/315*cs2; u6=1/630*cs2; u8=1/5040*cs2;
W=[ w1 w1 w1 w1 w1 w1 w2 w2 w2 w2 w2 w2 w2 w2 w0 ];
id=[ 1:6,1:6];
Cd1=[C_z(1),C_x(1),C_y(1)];
Cd3=[C_z(3),C_x(3),C_y(3)];
Cd5=[C_z(5),C_x(5),C_y(5)];

%c constants (sound speed related)
cs2=1/3; cs2x2=2*cs2; cs4x2=2*cs2.^2;
f1=1/cs2; f3=1/cs2x2; f2=1/cs4x2;
f1=3.; f2=4.5; f3=1.5; % coef. of the f equil.

% declarative statemets
f=zeros(Nr,Mc,Ld,N_c); % array of fluid density distribution
force=zeros(Nr,Mc,Ld,N_c);
feq=zeros(Nr,Mc,Ld,N_c); % f at equilibrium
rho=zeros(Nr,Mc,Ld); % macro-scopic density

% dimensionless velocities
ux=zeros(Nr,Mc,Ld);   uy=zeros(Nr,Mc,Ld); uz=zeros(Nr,Mc,Ld);


%%% EXTERNAL FORCES %%%%%%

%       1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
cForce=[1 -1  0  0  0  0  1 -1  1 -1  1 -1  1 -1  0];


if bf==1;
fGradx(:,:,:,1) = dPdL.*Channel3D*afA(1); %sum(force.*cForce,2); % 0
fGradx(:,:,:,2) = dPdL.*Channel3D*afA(2);
fGradx(:,:,:,3) = dPdL.*Channel3D*afA(3);
else;
fGradx= [0,0,0];
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%












%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% 3 %%% While .. MAIN TIME EVOLUTION LOOP %%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



StopFlag=false; % i.e. logical(0)
Max_Iter=400; % max allowed number of iteration
Check_Iter=1; Output_Every=10; % frequency check & output
Cur_Iter=0; % current iteration counter inizialization
toler=10^(-4); % tollerance to declare convegence
Cond_path=[]; % recording values of the convergence criterium
density_path=[]; % recording aver. density values for convergence
Condition2=toler; Iter_conv2=0; cond2=0;
Iter_Partial=Inf;
Max_Time=3600*12; %82800; %% 23h
saved=0;

for ic=1:N_c
f(:,:,:,ic)=1/N_c.*i_density.*Channel3D(:,:,:).*ChannelPhases(:,:,:);
end

tic
end % end if restart=0

% restart from partial
if restart==1;
tic
toler=10^(-7);
StopFlag=false;
Condition2=toler; Iter_conv2=0; cond2=3;
Max_Iter=1000000;
end;



						while(~StopFlag)
    
Cur_Iter=Cur_Iter+1 % iteration counter update

% Neighborhood Intermolecular Potential      
fPhix=zeros(Nr,Mc,Ld);
fPhiy=zeros(Nr,Mc,Ld);
fPhiz=zeros(Nr,Mc,Ld);

% init velocities and density
ux=zeros(Nr,Mc,Ld); uy=zeros(Nr,Mc,Ld); uz=zeros(Nr,Mc,Ld);

%%% DENSITY MOM %%%

rho=rhocalc_3d(f,Nr,Mc,Ld,Channel3D,i_density,l_density,w_density,phb_density);
Phiw=eos(z_density,w_density,Geff);
Phi=phicalc_3d(rho,Nr,Mc,Ld,C_x,C_y,C_z,N_c,id,Channel3D,i_density,l_density,z_density,Geff);


        %%% POTENTIAL BY DENSITY %%%%

        for ic=[1:6]

        C_xic=C_x(ic); C_yic=C_y(ic); C_zic=C_z(ic);
        C_xid2=C_x(id(ic+2)); C_xid4=C_x(id(ic+4));
        C_yid2=C_y(id(ic+2)); C_yid4=C_y(id(ic+4));
        C_zid2=C_z(id(ic+2)); C_zid4=C_z(id(ic+4));

        fPhix=fPhix-(1/omega).*(circshift(Phi,[C_zic,-C_xic,-C_yic]).*((Gb*W(ic)+Gb1*u1)*C_xic)+ circshift(Phi,2.*[C_zic,-C_xic,-C_yic]).*(Gb2*u4*2*C_xic)...
        + circshift(Phi,2.*[C_zic,-C_xic,-C_yic]+[C_zid2,-C_xid2,-C_yid2]).*(Gb2*u5*(2*C_xic+C_xid2))+ circshift(Phi,2.*[C_zic,-C_xic,-C_yic]-[C_zid2,-C_xid2,-C_yid2]).*(Gb2*u5*(2*C_xic-C_xid2))...
        + circshift(Phi,2.*[C_zic,-C_xic,-C_yic]+[C_zid4,-C_xid4,-C_yid4]).*(Gb2*u5*(2*C_xic+C_xid4))+ circshift(Phi,2.*[C_zic,-C_xic,-C_yic]-[C_zid4,-C_xid4,-C_yid4]).*(Gb2*u5*(2*C_xic-C_xid4))...
        + circshift(Phi,[C_zic,-C_xic,-C_yic]+[C_zid2,-C_xid2,-C_yid2]).*(Gb1*u2*(C_xic+C_xid2))+ circshift(Phi,[C_zic,-C_xic,-C_yic]-[C_zid2,-C_xid2,-C_yid2]).*(Gb1*u2*(C_xic-C_xid2))...
        + circshift(Phi,2.*[C_zic,-C_xic,-C_yic]+2.*[C_zid2,-C_xid2,-C_yid2]).*(Gb2*u8*(2*C_xic+2*C_xid2))+ circshift(Phi,2.*[C_zic,-C_xic,-C_yic]-2.*[C_zid2,-C_xid2,-C_yid2]).*(Gb2*u8*(2*C_xic-2*C_xid2))).*(Channel3D.*Phi);

        fPhiy=fPhiy-(1/omega).*(circshift(Phi,[C_zic,-C_xic,-C_yic]).*((Gb*W(ic)+Gb1*u1)*C_yic)+ circshift(Phi,2.*[C_zic,-C_xic,-C_yic]).*(Gb2*u4*2*C_yic)...
        + circshift(Phi,2.*[C_zic,-C_xic,-C_yic]+[C_zid2,-C_xid2,-C_yid2]).*(Gb2*u5*(2*C_yic+C_yid2))+ circshift(Phi,2.*[C_zic,-C_xic,-C_yic]-[C_zid2,-C_xid2,-C_yid2]).*(Gb2*u5*(2*C_yic-C_yid2))...
        + circshift(Phi,2.*[C_zic,-C_xic,-C_yic]+[C_zid4,-C_xid4,-C_yid4]).*(Gb2*u5*(2*C_yic+C_yid4))+ circshift(Phi,2.*[C_zic,-C_xic,-C_yic]-[C_zid4,-C_xid4,-C_yid4]).*(Gb2*u5*(2*C_yic-C_yid4))...
        + circshift(Phi,[C_zic,-C_xic,-C_yic]+[C_zid2,-C_xid2,-C_yid2]).*(Gb1*u2*(C_yic+C_yid2))+ circshift(Phi,[C_zic,-C_xic,-C_yic]-[C_zid2,-C_xid2,-C_yid2]).*(Gb1*u2*(C_yic-C_yid2))...
        + circshift(Phi,2.*[C_zic,-C_xic,-C_yic]+2.*[C_zid2,-C_xid2,-C_yid2]).*(Gb2*u8*(2*C_yic+2*C_yid2))+ circshift(Phi,2.*[C_zic,-C_xic,-C_yic]-2.*[C_zid2,-C_xid2,-C_yid2]).*(Gb2*u8*(2*C_yic-2*C_yid2))).*(Channel3D.*Phi);

        fPhiz=fPhiz-(1/omega).*(circshift(Phi,[C_zic,-C_xic,-C_yic]).*((Gb*W(ic)+Gb1*u1)*C_zic)+ circshift(Phi,2.*[C_zic,-C_xic,-C_yic]).*(Gb2*u4*2*C_zic)...
        + circshift(Phi,2.*[C_zic,-C_xic,-C_yic]+[C_zid2,-C_xid2,-C_yid2]).*(Gb2*u5*(2*C_zic+C_zid2))+ circshift(Phi,2.*[C_zic,-C_xic,-C_yic]-[C_zid2,-C_xid2,-C_yid2]).*(Gb2*u5*(2*C_zic-C_zid2))...
        + circshift(Phi,2.*[C_zic,-C_xic,-C_yic]+[C_zid4,-C_xid4,-C_yid4]).*(Gb2*u5*(2*C_zic+C_zid4))+ circshift(Phi,2.*[C_zic,-C_xic,-C_yic]-[C_zid4,-C_xid4,-C_yid4]).*(Gb2*u5*(2*C_zic-C_zid4))...
        + circshift(Phi,[C_zic,-C_xic,-C_yic]+[C_zid2,-C_xid2,-C_yid2]).*(Gb1*u2*(C_zic+C_zid2))+ circshift(Phi,[C_zic,-C_xic,-C_yic]-[C_zid2,-C_xid2,-C_yid2]).*(Gb1*u2*(C_zic-C_zid2))...
        + circshift(Phi,2.*[C_zic,-C_xic,-C_yic]+2.*[C_zid2,-C_xid2,-C_yid2]).*(Gb2*u8*(2*C_zic+2*C_zid2))+ circshift(Phi,2.*[C_zic,-C_xic,-C_yic]-2.*[C_zid2,-C_xid2,-C_yid2]).*(Gb2*u8*(2*C_zic-2*C_zid2))).*(Channel3D.*Phi);

        end

	for ic=[7:14]
        C_xic=C_x(ic); C_yic=C_y(ic); C_zic=C_z(ic);

        fPhix=fPhix-(1/omega).*(circshift(Phi,[C_zic,-C_xic,-C_yic]).*((Gb*W(ic)+Gb1*u3)*C_xic)+ circshift(Phi,[C_zic,-C_xic,-C_yic].*(1+Cd1)).*(Gb2*u6*(C_xic*(1+C_x(1))))+ circshift(Phi,[C_zic,-C_xic,-C_yic].*(1+Cd3)).*(Gb2*u6*(C_xic*(1+C_x(3))))+ circshift(Phi,[C_zic,-C_xic,-C_yic].*(1+Cd5)).*(Gb2*u6*(C_xic*(1+C_x(5)))) ).*(Channel3D.*Phi);

        fPhiy=fPhiy-(1/omega).*(circshift(Phi,[C_zic,-C_xic,-C_yic]).*((Gb*W(ic)+Gb1*u3)*C_yic)+ circshift(Phi,[C_zic,-C_xic,-C_yic].*(1+Cd1)).*(Gb2*u6*(C_yic*(1+C_y(1))))+ circshift(Phi,[C_zic,-C_xic,-C_yic].*(1+Cd3)).*(Gb2*u6*(C_yic*(1+C_y(3))))+ circshift(Phi,[C_zic,-C_xic,-C_yic].*(1+Cd5)).*(Gb2*u6*(C_yic*(1+C_y(5)))) ).*(Channel3D.*Phi);

        fPhiz=fPhiz-(1/omega).*(circshift(Phi,[C_zic,-C_xic,-C_yic]).*((Gb*W(ic)+Gb1*u3)*C_zic)+ circshift(Phi,[C_zic,-C_xic,-C_yic].*(1+Cd1)).*(Gb2*u6*(C_zic*(1+C_z(1))))+ circshift(Phi,[C_zic,-C_xic,-C_yic].*(1+Cd3)).*(Gb2*u6*(C_zic*(1+C_z(3))))+ circshift(Phi,[C_zic,-C_xic,-C_yic].*(1+Cd5)).*(Gb2*u6*(C_zic*(1+C_z(5)))) ).*(Channel3D.*Phi);

    	end


	%%% UPDATE VEL %%%%%%%%%%  

	for ic=1:1:N_c-1
	C_yic=C_y(ic); C_xic=C_x(ic); C_zic=C_z(ic);	
 
	if Cur_Iter > 0  % if start from a value
           
    	ux(:,:,:)=f(:,:,:,ic)*C_xic+ux(:,:,:);
	uy(:,:,:)=f(:,:,:,ic)*C_yic+uy(:,:,:);
	uz(:,:,:)=f(:,:,:,ic)*C_zic+uz(:,:,:);
	
    	else

    	for y=1:Ld;
	ux(:,:,y)=repmat(ux00(:,1,y),[1,Mc]).*Channel3D(:,:,y);
	uy(:,:,y)=repmat(uy00(:,1,y),[1,Mc]).*Channel3D(:,:,y);
	uz(:,:,y)=repmat(uz00(:,1,y),[1,Mc]).*Channel3D(:,:,y);
	end
    	
	end

	end

	% add force
	ux=ux+0.5.*fGradx(:,:,:,1)+fPhix;
	uy=uy+0.5.*fGradx(:,:,:,2)+fPhiy;
	uz=uz+0.5.*fGradx(:,:,:,3)+fPhiz;


%% UPDATE F EQ and RHO %%

% Equilibrium distribution
feq=zeros(Nr,Mc,Ld,N_c);	

rho=rho.*Channel3D;
% temp rho+1 to avoid NaN on boundaries
ux=ux./(rho+1-Channel3D); 
uy=uy./(rho+1-Channel3D); 
uz=uz./(rho+1-Channel3D);


	if Cur_Iter > 0 % if start from a value
	
	for ic=1:1:N_c

	C_yic=C_y(ic); C_xic=C_x(ic); C_zic=C_z(ic);
	feq(:,:,:,ic)=W(ic).*rho.*(1+f1.*(ux.*C_xic+uy.*C_yic+uz.*C_zic)+f2.*(ux.*C_xic+uy.*C_yic+uz.*C_zic).^2-f3.*(ux.^2+uy.^2+uz.^2));
 
	end

	else

	for y=1:Ld; for ic=1:1:N_c
	feq(:,:,y,ic)=repmat(feq00(:,1,y,ic),[1,Mc]).*Channel3D(:,:,y);
	f(:,:,y,ic)=repmat(f00(:,1,y,ic),[1,Mc]).*Channel3D(:,:,y);
	end; end

	end


%%% COLLISION %%%%% 

f=(1.-omega).*f + omega.*feq;

        % add body force
        for ic=1:N_c-1;
        C_yic=C_y(ic); C_xic=C_x(ic); C_zic=C_z(ic);

        f(:,:,:,ic)= f(:,:,:,ic) +(1-1/(2*tau)).*W(ic).*( ((C_xic-ux).*f1+(C_xic.*ux).*(2*f2*C_xic)).*fGradx(1) + ((C_yic-uy).*f1+(C_yic.*uy).*(2*f2*C_yic)).*fGradx(2) + ((C_zic-uz).*f1+(C_zic.*uz).*(2*f2*C_zic)).*fGradx(3) ) .*Channel3D;
        %+(1-1/(2*tau)).*force(ic).*Channel3D;

        end;
	

%%% STREAM %%%%%%%%

% Forward Propagation step & 
% Bounce Back (collision fluid with obstacles)
   
feq = f; % temp storage of f in feq

        for ic=1:1:N_c-1,
        C_yic = C_y(ic); C_xic = C_x(ic); C_zic=C_z(ic);
        ic2=ic_op(ic); % velocity opposite to ic for BB

        f(:,:,:,ic)=circshift(feq(:,:,:,ic),[-C_zic,C_xic,C_yic]).*Channel3D+feq(:,:,:,ic2).*circshift((1-Channel3D(:,:,:)),[-C_zic,C_xic,C_yic]);

        end ; %  for ic direction


%%% BBs %%%

%f=zouhebbss_3d(ijk_io,u_ioWg,u_ioEg,u_ioSl,u_ioSg,f,g_density,l_density, Cur_Iter,dP,bf,Iter_Partial);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ends of Forward Propagation step &  Bounce Back Sections

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%












%%%%%%%%%% CONVERGENCE ? %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Convergence check on velocity values
    
if (mod(Cur_Iter,Check_Iter)==0) ; % every 'Check_Iter' iterations

	% averaging velocity (in the wet area)
	rho=sum(f,4); % density
	ux=((rho+1-Channel3D).*ux+0.0.*fGradx(:,:,:,1))./(rho+1-Channel3D); %-0.5.*fGradx
	uy=((rho+1-Channel3D).*uy+0.0.*fGradx(:,:,:,2))./(rho+1-Channel3D);
	uz=((rho+1-Channel3D).*uz+0.0.*fGradx(:,:,:,3))./(rho+1-Channel3D);

	Af=sum(sum(Channel3D,2),3); 
	AfP=sum(sum(Channel3D(zzP+1:Nr-wall,:,:),2),3); 
	AfC=sum(sum(Channel3D(zmin:zzP,:,:),2),3);

	vrho0=(sum(sum(rho(:,:,:),2),3)./Af);	
	vrho0P=(sum(sum(rho(zzP+1:Nr-wall,:),2),3)./AfP);
	vrho0C=(sum(sum(rho(zmin:zzP,:),2),3)./AfC);

	vrho0(isnan(vrho0))=0; rho0=mean(vrho0);
	vrho0P(isnan(vrho0P))=0; rho0P=mean(vrho0P);
	vrho0C(isnan(vrho0C))=0; rho0C=mean(vrho0C);

	ux_da=(sum(sum(ux.*Channel3D,2),3))./Af;
	ux_daC=sum(sum(ux(zmin:zzP,:,:).*Channel3D(zmin:zzP,:,:),2),3)./AfC;
	ux_daP=sum(sum(ux(zzP+1:Nr-wall,:,:).*Channel3D(zzP+1:Nr-wall,:,:),2),3)./AfP;

	ux_da(isnan(ux_da))=0; av_vel=mean(ux_da);
	ux_daC(isnan(ux_daC))=0; av_vel_channel=mean(ux_daC);
	ux_daP(isnan(ux_daP))=0; av_vel_porous=mean(ux_daP);

	%k_darcy=av_vel_porous*cP_visco/dPdL;
        
	REtp0 = abs(av_vel)*(df)/cP_visco*rho0;
	REtpC = abs(av_vel_channel)*(dch)/cP_visco*rho0C; 
	%REtpP = abs(av_vel_porous)*sqrt(abs(k_darcy))/cP_visco*rho0P; 

	REtp=REtp0;
	Condition=abs( abs(REt/REtp )-1); % should --> 0        
	REt=REtp; % time t & t+1 
 

	%%% TOLERANCE BREAK %%%

        if (Cur_Iter > Max_Iter | isnan(REt) | (size(Condition2,2)>2) )
            StopFlag=true;
            display( 'Stop iteration: iteration exeeding the max allowed or NaN value of Re or ... Yuppi converged!' )
            display( ['Current iteration: ',num2str(Cur_Iter),...
                ' Max Number of iter: ',num2str(Max_Iter),...
		' Reynolds numberrrr: ',num2str(REt) ] )
            break % Terminate execution of WHILE ..
        end
	
	if (Condition < toler) ;
	if    (Cur_Iter == Iter_conv2+Check_Iter) ;	    
	      cond2=cond2+1,
	else; cond2=1; end;
	Condition2(cond2)=Condition;
	Iter_conv2=Cur_Iter;
	end

        time=toc;

end


if ( (time > Max_Time) && (saved==0) );
printf('\n Max Time reached .. I m sorry \n\n');
spr=sprintf([ name '_%i'], vA);
save( [ 'out/' spr '.part.mat' ],'ux','uy','uz','Channel3D','rho','convergence','Condition2','name')
StopFlag=true;
saved=1,
break
end

%%% outputs every iteration output

    if (mod(Cur_Iter,Output_Every)==0) ;

	convergence(Cur_Iter/Output_Every)=Condition,
	vec_Iter=[Output_Every:Output_Every:Cur_Iter];
	
        figure(2), contourf(rho(:,:,12))
	
    end % every

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
end %  End main time Evolution Loop

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




% TIME

if restart==1;
toc;
TOC=TOC+toc;
else
toc; TOC=toc
end;

% saving
if (saved==0);
spr=sprintf([ name '_%i.mat'],vA);
save( [ 'out/' spr ],'ux','uy','uz','Channel3D','rho','convergence','Condition2','name')
saved=1,
end;

end; end;

