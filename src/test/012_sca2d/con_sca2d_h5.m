load('dump.CONV.h5');
con=LBdm.con;

x1=31;
x2=21;
x3=11;

A=con(2:end-1,[x1,x2,x3],1);
save('scalar_lbdm.dat','-ascii','A');

sol=load('scalar_profile.dat');
figure(1),
plot(sol,'-');
hold on, plot(A,'o');
saveas(gcf,'profile.png','png');
