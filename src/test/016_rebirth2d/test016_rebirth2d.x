#!/bin/bash -e

echo " "
echo "#lbdm# Test 016 Rebirth 2D"
echo " "

git checkout user-defined.c

if ls data*.dat 1> /dev/null 2>&1; then
rm data*.dat
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

octave create_bin-txt.m

cp reb2d.tp.bin.txt ../../in/reb2d.bin.txt

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/016_rebirth2d/

nameconv='../../out/dump.REB1.00080.h5'
cp $nameconv ../../in/

octave getdata1.m

###########

sed -i -e 's/\/\/#define REBIRTH$/#define REBIRTH/g' user-defined.c
sed -i -e 's/define NAME .*/define NAME "REB2"/g' user-defined.c
sed -i -e 's/define MAX_ITER 120/define MAX_ITER 40/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/016_rebirth2d/

octave getdata2.m

##########

cp reb2d.sp.bin.txt ../../in/reb2d.bin.txt 

###########

sed -i -e 's/#define TWOPHASE/\/\/#define TWOPHASE/g' user-defined.c
sed -i -e 's/#define NEIGHBOURING/\/\/#define NEIGHBOURING/g' user-defined.c
sed -i -e 's/\/\/#define SCALAR/#define SCALAR/g' user-defined.c
sed -i -e 's/#define REBIRTH$/\/\/#define REBIRTH/g' user-defined.c
sed -i -e 's/\/\/#define CONCENTRATION_XM/#define CONCENTRATION_XM/g' user-defined.c
sed -i -e 's/\/\/#define CONCENTRATION_EQ_XP/#define CONCENTRATION_EQ_XP/g' user-defined.c
sed -i -e 's/define NAME .*/define NAME "REB3"/g' user-defined.c
sed -i -e 's/define MAX_ITER 40/define MAX_ITER 120/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (3) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/016_rebirth2d/

nameconv='../../out/dump.REB3.00080.h5'
cp $nameconv ../../in/

octave getdata3.m

###########

sed -i -e 's/\/\/#define REBIRTH$/#define REBIRTH/g' user-defined.c
sed -i -e 's/define NAME .*/define NAME "REB4"/g' user-defined.c
sed -i -e 's/define MAX_ITER 120/define MAX_ITER 40/g' user-defined.c
sed -i -e 's/define BIRTHPLACE .*/define BIRTHPLACE "dump.REB3.00080.h5"/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (4) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/016_rebirth2d/

octave getdata4.m

###########

octave geterror.m
awk 'NR<=24 {if (($1*$1)^0.5==0.0) print "ok"; else print "no" }' error.dat > errchk.dat

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/reb2d.bin.txt
rm in/dump.REB*.h5

# clean local test
cd test/016_rebirth2d/
git checkout user-defined.c
rm *.txt

########### 1) Check NaN 

for file in data1.dat data2.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 24 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
